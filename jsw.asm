; Jet Set Willy disassembly
; https://skoolkit.ca
;
; Copyright 1984 Software Projects Ltd (Jet Set Willy)
; Copyright 2012-2019 Richard Dymond (this disassembly)

  ORG 32765

  JP BEGIN

; Room layout
;
; Initialised upon entry to a room and then used by the routine at INITROOM,
; and also used by the routine at ROOMATTRS.
ROOMLAYOUT:
  DEFS 128

; Room name
;
; Initialised upon entry to a room and then used by the routine at INITROOM.
ROOMNAME:
  DEFS 32

; Room tiles
;
; Initialised upon entry to a room by the routine at INITROOM.
BACKGROUND:
  DEFS 9                  ; Background tile (used by the routines at DRAWROOM,
                          ; ROOMATTR, MOVEWILLY and WILLYATTR, and also by the
                          ; unused routine at U_SETATTRS)
FLOOR:
  DEFS 9                  ; Floor tile (used by the routines at DRAWROOM and
                          ; ROOMATTR)
WALL:
  DEFS 9                  ; Wall tile (used by the routines at DRAWROOM,
                          ; ROOMATTR, MOVEWILLY and MOVEWILLY3)
NASTY:
  DEFS 9                  ; Nasty tile (used by the routines at DRAWROOM,
                          ; ROOMATTR, MOVEWILLY and WILLYATTR)
RAMP:
  DEFS 9                  ; Ramp tile (used by the routines at DRAWROOM,
                          ; ROOMATTRS, MOVEWILLY3 and WILLYATTRS)
CONVEYOR:
  DEFS 9                  ; Conveyor tile (used by the routines at DRAWROOM,
                          ; ROOMATTRS and MOVEWILLY2)

; Conveyor definition
;
; Initialised upon entry to a room by the routine at INITROOM.
CONVDIR:
  DEFB 0                  ; Direction (0=left, 1=right; used by the routines at
                          ; MOVEWILLY2 and MVCONVEYOR)
CONVLOC:
  DEFW 0                  ; Address of the conveyor's location in the attribute
                          ; buffer at 24064 (used by the routines at ROOMATTRS
                          ; and MVCONVEYOR)
CONVLEN:
  DEFB 0                  ; Length (used by the routines at ROOMATTRS and
                          ; MVCONVEYOR)

; Ramp definition
;
; Initialised upon entry to a room by the routine at INITROOM.
RAMPDIR:
  DEFB 0                  ; Direction (0=up to the left, 1=up to the right;
                          ; used by the routines at ROOMATTRS, MOVEWILLY3 and
                          ; WILLYATTRS)
RAMPLOC:
  DEFW 0                  ; Address of the location of the bottom of the ramp
                          ; in the attribute buffer at 24064 (used by the
                          ; routine at ROOMATTRS)
RAMPLEN:
  DEFB 0                  ; Length (used by the routine at ROOMATTRS)

; Border colour
;
; Initialised upon entry to a room and then used by the routine at INITROOM,
; and also used by the routines at ENDPAUSE, MOVEWILLY, DRAWTHINGS and
; DRAWITEMS.
BORDER:
  DEFB 0

; Unused
;
; These bytes are overwritten upon entry to a room by the routine at INITROOM,
; but not used.
XROOM223:
  DEFS 2

; Item graphic
;
; Initialised upon entry to a room by the routine at INITROOM, and used by the
; routine at DRAWITEMS.
ITEM:
  DEFS 8

; Room exits
;
; Initialised upon entry to a room by the routine at INITROOM.
LEFT:
  DEFB 0                  ; Room to the left (used by the routine at ROOMLEFT)
RIGHT:
  DEFB 0                  ; Room to the right (used by the routine at
                          ; ROOMRIGHT)
ABOVE:
  DEFB 0                  ; Room above (used by the routines at DRAWTHINGS and
                          ; ROOMABOVE)
BELOW:
  DEFB 0                  ; Room below (used by the routine at ROOMBELOW)

; Unused
;
; These bytes are overwritten upon entry to a room by the routine at INITROOM,
; but not used.
XROOM237:
  DEFS 3

; Entity specifications
;
; Initialised upon entry to a room and then used by the routine at INITROOM.
;
; There are eight pairs of bytes here that hold the entity specifications for
; the current room. The first byte in each pair identifies one of the entity
; definitions at ENTITYDEFS. The meaning of the second byte depends on the
; entity type: it determines the base sprite index and x-coordinate of a
; guardian, the y-coordinate of an arrow, or the x-coordinate of the top of a
; rope.
ENTITIES:
  DEFS 2                  ; Entity 1
  DEFS 2                  ; Entity 2
  DEFS 2                  ; Entity 3
  DEFS 2                  ; Entity 4
  DEFS 2                  ; Entity 5
  DEFS 2                  ; Entity 6
  DEFS 2                  ; Entity 7
  DEFS 2                  ; Entity 8

; Entity buffers
;
; Initialised by the routine at INITROOM, and used by the routines at
; MOVETHINGS and DRAWTHINGS. There are eight buffers here, each one eight bytes
; long, used to hold the state of the entities (rope, arrows and guardians) in
; the current room.
;
; For a horizontal guardian, the eight bytes are used as follows:
;
; +------+-----------------------------------------------------------+
; | Byte | Contents                                                  |
; +------+-----------------------------------------------------------+
; | 0    | Bit 7: direction (0=left, 1=right)                        |
; |      | Bits 5-6: animation frame index                           |
; |      | Bits 3-4: unused                                          |
; |      | Bits 0-2: entity type (001)                               |
; | 1    | Bits 5-7: animation frame index mask                      |
; |      | Bit 4: unused                                             |
; |      | Bit 3: BRIGHT value                                       |
; |      | Bits 0-2: INK colour                                      |
; | 2    | Bits 5-7: base sprite index                               |
; |      | Bits 0-4: x-coordinate                                    |
; | 3    | Pixel y-coordinate x2 (index into the table at SBUFADDRS) |
; | 4    | Unused                                                    |
; | 5    | Page containing the sprite graphic data (see GUARDIANS)   |
; | 6    | Minimum x-coordinate                                      |
; | 7    | Maximum x-coordinate                                      |
; +------+-----------------------------------------------------------+
;
; For a vertical guardian, the eight bytes are used as follows:
;
; +------+-----------------------------------------------------------+
; | Byte | Contents                                                  |
; +------+-----------------------------------------------------------+
; | 0    | Bits 5-7: animation frame index                           |
; |      | Bits 3-4: animation frame update flags (see MOVETHINGS_9) |
; |      | Bits 0-2: entity type (010)                               |
; | 1    | Bits 5-7: animation frame index mask                      |
; |      | Bit 4: unused                                             |
; |      | Bit 3: BRIGHT value                                       |
; |      | Bits 0-2: INK colour                                      |
; | 2    | Bits 5-7: base sprite index                               |
; |      | Bits 0-4: x-coordinate                                    |
; | 3    | Pixel y-coordinate x2 (index into the table at SBUFADDRS) |
; | 4    | Pixel y-coordinate increment                              |
; | 5    | Page containing the sprite graphic data (see GUARDIANS)   |
; | 6    | Minimum y-coordinate                                      |
; | 7    | Maximum y-coordinate                                      |
; +------+-----------------------------------------------------------+
;
; For an arrow, the eight bytes are used as follows:
;
; +------+-----------------------------------------------------------+
; | Byte | Contents                                                  |
; +------+-----------------------------------------------------------+
; | 0    | Bit 7: direction (0=left, 1=right)                        |
; |      | Bits 3-6: unused                                          |
; |      | Bits 0-2: entity type (100)                               |
; | 1    | Unused                                                    |
; | 2    | Pixel y-coordinate x2 (index into the table at SBUFADDRS) |
; | 3    | Unused                                                    |
; | 4    | x-coordinate                                              |
; | 5    | Collision detection byte (0=off, 255=on)                  |
; | 6    | Top/bottom pixel row (drawn either side of the shaft)     |
; | 7    | Unused                                                    |
; +------+-----------------------------------------------------------+
;
; The rope uses the second and fourth bytes of the following buffer in addition
; to its own; these ten bytes are used as follows:
;
; +------+----------------------------------------------------------+
; | Byte | Contents                                                 |
; +------+----------------------------------------------------------+
; | 0    | Bit 7: direction (0=left, 1=right)                       |
; |      | Bits 3-6: unused                                         |
; |      | Bits 0-2: entity type (011)                              |
; | 1    | Animation frame index                                    |
; | 2    | x-coordinate of the top of the rope                      |
; | 3    | x-coordinate of the segment of rope being drawn          |
; | 4    | Length (32)                                              |
; | 5    | Segment drawing byte                                     |
; | 6    | Unused                                                   |
; | 7    | Animation frame at which the rope changes direction (54) |
; | 9    | Index of the segment of rope being drawn (0-32)          |
; | 11   | Bits 1-7: unused                                         |
; |      | Bit 0: Willy is on the rope (set), or not (reset)        |
; +------+----------------------------------------------------------+
;
; Note that if a rope were the eighth entity specified in a room, its buffer
; would use the first and third bytes in the otherwise unused area at
; EBOVERFLOW.
ENTITYBUF:
  DEFS 8                  ; Entity 1
  DEFS 8                  ; Entity 2
  DEFS 8                  ; Entity 3
  DEFS 8                  ; Entity 4
  DEFS 8                  ; Entity 5
  DEFS 8                  ; Entity 6
  DEFS 8                  ; Entity 7
  DEFS 8                  ; Entity 8
  DEFB 255                ; Terminator

; Unused
;
; This area is not used, but if a rope were the eighth entity specified in a
; room, its buffer would spill over from the eighth slot at ENTITYBUF and use
; the first and third bytes here.
EBOVERFLOW:
  DEFS 191

; Screen buffer address lookup table
;
; Used by the routines at GAMEOVER, DRAWTHINGS and DRAWWILLY. The value of the
; Nth entry (0<=N<=127) in this lookup table is the screen buffer address for
; the point with pixel coordinates (x,y)=(0,N), with the origin (0,0) at the
; top-left corner.
SBUFADDRS:
  DEFW 24576              ; y=0
  DEFW 24832              ; y=1
  DEFW 25088              ; y=2
  DEFW 25344              ; y=3
  DEFW 25600              ; y=4
  DEFW 25856              ; y=5
  DEFW 26112              ; y=6
  DEFW 26368              ; y=7
  DEFW 24608              ; y=8
  DEFW 24864              ; y=9
  DEFW 25120              ; y=10
  DEFW 25376              ; y=11
  DEFW 25632              ; y=12
  DEFW 25888              ; y=13
  DEFW 26144              ; y=14
  DEFW 26400              ; y=15
  DEFW 24640              ; y=16
  DEFW 24896              ; y=17
  DEFW 25152              ; y=18
  DEFW 25408              ; y=19
  DEFW 25664              ; y=20
  DEFW 25920              ; y=21
  DEFW 26176              ; y=22
  DEFW 26432              ; y=23
  DEFW 24672              ; y=24
  DEFW 24928              ; y=25
  DEFW 25184              ; y=26
  DEFW 25440              ; y=27
  DEFW 25696              ; y=28
  DEFW 25952              ; y=29
  DEFW 26208              ; y=30
  DEFW 26464              ; y=31
  DEFW 24704              ; y=32
  DEFW 24960              ; y=33
  DEFW 25216              ; y=34
  DEFW 25472              ; y=35
  DEFW 25728              ; y=36
  DEFW 25984              ; y=37
  DEFW 26240              ; y=38
  DEFW 26496              ; y=39
  DEFW 24736              ; y=40
  DEFW 24992              ; y=41
  DEFW 25248              ; y=42
  DEFW 25504              ; y=43
  DEFW 25760              ; y=44
  DEFW 26016              ; y=45
  DEFW 26272              ; y=46
  DEFW 26528              ; y=47
  DEFW 24768              ; y=48
  DEFW 25024              ; y=49
  DEFW 25280              ; y=50
  DEFW 25536              ; y=51
  DEFW 25792              ; y=52
  DEFW 26048              ; y=53
  DEFW 26304              ; y=54
  DEFW 26560              ; y=55
  DEFW 24800              ; y=56
  DEFW 25056              ; y=57
  DEFW 25312              ; y=58
  DEFW 25568              ; y=59
  DEFW 25824              ; y=60
  DEFW 26080              ; y=61
  DEFW 26336              ; y=62
  DEFW 26592              ; y=63
  DEFW 26624              ; y=64
  DEFW 26880              ; y=65
  DEFW 27136              ; y=66
  DEFW 27392              ; y=67
  DEFW 27648              ; y=68
  DEFW 27904              ; y=69
  DEFW 28160              ; y=70
  DEFW 28416              ; y=71
  DEFW 26656              ; y=72
  DEFW 26912              ; y=73
  DEFW 27168              ; y=74
  DEFW 27424              ; y=75
  DEFW 27680              ; y=76
  DEFW 27936              ; y=77
  DEFW 28192              ; y=78
  DEFW 28448              ; y=79
  DEFW 26688              ; y=80
  DEFW 26944              ; y=81
  DEFW 27200              ; y=82
  DEFW 27456              ; y=83
  DEFW 27712              ; y=84
  DEFW 27968              ; y=85
  DEFW 28224              ; y=86
  DEFW 28480              ; y=87
  DEFW 26720              ; y=88
  DEFW 26976              ; y=89
  DEFW 27232              ; y=90
  DEFW 27488              ; y=91
  DEFW 27744              ; y=92
  DEFW 28000              ; y=93
  DEFW 28256              ; y=94
  DEFW 28512              ; y=95
  DEFW 26752              ; y=96
  DEFW 27008              ; y=97
  DEFW 27264              ; y=98
  DEFW 27520              ; y=99
  DEFW 27776              ; y=100
  DEFW 28032              ; y=101
  DEFW 28288              ; y=102
  DEFW 28544              ; y=103
  DEFW 26784              ; y=104
  DEFW 27040              ; y=105
  DEFW 27296              ; y=106
  DEFW 27552              ; y=107
  DEFW 27808              ; y=108
  DEFW 28064              ; y=109
  DEFW 28320              ; y=110
  DEFW 28576              ; y=111
  DEFW 26816              ; y=112
  DEFW 27072              ; y=113
  DEFW 27328              ; y=114
  DEFW 27584              ; y=115
  DEFW 27840              ; y=116
  DEFW 28096              ; y=117
  DEFW 28352              ; y=118
  DEFW 28608              ; y=119
  DEFW 26848              ; y=120
  DEFW 27104              ; y=121
  DEFW 27360              ; y=122
  DEFW 27616              ; y=123
  DEFW 27872              ; y=124
  DEFW 28128              ; y=125
  DEFW 28384              ; y=126
  DEFW 28640              ; y=127

; Rope animation table
;
; Used by the routine at DRAWTHINGS. The first half of this table controls the
; x-coordinates at which the segments of rope are drawn, and the second half
; controls the y-coordinates. For a given rope animation frame F (0<=F<=54),
; the 32 entries from F to F+31 inclusive (one for each of the 32 segments of
; rope below the topmost one) in each half of the table are used; thus the
; batch of entries used 'slides' up and down the table as F increases and
; decreases.
ROPEANIM:
  DEFB 0,0,0,0,0,0,0,0    ; These values determine how much to rotate the rope
  DEFB 0,0,0,0,0,0,0,0    ; drawing byte (which in turn determines the
  DEFB 0,0,0,0,0,0,0,0    ; x-coordinate at which each segment of rope is
  DEFB 0,0,0,0,0,0,0,0    ; drawn)
  DEFB 1,1,1,1,1,1,1,1    ;
  DEFB 1,1,1,1,2,2,2,2    ;
  DEFB 2,2,2,2,2,2,2,2    ;
  DEFB 2,2,2,2,2,2,2,2    ;
  DEFB 2,2,1,2,2,1,1,2    ;
  DEFB 1,1,2,2,3,2,3,2    ;
  DEFB 3,3,3,3,3,3        ;
  DEFB 0,0,0,0,0,0,0,0    ; Unused
  DEFB 0,0,0,0,0,0,0,0    ;
  DEFB 0,0,0,0,0,0,0,0    ;
  DEFB 0,0,0,0,0,0,0,0    ;
  DEFB 0,0,0,0,0,0,0,0    ;
  DEFB 0,0                ;
  DEFB 6,6,6,6,6,6,6,6    ; These values determine the y-coordinate of each
  DEFB 6,6,6,6,6,6,6,6    ; segment of rope relative to the one above it
  DEFB 6,6,6,6,6,6,6,6    ;
  DEFB 6,6,6,6,6,6,6,6    ;
  DEFB 6,6,6,6,6,6,6,6    ;
  DEFB 6,6,6,6,6,6,6,6    ;
  DEFB 4,6,6,4,6,4,6,4    ;
  DEFB 6,4,4,4,6,4,4,4    ;
  DEFB 4,4,4,4,4,4,4,4    ;
  DEFB 4,4,4,4,4,4,4,4    ;
  DEFB 4,4,4,4,4,4        ;
  DEFB 0,0,0,0,0,0,0,0    ; Unused
  DEFB 0,0,0,0,0,0,0,0    ;
  DEFB 0,0,0,0,0,0,0,0    ;
  DEFB 0,0,0,0,0,0,0,0    ;
  DEFB 0,0,0,0,0,0,0,0    ;
  DEFB 0,0                ;

; The game has just loaded
;
; After the game has loaded, this is where it all starts.
BEGIN:
  DI                      ; Disable interrupts
  LD HL,23551             ; Place the address of the routine at ENTERCODES on
  LD (HL),134             ; the stack
  DEC HL                  ;
  LD (HL),159             ;
  LD SP,23550             ;
  SUB A                   ; Set HL=34048 in a roundabout way
  LD L,A                  ;
  XOR 10                  ;
  LD B,A                  ;
  INC B                   ;
  LD H,B                  ;
  RRC H                   ;
BEGIN_0:
  LD C,(HL)               ; Read through addresses 34048-65535, without
  LD A,L                  ; changing their contents; perhaps this code was once
  XOR C                   ; used to descramble the contents of pages 133-255,
  XOR H                   ; but now all it does is introduce a pause of about
  LD (HL),C               ; 0.47s before displaying the code entry screen
  INC HL                  ;
  BIT 7,H                 ;
  JR NZ,BEGIN_0           ;
  RET                     ; Make an indirect jump to ENTERCODES

; Current room number
;
; Initialised to 33 (The Bathroom) by the routine at TITLESCREEN, checked by
; the routines at INITROOM, DRAWTHINGS, DRAWITEMS, BEDANDBATH, CHKTOILET,
; DRAWTOILET and DRAWWILLY, and updated by the routines at ENDPAUSE, ROOMLEFT,
; ROOMRIGHT, ROOMABOVE and ROOMBELOW.
ROOM:
  DEFB 0

; Left-right movement table
;
; Used by the routine at MOVEWILLY2. The entries in this table are used to map
; the existing value (V) of Willy's direction and movement flags at DMFLAGS to
; a new value (V'), depending on the direction Willy is facing and how he is
; moving or being moved (by 'left' and 'right' keypresses and joystick input,
; or by a conveyor, or by an urge to visit the toilet).
;
; One of the first four entries is used when Willy is not moving.
LRMOVEMENT:
  DEFB 0                  ; V=0 (facing right, no movement) + no movement: V'=0
                          ; (no change)
  DEFB 1                  ; V=1 (facing left, no movement) + no movement: V'=1
                          ; (no change)
  DEFB 0                  ; V=2 (facing right, moving) + no movement: V'=0
                          ; (facing right, no movement) (i.e. stop)
  DEFB 1                  ; V=3 (facing left, moving) + no movement: V'=1
                          ; (facing left, no movement) (i.e. stop)
; One of the next four entries is used when Willy is moving left.
  DEFB 1                  ; V=0 (facing right, no movement) + move left: V'=1
                          ; (facing left, no movement) (i.e. turn around)
  DEFB 3                  ; V=1 (facing left, no movement) + move left: V'=3
                          ; (facing left, moving)
  DEFB 1                  ; V=2 (facing right, moving) + move left: V'=1
                          ; (facing left, no movement) (i.e. turn around)
  DEFB 3                  ; V=3 (facing left, moving) + move left: V'=3 (no
                          ; change)
; One of the next four entries is used when Willy is moving right.
  DEFB 2                  ; V=0 (facing right, no movement) + move right: V'=2
                          ; (facing right, moving)
  DEFB 0                  ; V=1 (facing left, no movement) + move right: V'=0
                          ; (facing right, no movement) (i.e. turn around)
  DEFB 2                  ; V=2 (facing right, moving) + move right: V'=2 (no
                          ; change)
  DEFB 0                  ; V=3 (facing left, moving) + move right: V'=0
                          ; (facing right, no movement) (i.e. turn around)
; One of the final four entries is used when Willy is being pulled both left
; and right; each entry leaves the flags at DMFLAGS unchanged (so Willy carries
; on moving in the direction he's already moving, or remains stationary).
  DEFB 0                  ; V=V'=0 (facing right, no movement)
  DEFB 1                  ; V=V'=1 (facing left, no movement)
  DEFB 2                  ; V=V'=2 (facing right, moving)
  DEFB 3                  ; V=V'=3 (facing left, moving)

; Triangle UDGs
;
; Used by the routine at TITLESCREEN.
TRIANGLE0:
  DEFB 192,240,252,255,255,255,255,255
TRIANGLE1:
  DEFB 0,0,0,0,192,240,252,255
TRIANGLE2:
  DEFB 255,255,255,255,252,240,192,0
TRIANGLE3:
  DEFB 252,240,192,0,0,0,0,0

; 'AIR'
;
; This message is not used.
  DEFM "AIR"

; '+++++ Press ENTER to Start +++++...'
;
; Used by the routine at TITLESCREEN.
MSG_INTRO:
  DEFM "+++++ Press ENTER to Start +++++"
  DEFM "  JET-SET WILLY by Matthew Smith  "
  DEFM 127," 1984 SOFTWARE PROJECTS Ltd . . . . ."
  DEFM "Guide Willy to collect all the items around "
  DEFM "the house before Midnight "
  DEFM "so Maria will let you get to your bed. . . . . . ."
  DEFM "+++++ Press ENTER to Start +++++"

; 'Items collected 000 Time 00:00 m'
;
; Used by the routine at INITROOM.
MSG_STATUS:
  DEFM "Items collected 000 Time 00:00 m"

; 'Game'
;
; Used by the routine at GAMEOVER.
MSG_GAME:
  DEFM "Game"

; 'Over'
;
; Used by the routine at GAMEOVER.
MSG_OVER:
  DEFM "Over"

; Number of items collected
;
; Initialised by the routine at TITLESCREEN, printed by the routine at
; MAINLOOP, and updated by the routine at DRAWITEMS.
MSG_ITEMS:
  DEFM "000"

; Current time
;
; Initialised by the routine at STARTGAME, and printed and updated by the
; routine at MAINLOOP.
MSG_CURTIME:
  DEFM " 7:00a"

; ' 7:00a'
;
; Copied by the routine at STARTGAME to MSG_CURTIME.
MSG_7AM:
  DEFM " 7:00a"

; 'Enter Code at grid location     '
;
; Used by the routine at ENTERCODES.
MSG_CODE1:
  DEFM "Enter Code at grid location     "

; 'Sorry, try code at location     '
;
; Used by the routine at ENTERCODES.
MSG_CODE2:
  DEFM "Sorry, try code at location     "

; Minute counter
;
; Initialised by the routine at TITLESCREEN; incremented on each pass through
; the main loop by the routine at MAINLOOP (which moves the game clock forward
; by a minute when the counter reaches 0); reset to zero by the routine at
; CHKTOILET when Willy sticks his head down the toilet; and used by the
; routines at DRAWITEMS (to cycle the colours of the items in the room),
; BEDANDBATH (to determine Maria's animation frame in Master Bedroom) and
; DRAWTOILET (to determine the animation frame for the toilet in The Bathroom).
TICKS:
  DEFB 0

; Lives remaining
;
; Initialised to 7 by the routine at TITLESCREEN, decremented by the routine at
; LOSELIFE, and used by the routines at DRAWLIVES (when drawing the remaining
; lives) and ENDPAUSE (to adjust the speed and pitch of the in-game music).
LIVES:
  DEFB 0

; Screen flash counter
;
; Initialised to zero by the routine at TITLESCREEN, but never used; the code
; at SCRFLASH makes the screen flash in Manic Miner fashion if this address
; holds a non-zero value.
FLASH:
  DEFB 0

; Kempston joystick indicator
;
; Initialised by the routine at TITLESCREEN, and checked by the routines at
; MOVEWILLY2 and CHECKENTER. Holds 1 if a joystick is present, 0 otherwise.
JOYSTICK:
  DEFB 0

; Willy's pixel y-coordinate (x2)
;
; Initialised to 208 by the routine at TITLESCREEN, and used by the routines at
; MAINLOOP, ENDPAUSE, MOVEWILLY, MOVEWILLY2, MOVEWILLY3, DRAWTHINGS, ROOMABOVE,
; ROOMBELOW, BEDANDBATH, WILLYATTRS and DRAWWILLY. Holds the LSB of the address
; of the entry in the screen buffer address lookup table at SBUFADDRS that
; corresponds to Willy's pixel y-coordinate; in practice, this is twice Willy's
; actual pixel y-coordinate. Note that when Willy is standing on a ramp, this
; holds his pixel y-coordinate rounded down to the nearest multiple of 16
; (8x2).
PIXEL_Y:
  DEFB 0

; Willy's direction and movement flags
;
; +--------+----------------------------------+-------------------------+
; | Bit(s) | Meaning                          | Used by                 |
; +--------+----------------------------------+-------------------------+
; | 0      | Direction Willy is facing        | MOVEWILLY2, MOVEWILLY3, |
; |        | (reset=right, set=left)          | DRAWWILLY               |
; | 1      | Willy's left/right movement flag | MOVEWILLY, MOVEWILLY2,  |
; |        | (set=moving)                     | MOVEWILLY3, DRAWTHINGS  |
; | 2-7    | Unused (always reset)            |                         |
; +--------+----------------------------------+-------------------------+
DMFLAGS:
  DEFB 0

; Airborne status indicator
;
; Initialised by the routine at TITLESCREEN, checked by the routines at
; ENDPAUSE, MOVEWILLY3 and WILLYATTRS, updated by the routines at KILLWILLY,
; DRAWTHINGS and ROOMABOVE, and checked and updated by the routines at
; MOVEWILLY, MOVEWILLY2 and ROOMBELOW. Possible values are:
;
; +-------+-----------------------------------------------------------------+
; | Value | Meaning                                                         |
; +-------+-----------------------------------------------------------------+
; | 0     | Willy is neither falling nor jumping                            |
; | 1     | Willy is jumping                                                |
; | 2-11  | Willy is falling, and can land safely                           |
; | 12-15 | Willy is falling, and has fallen too far to land safely         |
; | 255   | Willy has collided with a nasty, an arrow, a guardian, or Maria |
; |       | (see KILLWILLY)                                                 |
; +-------+-----------------------------------------------------------------+
AIRBORNE:
  DEFB 0

; Willy's animation frame
;
; Used by the routines at WILLYATTRS and DRAWWILLY, and updated by the routines
; at MAINLOOP, MOVEWILLY3 and DRAWTHINGS. Possible values are 0, 1, 2 and 3.
FRAME:
  DEFB 0

; Address of Willy's location in the attribute buffer at 23552
;
; Initialised by the routine at TITLESCREEN, and used by the routines at
; MOVEWILLY, MOVEWILLY3, DRAWTHINGS, ROOMLEFT, ROOMRIGHT, ROOMABOVE, ROOMBELOW,
; BEDANDBATH, CHKTOILET, WILLYATTRS and DRAWWILLY.
LOCATION:
  DEFW 0

; Jumping animation counter
;
; Used by the routines at MOVEWILLY and MOVEWILLY2.
JUMPING:
  DEFB 0

; Rope status indicator
;
; Initialised by the routine at INITROOM, checked by the routines at MOVEWILLY
; and MOVEWILLY3, and checked and updated by the routines at MOVEWILLY2 and
; DRAWTHINGS. Possible values are:
;
; +---------+-----------------------------------------------------------------+
; | Value   | Meaning                                                         |
; +---------+-----------------------------------------------------------------+
; | 0       | Willy is not on the rope                                        |
; | 3-32    | Willy is on the rope, with the centre of his sprite anchored at |
; |         | this segment                                                    |
; | 240-255 | Willy has just jumped or fallen off the rope                    |
; +---------+-----------------------------------------------------------------+
ROPE:
  DEFB 0

; Willy's state on entry to the room
;
; Initialised by the routine at INITROOM, and copied back into 34255-34261 by
; the routine at LOSELIFE.
INITSTATE:
  DEFB 0                  ; Willy's pixel y-coordinate (copied from PIXEL_Y)
  DEFB 0                  ; Willy's direction and movement flags (copied from
                          ; DMFLAGS)
  DEFB 0                  ; Airborne status indicator (copied from AIRBORNE)
  DEFB 0                  ; Willy's animation frame (copied from FRAME)
  DEFW 0                  ; Address of Willy's location in the attribute buffer
                          ; at 23552 (copied from LOCATION)
  DEFB 0                  ; Jumping animation counter (copied from JUMPING)

; 256 minus the number of items remaining
;
; Initialised by the routine at TITLESCREEN, and updated by the routine at
; DRAWITEMS when an item is collected.
ITEMS:
  DEFB 0

; Game mode indicator
;
; Initialised by the routine at TITLESCREEN, checked by the routines at
; MAINLOOP, MOVEWILLY2 and DRAWTOILET, and updated by the routines at
; DRAWITEMS, BEDANDBATH and CHKTOILET.
;
; +-------+---------------------------------+
; | Value | Meaning                         |
; +-------+---------------------------------+
; | 0     | Normal                          |
; | 1     | All items collected             |
; | 2     | Willy is running to the toilet  |
; | 3     | Willy's head is down the toilet |
; +-------+---------------------------------+
MODE:
  DEFB 0

; Inactivity timer
;
; Initialised by the routine at TITLESCREEN, and updated by the routines at
; MAINLOOP, ENDPAUSE and MOVEWILLY2.
INACTIVE:
  DEFB 0

; In-game music note index
;
; Initialised by the routine at TITLESCREEN, used by the routine at DRAWLIVES,
; and used and updated by the routine at ENDPAUSE.
NOTEINDEX:
  DEFB 0

; Music flags
;
; The keypress flag in bit 0 is initialised by the routine at TITLESCREEN; bits
; 0 and 1 are checked and updated by the routine at ENDPAUSE.
;
; +--------+-----------------------------------------------------------------+
; | Bit(s) | Meaning                                                         |
; +--------+-----------------------------------------------------------------+
; | 0      | Keypress flag (set=H-ENTER being pressed, reset=no key pressed) |
; | 1      | In-game music flag (set=music off, reset=music on)              |
; | 2-7    | Unused                                                          |
; +--------+-----------------------------------------------------------------+
MUSICFLAGS:
  DEFB 0

; WRITETYPER key counter
;
; Checked by the routine at MAINLOOP, and updated by the routine at ENDPAUSE.
TELEPORT:
  DEFB 0

; Temporary variable
;
; Used by the routines at CODESCREEN and READCODE to hold the entry code, by
; the routine at TITLESCREEN to hold the index into the message scrolled across
; the screen after the theme tune has finished playing, and by the routine at
; GAMEOVER to hold the distance of the foot from the top of the screen as it
; descends onto Willy.
TEMPVAR:
  DEFB 0

; WRITETYPER
;
; Used by the routine at ENDPAUSE. In each pair of bytes here, bits 0-4 of the
; first byte correspond to keys Q-W-E-R-T, and bits 0-4 of the second byte
; correspond to keys P-O-I-U-Y; among those bits, a zero indicates a key being
; pressed.
  DEFB %00011111,%00011111 ; (no keys pressed)
WRITETYPER:
  DEFB %00011101,%00011111 ; W
  DEFB %00010111,%00011111 ; R
  DEFB %00011111,%00011011 ; I
  DEFB %00001111,%00011111 ; T
  DEFB %00011011,%00011111 ; E
  DEFB %00001111,%00011111 ; T
  DEFB %00011111,%00001111 ; Y
  DEFB %00011111,%00011110 ; P
  DEFB %00011011,%00011111 ; E
  DEFB %00010111,%00011111 ; R

; Title screen tune data (Moonlight Sonata)
;
; Used by the routine at PLAYTUNE.
THEMETUNE:
  DEFB 81,60,51,81,60,51,81,60,51,81,60,51,81,60,51,81
  DEFB 60,51,81,60,51,81,60,51,76,60,51,76,60,51,76,57
  DEFB 45,76,57,45,81,64,45,81,60,51,81,60,54,91,64,54
  DEFB 102,81,60,81,60,51,81,60,51,40,60,40,40,54,45,81
  DEFB 54,45,81,54,45,40,54,40,40,60,51,81,60,51,38,60
  DEFB 45,76,60,45,40,64,51,81,64,51,45,64,54,32,64,54
  DEFB 61,121,61,255

; In-game tune data (If I Were a Rich Man)
;
; Used by the routine at ENDPAUSE.
GAMETUNE:
  DEFB 86,96,86,96,102,102,128,128,128,128,102,96,86,96,86,96
  DEFB 102,96,86,76,72,76,72,76,86,86,86,86,86,86,86,86
  DEFB 64,64,64,64,68,68,76,76,86,96,102,96,86,86,102,102
  DEFB 81,86,96,86,81,81,96,96,64,64,64,64,64,64,64,64

; Give two chances to enter a correct code
;
; Used by the routine at BEGIN.
ENTERCODES:
  LD HL,16384             ; Clear the display file and attribute file
  LD DE,16385             ;
  LD BC,6911              ;
  LD (HL),0               ;
  LDIR                    ;
  LD IX,MSG_CODE1         ; Point IX at the message at MSG_CODE1 ("Enter Code
                          ; at grid location     ")
  CALL CODESCREEN         ; Display the code entry screen and collect a
                          ; four-digit code from the user
  JP Z,TITLESCREEN        ; Start the game if the code is correct
  LD IX,MSG_CODE2         ; Point IX at the message at MSG_CODE2 ("Sorry, try
                          ; code at location     ")
  CALL CODESCREEN         ; Display the code entry screen and collect another
                          ; four-digit code from the user
  JP Z,TITLESCREEN        ; Start the game if the code is correct
  JP 0                    ; Otherwise reset the machine

; Display the code entry screen
;
; Used by the routine at ENTERCODES. Displays the code entry screen and waits
; for a code to be entered. Returns with the zero flag set if the code entered
; is correct.
;
; IX Address of the message to print (MSG_CODE1 or MSG_CODE2)
CODESCREEN:
  LD DE,18432             ; Print the message pointed to by IX at (8,0)
  LD C,32                 ;
  CALL PRINTMSG           ;
  LD HL,18498             ; Print the graphic for the '1' key at (10,2)
  LD DE,NUMBERKEYS        ;
  LD C,0                  ;
  CALL DRAWSPRITE         ;
  LD HL,18501             ; Print the graphic for the '2' key at (10,5)
  CALL DRAWSPRITE         ;
  LD HL,18504             ; Print the graphic for the '3' key at (10,8)
  CALL DRAWSPRITE         ;
  LD HL,18507             ; Print the graphic for the '4' key at (10,11)
  CALL DRAWSPRITE         ;
  LD HL,CODEATTRS         ; Copy the 128 attribute bytes from CODEATTRS to the
  LD DE,22784             ; screen (lines 8, 9, 10 and 11)
  LD BC,128               ;
  LDIR                    ;
  LD A,(23672)            ; Collect the LSB of the system variable FRAMES
  ADD A,37                ; Add 37 to this value and replace it; this ensures
  LD (23672),A            ; that the value collected on the second pass through
                          ; this routine is different from the value collected
                          ; on the first pass
  CP 179                  ; Is the value between 0 and 178?
  JR C,CODESCREEN_0       ; Jump if so
  SUB 180                 ; Otherwise subtract 180; note that if the original
                          ; value of the LSB of the system variable FRAMES was
                          ; 142, this leaves A holding 255, which is a bug
CODESCREEN_0:
  LD L,A                  ; Now L holds either 255 or some number between 0 and
                          ; 178
  LD H,158                ; Point HL at one of the entries in the table at
                          ; CODES (or at 40703 if L=255)
  LD A,(HL)               ; Pick up the table entry
  ADD A,L                 ; Add L to obtain the actual code
  LD (TEMPVAR),A          ; Store the code at TEMPVAR
  LD C,L                  ; Copy the code index to C; this will be used to
                          ; compute the grid location
  LD E,47                 ; Calculate the ASCII code of the grid location
CODESCREEN_1:
  INC E                   ; number (0-9) in E
  LD A,C                  ;
  CP 18                   ;
  JR C,CODESCREEN_2       ;
  SUB 18                  ;
  LD C,A                  ;
  JR CODESCREEN_1         ;
CODESCREEN_2:
  LD A,E                  ; Print the grid location number at (8,30)
  LD DE,18462             ;
  CALL PRINTCHAR          ;
  LD A,C                  ; Calculate the ASCII code of the grid location
  ADD A,65                ; letter (A-R) in A
  LD DE,18461             ; Print the grid location letter at (8,29)
  CALL PRINTCHAR          ;
; Here we enter a loop that prints a 2x2 coloured block at (10,16), (10,19),
; (10,22) or (10,25) whenever '1', '2', '3' or '4' is pressed, or returns to
; the calling routine at ENTERCODES if ENTER is pressed.
CODESCREEN_3:
  LD IX,22864             ; Point IX at the attribute file location of the
                          ; first coloured block at (10,16)
CODESCREEN_4:
  CALL READCODE           ; Print a coloured block when '1', '2', '3' or '4' is
                          ; pressed, or return to ENTERCODES if ENTER is
                          ; pressed
  INC IX                  ; Move IX along to the location of the next coloured
  INC IX                  ; block
  INC IX                  ;
  LD A,IXl                ; Have we just printed the fourth coloured block at
  CP 92                   ; (10,25)?
  JR NZ,CODESCREEN_4      ; If not, jump back to print the next one
  JR CODESCREEN_3         ; Otherwise rewind IX to the location of the first
                          ; coloured block

; Read the keyboard during code entry
;
; Used by the routine at CODESCREEN. Waits for '1', '2', '3', '4' or ENTER to
; be pressed and either prints a coloured block or validates the entered code.
; Returns to the routine at ENTERCODES if ENTER is being pressed, with the zero
; flag reset if the entered code is correct.
;
; IX Attribute file address of the flashing 2x2 block
READCODE:
  LD BC,63486             ; Read keys 1-2-3-4-5
  IN A,(C)                ;
  AND 15                  ; Is '1', '2', '3' or '4' (still) being pressed?
  CP 15                   ;
  JR NZ,READCODE          ; Jump back if so
READCODE_0:
  LD B,191                ; Read keys H-J-K-L-ENTER
  IN A,(C)                ;
  BIT 0,A                 ; Is ENTER being pressed?
  JR NZ,READCODE_1        ; Jump if not
  LD A,(22873)            ; Pick up the attribute byte of the fourth 2x2 block
                          ; at (10,25)
  AND 127                 ; Has the fourth digit been entered yet?
  CP 7                    ;
  JR Z,READCODE_1         ; Jump if not
; We have four coloured blocks, and ENTER is being pressed. Time to validate
; the entered code.
  SUB 8                   ; Compute bits 0 and 1 of the entered code from the
  AND 24                  ; attribute byte of the fourth coloured block at
  RRCA                    ; (10,25) and store them in C
  RRCA                    ;
  RRCA                    ;
  LD C,A                  ;
  LD A,(22867)            ; Compute bits 4 and 5 of the entered code from the
  SUB 8                   ; attribute byte of the second coloured block at
  AND 24                  ; (10,19) and store them in C (alongside bits 0 and
  RLCA                    ; 1)
  OR C                    ;
  LD C,A                  ;
  LD A,(22870)            ; Compute bits 2 and 3 of the entered code from the
  SUB 8                   ; attribute byte of the third coloured block at
  AND 24                  ; (10,22) and store them in C (alongside bits 0, 1, 4
  RRCA                    ; and 5)
  OR C                    ;
  LD C,A                  ;
  LD A,(22864)            ; Compute bits 6 and 7 of the entered code from the
  SUB 8                   ; attribute byte of the first coloured block at
  AND 24                  ; (10,16) in A
  RLCA                    ;
  RLCA                    ;
  RLCA                    ;
  POP HL                  ; Drop the return address from the stack
  OR C                    ; Merge bits 0-5 of the entered code into A; now A
                          ; holds all 8 bits of the entered code
  LD HL,TEMPVAR           ; Point HL at TEMPVAR (where the correct entry code
                          ; is stored)
  CP (HL)                 ; Set the zero flag if the entered code matches
  RET                     ; Return to the routine at ENTERCODES
; Here we check whether '1', '2', '3' or '4' is being pressed.
READCODE_1:
  SET 7,(IX+0)            ; Make sure the current 2x2 block is flashing
  SET 7,(IX+1)            ;
  SET 7,(IX+32)           ;
  SET 7,(IX+33)           ;
  LD BC,63486             ; Read keys 1-2-3-4-5
  IN A,(C)                ;
  AND 15                  ; Keep only bits 0-3 (keys 1-2-3-4)
  LD E,8                  ; E=8 (INK 0: PAPER 1)
  CP 14                   ; Is '1' alone being pressed?
  JR Z,READCODE_2         ; Jump if so
  LD E,16                 ; E=16 (INK 0: PAPER 2)
  CP 13                   ; Is '2' alone being pressed?
  JR Z,READCODE_2         ; Jump if so
  LD E,24                 ; E=24 (INK 0: PAPER 3)
  CP 11                   ; Is '3' alone being pressed?
  JR Z,READCODE_2         ; Jump if so
  LD E,32                 ; E=32 (INK 0: PAPER 4)
  CP 7                    ; Is '4' alone being pressed?
  JP NZ,READCODE_0        ; If not, jump back to check the ENTER key
; Exactly one of the number keys '1', '2', '3' or '4' is being pressed. E holds
; the corresponding attribute byte to use for the coloured block.
READCODE_2:
  LD (IX+0),E             ; Set the colour of the 2x2 block (no longer
  LD (IX+1),E             ; flashing)
  LD (IX+32),E            ;
  LD (IX+33),E            ;
  LD BC,24                ; Pause for about 0.02s
READCODE_3:
  DJNZ READCODE_3         ;
  DEC C                   ;
  JR NZ,READCODE_3        ;
  RET

; Display the title screen and play the theme tune
;
; Used by the routines at ENTERCODES, MAINLOOP and GAMEOVER.
;
; The first thing this routine does is initialise some game status buffer
; variables in preparation for the next game.
TITLESCREEN:
  XOR A                   ; A=0
  LD (JOYSTICK),A         ; Initialise the Kempston joystick indicator at
                          ; JOYSTICK
  LD (NOTEINDEX),A        ; Initialise the in-game music note index at
                          ; NOTEINDEX
  LD (FLASH),A            ; Initialise the (unused) screen flash counter at
                          ; FLASH
  LD (AIRBORNE),A         ; Initialise the airborne status indicator at
                          ; AIRBORNE
  LD (TICKS),A            ; Initialise the minute counter at TICKS
  LD (INACTIVE),A         ; Initialise the inactivity timer at INACTIVE
  LD (MODE),A             ; Initialise the game mode indicator at MODE
  LD A,7                  ; Initialise the number of lives remaining at LIVES
  LD (LIVES),A            ;
  LD A,208                ; Initialise Willy's pixel y-coordinate at PIXEL_Y
  LD (PIXEL_Y),A          ;
  LD A,33                 ; Initialise the current room number at ROOM to 33
  LD (ROOM),A             ; (The Bathroom)
  LD HL,23988             ; Initialise Willy's coordinates at LOCATION to
  LD (LOCATION),HL        ; (13,20)
  LD HL,MSG_ITEMS         ; Initialise the number of items collected at
  LD (HL),48              ; MSG_ITEMS to "000"
  INC HL                  ;
  LD (HL),48              ;
  INC HL                  ;
  LD (HL),48              ;
  LD H,164                ; Page 164 holds the first byte of each entry in the
                          ; item table
  LD A,(FIRSTITEM)        ; Pick up the index of the first item from FIRSTITEM
  LD L,A                  ; Point HL at the entry for the first item
  LD (ITEMS),A            ; Initialise the counter of items remaining at ITEMS
TITLESCREEN_0:
  SET 6,(HL)              ; Set the collection flag for every item in the item
  INC L                   ; table at ITEMTABLE1
  JR NZ,TITLESCREEN_0     ;
  LD HL,MUSICFLAGS        ; Initialise the keypress flag in bit 0 at MUSICFLAGS
  SET 0,(HL)              ;
; Next, prepare the screen.
TITLESCREEN_1:
  LD HL,16384             ; Clear the entire display file
  LD DE,16385             ;
  LD BC,6143              ;
  LD (HL),0               ;
  LDIR                    ;
  LD HL,ATTRSUPPER        ; Copy the attribute bytes for the title screen from
  LD BC,768               ; ATTRSUPPER and ATTRSLOWER to the attribute file
  LDIR                    ;
  LD HL,23136             ; Copy the attribute value 70 (INK 6: PAPER 0: BRIGHT
  LD DE,23137             ; 1) into the row of 32 cells from (19,0) to (19,31)
  LD BC,31                ; on the screen
  LD (HL),70              ;
  LDIR                    ;
  LD IX,MSG_INTRO         ; Print "+++++ Press ENTER to Start +++++" (see
  LD DE,20576             ; MSG_INTRO) at (19,0)
  LD C,32                 ;
  CALL PRINTMSG           ;
  LD DE,22528             ; Point DE at the first byte of the attribute file
; The following loop scans the top two-thirds of the attribute file, which
; contains values 0, 4, 5, 8, 9, 36, 37, 40, 41, 44, 45 and 211 (copied from
; ATTRSUPPER). Whenever a value other than 0, 9, 36, 45 or 211 is found, a
; triangle UDG is drawn at the corresponding location in the display file.
TITLESCREEN_2:
  LD A,(DE)               ; Pick up a byte from the attribute file
  OR A                    ; Is it 0 (INK 0: PAPER 0)?
  JR Z,TITLESCREEN_6      ; If so, jump to consider the next byte in the
                          ; attribute file
  CP 211                  ; Is it 211 (INK 3: PAPER 2: BRIGHT 1: FLASH 1)?
  JR Z,TITLESCREEN_6      ; If so, jump to consider the next byte in the
                          ; attribute file
  CP 9                    ; Is it 9 (INK 1: PAPER 1)?
  JR Z,TITLESCREEN_6      ; If so, jump to consider the next byte in the
                          ; attribute file
  CP 45                   ; Is it 45 (INK 5: PAPER 5)?
  JR Z,TITLESCREEN_6      ; If so, jump to consider the next byte in the
                          ; attribute file
  CP 36                   ; Is it 36 (INK 4: PAPER 4)?
  JR Z,TITLESCREEN_6      ; If so, jump to consider the next byte in the
                          ; attribute file
  LD C,0                  ; C=0; this will be used as an offset from the
                          ; triangle UDG base address (TRIANGLE0)
  CP 8                    ; Is the attribute value 8 (INK 0: PAPER 1)?
  JR Z,TITLESCREEN_4      ; Jump if so
  CP 41                   ; Is it 41 (INK 1: PAPER 5)?
  JR Z,TITLESCREEN_4      ; Jump if so
  CP 44                   ; Is it 44 (INK 4: PAPER 5)?
  JR Z,TITLESCREEN_3      ; Jump if so
  CP 5                    ; Is it 5 (INK 5: PAPER 0)?
  JR Z,TITLESCREEN_4      ; Jump if so
  LD C,16                 ; Set the triangle UDG offset to 16
  JR TITLESCREEN_4
TITLESCREEN_3:
  LD A,37                 ; Change the attribute byte here from 44 (INK 4:
  LD (DE),A               ; PAPER 5) to 37 (INK 5: PAPER 4)
TITLESCREEN_4:
  LD A,E                  ; Point HL at the triangle UDG to draw (TRIANGLE0,
  AND 1                   ; TRIANGLE1, TRIANGLE2 or TRIANGLE3)
  RLCA                    ;
  RLCA                    ;
  RLCA                    ;
  OR C                    ;
  LD C,A                  ;
  LD B,0                  ;
  LD HL,TRIANGLE0         ;
  ADD HL,BC               ;
  PUSH DE                 ; Save the attribute file address briefly
  BIT 0,D                 ; Set the zero flag if we're still in the top third
                          ; of the attribute file
  LD D,64                 ; Point DE at the top third of the display file
  JR Z,TITLESCREEN_5      ; Jump if we're still in the top third of the
                          ; attribute file
  LD D,72                 ; Point DE at the middle third of the display file
TITLESCREEN_5:
  LD B,8                  ; There are eight pixel rows in a triangle UDG
  CALL PRINTCHAR_0        ; Draw a triangle UDG on the screen
  POP DE                  ; Restore the attribute file address to DE
TITLESCREEN_6:
  INC DE                  ; Point DE at the next byte in the attribute file
  LD A,D                  ; Have we finished scanning the top two-thirds of the
  CP 90                   ; attribute file yet?
  JP NZ,TITLESCREEN_2     ; If not, jump back to examine the next byte
; Now check whether there is a joystick connected.
  LD BC,31                ; This is the joystick port
  DI                      ; Disable interrupts (which are already disabled)
  XOR A                   ; A=0
TITLESCREEN_7:
  IN E,(C)                ; Combine 256 readings of the joystick port in A; if
  OR E                    ; no joystick is connected, some of these readings
  DJNZ TITLESCREEN_7      ; will have bit 5 set
  AND 32                  ; Is a joystick connected (bit 5 reset)?
  JR NZ,TITLESCREEN_8     ; Jump if not
  LD A,1                  ; Set the Kempston joystick indicator at JOYSTICK to
  LD (JOYSTICK),A         ; 1
; And finally, play the theme tune and check for keypresses.
TITLESCREEN_8:
  LD HL,THEMETUNE         ; Point HL at the theme tune data at THEMETUNE
  CALL PLAYTUNE           ; Play the theme tune
  JP NZ,STARTGAME         ; Start the game if ENTER, 0 or the fire button was
                          ; pressed
  XOR A                   ; Initialise the temporary game status buffer
  LD (TEMPVAR),A          ; variable at TEMPVAR to 0; this will be used as an
                          ; index for the message scrolled across the screen
                          ; (see MSG_INTRO)
TITLESCREEN_9:
  CALL CYCLEATTRS         ; Cycle the INK and PAPER colours
  LD HL,23136             ; Copy the attribute value 79 (INK 7: PAPER 1: BRIGHT
  LD DE,23137             ; 1) into the row of 32 cells from (19,0) to (19,31)
  LD BC,31                ; on the screen
  LD (HL),79              ;
  LDIR                    ;
  LD A,(TEMPVAR)          ; Pick up the message index from TEMPVAR
  LD IX,MSG_INTRO         ; Point IX at the corresponding location in the
  LD E,A                  ; message at MSG_INTRO
  LD D,0                  ;
  ADD IX,DE               ;
  LD DE,20576             ; Print 32 characters of the message at (19,0)
  LD C,32                 ;
  CALL PRINTMSG           ;
  LD A,(TEMPVAR)          ; Prepare a value between 50 and 81 in A (for the
  AND 31                  ; routine at INTROSOUND)
  ADD A,50                ;
  CALL INTROSOUND         ; Make a sound effect
  LD BC,45054             ; Read keys H-J-K-L-ENTER and 6-7-8-9-0
  IN A,(C)                ;
  AND 1                   ; Keep only bit 0 of the result (ENTER, 0)
  CP 1                    ; Was ENTER or 0 pressed?
  JR NZ,STARTGAME         ; Jump if so to start the game
  LD A,(TEMPVAR)          ; Pick up the message index from TEMPVAR
  INC A                   ; Increment it
  CP 224                  ; Set the zero flag if we've reached the end of the
                          ; message
  LD (TEMPVAR),A          ; Store the new message index at TEMPVAR
  JR NZ,TITLESCREEN_9     ; Jump back unless we've finished scrolling the
                          ; message across the screen
  JP TITLESCREEN_1        ; Jump back to prepare the screen and play the theme
                          ; tune again

; Start the game
;
; Used by the routine at TITLESCREEN.
STARTGAME:
  LD HL,MSG_7AM           ; Copy the text at MSG_7AM (" 7:00a") to MSG_CURTIME,
  LD DE,MSG_CURTIME       ; thus resetting the clock
  LD BC,6                 ;
  LDIR                    ;
  LD HL,ATTRSLOWER        ; Copy the attribute bytes from ATTRSLOWER to the
  LD DE,23040             ; bottom third of the screen
  LD BC,256               ;
  LDIR                    ;
; This routine continues into the one at INITROOM.

; Initialise the current room
;
; Used by the routines at ENDPAUSE (to teleport into a room), LOSELIFE (to
; reinitialise the room after Willy has lost a life), ROOMLEFT (when Willy has
; entered the room from the right), ROOMRIGHT (when Willy has entered the room
; from the left), ROOMABOVE (when Willy has entered the room from below) and
; ROOMBELOW (when Willy has entered the room from above). The routine at
; STARTGAME also continues here.
INITROOM:
  LD A,(ROOM)             ; Pick up the current room number from ROOM
  OR 192                  ; Point HL at the first byte of the room definition
  LD H,A                  ;
  LD L,0                  ;
  LD DE,ROOMLAYOUT        ; Copy the room definition into the game status
  LD BC,256               ; buffer at 32768
  LDIR                    ;
  LD IX,ENTITIES          ; Point IX at the first byte of the first entity
                          ; specification for the current room at ENTITIES
  LD DE,ENTITYBUF         ; Point DE at the first byte of the first entity
                          ; buffer at ENTITYBUF; this instruction is redundant,
                          ; since DE already holds 33024
  LD A,8                  ; There are at most eight entities in a room
INITROOM_0:
  LD L,(IX+0)             ; Pick up the first byte of the entity specification
  RES 7,L                 ; Point HL at the corresponding entry in the table of
  LD H,20                 ; entity definitions at ENTITYDEFS
  ADD HL,HL               ;
  ADD HL,HL               ;
  ADD HL,HL               ;
  LD BC,2                 ; Copy the first two bytes of the entity definition
  LDIR                    ; into the entity buffer
  LD C,(IX+1)             ; Copy the second byte of the entity specification
  LD (HL),C               ; into the third byte of the entity definition
  LD BC,6                 ; Copy the remaining six bytes of the entity
  LDIR                    ; definition into the entity buffer
  INC IX                  ; Point IX at the first byte of the next entity
  INC IX                  ; specification
  DEC A                   ; Have we copied all eight entity definitions into
                          ; the entity buffers yet?
  JR NZ,INITROOM_0        ; If not, jump back to copy the next one
  LD HL,PIXEL_Y           ; Copy the seven bytes that define Willy's state
  LD DE,INITSTATE         ; (position, animation frame etc.) on entry to this
  LD BC,7                 ; room from 34255-34261 to INITSTATE
  LDIR                    ;
  CALL DRAWROOM           ; Draw the current room to the screen buffer at 28672
                          ; and the attribute buffer at 24064
  LD HL,20480             ; Clear the bottom third of the display file
  LD DE,20481             ;
  LD BC,2047              ;
  LD (HL),0               ;
  LDIR                    ;
  LD IX,ROOMNAME          ; Print the room name (see ROOMNAME) at (16,0)
  LD C,32                 ;
  LD DE,20480             ;
  CALL PRINTMSG           ;
  LD IX,MSG_STATUS        ; Print "Items collected 000 Time 00:00 m" (see
  LD DE,20576             ; MSG_STATUS) at (19,0)
  LD C,32                 ;
  CALL PRINTMSG           ;
  LD A,(BORDER)           ; Pick up the border colour for the current room from
                          ; BORDER
  LD C,254                ; Set the border colour
  OUT (C),A               ;
  XOR A                   ; Initialise the rope status indicator at ROPE to 0
  LD (ROPE),A             ;
  JP MAINLOOP             ; Enter the main loop

; Draw the remaining lives
;
; Used by the routine at MAINLOOP.
DRAWLIVES:
  LD A,(LIVES)            ; Pick up the number of lives remaining from LIVES
  LD HL,20640             ; Set HL to the display file address at which to draw
                          ; the first Willy sprite
  OR A                    ; Are there any lives remaining?
  RET Z                   ; Return if not
  LD B,A                  ; Initialise B to the number of lives remaining
; The sprite-drawing loop begins.
DRAWLIVES_0:
  LD C,0                  ; C=0; this tells the sprite-drawing routine at
                          ; DRAWSPRITE to overwrite any existing graphics
  PUSH HL                 ; Save HL and BC briefly
  PUSH BC                 ;
  LD A,(NOTEINDEX)        ; Pick up the in-game music note index from
                          ; NOTEINDEX; this will determine the animation frame
                          ; for the Willy sprites
  RLCA                    ; Now A=0 (frame 0), 32 (frame 1), 64 (frame 2) or 96
  RLCA                    ; (frame 3)
  RLCA                    ;
  AND 96                  ;
  LD E,A                  ; Point DE at the corresponding Willy sprite (at
  LD D,157                ; MANDAT+A)
  CALL DRAWSPRITE         ; Draw the Willy sprite on the screen
  POP BC                  ; Restore HL and BC
  POP HL                  ;
  INC HL                  ; Move HL along to the location at which to draw the
  INC HL                  ; next Willy sprite
  DJNZ DRAWLIVES_0        ; Jump back to draw any remaining sprites
  RET

; Main loop (1)
;
; Used by the routines at INITROOM and ENDPAUSE.
MAINLOOP:
  CALL DRAWLIVES          ; Draw the remaining lives
  LD HL,24064             ; Copy the contents of the attribute buffer at 24064
  LD DE,23552             ; (the attributes for the empty room) into the
  LD BC,512               ; attribute buffer at 23552
  LDIR                    ;
  LD HL,28672             ; Copy the contents of the screen buffer at 28672
  LD DE,24576             ; (the tiles for the empty room) into the screen
  LD BC,4096              ; buffer at 24576
  LDIR                    ;
  CALL MOVETHINGS         ; Move the rope and guardians in the current room
  LD A,(MODE)             ; Pick up the game mode indicator from MODE
  CP 3                    ; Is Willy's head down the toilet?
  CALL NZ,MOVEWILLY       ; If not, move Willy
AFTERMOVE1:
  LD A,(PIXEL_Y)          ; Pick up Willy's pixel y-coordinate from PIXEL_Y
  CP 225                  ; Has Willy just moved up a ramp or a rope past the
                          ; top of the screen?
  CALL NC,ROOMABOVE       ; If so, move Willy into the room above
AFTERMOVE2:
  LD A,(MODE)             ; Pick up the game mode indicator from MODE
  CP 3                    ; Is Willy's head down the toilet?
  CALL NZ,WILLYATTRS      ; If not, check and set the attribute bytes for
                          ; Willy's sprite in the buffer at 23552, and draw
                          ; Willy to the screen buffer at 24576
  LD A,(MODE)             ; Pick up the game mode indicator from MODE
  CP 2                    ; Is Willy on his way to the toilet?
  CALL Z,CHKTOILET        ; If so, check whether he's reached it yet
  CALL BEDANDBATH         ; Deal with special rooms (Master Bedroom, The
                          ; Bathroom)
  CALL DRAWTHINGS         ; Draw the rope, arrows and guardians in the current
                          ; room
  CALL MVCONVEYOR         ; Move the conveyor in the current room (if there is
                          ; one)
  CALL DRAWITEMS          ; Draw the items in the current room (if there are
                          ; any) and collect any that Willy is touching
; This entry point is used by the routine at KILLWILLY.
MAINLOOP_0:
  LD HL,24576             ; Copy the contents of the screen buffer at 24576 to
  LD DE,16384             ; the display file
  LD BC,4096              ;
  LDIR                    ;
  LD A,(MODE)             ; Pick up the game mode indicator from MODE
  AND 2                   ; Now A=1 if Willy is running to the toilet or
  RRCA                    ; already has his head down it, 0 otherwise
  LD HL,FRAME             ; Set Willy's animation frame at FRAME to 1 or 3 if
  OR (HL)                 ; Willy is running to the toilet or already has his
  LD (HL),A               ; head down it; this has the effect of moving Willy
                          ; at twice his normal speed as he makes his way to
                          ; the toilet (using animation frames 2 and 0)
SCRFLASH:
  LD A,(FLASH)            ; Pick up the screen flash counter (unused and always
                          ; 0) from FLASH
  OR A                    ; Is it zero?
  JR Z,MAINLOOP_1         ; Jump if so (this jump is always made)
; The next section of code is never executed.
  DEC A                   ; Decrement the screen flash counter at FLASH
  LD (FLASH),A            ;
  RLCA                    ; Move bits 0-2 into bits 3-5 and clear all the other
  RLCA                    ; bits
  RLCA                    ;
  AND 56                  ;
  LD HL,23552             ; Set every attribute byte in the buffer at 23552 to
  LD DE,23553             ; this value
  LD BC,511               ;
  LD (HL),A               ;
  LDIR                    ;
; Normal service resumes here.
MAINLOOP_1:
  LD HL,23552             ; Copy the contents of the attribute buffer at 23552
  LD DE,22528             ; to the attribute file
  LD BC,512               ;
  LDIR                    ;
  LD IX,MSG_CURTIME       ; Print the current time (see MSG_CURTIME) at (19,25)
  LD DE,20601             ;
  LD C,6                  ;
  CALL PRINTMSG           ;
  LD IX,MSG_ITEMS         ; Print the number of items collected (see MSG_ITEMS)
  LD DE,20592             ; at (19,16)
  LD C,3                  ;
  CALL PRINTMSG           ;
  LD A,(TICKS)            ; Increment the minute counter at TICKS
  INC A                   ;
  LD (TICKS),A            ;
  JR NZ,MAINLOOP_3        ; Jump unless the minute counter has ticked over to 0
; A minute of game time has passed. Update the game clock accordingly.
  LD IX,MSG_CURTIME       ; Point IX at the current time at MSG_CURTIME
  INC (IX+4)              ; Increment the units digit of the minute
  LD A,(IX+4)             ; Pick up the new units digit
  CP 58                   ; Was it '9' before?
  JR NZ,MAINLOOP_3        ; Jump if not
  LD (IX+4),48            ; Set the units digit of the minute to '0'
  INC (IX+3)              ; Increment the tens digit of the minute
  LD A,(IX+3)             ; Pick up the new tens digit
  CP 54                   ; Was it '5' before?
  JR NZ,MAINLOOP_3        ; Jump if not
  LD (IX+3),48            ; Set the tens digit of the minute to '0'
  LD A,(IX+0)             ; Pick up the tens digit of the hour
  CP 49                   ; Is it currently '1'?
  JR NZ,MAINLOOP_2        ; Jump if not
  INC (IX+1)              ; Increment the units digit of the hour
  LD A,(IX+1)             ; Pick up the new units digit
  CP 51                   ; Was it '2' before?
  JR NZ,MAINLOOP_3        ; Jump if not
  LD A,(IX+5)             ; Pick up the 'a' or 'p' of 'am/pm'
  CP 112                  ; Is it 'p'?
  JP Z,TITLESCREEN        ; If so, quit the game (it's 1am)
  LD (IX+0),32            ; Set the tens digit of the hour to ' ' (space)
  LD (IX+1),49            ; Set the units digit of the hour to '1'
  LD (IX+5),112           ; Change the 'a' of 'am' to 'p'
  JR MAINLOOP_3
MAINLOOP_2:
  INC (IX+1)              ; Increment the units digit of the hour
  LD A,(IX+1)             ; Pick up the new units digit
  CP 58                   ; Was it '9' before?
  JR NZ,MAINLOOP_3        ; Jump if not
  LD (IX+1),48            ; Set the units digit of the hour to '0'
  LD (IX+0),49            ; Set the tens digit of the hour to '1'
; Now check whether any non-movement keys are being pressed.
MAINLOOP_3:
  LD BC,65278             ; Read keys SHIFT-Z-X-C-V
  IN A,(C)                ;
  LD E,A                  ; Save the result in E
  LD B,127                ; Read keys B-N-M-SS-SPACE
  IN A,(C)                ;
  OR E                    ; Combine the results
  AND 1                   ; Are SHIFT and SPACE being pressed?
  JP Z,TITLESCREEN        ; If so, quit the game
  LD A,(INACTIVE)         ; Increment the inactivity timer at INACTIVE
  INC A                   ;
  LD (INACTIVE),A         ;
  JR Z,PAUSE              ; Jump if the inactivity timer is now 0 (no keys have
                          ; been pressed for a while)
  LD B,253                ; Read keys A-S-D-F-G
  IN A,(C)                ;
  AND 31                  ; Are any of these keys being pressed?
  CP 31                   ;
  JR Z,ENDPAUSE_0         ; Jump if not
  LD DE,0                 ; Prepare the delay counters in D and E for the pause
                          ; loop that follows
; The following loop pauses the game until any key except A, S, D, F or G is
; pressed.
PAUSE:
  LD B,2                  ; Read every half-row of keys except A-S-D-F-G
  IN A,(C)                ;
  AND 31                  ; Are any of these keys being pressed?
  CP 31                   ;
SEE39936:
  JR NZ,ENDPAUSE          ; If so, resume the game
  INC E                   ; Increment the delay counter in E
  JR NZ,PAUSE             ; Jump back unless it's zero
  INC D                   ; Increment the delay counter in D
  JR NZ,PAUSE             ; Jump back unless it's zero
  LD A,(TELEPORT)         ; Pick up the WRITETYPER key counter from TELEPORT
  CP 10                   ; Has WRITETYPER been keyed in yet?
  CALL NZ,CYCLEATTRS      ; If not, cycle the INK and PAPER colours
  JR PAUSE                ; Jump back to the beginning of the pause loop

; Cycle the INK and PAPER colours
;
; Used by the routines at TITLESCREEN (while scrolling the instructions across
; the screen) and MAINLOOP (while the game is paused).
CYCLEATTRS:
  LD HL,22528             ; Point HL at the first byte of the attribute file
  LD A,(HL)               ; Pick up this byte
  AND 7                   ; Keep only bits 0-2 (the INK colour)
  OUT (254),A             ; Set the border colour to match
; Now we loop over every byte in the attribute file.
CYCLEATTRS_0:
  LD A,(HL)               ; Pick up an attribute file byte
  ADD A,3                 ; Cycle the INK colour forward by three
  AND 7                   ;
  LD D,A                  ; Save the new INK colour in D
  LD A,(HL)               ; Pick up the attribute file byte again
  ADD A,24                ; Cycle the PAPER colour forward by three (and turn
  AND 184                 ; off any BRIGHT colours)
  OR D                    ; Merge in the new INK colour
  LD (HL),A               ; Save the new attribute byte
  INC HL                  ; Point HL at the next byte in the attribute file
  LD A,H                  ; Have we reached the end of the attribute file yet?
  CP 91                   ;
  JR NZ,CYCLEATTRS_0      ; If not, jump back to modify the next byte
  RET

; Main loop (2)
;
; Used by the routine at MAINLOOP. The main entry point is used when resuming
; the game after it has been paused.
ENDPAUSE:
  LD HL,ATTRSLOWER        ; Copy the attribute bytes from ATTRSLOWER to the
  LD DE,23040             ; bottom third of the screen
  LD BC,256               ;
  LDIR                    ;
  LD A,(BORDER)           ; Pick up the border colour for the current room from
                          ; BORDER
  OUT (254),A             ; Restore the border colour
; This entry point is used by the routine at MAINLOOP.
ENDPAUSE_0:
  LD A,(AIRBORNE)         ; Pick up the airborne status indicator from AIRBORNE
  CP 255                  ; Has Willy landed after falling from too great a
                          ; height, or collided with a nasty, an arrow, a
                          ; guardian, or Maria?
  JP Z,LOSELIFE           ; If so, lose a life
; Now read the keys H, J, K, L and ENTER (which toggle the in-game music).
  LD B,191                ; Prepare B for reading keys H-J-K-L-ENTER
  LD HL,MUSICFLAGS        ; Point HL at the music flags at MUSICFLAGS
  IN A,(C)                ; Read keys H-J-K-L-ENTER; note that if the game has
                          ; just resumed after being paused, C holds 0 instead
                          ; of 254, which is a bug
  AND 31                  ; Are any of these keys being pressed?
  CP 31                   ;
  JR Z,ENDPAUSE_1         ; Jump if not
  BIT 0,(HL)              ; Were any of these keys being pressed the last time
                          ; we checked?
  JR NZ,ENDPAUSE_2        ; Jump if so
  LD A,(HL)               ; Set bit 0 (the keypress flag) and flip bit 1 (the
  XOR 3                   ; in-game music flag) at MUSICFLAGS
  LD (HL),A               ;
  JR ENDPAUSE_2
ENDPAUSE_1:
  RES 0,(HL)              ; Reset bit 0 (the keypress flag) at MUSICFLAGS
ENDPAUSE_2:
  BIT 1,(HL)              ; Has the in-game music been switched off?
  JR NZ,ENDPAUSE_5        ; Jump if so
; The next section of code plays a note of the in-game music.
  XOR A                   ; Reset the inactivity timer at INACTIVE (the game
  LD (INACTIVE),A         ; does not automatically pause after a period of
                          ; inactivity if the in-game music is playing)
  LD A,(NOTEINDEX)        ; Increment the in-game music note index at NOTEINDEX
  INC A                   ;
  LD (NOTEINDEX),A        ;
  AND 126                 ; Point HL at the appropriate entry in the tune data
  RRCA                    ; table at GAMETUNE
  LD E,A                  ;
  LD D,0                  ;
  LD HL,GAMETUNE          ;
  ADD HL,DE               ;
  LD A,(LIVES)            ; Pick up the number of lives remaining (0-7) from
                          ; LIVES
  RLCA                    ; A=28-4A; this value adjusts the pitch of the note
  RLCA                    ; that is played depending on how many lives are
  SUB 28                  ; remaining (the more lives remaining, the higher the
  NEG                     ; pitch)
  ADD A,(HL)              ; Add the entry from the tune data table for the
                          ; current note
  LD D,A                  ; Copy this value to D (which determines the pitch of
                          ; the note)
  LD A,(BORDER)           ; Pick up the border colour for the current room from
                          ; BORDER
  LD E,D                  ; Initialise the pitch delay counter in E
  LD BC,3                 ; Initialise the duration delay counters in B (0) and
                          ; C (3)
ENDPAUSE_3:
  OUT (254),A             ; Produce a note of the in-game music
  DEC E                   ;
  JR NZ,ENDPAUSE_4        ;
  LD E,D                  ;
  XOR 24                  ;
ENDPAUSE_4:
  DJNZ ENDPAUSE_3         ;
  DEC C                   ;
  JR NZ,ENDPAUSE_3        ;
; Here we check the teleport keys.
ENDPAUSE_5:
  LD BC,61438             ; Read keys 6-7-8-9-0
  IN A,(C)                ;
  BIT 1,A                 ; Is '9' (the activator key) being pressed?
  JP NZ,ENDPAUSE_6        ; Jump if not
  AND 16                  ; Keep only bit 4 (corresponding to the '6' key),
  XOR 16                  ; flip it, and move it into bit 5
  RLCA                    ;
  LD D,A                  ; Now bit 5 of D is set if '6' is being pressed
  LD A,(TELEPORT)         ; Pick up the WRITETYPER key counter from TELEPORT
  CP 10                   ; Has WRITETYPER been keyed in yet?
  JP NZ,ENDPAUSE_6        ; Jump if not
  LD BC,63486             ; Read keys 1-2-3-4-5
  IN A,(C)                ;
  CPL                     ; Keep only bits 0-4 and flip them
  AND 31                  ;
  OR D                    ; Copy bit 5 of D into A; now A holds the number of
                          ; the room to teleport to
  LD (ROOM),A             ; Store the room number at ROOM
  JP INITROOM             ; Teleport into the room
; Finally, check the WRITETYPER keys.
ENDPAUSE_6:
  LD A,(TELEPORT)         ; Pick up the WRITETYPER key counter from TELEPORT
  CP 10                   ; Has WRITETYPER been keyed in yet?
  JP Z,MAINLOOP           ; If so, jump back to the start of the main loop
  LD A,(ROOM)             ; Pick up the current room number from ROOM
  CP 28                   ; Are we in First Landing?
  JP NZ,MAINLOOP          ; If not, jump back to the start of the main loop
  LD A,(PIXEL_Y)          ; Pick up Willy's pixel y-coordinate from PIXEL_Y
  CP 208                  ; Is Willy on the floor at the bottom of the
                          ; staircase?
  JP NZ,MAINLOOP          ; If not, jump back to the start of the main loop
  LD A,(TELEPORT)         ; Pick up the WRITETYPER key counter (0-9) from
                          ; TELEPORT
  RLCA                    ; Point IX at the corresponding entry in the
  LD E,A                  ; WRITETYPER table at WRITETYPER
  LD D,0                  ;
  LD IX,WRITETYPER        ;
  ADD IX,DE               ;
  LD BC,64510             ; Read keys Q-W-E-R-T
  IN A,(C)                ;
  AND 31                  ; Keep only bits 0-4
  CP (IX+0)               ; Does this match the first byte of the entry in the
                          ; WRITETYPER table?
  JR Z,ENDPAUSE_7         ; Jump if so
  CP 31                   ; Are any of the keys Q-W-E-R-T being pressed?
  JP Z,MAINLOOP           ; If not, jump back to the start of the main loop
  CP (IX-2)               ; Does the keyboard reading match the first byte of
                          ; the previous entry in the WRITETYPER table?
  JP Z,MAINLOOP           ; If so, jump back to the start of the main loop
  XOR A                   ; Reset the WRITETYPER key counter at TELEPORT to 0
  LD (TELEPORT),A         ; (an incorrect key was pressed)
  JP MAINLOOP             ; Jump back to the start of the main loop
ENDPAUSE_7:
  LD B,223                ; Read keys Y-U-I-O-P
  IN A,(C)                ;
  AND 31                  ; Keep only bits 0-4
  CP (IX+1)               ; Does this match the second byte of the entry in the
                          ; WRITETYPER table?
  JR Z,ENDPAUSE_8         ; If so, jump to increment the WRITETYPER key counter
  CP 31                   ; Are any of the keys Y-U-I-O-P being pressed?
  JP Z,MAINLOOP           ; If not, jump back to the start of the main loop
  CP (IX-1)               ; Does the keyboard reading match the second byte of
                          ; the previous entry in the WRITETYPER table?
  JP Z,MAINLOOP           ; If so, jump back to the start of the main loop
  XOR A                   ; Reset the WRITETYPER key counter at TELEPORT to 0
  LD (TELEPORT),A         ; (an incorrect key was pressed)
  JP MAINLOOP             ; Jump back to the start of the main loop
ENDPAUSE_8:
  LD A,(TELEPORT)         ; Increment the WRITETYPER key counter at TELEPORT
  INC A                   ;
  LD (TELEPORT),A         ;
  JP MAINLOOP             ; Jump back to the start of the main loop

; Lose a life
;
; Used by the routine at ENDPAUSE.
LOSELIFE:
  LD A,71                 ; A=71 (INK 7: PAPER 0: BRIGHT 1)
; The following loop fills the top two thirds of the attribute file with a
; single value (71, 70, 69, 68, 67, 66, 65 or 64) and makes a sound effect.
LOSELIFE_0:
  LD HL,22528             ; Fill the top two thirds of the attribute file with
  LD DE,22529             ; the value in A
  LD BC,511               ;
  LD (HL),A               ;
  LDIR                    ;
  LD E,A                  ; Save the attribute byte (64-71) in E for later
                          ; retrieval
  CPL                     ; D=63-8*(E AND 7); this value determines the pitch
  AND 7                   ; of the short note that will be played
  RLCA                    ;
  RLCA                    ;
  RLCA                    ;
  OR 7                    ;
  LD D,A                  ;
  LD C,E                  ; C=8+32*(E AND 7); this value determines the
  RRC C                   ; duration of the short note that will be played
  RRC C                   ;
  RRC C                   ;
  OR 16                   ; Set bit 4 of A (for no apparent reason)
  XOR A                   ; Set A=0 (this will make the border black)
LOSELIFE_1:
  OUT (254),A             ; Produce a short note whose pitch is determined by D
  XOR 24                  ; and whose duration is determined by C
  LD B,D                  ;
LOSELIFE_2:
  DJNZ LOSELIFE_2         ;
  DEC C                   ;
  JR NZ,LOSELIFE_1        ;
  LD A,E                  ; Restore the attribute byte (originally 71) to A
  DEC A                   ; Decrement it (effectively decrementing the INK
                          ; colour)
  CP 63                   ; Have we used attribute value 64 (INK 0) yet?
  JR NZ,LOSELIFE_0        ; If not, jump back to update the INK colour in the
                          ; top two thirds of the screen and make another sound
                          ; effect
; Now check whether any lives remain.
  LD HL,LIVES             ; Pick up the number of lives remaining from LIVES
  LD A,(HL)               ;
  OR A                    ; Are there any lives remaining?
  JP Z,GAMEOVER           ; If not, display the game over sequence
  DEC (HL)                ; Decrease the number of lives remaining by one
  LD HL,INITSTATE         ; Restore Willy's state upon entry to the room by
  LD DE,PIXEL_Y           ; copying the seven bytes at INITSTATE back into
  LD BC,7                 ; 34255-34261
  LDIR                    ;
  JP INITROOM             ; Reinitialise the room and resume the game

; Display the game over sequence
;
; Used by the routine at LOSELIFE.
GAMEOVER:
  LD HL,16384             ; Clear the top two-thirds of the display file
  LD DE,16385             ;
  LD BC,4095              ;
  LD (HL),0               ;
  LDIR                    ;
  XOR A                   ; Initialise the temporary game status buffer
  LD (TEMPVAR),A          ; variable at TEMPVAR; this variable will determine
                          ; the distance of the foot from the top of the screen
  LD DE,WILLYR2           ; Draw Willy at (12,15)
  LD HL,18575             ;
  LD C,0                  ;
  CALL DRAWSPRITE         ;
  LD DE,BARREL            ; Draw the barrel underneath Willy at (14,15)
  LD HL,18639             ;
  LD C,0                  ;
  CALL DRAWSPRITE         ;
; The following loop draws the foot's descent onto the barrel that supports
; Willy.
GAMEOVER_0:
  LD A,(TEMPVAR)          ; Pick up the distance variable from TEMPVAR
  LD C,A                  ; Point BC at the corresponding entry in the screen
  LD B,130                ; buffer address lookup table at SBUFADDRS
  LD A,(BC)               ; Point HL at the corresponding location in the
  OR 15                   ; display file
  LD L,A                  ;
  INC BC                  ;
  LD A,(BC)               ;
  SUB 32                  ;
  LD H,A                  ;
  LD DE,FOOT              ; Draw the foot at this location, without erasing the
  LD C,0                  ; foot at the previous location; this leaves the
  CALL DRAWSPRITE         ; portion of the foot sprite that's above the ankle
                          ; in place, and makes the foot appear as if it's at
                          ; the end of a long, extending leg
  LD A,(TEMPVAR)          ; Pick up the distance variable from TEMPVAR
  CPL                     ; A=255-A
  LD E,A                  ; Store this value (63-255) in E; it determines the
                          ; (rising) pitch of the sound effect that will be
                          ; made
  XOR A                   ; A=0 (black border)
  LD BC,64                ; C=64; this value determines the duration of the
                          ; sound effect
GAMEOVER_1:
  OUT (254),A             ; Produce a short note whose pitch is determined by E
  XOR 24                  ;
  LD B,E                  ;
GAMEOVER_2:
  DJNZ GAMEOVER_2         ;
  DEC C                   ;
  JR NZ,GAMEOVER_1        ;
  LD HL,22528             ; Prepare BC, DE and HL for setting the attribute
  LD DE,22529             ; bytes in the top two-thirds of the screen
  LD BC,511               ;
  LD A,(TEMPVAR)          ; Pick up the distance variable from TEMPVAR
  AND 12                  ; Keep only bits 2 and 3
  RLCA                    ; Shift bits 2 and 3 into bits 3 and 4; these bits
                          ; determine the PAPER colour: 0, 1, 2 or 3
  OR 71                   ; Set bits 0-2 (INK 7) and 6 (BRIGHT 1)
  LD (HL),A               ; Copy this attribute value into the top two-thirds
  LDIR                    ; of the screen
  AND 250                 ; Reset bits 0 and 2, and retain all other bits
  OR 2                    ; Set bit 1 (INK 2)
  LD (22991),A            ; Copy this attribute value to the cells at (14,15),
  LD (22992),A            ; (14,16), (15, 15) and (15, 16) (where the barrel
  LD (23023),A            ; is, so that it remains red)
  LD (23024),A            ;
  LD A,(TEMPVAR)          ; Add 4 to the distance variable at TEMPVAR; this
  ADD A,4                 ; will move the foot sprite down two pixel rows
  LD (TEMPVAR),A          ;
  CP 196                  ; Has the foot met the barrel yet?
  JR NZ,GAMEOVER_0        ; Jump back if not
; Now print the "Game Over" message, just to drive the point home.
  LD IX,MSG_GAME          ; Print "Game" (see MSG_GAME) at (6,10)
  LD C,4                  ;
  LD DE,16586             ;
  CALL PRINTMSG           ;
  LD IX,MSG_OVER          ; Print "Over" (see MSG_OVER) at (6,18)
  LD C,4                  ;
  LD DE,16594             ;
  CALL PRINTMSG           ;
  LD BC,0                 ; Prepare the delay counters for the following loop;
  LD D,6                  ; the counter in C will also determine the INK
                          ; colours to use for the "Game Over" message
; The following loop makes the "Game Over" message glisten for about 1.57s.
GAMEOVER_3:
  DJNZ GAMEOVER_3         ; Delay for about a millisecond
  LD A,C                  ; Change the INK colour of the "G" in "Game" at
  AND 7                   ; (6,10)
  OR 64                   ;
  LD (22730),A            ;
  INC A                   ; Change the INK colour of the "a" in "Game" at
  AND 7                   ; (6,11)
  OR 64                   ;
  LD (22731),A            ;
  INC A                   ; Change the INK colour of the "m" in "Game" at
  AND 7                   ; (6,12)
  OR 64                   ;
  LD (22732),A            ;
  INC A                   ; Change the INK colour of the "e" in "Game" at
  AND 7                   ; (6,13)
  OR 64                   ;
  LD (22733),A            ;
  INC A                   ; Change the INK colour of the "O" in "Over" at
  AND 7                   ; (6,18)
  OR 64                   ;
  LD (22738),A            ;
  INC A                   ; Change the INK colour of the "v" in "Over" at
  AND 7                   ; (6,19)
  OR 64                   ;
  LD (22739),A            ;
  INC A                   ; Change the INK colour of the "e" in "Over" at
  AND 7                   ; (6,20)
  OR 64                   ;
  LD (22740),A            ;
  INC A                   ; Change the INK colour of the "r" in "Over" at
  AND 7                   ; (6,21)
  OR 64                   ;
  LD (22741),A            ;
  DEC C                   ; Decrement the counter in C
  JR NZ,GAMEOVER_3        ; Jump back unless it's zero
  DEC D                   ; Decrement the counter in D (initially 6)
  JR NZ,GAMEOVER_3        ; Jump back unless it's zero
  JP TITLESCREEN          ; Display the title screen and play the theme tune

; Draw the current room to the screen buffer at 28672
;
; Used by the routine at INITROOM.
DRAWROOM:
  CALL ROOMATTRS          ; Fill the buffer at 24064 with attribute bytes for
                          ; the current room
  LD IX,24064             ; Point IX at the first byte of the attribute buffer
                          ; at 24064
  LD A,112                ; Set the operand of the 'LD D,n' instruction at
  LD (36189),A            ; BUFMSB (below) to 112
  CALL DRAWROOM_0         ; Draw the tiles for the top half of the room to the
                          ; screen buffer at 28672
  LD IX,24320             ; Point IX at the 256th byte of the attribute buffer
                          ; at 24064 in preparation for drawing the bottom half
                          ; of the room; this instruction is redundant, since
                          ; IX already holds 24320
  LD A,120                ; Set the operand of the 'LD D,n' instruction at
  LD (36189),A            ; BUFMSB (below) to 120
DRAWROOM_0:
  LD C,0                  ; C will count 256 tiles
; The following loop draws 256 tiles (for either the top half or the bottom
; half of the room) to the screen buffer at 28672.
DRAWROOM_1:
  LD E,C                  ; E holds the LSB of the screen buffer address
  LD A,(IX+0)             ; Pick up an attribute byte from the buffer at 24064;
                          ; this identifies the type of tile (background,
                          ; floor, wall, nasty, ramp or conveyor) to be drawn
  LD HL,BACKGROUND        ; Move HL through the attribute bytes and graphic
  LD BC,54                ; data of the background, floor, wall, nasty, ramp
  CPIR                    ; and conveyor tiles starting at BACKGROUND until we
                          ; find a byte that matches the attribute byte of the
                          ; tile to be drawn; note that if a graphic data byte
                          ; matches the attribute byte being searched for, the
                          ; CPIR instruction can exit early, which is a bug
  LD C,E                  ; Restore the value of the tile counter in C
  LD B,8                  ; There are eight bytes in the tile
BUFMSB:
  LD D,0                  ; This instruction is set to either 'LD D,112' or 'LD
                          ; D,120' above; now DE holds the appropriate address
                          ; in the screen buffer at 28672
DRAWROOM_2:
  LD A,(HL)               ; Copy the tile graphic data to the screen buffer at
  LD (DE),A               ; 28672
  INC HL                  ;
  INC D                   ;
  DJNZ DRAWROOM_2         ;
  INC IX                  ; Move IX along to the next byte in the attribute
                          ; buffer
  INC C                   ; Have we drawn 256 tiles yet?
  JP NZ,DRAWROOM_1        ; If not, jump back to draw the next one
  RET

; Fill the buffer at 24064 with attribute bytes for the current room
;
; Used by the routine at DRAWROOM. Fills the buffer at 24064 with attribute
; bytes for the background, floor, wall, nasty, conveyor and ramp tiles in the
; current room.
ROOMATTRS:
  LD HL,ROOMLAYOUT        ; Point HL at the first room layout byte at
                          ; ROOMLAYOUT
  LD IX,24064             ; Point IX at the first byte of the attribute buffer
                          ; at 24064
; The following loop copies the attribute bytes for the background, floor, wall
; and nasty tiles into the buffer at 24064.
ROOMATTRS_0:
  LD A,(HL)               ; Pick up a room layout byte
  RLCA                    ; Move bits 6 and 7 into bits 0 and 1
  RLCA                    ;
  CALL ROOMATTR           ; Copy the attribute byte for this tile into the
                          ; buffer at 24064
  LD A,(HL)               ; Pick up the room layout byte again
  RRCA                    ; Move bits 4 and 5 into bits 0 and 1
  RRCA                    ;
  RRCA                    ;
  RRCA                    ;
  CALL ROOMATTR           ; Copy the attribute byte for this tile into the
                          ; buffer at 24064
  LD A,(HL)               ; Pick up the room layout byte again
  RRCA                    ; Move bits 2 and 3 into bits 0 and 1
  RRCA                    ;
  CALL ROOMATTR           ; Copy the attribute byte for this tile into the
                          ; buffer at 24064
  LD A,(HL)               ; Pick up the room layout byte again; this time the
                          ; required bit-pair is already in bits 0 and 1
  CALL ROOMATTR           ; Copy the attribute byte for this tile into the
                          ; buffer at 24064
  INC HL                  ; Point HL at the next room layout byte
  LD A,L                  ; Have we processed all 128 room layout bytes yet?
  AND 128                 ;
  JR Z,ROOMATTRS_0        ; If not, jump back to process the next one
; Next consider the conveyor tiles (if any).
  LD A,(CONVLEN)          ; Pick up the length of the conveyor from CONVLEN
  OR A                    ; Is there a conveyor in the room?
  JR Z,ROOMATTRS_2        ; Jump if not
  LD HL,(CONVLOC)         ; Pick up the address of the conveyor's location in
                          ; the attribute buffer at 24064 from CONVLOC
  LD B,A                  ; B will count the conveyor tiles
  LD A,(CONVEYOR)         ; Pick up the attribute byte for the conveyor tile
                          ; from CONVEYOR
ROOMATTRS_1:
  LD (HL),A               ; Copy the attribute bytes for the conveyor tiles
  INC HL                  ; into the buffer at 24064
  DJNZ ROOMATTRS_1        ;
; And finally consider the ramp tiles (if any).
ROOMATTRS_2:
  LD A,(RAMPLEN)          ; Pick up the length of the ramp from RAMPLEN
  OR A                    ; Is there a ramp in the room?
  RET Z                   ; Return if not
  LD HL,(RAMPLOC)         ; Pick up the address of the ramp's location in the
                          ; attribute buffer at 24064 from RAMPLOC
  LD A,(RAMPDIR)          ; Pick up the ramp direction from RAMPDIR; A=0 (ramp
  AND 1                   ; goes up to the left) or 1 (ramp goes up to the
                          ; right)
  RLCA                    ; Now DE=-33 (ramp goes up to the left) or -31 (ramp
  ADD A,223               ; goes up to the right)
  LD E,A                  ;
  LD D,255                ;
  LD A,(RAMPLEN)          ; Pick up the length of the ramp from RAMPLEN
  LD B,A                  ; B will count the ramp tiles
  LD A,(RAMP)             ; Pick up the attribute byte for the ramp tile from
                          ; RAMP
ROOMATTRS_3:
  LD (HL),A               ; Copy the attribute bytes for the ramp tiles into
  ADD HL,DE               ; the buffer at 24064
  DJNZ ROOMATTRS_3        ;
  RET

; Copy a room attribute byte into the buffer at 24064
;
; Used by the routine at ROOMATTRS. On entry, A holds a room layout byte,
; rotated such that the bit-pair corresponding to the tile of interest is in
; bits 0 and 1.
;
; A Room layout byte (rotated)
; IX Attribute buffer address (24064-24575)
ROOMATTR:
  AND 3                   ; Keep only bits 0 and 1; A=0 (background), 1
                          ; (floor), 2 (wall) or 3 (nasty)
  LD C,A                  ; Multiply by 9 and add 160; now A=160 (background),
  RLCA                    ; 169 (floor), 178 (wall) or 187 (nasty)
  RLCA                    ;
  RLCA                    ;
  ADD A,C                 ;
  ADD A,160               ;
  LD E,A                  ; Point DE at the attribute byte for the background,
  LD D,128                ; floor, wall or nasty tile (see BACKGROUND)
  LD A,(DE)               ; Copy the attribute byte into the buffer at 24064
  LD (IX+0),A             ;
  INC IX                  ; Move IX along to the next byte in the attribute
                          ; buffer
  RET

; Move Willy (1)
;
; Used by the routine at MAINLOOP. This routine deals with Willy if he's
; jumping or falling.
MOVEWILLY:
  LD A,(ROPE)             ; Pick up the rope status indicator from ROPE
  DEC A                   ; Is Willy on a rope?
  BIT 7,A                 ;
  JP Z,MOVEWILLY2         ; Jump if so
  LD A,(AIRBORNE)         ; Pick up the airborne status indicator from AIRBORNE
  CP 1                    ; Is Willy jumping?
  JR NZ,MOVEWILLY_3       ; Jump if not
; Willy is currently jumping.
  LD A,(JUMPING)          ; Pick up the jumping animation counter (0-17) from
                          ; JUMPING
  AND 254                 ; Discard bit 0
  SUB 8                   ; Now -8<=A<=8 (and A is even)
  LD HL,PIXEL_Y           ; Adjust Willy's pixel y-coordinate at PIXEL_Y
  ADD A,(HL)              ; depending on where Willy is in the jump
  LD (HL),A               ;
  CP 240                  ; Is the new value negative (above the top of the
                          ; screen)?
  JP NC,ROOMABOVE         ; If so, move Willy into the room above
  CALL MOVEWILLY_8        ; Adjust Willy's attribute buffer location at
                          ; LOCATION depending on his pixel y-coordinate
  LD A,(WALL)             ; Pick up the attribute byte of the wall tile for the
                          ; current room from WALL
  CP (HL)                 ; Is the top-left cell of Willy's sprite overlapping
                          ; a wall tile?
  JP Z,MOVEWILLY_11       ; Jump if so
  INC HL                  ; Point HL at the top-right cell occupied by Willy's
                          ; sprite
  CP (HL)                 ; Is the top-right cell of Willy's sprite overlapping
                          ; a wall tile?
  JP Z,MOVEWILLY_11       ; Jump if so
  LD A,(JUMPING)          ; Increment the jumping animation counter at JUMPING
  INC A                   ;
  LD (JUMPING),A          ;
  SUB 8                   ; A=J-8, where J (1-18) is the new value of the
                          ; jumping animation counter
  JP P,MOVEWILLY_0        ; Jump if J>=8
  NEG                     ; A=8-J (1<=J<=7, 1<=A<=7)
MOVEWILLY_0:
  INC A                   ; A=1+ABS(J-8)
  RLCA                    ; D=8*(1+ABS(J-8)); this value determines the pitch
  RLCA                    ; of the jumping sound effect (rising as Willy rises,
  RLCA                    ; falling as Willy falls)
  LD D,A                  ;
  LD C,32                 ; This value determines the duration of the jumping
                          ; sound effect
  LD A,(BORDER)           ; Pick up the border colour for the current room from
                          ; BORDER
MOVEWILLY_1:
  OUT (254),A             ; Make a jumping sound effect
  XOR 24                  ;
  LD B,D                  ;
MOVEWILLY_2:
  DJNZ MOVEWILLY_2        ;
  DEC C                   ;
  JR NZ,MOVEWILLY_1       ;
  LD A,(JUMPING)          ; Pick up the jumping animation counter (1-18) from
                          ; JUMPING
  CP 18                   ; Has Willy reached the end of the jump?
  JP Z,MOVEWILLY_9        ; Jump if so
  CP 16                   ; Is the jumping animation counter now 16?
  JR Z,MOVEWILLY_3        ; Jump if so
  CP 13                   ; Is the jumping animation counter now 13?
  JP NZ,MOVEWILLY3        ; Jump if not
; If we get here, then Willy is standing on the floor or a ramp, or he's
; falling, or his jumping animation counter is 13 (at which point Willy is on
; his way down and is exactly two cell-heights above where he started the jump)
; or 16 (at which point Willy is on his way down and is exactly one cell-height
; above where he started the jump).
MOVEWILLY_3:
  LD A,(PIXEL_Y)          ; Pick up Willy's pixel y-coordinate from PIXEL_Y
  AND 14                  ; Is Willy either on a ramp, or occupying only four
                          ; cells?
  JR NZ,MOVEWILLY_4       ; Jump if not
  LD HL,(LOCATION)        ; Pick up Willy's attribute buffer coordinates from
                          ; LOCATION
  LD DE,64                ; Point HL at the left-hand cell below Willy's sprite
  ADD HL,DE               ;
  BIT 1,H                 ; Is this location below the floor of the current
                          ; room?
  JP NZ,ROOMBELOW         ; If so, move Willy into the room below
  LD A,(NASTY)            ; Pick up the attribute byte of the nasty tile for
                          ; the current room from NASTY
  CP (HL)                 ; Does the left-hand cell below Willy's sprite
                          ; contain a nasty?
  JR Z,MOVEWILLY_4        ; Jump if so
  INC HL                  ; Point HL at the right-hand cell below Willy's
                          ; sprite
  LD A,(NASTY)            ; Pick up the attribute byte of the nasty tile for
                          ; the current room from NASTY (again, redundantly)
  CP (HL)                 ; Does the right-hand cell below Willy's sprite
                          ; contain a nasty?
  JR Z,MOVEWILLY_4        ; Jump if so
  LD A,(BACKGROUND)       ; Pick up the attribute byte of the background tile
                          ; for the current room from BACKGROUND
  CP (HL)                 ; Set the zero flag if the right-hand cell below
                          ; Willy's sprite is empty
  DEC HL                  ; Point HL at the left-hand cell below Willy's sprite
  JP NZ,MOVEWILLY2        ; Jump if the right-hand cell below Willy's sprite is
                          ; not empty
  CP (HL)                 ; Is the left-hand cell below Willy's sprite empty?
  JP NZ,MOVEWILLY2        ; Jump if not
MOVEWILLY_4:
  LD A,(AIRBORNE)         ; Pick up the airborne status indicator from AIRBORNE
  CP 1                    ; Is Willy jumping?
  JP Z,MOVEWILLY3         ; Jump if so
; If we get here, then Willy is either in the process of falling or just about
; to start falling.
  LD HL,DMFLAGS           ; Reset bit 1 at DMFLAGS: Willy is not moving left or
  RES 1,(HL)              ; right
  LD A,(AIRBORNE)         ; Pick up the airborne status indicator from AIRBORNE
  OR A                    ; Is Willy already falling?
  JP Z,MOVEWILLY_10       ; Jump if not
  INC A                   ; Increment the airborne status indicator
  CP 16                   ; Is it 16 now?
  JR NZ,MOVEWILLY_5       ; Jump if not
  LD A,12                 ; Decrease the airborne status indicator from 16 to
                          ; 12
MOVEWILLY_5:
  LD (AIRBORNE),A         ; Update the airborne status indicator at AIRBORNE
  RLCA                    ; D=16*A; this value determines the pitch of the
  RLCA                    ; falling sound effect
  RLCA                    ;
  RLCA                    ;
  LD D,A                  ;
  LD C,32                 ; This value determines the duration of the falling
                          ; sound effect
  LD A,(BORDER)           ; Pick up the border colour for the current room from
                          ; BORDER
MOVEWILLY_6:
  OUT (254),A             ; Make a falling sound effect
  XOR 24                  ;
  LD B,D                  ;
MOVEWILLY_7:
  DJNZ MOVEWILLY_7        ;
  DEC C                   ;
  JR NZ,MOVEWILLY_6       ;
  LD A,(PIXEL_Y)          ; Add 8 to Willy's pixel y-coordinate at PIXEL_Y;
  ADD A,8                 ; this moves Willy downwards by 4 pixels
  LD (PIXEL_Y),A          ;
; This entry point is used by the routine at DRAWTHINGS to update Willy's
; attribute buffer location when he's on a rope.
MOVEWILLY_8:
  AND 240                 ; L=16*Y, where Y is Willy's screen y-coordinate
  LD L,A                  ; (0-14)
  XOR A                   ; Clear A and the carry flag
  RL L                    ; Now L=32*(Y-8*INT(Y/8)), and the carry flag is set
                          ; if Willy is in the lower half of the room (Y>=8)
  ADC A,92                ; H=92 or 93 (MSB of the address of Willy's location
  LD H,A                  ; in the attribute buffer)
  LD A,(LOCATION)         ; Pick up Willy's screen x-coordinate (0-30) from
  AND 31                  ; bits 0-4 at LOCATION
  OR L                    ; Now L holds the LSB of Willy's attribute buffer
  LD L,A                  ; address
  LD (LOCATION),HL        ; Store Willy's updated attribute buffer location at
                          ; LOCATION
  RET
; Willy has just finished a jump.
MOVEWILLY_9:
  LD A,6                  ; Set the airborne status indicator at AIRBORNE to 6:
  LD (AIRBORNE),A         ; Willy will continue to fall unless he's landed on a
                          ; wall or floor block
  RET
; Willy has just started falling.
MOVEWILLY_10:
  LD A,2                  ; Set the airborne status indicator at AIRBORNE to 2
  LD (AIRBORNE),A         ;
  RET
; The top-left or top-right cell of Willy's sprite is overlapping a wall tile.
MOVEWILLY_11:
  LD A,(PIXEL_Y)          ; Adjust Willy's pixel y-coordinate at PIXEL_Y so
  ADD A,16                ; that the top row of cells of his sprite is just
  AND 240                 ; below the wall tile
  LD (PIXEL_Y),A          ;
  CALL MOVEWILLY_8        ; Adjust Willy's attribute buffer location at
                          ; LOCATION to account for this new pixel y-coordinate
  LD A,2                  ; Set the airborne status indicator at AIRBORNE to 2:
  LD (AIRBORNE),A         ; Willy has started falling
  LD HL,DMFLAGS           ; Reset bit 1 at DMFLAGS: Willy is not moving left or
  RES 1,(HL)              ; right
  RET

; Move Willy (2)
;
; Used by the routine at MOVEWILLY. This routine checks the keyboard and
; joystick.
;
; HL Attribute buffer address of the left-hand cell below Willy's sprite (if
;    Willy is not on a rope)
MOVEWILLY2:
  LD E,255                ; Initialise E to 255 (all bits set); it will be used
                          ; to hold keyboard and joystick readings
  LD A,(ROPE)             ; Pick up the rope status indicator from ROPE
  DEC A                   ; Is Willy on a rope?
  BIT 7,A                 ;
  JR Z,MOVEWILLY2_1       ; Jump if so
  LD A,(AIRBORNE)         ; Pick up the airborne status indicator from AIRBORNE
  CP 12                   ; Has Willy just landed after falling from too great
                          ; a height?
  JP NC,KILLWILLY_0       ; If so, kill him
  XOR A                   ; Reset the airborne status indicator at AIRBORNE
  LD (AIRBORNE),A         ; (Willy has landed safely)
  LD A,(CONVEYOR)         ; Pick up the attribute byte of the conveyor tile for
                          ; the current room from CONVEYOR
  CP (HL)                 ; Does the attribute byte of the left-hand cell below
                          ; Willy's sprite match that of the conveyor tile?
  JR Z,MOVEWILLY2_0       ; Jump if so
  INC HL                  ; Point HL at the right-hand cell below Willy's
                          ; sprite
  CP (HL)                 ; Does the attribute byte of the right-hand cell
                          ; below Willy's sprite match that of the conveyor
                          ; tile?
  JR NZ,MOVEWILLY2_1      ; Jump if not
MOVEWILLY2_0:
  LD A,(CONVDIR)          ; Pick up the direction byte of the conveyor
                          ; definition from CONVDIR (0=left, 1=right)
  SUB 3                   ; Now E=253 (bit 1 reset) if the conveyor is moving
  LD E,A                  ; left, or 254 (bit 0 reset) if it's moving right
MOVEWILLY2_1:
  LD BC,57342             ; Read keys P-O-I-U-Y (right, left, right, left,
  IN A,(C)                ; right) into bits 0-4 of A
  AND 31                  ; Set bit 5 and reset bits 6 and 7
  OR 32                   ;
  AND E                   ; Reset bit 0 if the conveyor is moving right, or bit
                          ; 1 if it's moving left
  LD E,A                  ; Save the result in E
  LD A,(MODE)             ; Pick up the game mode indicator (0, 1 or 2) from
                          ; MODE
  AND 2                   ; Now A=1 if Willy is running to the toilet, 0
  RRCA                    ; otherwise
  XOR E                   ; Flip bit 0 of E if Willy is running to the toilet,
  LD E,A                  ; forcing him to move right (unless he's jumped onto
                          ; the bed, in which case bit 0 of E is now set,
                          ; meaning that the conveyor does not move him, and
                          ; the 'P' key has no effect; this is a bug)
  LD BC,64510             ; Read keys Q-W-E-R-T (left, right, left, right,
  IN A,(C)                ; left) into bits 0-4 of A
  AND 31                  ; Keep only bits 0-4, shift them into bits 1-5, and
  RLC A                   ; set bit 0
  OR 1                    ;
  AND E                   ; Merge this keyboard reading into bits 1-5 of E
  LD E,A                  ;
  LD B,231                ; Read keys 1-2-3-4-5 ('5' is left) and 0-9-8-7-6
  IN A,(C)                ; (jump, nothing, right, right, left) into bits 0-4
                          ; of A
  RRCA                    ; Rotate the result right and set bits 0-2 and 4-7;
  OR 247                  ; this ignores every key except '5' and '6' (left)
  AND E                   ; Merge this reading of the '5' and '6' keys into bit
  LD E,A                  ; 3 of E
  LD B,239                ; Read keys 0-9-8-7-6 (jump, nothing, right, right,
  IN A,(C)                ; left) into bits 0-4 of A
  OR 251                  ; Set bits 0, 1 and 3-7; this ignores every key
                          ; except '8' (right)
  AND E                   ; Merge this reading of the '8' key into bit 2 of E
  LD E,A                  ;
  IN A,(C)                ; Read keys 0-9-8-7-6 (jump, nothing, right, right,
                          ; left) into bits 0-4 of A
  RRCA                    ; Rotate the result right and set bits 0, 1 and 3-7;
  OR 251                  ; this ignores every key except '7' (right)
  AND E                   ; Merge this reading of the '7' key into bit 2 of E
  LD E,A                  ;
  LD A,(JOYSTICK)         ; Collect the Kempston joystick indicator from
                          ; JOYSTICK
  OR A                    ; Is the joystick connected?
  JR Z,MOVEWILLY2_2       ; Jump if not
  LD BC,31                ; Collect input from the joystick
  IN A,(C)                ;
  AND 3                   ; Keep only bits 0 (right) and 1 (left) and flip them
  CPL                     ;
  AND E                   ; Merge this reading of the joystick right and left
  LD E,A                  ; buttons into bits 0 and 1 of E
; At this point, bits 0-5 in E indicate the direction in which Willy is being
; moved or trying to move. If bit 0, 2 or 4 is reset, Willy is being moved or
; trying to move right; if bit 1, 3 or 5 is reset, Willy is being moved or
; trying to move left.
MOVEWILLY2_2:
  LD C,0                  ; Initialise C to 0 (no movement)
  LD A,E                  ; Copy the movement bits into A
  AND 42                  ; Keep only bits 1, 3 and 5 (the 'left' bits)
  CP 42                   ; Are any of these bits reset?
  JR Z,MOVEWILLY2_3       ; Jump if not
  LD C,4                  ; Set bit 2 of C: Willy is moving left
  XOR A                   ; Reset the inactivity timer at INACTIVE
  LD (INACTIVE),A         ;
MOVEWILLY2_3:
  LD A,E                  ; Copy the movement bits into A
  AND 21                  ; Keep only bits 0, 2 and 4 (the 'right' bits)
  CP 21                   ; Are any of these bits reset?
  JR Z,MOVEWILLY2_4       ; Jump if not
  SET 3,C                 ; Set bit 3 of C: Willy is moving right
  XOR A                   ; Reset the inactivity timer at INACTIVE
  LD (INACTIVE),A         ;
MOVEWILLY2_4:
  LD A,(DMFLAGS)          ; Pick up Willy's direction and movement flags from
                          ; DMFLAGS
  ADD A,C                 ; Point HL at the entry in the left-right movement
  LD C,A                  ; table at LRMOVEMENT that corresponds to the
  LD B,0                  ; direction Willy is facing, and the direction in
  LD HL,LRMOVEMENT        ; which he is being moved or trying to move
  ADD HL,BC               ;
  LD A,(HL)               ; Update Willy's direction and movement flags at
  LD (DMFLAGS),A          ; DMFLAGS with the entry from the left-right movement
                          ; table
; That is left-right movement taken care of. Now check the jump keys.
  LD BC,32510             ; Read keys SHIFT-Z-X-C-V and B-N-M-SS-SPACE
  IN A,(C)                ;
  AND 31                  ; Are any of these keys being pressed?
  CP 31                   ;
  JR NZ,MOVEWILLY2_5      ; Jump if so
  LD B,239                ; Read keys 6-7-8-9-0
  IN A,(C)                ;
  BIT 0,A                 ; Is '0' being pressed?
  JR Z,MOVEWILLY2_5       ; Jump if so
  LD A,(JOYSTICK)         ; Collect the Kempston joystick indicator from
                          ; JOYSTICK
  OR A                    ; Is the joystick connected?
  JR Z,MOVEWILLY3         ; Jump if not
  LD BC,31                ; Collect input from the joystick
  IN A,(C)                ;
  BIT 4,A                 ; Is the fire button being pressed?
  JR Z,MOVEWILLY3         ; Jump if not
; A jump key or the fire button is being pressed. Time to make Willy jump.
MOVEWILLY2_5:
  LD A,(MODE)             ; Pick up the game mode indicator from MODE
  BIT 1,A                 ; Is Willy running to the toilet?
  JR NZ,MOVEWILLY3        ; Jump if so
  XOR A                   ; Initialise the jumping animation counter at JUMPING
  LD (JUMPING),A          ; to 0
  LD (INACTIVE),A         ; Reset the inactivity timer at INACTIVE
  INC A                   ; Set the airborne status indicator at AIRBORNE to 1:
  LD (AIRBORNE),A         ; Willy is jumping
  LD A,(ROPE)             ; Pick up the rope status indicator from ROPE
  DEC A                   ; Is Willy on a rope?
  BIT 7,A                 ;
  JR NZ,MOVEWILLY3        ; Jump if not
  LD A,240                ; Set the rope status indicator at ROPE to 240
  LD (ROPE),A             ;
  LD A,(PIXEL_Y)          ; Round down Willy's pixel y-coordinate at PIXEL_Y to
  AND 240                 ; the nearest multiple of 16; this might move him
  LD (PIXEL_Y),A          ; upwards a little, but ensures that his actual pixel
                          ; y-coordinate is a multiple of 8 (making his sprite
                          ; cell-aligned) before he begins the jump off the
                          ; rope
  LD HL,DMFLAGS           ; Set bit 1 at DMFLAGS: during this jump off the
  SET 1,(HL)              ; rope, Willy will move in the direction he's facing
  RET

; Move Willy (3)
;
; Used by the routines at MOVEWILLY and MOVEWILLY2. This routine moves Willy
; left or right if necessary.
MOVEWILLY3:
  LD A,(DMFLAGS)          ; Pick up Willy's direction and movement flags from
                          ; DMFLAGS
  AND 2                   ; Is Willy moving left or right?
  RET Z                   ; Return if not
  LD A,(ROPE)             ; Pick up the rope status indicator from ROPE
  DEC A                   ; Is Willy on a rope?
  BIT 7,A                 ;
  RET Z                   ; Return if so (Willy's movement along a rope is
                          ; handled at DRAWTHINGS_19)
  LD A,(DMFLAGS)          ; Pick up Willy's direction and movement flags from
                          ; DMFLAGS
  AND 1                   ; Is Willy facing right?
  JP Z,MOVEWILLY3_3       ; Jump if so
; Willy is moving left.
  LD A,(FRAME)            ; Pick up Willy's animation frame from FRAME
  OR A                    ; Is it 0?
  JR Z,MOVEWILLY3_0       ; If so, jump to move Willy's sprite left across a
                          ; cell boundary
  DEC A                   ; Decrement Willy's animation frame at FRAME
  LD (FRAME),A            ;
  RET
; Willy's sprite is moving left across a cell boundary. In the comments that
; follow, (x,y) refers to the coordinates of the top-left cell currently
; occupied by Willy's sprite.
MOVEWILLY3_0:
  LD A,(AIRBORNE)         ; Pick up the airborne status indicator from AIRBORNE
  LD BC,0                 ; Prepare BC for later addition
  CP 0                    ; Is Willy jumping?
  JR NZ,MOVEWILLY3_1      ; Jump if so
  LD HL,(LOCATION)        ; Collect Willy's attribute buffer coordinates from
                          ; LOCATION
  LD BC,0                 ; Prepare BC for later addition (again, redundantly)
  LD A,(RAMPDIR)          ; Pick up the direction byte of the ramp definition
                          ; for the current room from RAMPDIR
  DEC A                   ; Now A=31 if the ramp goes up to the left, or 65 if
  OR 161                  ; it goes up to the right
  XOR 224                 ;
  LD E,A                  ; Point HL at the cell at (x-1,y+1) if the ramp goes
  LD D,0                  ; up to the left, or at the cell at (x+1,y+2) if the
  ADD HL,DE               ; ramp goes up to the right
  LD A,(RAMP)             ; Pick up the attribute byte of the ramp tile for the
                          ; current room from RAMP
  CP (HL)                 ; Is there a ramp tile in the cell pointed to by HL?
  JR NZ,MOVEWILLY3_1      ; Jump if not
  LD BC,32                ; Prepare BC for later addition
  LD A,(RAMPDIR)          ; Pick up the direction byte of the ramp definition
                          ; for the current room from RAMPDIR
  OR A                    ; Does the ramp go up to the right?
  JR NZ,MOVEWILLY3_1      ; Jump if so
  LD BC,65504             ; BC=-32 (the ramp goes up to the left)
MOVEWILLY3_1:
  LD HL,(LOCATION)        ; Collect Willy's attribute buffer coordinates from
                          ; LOCATION
  LD A,L                  ; Is Willy's screen x-coordinate 0 (on the far left)?
  AND 31                  ;
  JP Z,ROOMLEFT           ; If so, move Willy into the room to the left
  ADD HL,BC               ; Point HL at the cell at (x-1,y+1), or at the cell
  DEC HL                  ; at (x-1,y) if Willy is on or about to step onto a
  LD DE,32                ; ramp that goes up to the left, or at the cell at
  ADD HL,DE               ; (x-1,y+2) if Willy is walking down a ramp
  LD A,(WALL)             ; Pick up the attribute byte of the wall tile for the
                          ; current room from WALL
  CP (HL)                 ; Is there a wall tile in the cell pointed to by HL?
  RET Z                   ; Return if so without moving Willy (his path is
                          ; blocked)
  LD A,(PIXEL_Y)          ; Pick up Willy's pixel y-coordinate (Y) from PIXEL_Y
  SRA C                   ; Now B=Y (if Willy is neither on nor about to step
  ADD A,C                 ; onto a ramp), or Y+16 (if Willy is walking down a
  LD B,A                  ; ramp), or Y-16 (if Willy is on or about to step
                          ; onto a ramp that goes up to the left); this will be
                          ; Willy's new pixel y-coordinate
  AND 15                  ; Is Willy at a point in a jump (left) where his
                          ; sprite occupies three rows of cells?
  JR Z,MOVEWILLY3_2       ; Jump if not (Willy's sprite is cell-aligned)
  LD A,(WALL)             ; Pick up the attribute byte of the wall tile for the
                          ; current room from WALL
  ADD HL,DE               ; Point HL at the cell at (x-1,y+2)
  CP (HL)                 ; Is there a wall tile there?
  RET Z                   ; Return if so without moving Willy (his path is
                          ; blocked)
  OR A                    ; Point HL at the cell at (x-1,y+1)
  SBC HL,DE               ;
MOVEWILLY3_2:
  OR A                    ; Point HL at the cell at (x-1,y), or at the cell at
  SBC HL,DE               ; (x-1,y-1) if Willy is on or about to step onto a
                          ; ramp that goes up to the left, or at the cell at
                          ; (x-1,y+1) if Willy is walking down a ramp
  LD (LOCATION),HL        ; Save Willy's new attribute buffer coordinates (in
                          ; HL) at LOCATION
  LD A,B                  ; Save Willy's new pixel y-coordinate at PIXEL_Y
  LD (PIXEL_Y),A          ;
  LD A,3                  ; Change Willy's animation frame at FRAME from 0 to 3
  LD (FRAME),A            ;
  RET
; Willy is moving right.
MOVEWILLY3_3:
  LD A,(FRAME)            ; Pick up Willy's animation frame from FRAME
  CP 3                    ; Is it 3?
  JR Z,MOVEWILLY3_4       ; If so, jump to move Willy's sprite right across a
                          ; cell boundary
  INC A                   ; Increment Willy's animation frame at FRAME
  LD (FRAME),A            ;
  RET
; Willy's sprite is moving right across a cell boundary. In the comments that
; follow, (x,y) refers to the coordinates of the top-left cell currently
; occupied by Willy's sprite.
MOVEWILLY3_4:
  LD A,(AIRBORNE)         ; Pick up the airborne status indicator from AIRBORNE
  LD BC,0                 ; Prepare BC for later addition
  OR A                    ; Is Willy jumping?
  JR NZ,MOVEWILLY3_5      ; Jump if so
  LD HL,(LOCATION)        ; Collect Willy's attribute buffer coordinates from
                          ; LOCATION
  LD A,(RAMPDIR)          ; Pick up the direction byte of the ramp definition
                          ; for the current room from RAMPDIR
  DEC A                   ; Now A=64 if the ramp goes up to the left, or 34 if
  OR 157                  ; it goes up to the right
  XOR 191                 ;
  LD E,A                  ; Point HL at the cell at (x,y+2) if the ramp goes up
  LD D,0                  ; to the left, or at the cell at (x+2,y+1) if the
  ADD HL,DE               ; ramp goes up to the right
  LD A,(RAMP)             ; Pick up the attribute byte of the ramp tile for the
                          ; current room from RAMP
  CP (HL)                 ; Is there a ramp tile in the cell pointed to by HL?
  JR NZ,MOVEWILLY3_5      ; Jump if not
  LD BC,32                ; Prepare BC for later addition
  LD A,(RAMPDIR)          ; Pick up the direction byte of the ramp definition
                          ; for the current room from RAMPDIR
  OR A                    ; Does the ramp go up to the left?
  JR Z,MOVEWILLY3_5       ; Jump if so
  LD BC,65504             ; BC=-32 (the ramp goes up to the right)
MOVEWILLY3_5:
  LD HL,(LOCATION)        ; Collect Willy's attribute buffer coordinates from
                          ; LOCATION
  ADD HL,BC               ; Point HL at the cell at (x+2,y), or at the cell at
  INC HL                  ; (x+2,y+1) if Willy is walking down a ramp, or at
  INC HL                  ; the cell at (x+2,y-1) if Willy is on or about to
                          ; step onto a ramp that goes up to the right
  LD A,L                  ; Is Willy's screen x-coordinate 30 (on the far
  AND 31                  ; right)?
  JP Z,ROOMRIGHT          ; If so, move Willy into the room on the right
  LD DE,32                ; Prepare DE for addition
  LD A,(WALL)             ; Pick up the attribute byte of the wall tile for the
                          ; current room from WALL
  ADD HL,DE               ; Point HL at the cell at (x+2,y+1), or at the cell
                          ; at (x+2,y+2) if Willy is walking down a ramp, or at
                          ; the cell at (x+2,y) if Willy is on or about to step
                          ; onto a ramp that goes up to the right
  CP (HL)                 ; Is there a wall tile in the cell pointed to by HL?
  RET Z                   ; Return if so without moving Willy (his path is
                          ; blocked)
  LD A,(PIXEL_Y)          ; Pick up Willy's pixel y-coordinate (Y) from PIXEL_Y
  SRA C                   ; Now B=Y (if Willy is neither on nor about to step
  ADD A,C                 ; onto a ramp), or Y+16 (if Willy is walking down a
  LD B,A                  ; ramp), or Y-16 (if Willy is on or about to step
                          ; onto a ramp that goes up to the right); this will
                          ; be Willy's new pixel y-coordinate
  AND 15                  ; Is Willy at a point in a jump (right) where his
                          ; sprite occupies three rows of cells?
  JR Z,MOVEWILLY3_6       ; Jump if not (Willy's sprite is cell-aligned)
  LD A,(WALL)             ; Pick up the attribute byte of the wall tile for the
                          ; current room from WALL
  ADD HL,DE               ; Point HL at the cell at (x+2,y+2)
  CP (HL)                 ; Is there a wall tile there?
  RET Z                   ; Return if so without moving Willy (his path is
                          ; blocked)
  OR A                    ; Point HL at the cell at (x+2,y+1)
  SBC HL,DE               ;
MOVEWILLY3_6:
  LD A,(WALL)             ; Pick up the attribute byte of the wall tile for the
                          ; current room from WALL
  OR A                    ; Point HL at the cell at (x+2,y), or at the cell at
  SBC HL,DE               ; (x+2,y+1) if Willy is walking down a ramp, or at
                          ; the cell at (x+2,y-1) if Willy is on or about to
                          ; step onto a ramp that goes up to the right
  CP (HL)                 ; Is there a wall tile in the cell pointed to by HL?
  RET Z                   ; Return if so without moving Willy (his path is
                          ; blocked)
  DEC HL                  ; Point HL at the cell at (x+1,y), or at the cell at
                          ; (x+1,y+1) if Willy is walking down a ramp, or at
                          ; the cell at (x+1,y-1) if Willy is on or about to
                          ; step onto a ramp that goes up to the right
  LD (LOCATION),HL        ; Save Willy's new attribute buffer coordinates (in
                          ; HL) at LOCATION
  XOR A                   ; Change Willy's animation frame at FRAME from 3 to 0
  LD (FRAME),A            ;
  LD A,B                  ; Save Willy's new pixel y-coordinate at PIXEL_Y
  LD (PIXEL_Y),A          ;
  RET

; Kill Willy
;
; Used by the routine at WILLYATTR when Willy hits a nasty.
KILLWILLY:
  POP HL                  ; Drop the return address from the stack
; This entry point is used by the routines at MOVEWILLY2 (when Willy lands
; after falling from too great a height), DRAWTHINGS (when an arrow or guardian
; hits Willy) and BEDANDBATH (when Willy gets too close to Maria).
KILLWILLY_0:
  POP HL                  ; Drop the return address from the stack
  LD A,255                ; Set the airborne status indicator at AIRBORNE to
  LD (AIRBORNE),A         ; 255 (meaning Willy has had a fatal accident)
  JP MAINLOOP_0           ; Jump back into the main loop

; Move the rope and guardians in the current room
;
; Used by the routine at MAINLOOP.
MOVETHINGS:
  LD IX,ENTITYBUF         ; Point IX at the first byte of the first entity
                          ; buffer at ENTITYBUF
; The entity-moving loop begins here.
MOVETHINGS_0:
  LD A,(IX+0)             ; Pick up the first byte of the current entity's
                          ; buffer
  CP 255                  ; Have we already dealt with every entity?
  RET Z                   ; Return if so
  AND 3                   ; Keep only bits 0 and 1 (which determine the type of
                          ; entity)
  JP Z,MOVETHINGS_13      ; Jump to consider the next entity buffer if this one
                          ; belongs to an arrow or is unused
  CP 1                    ; Is this a horizontal guardian?
  JP Z,MOVETHINGS_5       ; Jump if so
  CP 2                    ; Is this a vertical guardian?
  JP Z,MOVETHINGS_9       ; Jump if so
; We are dealing with a rope.
  BIT 7,(IX+0)            ; Is the rope currently swinging right to left?
  JR Z,MOVETHINGS_2       ; Jump if so
; The rope is swinging left to right.
  LD A,(IX+1)             ; Pick up the animation frame index
  BIT 7,A                 ; Is the rope currently swinging away from the
                          ; centre?
  JR Z,MOVETHINGS_1       ; Jump if so
; The rope is swinging left to right, towards the centre (132<=A<=182).
  SUB 2                   ; Subtract 2 from the animation frame index in A
  CP 148                  ; Is it still 148 or greater?
  JR NC,MOVETHINGS_4      ; If so, use it as the next animation frame index
  SUB 2                   ; Subtract 2 from the animation frame index again
  CP 128                  ; Is it 128 now?
  JR NZ,MOVETHINGS_4      ; If not, use it as the next animation frame index
  XOR A                   ; The rope has reached the centre, so the next
                          ; animation frame index is 0
  JR MOVETHINGS_4         ; Jump to set it
; The rope is swinging left to right, away from the centre (0<=A<=52).
MOVETHINGS_1:
  ADD A,2                 ; Add 2 to the animation frame index in A
  CP 18                   ; Is it now 18 or greater?
  JR NC,MOVETHINGS_4      ; If so, use it as the next animation frame index
  ADD A,2                 ; Add 2 to the animation frame index again
  JR MOVETHINGS_4         ; Use this value as the next animation frame index
; The rope is swinging right to left.
MOVETHINGS_2:
  LD A,(IX+1)             ; Pick up the animation frame index
  BIT 7,A                 ; Is the rope currently swinging away from the
                          ; centre?
  JR NZ,MOVETHINGS_3      ; Jump if so
; The rope is swinging right to left, towards the centre (4<=A<=54).
  SUB 2                   ; Subtract 2 from the animation frame index in A
  CP 20                   ; Is it still 20 or greater?
  JR NC,MOVETHINGS_4      ; If so, use it as the next animation frame index
  SUB 2                   ; Subtract 2 from the animation frame index again
  OR A                    ; Is it 0 now?
  JR NZ,MOVETHINGS_4      ; If not, use it as the next animation frame index
  LD A,128                ; The rope has reached the centre, so the next
                          ; animation frame index is 128
  JR MOVETHINGS_4         ; Jump to set it
; The rope is swinging right to left, away from the centre (128<=A<=180).
MOVETHINGS_3:
  ADD A,2                 ; Add 2 to the animation frame index in A
  CP 146                  ; Is it now 146 or greater?
  JR NC,MOVETHINGS_4      ; If so, use it as the next animation frame index
  ADD A,2                 ; Add 2 to the animation frame index again
; Now A holds the rope's next animation frame index.
MOVETHINGS_4:
  LD (IX+1),A             ; Update the animation frame index
  AND 127                 ; Reset bit 7
  CP (IX+7)               ; Does A match the eighth byte of the rope's buffer
                          ; (54)?
  JP NZ,MOVETHINGS_13     ; If not, jump to consider the next entity
  LD A,(IX+0)             ; Flip bit 7 of the first byte of the rope's buffer:
  XOR 128                 ; the rope has just changed direction and will now
  LD (IX+0),A             ; swing back towards the centre
  JP MOVETHINGS_13        ; Jump to consider the next entity
; We are dealing with a horizontal guardian.
MOVETHINGS_5:
  BIT 7,(IX+0)            ; Is the guardian currently moving left to right?
  JR NZ,MOVETHINGS_7      ; Jump if so
; This guardian is moving right to left.
  LD A,(IX+0)             ; Update the guardian's animation frame (in bits 5
  SUB 32                  ; and 6 of the first byte of its buffer)
  AND 127                 ;
  LD (IX+0),A             ;
  CP 96                   ; Is it time to update the x-coordinate of the
                          ; guardian sprite?
  JR C,MOVETHINGS_13      ; If not, jump to consider the next entity
  LD A,(IX+2)             ; Pick up the sprite's current screen x-coordinate
  AND 31                  ; (0-31)
  CP (IX+6)               ; Has the guardian reached the leftmost point of its
                          ; path?
  JR Z,MOVETHINGS_6       ; Jump if so
  DEC (IX+2)              ; Decrement the sprite's x-coordinate
  JR MOVETHINGS_13        ; Jump to consider the next entity
MOVETHINGS_6:
  LD (IX+0),129           ; The guardian will now start moving left to right
  JR MOVETHINGS_13        ; Jump to consider the next entity
; This guardian is moving left to right.
MOVETHINGS_7:
  LD A,(IX+0)             ; Update the guardian's animation frame (in bits 5
  ADD A,32                ; and 6 of the first byte of its buffer)
  OR 128                  ;
  LD (IX+0),A             ;
  CP 160                  ; Is it time to update the x-coordinate of the
                          ; guardian sprite?
  JR NC,MOVETHINGS_13     ; If not, jump to consider the next entity
  LD A,(IX+2)             ; Pick up the sprite's current screen x-coordinate
  AND 31                  ; (0-31)
  CP (IX+7)               ; Has the guardian reached the rightmost point of its
                          ; path?
  JR Z,MOVETHINGS_8       ; Jump if so
  INC (IX+2)              ; Increment the sprite's x-coordinate
  JR MOVETHINGS_13        ; Jump to consider the next entity
MOVETHINGS_8:
  LD (IX+0),97            ; The guardian will now start moving right to left
  JR MOVETHINGS_13        ; Jump to consider the next entity
; We are dealing with a vertical guardian.
MOVETHINGS_9:
  LD A,(IX+0)             ; Flip bit 3 of the first byte of the guardian's
  XOR 8                   ; buffer (if bit 4 is set, the guardian's animation
  LD (IX+0),A             ; frame is updated on every pass through this
                          ; routine; otherwise, it is updated on every second
                          ; pass when bit 3 is set)
  AND 24                  ; Are bits 3 and 4 both reset now?
  JR Z,MOVETHINGS_10      ; Jump if so
  LD A,(IX+0)             ; Update the guardian's animation frame (in bits 5-7
  ADD A,32                ; of the first byte of its buffer)
  LD (IX+0),A             ;
MOVETHINGS_10:
  LD A,(IX+3)             ; Update the guardian's y-coordinate
  ADD A,(IX+4)            ;
  LD (IX+3),A             ;
  CP (IX+7)               ; Has the guardian reached the lowest point of its
                          ; path (maximum y-coordinate)?
  JR NC,MOVETHINGS_12     ; If so, jump to change its direction of movement
  CP (IX+6)               ; Compare the new y-coordinate with the minimum value
                          ; (the highest point of its path)
  JR Z,MOVETHINGS_11      ; If they match, jump to change the guardian's
                          ; direction of movement
  JR NC,MOVETHINGS_13     ; If the new y-coordinate is above the minimum value,
                          ; jump to consider the next entity
MOVETHINGS_11:
  LD A,(IX+6)             ; Make sure that the guardian's y-coordinate is set
  LD (IX+3),A             ; to its minimum value
MOVETHINGS_12:
  LD A,(IX+4)             ; Negate the y-coordinate increment; this changes the
  NEG                     ; guardian's direction of movement
  LD (IX+4),A             ;
; The current entity has been dealt with. Time for the next one.
MOVETHINGS_13:
  LD DE,8                 ; Point IX at the first byte of the next entity's
  ADD IX,DE               ; buffer
  JP MOVETHINGS_0         ; Jump back to deal with it

; Draw the rope, arrows and guardians in the current room
;
; Used by the routine at MAINLOOP. Draws the rope, arrows and guardians in the
; current room to the screen buffer at 24576.
DRAWTHINGS:
  LD IX,ENTITYBUF         ; Point IX at the first byte of the first entity
                          ; buffer at ENTITYBUF
; The drawing loop begins here.
DRAWTHINGS_0:
  LD A,(IX+0)             ; Pick up the first byte of the current entity's
                          ; buffer
  CP 255                  ; Have we already dealt with every entity?
  RET Z                   ; Return if so
  AND 7                   ; Keep only bits 0-2 (which determine the type of
                          ; entity)
  JP Z,DRAWTHINGS_22      ; Jump to consider the next entity buffer if this one
                          ; is not being used
  CP 3                    ; Is this a rope?
  JP Z,DRAWTHINGS_9       ; Jump if so
  CP 4                    ; Is this an arrow?
  JR Z,DRAWTHINGS_2       ; Jump if so
; We are dealing with a horizontal or vertical guardian.
  LD E,(IX+3)             ; Point DE at the entry in the screen buffer address
  LD D,130                ; lookup table at SBUFADDRS that corresponds to the
                          ; guardian's y-coordinate
  LD A,(DE)               ; Copy the LSB of the screen buffer address to L
  LD L,A                  ;
  LD A,(IX+2)             ; Pick up the guardian's x-coordinate from bits 0-4
  AND 31                  ; of the third byte of its buffer
  ADD A,L                 ; Adjust the LSB of the screen buffer address in L
  LD L,A                  ; for the guardian's x-coordinate
  LD A,E                  ; Copy the fourth byte of the guardian's buffer to A
  RLCA                    ; H=92 or 93; now HL holds the address of the
  AND 1                   ; guardian's current location in the attribute buffer
  OR 92                   ; at 23552
  LD H,A                  ;
  LD DE,31                ; Prepare DE for later addition
  LD A,(IX+1)             ; Pick up the second byte of the guardian's buffer
  AND 15                  ; Keep only bits 0-2 (INK colour) and 3 (BRIGHT
                          ; value)
  ADD A,56                ; Push bit 3 up to bit 6
  AND 71                  ; Keep only bits 0-2 (INK colour) and 6 (BRIGHT
                          ; value)
  LD C,A                  ; Save this value in C temporarily
  LD A,(HL)               ; Pick up the room attribute byte at the guardian's
                          ; location from the buffer at 23552
  AND 56                  ; Keep only bits 3-5 (PAPER colour)
  XOR C                   ; Merge the INK colour and BRIGHT value from C
  LD C,A                  ; Copy this attribute value to C
  LD (HL),C               ; Set the attribute bytes in the buffer at 23552 for
  INC HL                  ; the top two rows of cells occupied by the
  LD (HL),C               ; guardian's sprite
  ADD HL,DE               ;
  LD (HL),C               ;
  INC HL                  ;
  LD (HL),C               ;
  LD A,(IX+3)             ; Pick up the fourth byte of the guardian's buffer
  AND 14                  ; Does the guardian's sprite occupy only two rows of
                          ; cells at the moment?
  JR Z,DRAWTHINGS_1       ; Jump if so
  ADD HL,DE               ; Set the attribute bytes in the buffer at 23552 for
  LD (HL),C               ; the third row of cells occupied by the guardian's
  INC HL                  ; sprite
  LD (HL),C               ;
DRAWTHINGS_1:
  LD C,1                  ; Prepare C for the call to DRAWSPRITE later on
  LD A,(IX+1)             ; Now bits 5-7 of A hold the animation frame mask
  AND (IX+0)              ; 'AND' on the current animation frame (bits 5-7)
  OR (IX+2)               ; 'OR' on the base sprite index (bits 5-7)
  AND 224                 ; Keep only bits 5-7
  LD E,A                  ; Point DE at the graphic data for the guardian's
  LD D,(IX+5)             ; current animation frame (see GUARDIANS)
  LD H,130                ; Point HL at the guardian's current location in the
  LD L,(IX+3)             ; screen buffer at 24576
  LD A,(IX+2)             ;
  AND 31                  ;
  OR (HL)                 ;
  INC HL                  ;
  LD H,(HL)               ;
  LD L,A                  ;
  CALL DRAWSPRITE         ; Draw the guardian
  JP NZ,KILLWILLY_0       ; Kill Willy if the guardian collided with him
  JP DRAWTHINGS_22        ; Jump to consider the next entity
; We are dealing with an arrow.
DRAWTHINGS_2:
  BIT 7,(IX+0)            ; Is the arrow travelling left to right?
  JR NZ,DRAWTHINGS_3      ; Jump if so
  DEC (IX+4)              ; Decrement the arrow's x-coordinate
  LD C,44                 ; The sound effect for an arrow travelling right to
                          ; left is made when the x-coordinate is 44
  JR DRAWTHINGS_4
DRAWTHINGS_3:
  INC (IX+4)              ; Increment the arrow's x-coordinate
  LD C,244                ; The sound effect for an arrow travelling left to
                          ; right is made when the x-coordinate is 244
DRAWTHINGS_4:
  LD A,(IX+4)             ; Pick up the arrow's x-coordinate (0-255)
  CP C                    ; Is it time to make the arrow sound effect?
  JR NZ,DRAWTHINGS_7      ; Jump if not
  LD BC,640               ; Prepare the delay counters (B=2, C=128) for the
                          ; arrow sound effect
  LD A,(BORDER)           ; Pick up the border colour for the current room from
                          ; BORDER
DRAWTHINGS_5:
  OUT (254),A             ; Produce the arrow sound effect
  XOR 24                  ;
DRAWTHINGS_6:
  DJNZ DRAWTHINGS_6       ;
  LD B,C                  ;
  DEC C                   ;
  JR NZ,DRAWTHINGS_5      ;
  JP DRAWTHINGS_22        ; Jump to consider the next entity
DRAWTHINGS_7:
  AND 224                 ; Is the arrow's x-coordinate in the range 0-31 (i.e.
                          ; on-screen)?
  JP NZ,DRAWTHINGS_22     ; If not, jump to consider the next entity
  LD E,(IX+2)             ; Point DE at the entry in the screen buffer address
  LD D,130                ; lookup table at SBUFADDRS that corresponds to the
                          ; arrow's y-coordinate
  LD A,(DE)               ; Pick up the LSB of the screen buffer address
  ADD A,(IX+4)            ; Adjust it for the arrow's x-coordinate
  LD L,A                  ; Point HL at the arrow's current location in the
  LD A,E                  ; attribute buffer at 23552
  AND 128                 ;
  RLCA                    ;
  OR 92                   ;
  LD H,A                  ;
  LD (IX+5),0             ; Initialise the collision detection byte (0=off,
                          ; 255=on)
  LD A,(HL)               ; Pick up the room attribute byte at the arrow's
                          ; location
  AND 7                   ; Keep only bits 0-2 (INK colour)
  CP 7                    ; Is the INK white?
  JR NZ,DRAWTHINGS_8      ; Jump if not
  DEC (IX+5)              ; Activate collision detection
DRAWTHINGS_8:
  LD A,(HL)               ; Set the INK colour to white at the arrow's location
  OR 7                    ;
  LD (HL),A               ;
  INC DE                  ; Pick up the MSB of the screen buffer address for
  LD A,(DE)               ; the arrow's location
  LD H,A                  ; Point HL at the top pixel row of the arrow's
  DEC H                   ; location in the screen buffer at 24576
  LD A,(IX+6)             ; Draw the top pixel row of the arrow
  LD (HL),A               ;
  INC H                   ; Point HL at the middle pixel row of the arrow's
                          ; location in the screen buffer at 24576
  LD A,(HL)               ; Pick up the graphic byte that's already here
  AND (IX+5)              ; Has the arrow hit anything that has white INK (e.g.
                          ; Willy)?
  JP NZ,KILLWILLY_0       ; If so, kill Willy
  LD (HL),255             ; Draw the shaft of the arrow
  INC H                   ; Point HL at the bottom pixel row of the arrow's
                          ; location in the screen buffer at 24576
  LD A,(IX+6)             ; Draw the bottom pixel row of the arrow
  LD (HL),A               ;
  JP DRAWTHINGS_22        ; Jump to consider the next entity
; We are dealing with a rope.
DRAWTHINGS_9:
  LD IY,SBUFADDRS         ; Point IY at the first byte of the screen buffer
                          ; address lookup table at SBUFADDRS
  LD (IX+9),0             ; Initialise the second byte in the following entity
                          ; buffer to zero; this will count the segments of
                          ; rope to draw
  LD A,(IX+2)             ; Initialise the fourth byte of the rope's buffer;
  LD (IX+3),A             ; this holds the x-coordinate of the cell in which
                          ; the segment of rope under consideration will be
                          ; drawn
  LD (IX+5),128           ; Initialise the sixth byte of the rope's buffer to
                          ; 128 (bit 7 set); the value held here is used to
                          ; draw the segment of rope under consideration
; The following loop draws each segment of the rope from top to bottom.
DRAWTHINGS_10:
  LD A,(IY+0)             ; Point HL at the location of the segment of rope
  ADD A,(IX+3)            ; under consideration in the screen buffer at 24576
  LD L,A                  ;
  LD H,(IY+1)             ;
  LD A,(ROPE)             ; Pick up the rope status indicator at ROPE
  OR A                    ; Is Willy on the rope, or has he recently jumped or
                          ; dropped off it?
  JR NZ,DRAWTHINGS_11     ; Jump if so
  LD A,(IX+5)             ; Pick up the drawing byte
  AND (HL)                ; Is this segment of rope touching anything else
                          ; that's been drawn so far (e.g. Willy)?
  JR Z,DRAWTHINGS_13      ; Jump if not
  LD A,(IX+9)             ; Copy the segment counter into the rope status
  LD (ROPE),A             ; indicator at ROPE
  SET 0,(IX+11)           ; Signal: Willy is on the rope
DRAWTHINGS_11:
  CP (IX+9)               ; Does the rope status indicator at ROPE match the
                          ; segment counter?
  JR NZ,DRAWTHINGS_13     ; Jump if not
  BIT 0,(IX+11)           ; Is Willy on the rope (and clinging to this
                          ; particular segment)?
  JR Z,DRAWTHINGS_13      ; Jump if not
  LD B,(IX+3)             ; Copy the x-coordinate of the cell containing the
                          ; segment of rope under consideration to B
  LD A,(IX+5)             ; Pick up the drawing byte in A
  LD C,1                  ; The value in C will specify Willy's next animation
                          ; frame; initialise it to 1
  CP 4                    ; Is the set bit of the drawing byte in bit 0 or 1?
  JR C,DRAWTHINGS_12      ; Jump if so
  LD C,0                  ; Assume that Willy's next animation frame will be 0
  CP 16                   ; Is the set bit of the drawing byte in bit 2 or 3?
  JR C,DRAWTHINGS_12      ; Jump if so
  DEC B                   ; Decrement the x-coordinate
  LD C,3                  ; Assume that Willy's next animation frame will be 3
  CP 64                   ; Is the set bit of the drawing byte in bit 4 or 5?
  JR C,DRAWTHINGS_12      ; Jump if so
  LD C,2                  ; Willy's next animation frame will be 2 (the set bit
                          ; of the drawing byte is in bit 6 or 7)
DRAWTHINGS_12:
  LD (FRAME),BC           ; Set Willy's animation frame at FRAME, and
                          ; temporarily store his x-coordinate at LOCATION
  LD A,IYl                ; Update Willy's pixel y-coordinate at PIXEL_Y to
  SUB 16                  ; account for his change of location as the rope
  LD (PIXEL_Y),A          ; moves
  PUSH HL                 ; Save HL briefly
  CALL MOVEWILLY_8        ; Update Willy's attribute buffer address at LOCATION
                          ; to account for his change of location as the rope
                          ; moves
  POP HL                  ; Restore the screen buffer address of the segment of
                          ; rope under consideration to HL
  JR DRAWTHINGS_13        ; Make a redundant jump to the next instruction
DRAWTHINGS_13:
  LD A,(IX+5)             ; Draw a pixel of the rope to the screen buffer at
  OR (HL)                 ; 24576
  LD (HL),A               ;
  LD A,(IX+9)             ; Point HL at the relevant entry in the second half
  ADD A,(IX+1)            ; of the rope animation table at ROPEANIM
  LD L,A                  ;
  SET 7,L                 ;
  LD H,131                ;
  LD E,(HL)               ; Add its value to IY; now IY points at the entry in
  LD D,0                  ; the screen buffer address lookup table at SBUFADDRS
  ADD IY,DE               ; that corresponds to the next segment of rope to
                          ; consider
  RES 7,L                 ; Point HL at the relevant entry in the first half of
                          ; the rope animation table at ROPEANIM
  LD A,(HL)               ; Pick up its value
  OR A                    ; Is it zero?
  JR Z,DRAWTHINGS_18      ; Jump if so
  LD B,A                  ; Copy the rope animation table entry value to B;
                          ; this will count the rotations of the drawing byte
  BIT 7,(IX+1)            ; Is the rope currently right of centre?
  JR Z,DRAWTHINGS_16      ; Jump if so
DRAWTHINGS_14:
  RLC (IX+5)              ; Rotate the drawing byte left once
  BIT 0,(IX+5)            ; Did that push the set bit from bit 7 into bit 0?
  JR Z,DRAWTHINGS_15      ; Jump if not
  DEC (IX+3)              ; Decrement the x-coordinate for the cell containing
                          ; this segment of rope
DRAWTHINGS_15:
  DJNZ DRAWTHINGS_14      ; Jump back until the drawing byte has been rotated
                          ; as required
  JR DRAWTHINGS_18        ; Jump to consider the next segment of rope
DRAWTHINGS_16:
  RRC (IX+5)              ; Rotate the drawing byte right once
  BIT 7,(IX+5)            ; Did that push the set bit from bit 0 into bit 7?
  JR Z,DRAWTHINGS_17      ; Jump if not
  INC (IX+3)              ; Increment the x-coordinate for the cell containing
                          ; this segment of rope
DRAWTHINGS_17:
  DJNZ DRAWTHINGS_16      ; Jump back until the drawing byte has been rotated
                          ; as required
DRAWTHINGS_18:
  LD A,(IX+9)             ; Pick up the segment counter
  CP (IX+4)               ; Have we drawn every segment of the rope yet?
  JR Z,DRAWTHINGS_19      ; Jump if so
  INC (IX+9)              ; Increment the segment counter
  JP DRAWTHINGS_10        ; Jump back to draw the next segment of rope
; Now that the entire rope has been drawn, deal with Willy's movement along it.
DRAWTHINGS_19:
  LD A,(ROPE)             ; Pick up the rope status indicator at ROPE
  BIT 7,A                 ; Has Willy recently jumped off the rope or dropped
                          ; off the bottom of it (A>=240)?
  JR Z,DRAWTHINGS_20      ; Jump if not
  INC A                   ; Update the rope status indicator at ROPE
  LD (ROPE),A             ;
  RES 0,(IX+11)           ; Signal: Willy is not on the rope
  JR DRAWTHINGS_22        ; Jump to consider the next entity
DRAWTHINGS_20:
  BIT 0,(IX+11)           ; Is Willy on the rope?
  JR Z,DRAWTHINGS_22      ; If not, jump to consider the next entity
  LD A,(DMFLAGS)          ; Pick up Willy's direction and movement flags from
                          ; DMFLAGS
  BIT 1,A                 ; Is Willy moving up or down the rope?
  JR Z,DRAWTHINGS_22      ; If not, jump to consider the next entity
  RRCA                    ; XOR Willy's direction bit (0=facing right, 1=facing
  XOR (IX+0)              ; left) with the rope's direction bit (0=swinging
  RLCA                    ; right to left, 1=swinging left to right)
  RLCA                    ; Now A=1 if Willy is facing the same direction as
  AND 2                   ; the rope is swinging (he will move down the rope),
  DEC A                   ; or -1 otherwise (he will move up the rope)
  LD HL,ROPE              ; Increment or decrement the rope status indicator at
  ADD A,(HL)              ; ROPE
  LD (HL),A               ;
  LD A,(ABOVE)            ; Pick up the number of the room above from ABOVE and
  LD C,A                  ; copy it to C
  LD A,(ROOM)             ; Pick up the number of the current room from ROOM
  CP C                    ; Is there a room above this one?
  JR NZ,DRAWTHINGS_21     ; Jump if so
  LD A,(HL)               ; Pick up the rope status indicator at ROPE
  CP 12                   ; Is it 12 or greater?
  JR NC,DRAWTHINGS_21     ; Jump if so
  LD (HL),12              ; Set the rope status indicator at ROPE to 12 (there
                          ; is nowhere to go above this rope)
DRAWTHINGS_21:
  LD A,(HL)               ; Pick up the rope status indicator at ROPE
  CP (IX+4)               ; Compare it with the length of the rope
  JR C,DRAWTHINGS_22      ; If Willy is at or above the bottom of the rope,
  JR Z,DRAWTHINGS_22      ; jump to consider the next entity
  LD (HL),240             ; Set the rope status indicator at ROPE to 240 (Willy
                          ; has just dropped off the bottom of the rope)
  LD A,(PIXEL_Y)          ; Round down Willy's pixel y-coordinate at PIXEL_Y to
  AND 248                 ; the nearest multiple of 8; this might move him
  LD (PIXEL_Y),A          ; upwards a little, but ensures that his actual pixel
                          ; y-coordinate is a multiple of 4 before he starts
                          ; falling
  XOR A                   ; Initialise the airborne status indicator at
  LD (AIRBORNE),A         ; AIRBORNE
  JR DRAWTHINGS_22        ; Make a redundant jump to the next instruction
; The current entity has been dealt with. Time for the next one.
DRAWTHINGS_22:
  LD DE,8                 ; Point IX at the first byte of the next entity's
  ADD IX,DE               ; buffer
  JP DRAWTHINGS_0         ; Jump back to deal with it

; Unused routine
;
; This routine is not used, but if it were, it would set the INK colour for a
; 3x2 block of cells, maintaining the PAPER, BRIGHT and FLASH attributes of the
; current room background. It is identical to the code at 36447 in Manic Miner
; that is used to set the attributes for a vertical guardian.
;
; A INK colour (0-7)
; HL Attribute buffer address
U_SETATTRS:
  LD (HL),A               ; Store the INK colour (bits 0-2)
  LD A,(BACKGROUND)       ; Collect the current room's background tile
                          ; attribute from BACKGROUND
  AND 248                 ; Keep only bits 3-7 (PAPER, BRIGHT, FLASH)
  OR (HL)                 ; Merge the INK bits
  LD (HL),A               ; Store the resultant attribute byte
  LD DE,31                ; Prepare DE for later addition
  INC HL                  ; Move right one cell and store the attribute byte
  LD (HL),A               ; there
  ADD HL,DE               ; Move left one cell and down a row and store the
  LD (HL),A               ; attribute byte there
  INC HL                  ; Move right one cell and store the attribute byte
  LD (HL),A               ; there
  ADD HL,DE               ; Move left one cell and down a row and store the
  LD (HL),A               ; attribute byte there
  INC HL                  ; Move right one cell and store the attribute byte
  LD (HL),A               ; there
  RET

; Draw the items in the current room and collect any that Willy is touching
;
; Used by the routine at MAINLOOP.
DRAWITEMS:
  LD H,164                ; Page 164 holds the first byte of each entry in the
                          ; item table
  LD A,(FIRSTITEM)        ; Pick up the index of the first item from FIRSTITEM
  LD L,A                  ; Point HL at the first byte of the first entry in
                          ; the item table
; The item-drawing loop begins here.
DRAWITEMS_0:
  LD C,(HL)               ; Pick up the first byte of the current entry in the
                          ; item table
  RES 7,C                 ; Reset bit 7; bit 6 holds the collection flag, and
                          ; bits 0-5 hold the room number
  LD A,(ROOM)             ; Pick up the number of the current room from ROOM
  OR 64                   ; Set bit 6 (corresponding to the collection flag)
  CP C                    ; Is the item in the current room and still
                          ; uncollected?
  JR NZ,DRAWITEMS_7       ; If not, jump to consider the next entry in the item
                          ; table
; This item is in the current room and has not been collected yet.
  LD A,(HL)               ; Pick up the first byte of the current entry in the
                          ; item table
  RLCA                    ; Point DE at the location of the item in the
  AND 1                   ; attribute buffer at 23552
  ADD A,92                ;
  LD D,A                  ;
  INC H                   ;
  LD E,(HL)               ;
  DEC H                   ;
  LD A,(DE)               ; Pick up the current attribute byte at the item's
                          ; location
  AND 7                   ; Is the INK white (which happens if Willy is
  CP 7                    ; touching the item, or the room's background tile
                          ; has white INK, as in Swimming Pool)?
  JR NZ,DRAWITEMS_6       ; Jump if not
; Willy is touching this item (or the room's background tile has white INK), so
; add it to his collection.
  LD IX,MSG_ITEMS         ; Point IX at the number of items collected at
                          ; MSG_ITEMS
DRAWITEMS_1:
  INC (IX+2)              ; Increment a digit of the number of items collected
  LD A,(IX+2)             ; Was the digit originally '9'?
  CP 58                   ;
  JR NZ,DRAWITEMS_2       ; Jump if not
  LD (IX+2),48            ; Set the digit to '0'
  DEC IX                  ; Move back to the digit on the left
  JR DRAWITEMS_1          ; Jump back to increment this digit
DRAWITEMS_2:
  LD A,(BORDER)           ; Pick up the border colour for the current room from
                          ; BORDER
  LD C,128                ; Produce the sound effect for collecting an item
DRAWITEMS_3:
  OUT (254),A             ;
  XOR 24                  ;
  LD E,A                  ;
  LD A,144                ;
  SUB C                   ;
  LD B,A                  ;
  LD A,E                  ;
DRAWITEMS_4:
  DJNZ DRAWITEMS_4        ;
  DEC C                   ;
  DEC C                   ;
  JR NZ,DRAWITEMS_3       ;
  LD A,(ITEMS)            ; Update the counter of items remaining at ITEMS, and
  INC A                   ; set the zero flag if there are no more items to
  LD (ITEMS),A            ; collect
  JR NZ,DRAWITEMS_5       ; Jump if there are any items still to be collected
  LD A,1                  ; Update the game mode indicator at MODE to 1 (all
  LD (MODE),A             ; items collected)
DRAWITEMS_5:
  RES 6,(HL)              ; Reset bit 6 of the first byte of the entry in the
                          ; item table: the item has been collected
  JR DRAWITEMS_7          ; Jump to consider the next entry in the item table
; Willy is not touching this item, so draw it and cycle its INK colour.
DRAWITEMS_6:
  LD A,(TICKS)            ; Generate the INK colour for the item from the value
  ADD A,L                 ; of the minute counter at TICKS (0-255) and the
  AND 3                   ; index of the item in the item table (173-255)
  ADD A,3                 ;
  LD C,A                  ;
  LD A,(DE)               ; Change the INK colour of the item in the attribute
  AND 248                 ; buffer at 23552
  OR C                    ;
  LD (DE),A               ;
  LD A,(HL)               ; Point DE at the location of the item in the screen
  RLCA                    ; buffer at 24576
  RLCA                    ;
  RLCA                    ;
  RLCA                    ;
  AND 8                   ;
  ADD A,96                ;
  LD D,A                  ;
  PUSH HL                 ; Save HL briefly
  LD HL,ITEM              ; Point HL at the item graphic for the current room
                          ; (at ITEM)
  LD B,8                  ; There are eight pixel rows to copy
  CALL PRINTCHAR_0        ; Draw the item to the screen buffer at 24576
  POP HL                  ; Restore the item table pointer to HL
; The current item has been dealt with (skipped, collected or drawn) as
; appropriate. Time to consider the next one.
DRAWITEMS_7:
  INC L                   ; Point HL at the first byte of the next entry in the
                          ; item table
  JR NZ,DRAWITEMS_0       ; Jump back unless we've examined every entry
  RET

; Draw a sprite
;
; Used by the routines at CODESCREEN (to draw the number key graphics on the
; code entry screen), DRAWLIVES (to draw the remaining lives), GAMEOVER (to
; draw Willy, the foot and the barrel during the game over sequence),
; DRAWTHINGS (to draw guardians in the current room) and BEDANDBATH (to draw
; Maria in Master Bedroom). If C=1 on entry, this routine returns with the zero
; flag reset if any of the set bits in the sprite being drawn collides with a
; set bit in the background.
;
; C Drawing mode: 0 (overwrite) or 1 (blend)
; DE Address of sprite graphic data
; HL Address to draw at
DRAWSPRITE:
  LD B,16                 ; There are 16 rows of pixels to draw
DRAWSPRITE_0:
  BIT 0,C                 ; Set the zero flag if we're in overwrite mode
  LD A,(DE)               ; Pick up a sprite graphic byte
  JR Z,DRAWSPRITE_1       ; Jump if we're in overwrite mode
  AND (HL)                ; Return with the zero flag reset if any of the set
  RET NZ                  ; bits in the sprite graphic byte collide with a set
                          ; bit in the background (e.g. in Willy's sprite)
  LD A,(DE)               ; Pick up the sprite graphic byte again
  OR (HL)                 ; Blend it with the background byte
DRAWSPRITE_1:
  LD (HL),A               ; Copy the graphic byte to its destination cell
  INC L                   ; Move HL along to the next cell on the right
  INC DE                  ; Point DE at the next sprite graphic byte
  BIT 0,C                 ; Set the zero flag if we're in overwrite mode
  LD A,(DE)               ; Pick up a sprite graphic byte
  JR Z,DRAWSPRITE_2       ; Jump if we're in overwrite mode
  AND (HL)                ; Return with the zero flag reset if any of the set
  RET NZ                  ; bits in the sprite graphic byte collide with a set
                          ; bit in the background (e.g. in Willy's sprite)
  LD A,(DE)               ; Pick up the sprite graphic byte again
  OR (HL)                 ; Blend it with the background byte
DRAWSPRITE_2:
  LD (HL),A               ; Copy the graphic byte to its destination cell
  DEC L                   ; Move HL to the next pixel row down in the cell on
  INC H                   ; the left
  INC DE                  ; Point DE at the next sprite graphic byte
  LD A,H                  ; Have we drawn the bottom pixel row in this pair of
  AND 7                   ; cells yet?
  JR NZ,DRAWSPRITE_3      ; Jump if not
  LD A,H                  ; Otherwise move HL to the top pixel row in the cell
  SUB 8                   ; below
  LD H,A                  ;
  LD A,L                  ;
  ADD A,32                ;
  LD L,A                  ;
  AND 224                 ; Was the last pair of cells at y-coordinate 7 or 15?
  JR NZ,DRAWSPRITE_3      ; Jump if not
  LD A,H                  ; Otherwise adjust HL to account for the movement
  ADD A,8                 ; from the top or middle third of the screen to the
  LD H,A                  ; next one down
DRAWSPRITE_3:
  DJNZ DRAWSPRITE_0       ; Jump back until all 16 rows of pixels have been
                          ; drawn
  XOR A                   ; Set the zero flag (to indicate no collision)
  RET

; Move Willy into the room to the left
;
; Used by the routine at MOVEWILLY3.
ROOMLEFT:
  LD A,(LEFT)             ; Pick up the number of the room to the left from
                          ; LEFT
  LD (ROOM),A             ; Make it the current room number by copying it to
                          ; ROOM
  LD A,(LOCATION)         ; Adjust Willy's screen x-coordinate (at LOCATION) to
  OR 31                   ; 30 (on the far right)
  AND 254                 ;
  LD (LOCATION),A         ;
  POP HL                  ; Drop the return address (AFTERMOVE1, in the main
                          ; loop) from the stack
  JP INITROOM             ; Draw the room and re-enter the main loop

; Move Willy into the room to the right
;
; Used by the routine at MOVEWILLY3.
ROOMRIGHT:
  LD A,(RIGHT)            ; Pick up the number of the room to the right from
                          ; RIGHT
  LD (ROOM),A             ; Make it the current room number by copying it to
                          ; ROOM
  LD A,(LOCATION)         ; Adjust Willy's screen x-coordinate (at LOCATION) to
  AND 224                 ; 0 (on the far left)
  LD (LOCATION),A         ;
  POP HL                  ; Drop the return address (AFTERMOVE1, in the main
                          ; loop) from the stack
  JP INITROOM             ; Draw the room and re-enter the main loop

; Move Willy into the room above
;
; Used by the routines at MAINLOOP and MOVEWILLY.
ROOMABOVE:
  LD A,(ABOVE)            ; Pick up the number of the room above from ABOVE
  LD (ROOM),A             ; Make it the current room number by copying it to
                          ; ROOM
  LD A,(LOCATION)         ; Willy should now appear on the bottom floor of the
  AND 31                  ; room, so adjust his attribute buffer coordinates
  ADD A,160               ; (at LOCATION) accordingly
  LD (LOCATION),A         ;
  LD A,93                 ;
  LD (34260),A            ;
  LD A,208                ; Adjust Willy's pixel y-coordinate (at PIXEL_Y) as
  LD (PIXEL_Y),A          ; well
  XOR A                   ; Reset the airborne status indicator at AIRBORNE
  LD (AIRBORNE),A         ;
  POP HL                  ; Drop the return address (either AFTERMOVE1 or
                          ; AFTERMOVE2, in the main loop) from the stack
  JP INITROOM             ; Draw the room and re-enter the main loop

; Move Willy into the room below
;
; Used by the routine at MOVEWILLY.
ROOMBELOW:
  LD A,(BELOW)            ; Pick up the number of the room below from BELOW
  LD (ROOM),A             ; Make it the current room number by copying it to
                          ; ROOM
  XOR A                   ; Set Willy's pixel y-coordinate (at PIXEL_Y) to 0
  LD (PIXEL_Y),A          ;
  LD A,(AIRBORNE)         ; Pick up the airborne status indicator from AIRBORNE
  CP 11                   ; Is it 11 or greater (meaning Willy has already been
                          ; falling for a while)?
  JR NC,ROOMBELOW_0       ; Jump if so
  LD A,2                  ; Otherwise set the airborne status indicator to 2
  LD (AIRBORNE),A         ; (Willy will start falling here if there's no floor
                          ; beneath him)
ROOMBELOW_0:
  LD A,(LOCATION)         ; Willy should now appear at the top of the room, so
  AND 31                  ; adjust his attribute buffer coordinates (at
  LD (LOCATION),A         ; LOCATION) accordingly
  LD A,92                 ;
  LD (34260),A            ;
  POP HL                  ; Drop the return address (AFTERMOVE1, in the main
                          ; loop) from the stack
  JP INITROOM             ; Draw the room and re-enter the main loop

; Move the conveyor in the current room
;
; Used by the routine at MAINLOOP.
MVCONVEYOR:
  LD HL,(CONVLOC)         ; Pick up the address of the conveyor's location in
                          ; the attribute buffer at 24064 from CONVLOC
  LD A,H                  ; Point DE and HL at the location of the left end of
  AND 1                   ; the conveyor in the screen buffer at 28672
  RLCA                    ;
  RLCA                    ;
  RLCA                    ;
  ADD A,112               ;
  LD H,A                  ;
  LD E,L                  ;
  LD D,H                  ;
  LD A,(CONVLEN)          ; Pick up the length of the conveyor from CONVLEN
  OR A                    ; Is there a conveyor in the room?
  RET Z                   ; Return if not
  LD B,A                  ; B will count the conveyor tiles
  LD A,(CONVDIR)          ; Pick up the direction of the conveyor from CONVDIR
                          ; (0=left, 1=right)
  OR A                    ; Is the conveyor moving right?
  JR NZ,MVCONVEYOR_1      ; Jump if so
; The conveyor is moving left.
  LD A,(HL)               ; Copy the first pixel row of the conveyor tile to A
  RLC A                   ; Rotate it left twice
  RLC A                   ;
  INC H                   ; Point HL at the third pixel row of the conveyor
  INC H                   ; tile
  LD C,(HL)               ; Copy this pixel row to C
  RRC C                   ; Rotate it right twice
  RRC C                   ;
MVCONVEYOR_0:
  LD (DE),A               ; Update the first and third pixel rows of every
  LD (HL),C               ; conveyor tile in the screen buffer at 28672
  INC L                   ;
  INC E                   ;
  DJNZ MVCONVEYOR_0       ;
  RET
; The conveyor is moving right.
MVCONVEYOR_1:
  LD A,(HL)               ; Copy the first pixel row of the conveyor tile to A
  RRC A                   ; Rotate it right twice
  RRC A                   ;
  INC H                   ; Point HL at the third pixel row of the conveyor
  INC H                   ; tile
  LD C,(HL)               ; Copy this pixel row to C
  RLC C                   ; Rotate it left twice
  RLC C                   ;
  JR MVCONVEYOR_0         ; Jump back to update the first and third pixel rows
                          ; of every conveyor tile

; Deal with special rooms (Master Bedroom, The Bathroom)
;
; Used by the routine at MAINLOOP.
BEDANDBATH:
  LD A,(ROOM)             ; Pick up the number of the current room from ROOM
  CP 35                   ; Are we in Master Bedroom?
  JR NZ,DRAWTOILET        ; Jump if not
  LD A,(MODE)             ; Pick up the game mode indicator from MODE
  OR A                    ; Has Willy collected all the items?
  JR NZ,BEDANDBATH_1      ; Jump if so
; Willy hasn't collected all the items yet, so Maria is on guard.
  LD A,(TICKS)            ; Pick up the minute counter from TICKS; this will
                          ; determine Maria's animation frame
  AND 2                   ; Keep only bit 1, move it to bit 5, and set bit 7
  RRCA                    ;
  RRCA                    ;
  RRCA                    ;
  RRCA                    ;
  OR 128                  ;
  LD E,A                  ; Now E=128 (foot down) or 160 (foot raised)
  LD A,(PIXEL_Y)          ; Pick up Willy's pixel y-coordinate from PIXEL_Y
  CP 208                  ; Is Willy on the floor below the ramp?
  JR Z,BEDANDBATH_0       ; Jump if so
  LD E,192                ; E=192 (raising arm)
  CP 192                  ; Is Willy 8 or fewer pixels above floor level?
  JR NC,BEDANDBATH_0      ; Jump if so
  LD E,224                ; E=224 (arm raised)
BEDANDBATH_0:
  LD D,156                ; Point DE at the sprite graphic data for Maria
                          ; (MARIA0, MARIA1, MARIA2 or MARIA3)
  LD HL,26734             ; Draw Maria at (11,14) in the screen buffer at 24576
  LD C,1                  ;
  CALL DRAWSPRITE         ;
  JP NZ,KILLWILLY_0       ; Kill Willy if Maria collided with him
  LD HL,17733             ; H=L=69 (INK 5: PAPER 0: BRIGHT 1)
  LD (23918),HL           ; Set the attribute bytes for the top half of Maria's
                          ; sprite in the buffer at 23552
  LD HL,1799              ; H=L=7 (INK 7: PAPER 0: BRIGHT 0)
  LD (23950),HL           ; Set the attribute bytes for the bottom half of
                          ; Maria's sprite in the buffer at 23552
  RET
; Willy has collected all the items, so Maria is gone.
BEDANDBATH_1:
  LD A,(LOCATION)         ; Pick up Willy's screen x-coordinate from LOCATION
  AND 31                  ;
  CP 6                    ; Has Willy reached the bed (at x=5) yet?
  RET NC                  ; Return if not
  LD A,2                  ; Update the game mode indicator at MODE to 2 (Willy
  LD (MODE),A             ; is running to the toilet)
  RET

; Check whether Willy has reached the toilet
;
; Called by the routine at MAINLOOP when Willy is on his way to the toilet.
CHKTOILET:
  LD A,(ROOM)             ; Pick up the number of the current room from ROOM
  CP 33                   ; Are we in The Bathroom?
  RET NZ                  ; Return if not
  LD A,(LOCATION)         ; Pick up the LSB of Willy's attribute buffer
                          ; location from LOCATION
  CP 188                  ; Is Willy's screen x-coordinate 28 (where the toilet
                          ; is)?
  RET NZ                  ; Return if not
; Willy has reached the toilet.
  XOR A                   ; Reset the minute counter at TICKS to 0 (so that we
  LD (TICKS),A            ; get to see Willy's head down the toilet for at
                          ; least a whole game minute)
  LD A,3                  ; Update the game mode indicator at MODE to 3
  LD (MODE),A             ; (Willy's head is down the toilet)
  RET

; Animate the toilet in The Bathroom
;
; Used by the routine at BEDANDBATH.
DRAWTOILET:
  LD A,(ROOM)             ; Pick up the number of the current room from ROOM
  CP 33                   ; Are we in The Bathroom?
  RET NZ                  ; Return if not
  LD A,(TICKS)            ; Pick up the minute counter from TICKS; this will
                          ; determine the animation frame to use for the toilet
  AND 1                   ; Keep only bit 0 and move it to bit 5
  RRCA                    ;
  RRCA                    ;
  RRCA                    ;
  LD E,A                  ; Now E=0 or 32
  LD A,(MODE)             ; Pick up the game mode indicator from MODE
  CP 3                    ; Is Willy's head down the toilet?
  JR NZ,DRAWTOILET_0      ; Jump if not
  SET 6,E                 ; Now E=64 or 96
DRAWTOILET_0:
  LD D,166                ; Point DE at the toilet sprite to use (TOILET0,
                          ; TOILET1, TOILET2 or TOILET3)
  LD IX,33488             ; Draw the toilet at (13,28) in the screen buffer at
  LD BC,4124              ; 24576
  CALL DRAWWILLY_1        ;
  LD HL,1799              ; H=L=7 (INK 7: PAPER 0)
  LD (23996),HL           ; Set the attribute bytes for the toilet in the
  LD (24028),HL           ; buffer at 23552
  RET

; Check and set the attribute bytes for Willy's sprite in the buffer at 23552
;
; Used by the routine at MAINLOOP. Sets the attribute bytes in the buffer at
; 23552 for the six cells (in three rows of two) occupied by or under Willy's
; sprite, or kills Willy if any of the cells contains a nasty.
WILLYATTRS:
  LD HL,(LOCATION)        ; Pick up Willy's attribute buffer coordinates from
                          ; LOCATION
  LD B,0                  ; Initialise B to 0 (in case Willy is not standing on
                          ; a ramp)
  LD A,(RAMPDIR)          ; Pick up the direction byte of the ramp definition
                          ; for the current room from RAMPDIR
  AND 1                   ; Point HL at one of the cells under Willy's feet
  ADD A,64                ; (the one on the left if the ramp goes up to the
  LD E,A                  ; left, the one on the right if the ramp goes up to
  LD D,0                  ; the right)
  ADD HL,DE               ;
  LD A,(RAMP)             ; Pick up the ramp's attribute byte from RAMP
  CP (HL)                 ; Is Willy on or just above the ramp?
  JR NZ,WILLYATTRS_0      ; Jump if not
  LD A,(AIRBORNE)         ; Pick up the airborne status indicator from AIRBORNE
  OR A                    ; Is Willy airborne?
  JR NZ,WILLYATTRS_0      ; Jump if so
; Willy is standing on a ramp. Calculate the offset that needs to be added to
; the y-coordinate stored at PIXEL_Y to obtain Willy's true pixel y-coordinate.
  LD A,(FRAME)            ; Pick up Willy's current animation frame (0-3) from
                          ; FRAME
  AND 3                   ; B=0, 4, 8 or 12
  RLCA                    ;
  RLCA                    ;
  LD B,A                  ;
  LD A,(RAMPDIR)          ; Pick up the direction byte of the ramp definition
                          ; for the current room from RAMPDIR
  AND 1                   ; A=B (if the ramp goes up to the left) or 12-B (if
  DEC A                   ; the ramp goes up to the right)
  XOR 12                  ;
  XOR B                   ;
  AND 12                  ;
  LD B,A                  ; Copy this value to B
; Now B holds a y-coordinate offset of 0, 4, 8 or 12 if Willy is standing on a
; ramp, or 0 otherwise.
WILLYATTRS_0:
  LD HL,(LOCATION)        ; Pick up Willy's attribute buffer coordinates from
                          ; LOCATION
  LD DE,31                ; Prepare DE for later addition
  LD C,15                 ; Set C=15 for the top two rows of cells (to make the
                          ; routine at WILLYATTR force white INK)
  CALL WILLYATTR          ; Check and set the attribute byte for the top-left
                          ; cell
  INC HL                  ; Move HL to the next cell to the right
  CALL WILLYATTR          ; Check and set the attribute byte for the top-right
                          ; cell
  ADD HL,DE               ; Move HL down a row and back one cell to the left
  CALL WILLYATTR          ; Check and set the attribute byte for the mid-left
                          ; cell
  INC HL                  ; Move HL to the next cell to the right
  CALL WILLYATTR          ; Check and set the attribute byte for the mid-right
                          ; cell
  LD A,(PIXEL_Y)          ; Pick up Willy's pixel y-coordinate from PIXEL_Y
  ADD A,B                 ; Add the y-coordinate offset calculated earlier (to
  LD C,A                  ; get Willy's true pixel y-coordinate if he's
                          ; standing on a ramp) and transfer the result to C
  ADD HL,DE               ; Move HL down a row and back one cell to the left;
                          ; at this point HL may be pointing at one of the
                          ; cells in the top row of the buffer at 24064, which
                          ; is a bug
  CALL WILLYATTR          ; Check and set the attribute byte for the
                          ; bottom-left cell
  INC HL                  ; Move HL to the next cell to the right
  CALL WILLYATTR          ; Check and set the attribute byte for the
                          ; bottom-right cell
  JR DRAWWILLY            ; Draw Willy to the screen buffer at 24576

; Check and set the attribute byte for a cell occupied by Willy's sprite
;
; Used by the routine at WILLYATTRS. Sets the attribute byte in the buffer at
; 23552 for one of the six cells (in three rows of two) occupied by or under
; Willy's sprite, or kills Willy if the cell contains a nasty. On entry, C
; holds either 15 if the cell is in the top two rows, or Willy's pixel
; y-coordinate if the cell is in the bottom row.
;
; C 15 or Willy's pixel y-coordinate
; HL Address of the attribute byte in the buffer at 23552
WILLYATTR:
  LD A,(BACKGROUND)       ; Pick up the attribute byte of the background tile
                          ; in the current room from BACKGROUND
  CP (HL)                 ; Does this cell contain a background tile?
  JR NZ,WILLYATTR_0       ; Jump if not
  LD A,C                  ; Set the zero flag if we are going to retain the INK
  AND 15                  ; colour in this cell; this happens only if the cell
                          ; is in the bottom row and Willy's sprite is confined
                          ; to the top two rows
  JR Z,WILLYATTR_0        ; Jump if we are going to retain the current INK
                          ; colour in this cell
  LD A,(BACKGROUND)       ; Pick up the attribute byte of the background tile
                          ; in the current room from BACKGROUND
  OR 7                    ; Set bits 0-2, making the INK white
  LD (HL),A               ; Set the attribute byte for this cell in the buffer
                          ; at 23552
WILLYATTR_0:
  LD A,(NASTY)            ; Pick up the attribute byte of the nasty tile in the
                          ; current room from NASTY
  CP (HL)                 ; Has Willy hit a nasty?
  JP Z,KILLWILLY          ; Kill Willy if so
  RET

; Draw Willy to the screen buffer at 24576
;
; Used by the routine at WILLYATTRS.
;
; B y-coordinate offset (0, 4, 8 or 12)
DRAWWILLY:
  LD A,(PIXEL_Y)          ; Pick up Willy's pixel y-coordinate from PIXEL_Y
  ADD A,B                 ; Add the y-coordinate offset (to get Willy's true
                          ; pixel y-coordinate if he's standing on a ramp)
  LD IXh,130              ; Point IX at the entry in the screen buffer address
  LD IXl,A                ; lookup table at SBUFADDRS that corresponds to
                          ; Willy's y-coordinate
  LD A,(DMFLAGS)          ; Pick up Willy's direction and movement flags from
                          ; DMFLAGS
  AND 1                   ; Now E=0 if Willy is facing right, or 128 if he's
  RRCA                    ; facing left
  LD E,A                  ;
  LD A,(FRAME)            ; Pick up Willy's animation frame (0-3) from FRAME
  AND 3                   ; Point DE at the sprite graphic data for Willy's
  RRCA                    ; current animation frame (see MANDAT)
  RRCA                    ;
  RRCA                    ;
  OR E                    ;
  LD E,A                  ;
  LD D,157                ;
  LD A,(ROOM)             ; Pick up the number of the current room from ROOM
  CP 29                   ; Are we in the The Nightmare Room?
  JR NZ,DRAWWILLY_0       ; Jump if not
  LD D,182                ; Point DE at the graphic data for the flying pig
  LD A,E                  ; sprite (FLYINGPIG0+E)
  XOR 128                 ;
  LD E,A                  ;
DRAWWILLY_0:
  LD B,16                 ; There are 16 rows of pixels to copy
  LD A,(LOCATION)         ; Pick up Willy's screen x-coordinate (0-31) from
  AND 31                  ; LOCATION
  LD C,A                  ; Copy it to C
; This entry point is used by the routine at DRAWTOILET to draw the toilet in
; The Bathroom.
DRAWWILLY_1:
  LD A,(IX+0)             ; Set HL to the address in the screen buffer at 24576
  LD H,(IX+1)             ; that corresponds to where we are going to draw the
  OR C                    ; next pixel row of the sprite graphic
  LD L,A                  ;
  LD A,(DE)               ; Pick up a sprite graphic byte
  OR (HL)                 ; Merge it with the background
  LD (HL),A               ; Save the resultant byte to the screen buffer
  INC HL                  ; Move HL along to the next cell to the right
  INC DE                  ; Point DE at the next sprite graphic byte
  LD A,(DE)               ; Pick it up in A
  OR (HL)                 ; Merge it with the background
  LD (HL),A               ; Save the resultant byte to the screen buffer
  INC IX                  ; Point IX at the next entry in the screen buffer
  INC IX                  ; address lookup table at SBUFADDRS
  INC DE                  ; Point DE at the next sprite graphic byte
  DJNZ DRAWWILLY_1        ; Jump back until all 16 rows of pixels have been
                          ; drawn
  RET

; Print a message
;
; Used by the routines at CODESCREEN, TITLESCREEN, INITROOM, MAINLOOP and
; GAMEOVER.
;
; IX Address of the message
; C Length of the message
; DE Display file address
PRINTMSG:
  LD A,(IX+0)             ; Collect a character from the message
  CALL PRINTCHAR          ; Print it
  INC IX                  ; Point IX at the next character in the message
  INC E                   ; Point DE at the next character cell (subtracting 8
  LD A,D                  ; from D compensates for the operations performed by
  SUB 8                   ; the routine at PRINTCHAR)
  LD D,A                  ;
  DEC C                   ; Have we printed the entire message yet?
  JR NZ,PRINTMSG          ; If not, jump back to print the next character
  RET

; Print a single character
;
; Used by the routines at CODESCREEN and PRINTMSG.
;
; A ASCII code of the character
; DE Display file address
PRINTCHAR:
  LD H,7                  ; Point HL at the bitmap for the character (in the
  LD L,A                  ; ROM)
  SET 7,L                 ;
  ADD HL,HL               ;
  ADD HL,HL               ;
  ADD HL,HL               ;
  LD B,8                  ; There are eight pixel rows in a character bitmap
; This entry point is used by the routine at TITLESCREEN to draw a triangle UDG
; on the title screen, and by the routine at DRAWITEMS to draw an item in the
; current room.
PRINTCHAR_0:
  LD A,(HL)               ; Copy the character bitmap (or triangle UDG, or item
  LD (DE),A               ; graphic) to the screen (or screen buffer)
  INC HL                  ;
  INC D                   ;
  DJNZ PRINTCHAR_0        ;
  RET

; Play the theme tune (Moonlight Sonata)
;
; Used by the routine at TITLESCREEN. For each of the 99 bytes in the tune data
; table at THEMETUNE, this routine produces two notes, each lasting
; approximately 0.15s; the second note is played at half the frequency of the
; first. Returns with the zero flag reset if ENTER, 0 or the fire button is
; pressed while the tune is being played.
;
; HL THEMETUNE
PLAYTUNE:
  LD A,(HL)               ; Pick up the next byte of tune data from the table
                          ; at THEMETUNE
  CP 255                  ; Has the tune finished?
  RET Z                   ; Return (with the zero flag set) if so
  LD BC,100               ; B=0 (short note duration counter), C=100 (short
                          ; note counter)
  XOR A                   ; A=0 (border colour and speaker state)
  LD E,(HL)               ; Save the byte of tune data in E for retrieval
                          ; during the short note loop
  LD D,E                  ; Initialise D (pitch delay counter)
PLAYTUNE_0:
  OUT (254),A             ; Produce a short note (approximately 0.003s) whose
  DEC D                   ; pitch is determined by the value in E
  JR NZ,PLAYTUNE_1        ;
  LD D,E                  ;
  XOR 24                  ;
PLAYTUNE_1:
  DJNZ PLAYTUNE_0         ;
  EX AF,AF'               ; Save A briefly
  LD A,C                  ; Is the short note counter in C (which starts off at
  CP 50                   ; 100) down to 50 yet?
  JR NZ,PLAYTUNE_2        ; Jump if not
  RL E                    ; Otherwise double the value in E (which halves the
                          ; note frequency)
PLAYTUNE_2:
  EX AF,AF'               ; Restore the value of A
  DEC C                   ; Decrement the short note counter in C
  JR NZ,PLAYTUNE_0        ; Jump back unless we've finished playing 50 short
                          ; notes at the lower frequency
  CALL CHECKENTER         ; Check whether ENTER, 0 or the fire button is being
                          ; pressed
  RET NZ                  ; Return (with the zero flag reset) if it is
  INC HL                  ; Move HL along to the next byte of tune data
  JR PLAYTUNE             ; Jump back to play the next batch of 100 short notes

; Check whether ENTER, 0 or the fire button is being pressed
;
; Used by the routine at PLAYTUNE. Returns with the zero flag reset if ENTER, 0
; or the fire button on the joystick is being pressed.
CHECKENTER:
  LD A,(JOYSTICK)         ; Collect the Kempston joystick indicator from
                          ; JOYSTICK
  OR A                    ; Is the joystick connected?
  JR Z,CHECKENTER_0       ; Jump if not
  IN A,(31)               ; Collect input from the joystick
  BIT 4,A                 ; Is the fire button being pressed?
  RET NZ                  ; Return (with the zero flag reset) if so
CHECKENTER_0:
  LD BC,45054             ; Read keys H-J-K-L-ENTER and 6-7-8-9-0
  IN A,(C)                ;
  AND 1                   ; Keep only bit 0 of the result (ENTER, 0)
  CP 1                    ; Reset the zero flag if ENTER or 0 is being pressed
  RET

; Play an intro message sound effect
;
; Used by the routine at TITLESCREEN.
;
; A Value between 50 and 81
INTROSOUND:
  LD E,A                  ; Save the value of A in E for later retrieval
  LD C,254                ; We will output to port 254
INTROSOUND_0:
  LD D,A                  ; Copy A into D; bits 0-2 of D determine the initial
                          ; border colour
  RES 4,D                 ; Reset bit 4 of D (initial speaker state)
  RES 3,D                 ; Reset bit 3 of D (initial MIC state)
  LD B,E                  ; Initialise B (delay counter for the inner loop)
INTROSOUND_1:
  CP B                    ; Is it time to flip the MIC and speaker and make the
                          ; border black?
  JR NZ,INTROSOUND_2      ; Jump if not
  LD D,24                 ; Set bits 3 (MIC) and 4 (speaker) of D, and reset
                          ; bits 0-2 (black border)
INTROSOUND_2:
  OUT (C),D               ; Set the MIC state, speaker state and border colour
  DJNZ INTROSOUND_1       ; Jump back until the inner loop is finished
  DEC A                   ; Is the outer loop finished too?
  JR NZ,INTROSOUND_0      ; Jump back if not
  RET

; Unused routine
;
; This routine copies the attribute bytes for an empty room to the screen, so
; perhaps it was used during development of the game to check that the layout
; of a room had been defined correctly.
  LD HL,24064             ; Copy the attribute buffer at 24064 to the top
  LD DE,22528             ; two-thirds of the screen
  LD BC,512               ;
  LDIR                    ;
  LD HL,16384             ; Fill the top two-thirds of the display file with
  LD DE,16385             ; the byte value 24 (00011000)
  LD BC,4095              ;
  LD (HL),24              ;
  LDIR                    ;
  LD BC,65278             ; Prepare BC for reading keys SHIFT-Z-X-C-V
  IN A,(C)                ; Read these keys
  BIT 2,A                 ; Is 'X' being pressed?
  JP Z,0                  ; Jump if so to reset the machine
  JR 38671                ; Otherwise jump back to read the keyboard again

; Unused
  DEFS 232

; Attributes for the top two-thirds of the title screen
;
; Used by the routine at TITLESCREEN.
ATTRSUPPER:
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  DEFB 0,0,40,40,5,5,0,0,0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,211,211,211,0,211,211,211,0,211,211,211,0
  DEFB 40,211,211,211,37,211,211,211,0,211,211,211,0,0,0,0
  DEFB 0,0,0,0,0,211,0,0,211,0,0,0,0,211,40,40
  DEFB 45,211,37,37,36,211,0,0,0,0,211,0,0,0,0,0
  DEFB 0,0,0,0,0,211,0,0,211,211,211,0,40,211,45,45
  DEFB 37,211,211,211,36,211,211,211,0,0,211,0,0,0,0,0
  DEFB 0,0,0,0,0,211,0,0,211,0,40,40,45,211,37,37
  DEFB 36,36,4,211,36,211,0,0,0,0,211,0,0,0,0,0
  DEFB 0,0,0,0,211,211,0,0,211,211,211,45,37,211,36,36
  DEFB 4,211,211,211,36,211,211,211,0,0,211,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0,41,41,45,45,44,44,4,4
  DEFB 0,0,9,9,36,36,0,0,0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0,9,9,41,41,45,45,5,5
  DEFB 0,0,9,9,36,36,0,0,0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,211,0,8,8,211,9,211,41,211,45
  DEFB 5,5,211,9,36,211,0,0,0,211,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,211,0,0,0,211,8,211,9,211,41
  DEFB 45,45,211,9,36,211,0,0,0,211,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,211,0,211,0,211,0,211,8,211,9
  DEFB 41,41,211,9,36,211,211,211,211,211,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,211,0,211,0,211,0,211,0,211,8
  DEFB 9,9,211,9,36,36,0,211,0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,211,211,211,211,211,0,211,0,211,211
  DEFB 211,8,211,211,211,36,0,211,0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  DEFB 0,0,8,8,4,4,0,0,0,0,0,0,0,0,0,0

; Attributes for the bottom third of the screen
;
; Used by the routines at TITLESCREEN, STARTGAME and ENDPAUSE.
ATTRSLOWER:
  DEFB 70,70,70,70,70,70,70,70,70,70,70,70,70,70,70,70
  DEFB 70,70,70,70,70,70,70,70,70,70,70,70,70,70,70,70
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  DEFB 1,2,3,4,5,6,7,7,7,7,7,7,7,7,7,7
  DEFB 7,7,7,7,7,7,7,7,7,7,6,5,4,3,2,1
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  DEFB 69,69,6,6,4,4,65,65,5,5,67,67,68,68,0,0
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  DEFB 69,69,6,6,4,4,65,65,5,5,67,67,68,68,0,0
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0

; Number key graphics
;
; Used by the routine at CODESCREEN.
NUMBERKEYS:
  DEFB 127,254,195,3,191,253,191,253,255,253,255,253,255,253,249,253
  DEFB 241,255,249,255,249,255,249,255,249,253,249,253,255,251,127,254
  DEFB 127,254,199,3,191,253,191,253,255,253,255,253,255,253,241,255
  DEFB 236,255,252,255,249,255,243,255,231,255,224,253,255,243,127,254
  DEFB 127,254,198,3,191,253,191,253,191,253,255,253,255,255,241,255
  DEFB 236,255,252,255,241,255,252,255,236,255,241,253,255,231,127,254
  DEFB 127,254,198,3,191,253,191,253,191,253,191,255,255,255,253,255
  DEFB 249,255,241,255,233,255,224,255,249,255,249,253,255,155,127,254

; Attributes for the code entry screen
;
; Used by the routine at CODESCREEN.
CODEATTRS:
  DEFB 69,69,69,69,69,69,69,69,69,69,69,69,69,69,69,69
  DEFB 69,69,69,69,69,69,69,69,69,69,69,69,71,71,71,71
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  DEFB 0,0,65,121,0,66,122,0,3,59,0,4,60,0,0,0
  DEFB 7,7,0,7,7,0,7,7,0,7,7,0,0,0,0,0
  DEFB 0,0,65,65,0,66,66,0,3,3,0,4,4,0,0,0
  DEFB 7,7,0,7,7,0,7,7,0,7,7,0,0,0,0,0

; Source code remnants
;
; The source code here corresponds to the code at SEE39936.
  DEFM "NZ,ENDPAUSE"      ; [JR ]NZ,ENDPAUSE
  DEFW 19074              ; INC E
  DEFB 6                  ;
  DEFM 9,"INC",9,"E"      ;
  DEFW 19107               ; JR NZ,PAUSE
  DEFB 12                  ;
  DEFM 9,"JR",9,"NZ,PAUSE" ;
  DEFW 19140              ; INC D
  DEFB 6                  ;
  DEFM 9,"INC",9,"D"      ;
  DEFW 19173               ; JR NZ,PAUSE
  DEFB 12                  ;
  DEFM 9,"JR",9,"NZ,PAUSE" ;
  DEFW 19206              ; C[P 10]
  DEFB 12                 ;
  DEFM 9,"C"              ;

; Foot/barrel graphic data
;
; Used by the routine at GAMEOVER to display the game over sequence.
;
; The foot also appears as a guardian in The Nightmare Room.
FOOT:
  DEFB 16,128,16,128,16,128,16,128,16,128,16,128,16,128,32,128
  DEFB 32,128,72,66,136,53,132,9,128,1,128,2,67,141,60,118
; The barrel also appears as a guardian in Ballroom East and Top Landing.
BARREL:
  DEFB 55,236,119,238,0,0,111,246,239,247,239,247,213,91,219,187
  DEFB 213,91,223,251,237,119,238,247,109,118,0,0,119,238,55,236

; Maria sprite graphic data
;
; Used by the routine at BEDANDBATH to draw Maria in Master Bedroom.
;
; Maria also appears as a guardian in The Nightmare Room.
MARIA0:
  DEFB 3,0,3,192,1,224,1,64,1,224,7,128,31,248,63,252
  DEFB 55,108,20,152,15,240,15,240,15,240,2,64,2,64,6,96
MARIA1:
  DEFB 3,0,3,192,1,224,1,64,1,224,7,128,31,248,63,252
  DEFB 55,108,20,152,15,240,15,240,15,240,2,64,6,64,2,96
MARIA2:
  DEFB 3,0,3,192,1,224,1,64,1,224,7,128,31,252,63,254
  DEFB 55,102,20,146,15,240,15,240,15,240,2,64,2,64,6,96
MARIA3:
  DEFB 3,0,3,192,1,224,1,64,1,224,7,128,31,255,63,254
  DEFB 55,96,20,144,15,240,15,240,15,240,2,64,2,64,6,96

; Willy sprite graphic data
;
; Used by the routines at DRAWLIVES, GAMEOVER and DRAWWILLY.
MANDAT:
  DEFB 60,0,60,0,126,0,52,0,62,0,60,0,24,0,60,0
  DEFB 126,0,126,0,247,0,251,0,60,0,118,0,110,0,119,0
  DEFB 15,0,15,0,31,128,13,0,15,128,15,0,6,0,15,0
  DEFB 27,128,27,128,27,128,29,128,15,0,6,0,6,0,7,0
WILLYR2:
  DEFB 3,192,3,192,7,224,3,64,3,224,3,192,1,128,3,192
  DEFB 7,224,7,224,15,112,15,176,3,192,7,96,6,224,7,112
  DEFB 0,240,0,240,1,248,0,208,0,248,0,240,0,96,0,240
  DEFB 1,248,3,252,7,254,6,246,0,248,1,218,3,14,3,140
  DEFB 15,0,15,0,31,128,11,0,31,0,15,0,6,0,15,0
  DEFB 31,128,63,192,127,224,111,96,31,0,91,128,112,192,49,192
  DEFB 3,192,3,192,7,224,2,192,7,192,3,192,1,128,3,192
  DEFB 7,224,7,224,14,240,13,240,3,192,6,224,7,96,14,224
  DEFB 0,240,0,240,1,248,0,176,1,240,0,240,0,96,0,240
  DEFB 1,216,1,216,1,216,1,184,0,240,0,96,0,96,0,224
  DEFB 0,60,0,60,0,126,0,44,0,124,0,60,0,24,0,60
  DEFB 0,126,0,126,0,239,0,223,0,60,0,110,0,118,0,238

; Codes
;
; Used by the routine at CODESCREEN. These code remnants define the codes for
; grid locations A0-Q9.
CODES:
  DEC HL
  LD C,E
CODES_0:
  DEC C
  LD A,B
  JP Z,7520
  CALL 7771
  CP 44
  RET NZ
  JR CODES_0
  LD DE,16626
  LD A,(DE)
  OR A
  JP Z,6560
  INC A
  LD (16538),A
  LD (DE),A
  LD A,(HL)
  CP 135
  JR Z,CODES_1
  CALL 7770
  RET NZ
  LD A,D
  OR E
  JP NZ,7877
  INC A
  JR CODES_2
CODES_1:
  RST 16
  RET NZ
CODES_2:
  LD HL,(16622)
  EX DE,HL
  LD HL,(16618)
  LD (16546),HL
  EX DE,HL
  RET NZ
  LD A,(HL)
  OR A
  JR NZ,CODES_3
  INC HL
  INC HL
  INC HL
  INC HL
CODES_3:
  INC HL
  LD A,D
  AND E
  INC A
  JP NZ,7941
  LD A,(16605)
  DEC A
  JP Z,7614
  JP 7941
  CALL 11036
  RET NZ
  OR A
  JP Z,7754
  DEC A
  ADD A,A
  LD E,A
  CP 45
  JR C,CODES_4
  LD E,38
CODES_4:
  JP 6562
  LD DE,10
  PUSH DE
  JR Z,CODES_5
  CALL 7759
  EX DE,HL
  EX (SP),HL
  JR Z,CODES_6
  EX DE,HL
  RST 8
  INC L
  EX DE,HL
  LD HL,(16612)
  EX DE,HL
  JR Z,CODES_5
  CALL 7770
  JP NZ,6551
CODES_5:
  EX DE,HL
CODES_6:
  LD A,H
  OR L
  JP Z,7754
  LD (16612),HL
  LD (16609),A
  POP HL
  LD (16610),HL
  POP BC
  JP 6707
  CALL 9015
  LD A,(HL)
  CP 44
  CALL Z,7544
  CP 202
  CALL Z,7544
  DEC HL
  PUSH HL
  CALL 2452
  POP HL
  JR Z,40630
CODES_7:
  RST 16
  JP C,7874

; Unused
  JP 7519
  LD D,1
  CALL 7941
  OR A
  RET Z
  RST 16
  CP 149
  JR NZ,40632
  DEC D
  JR NZ,40632
  JR CODES_7
  LD A,1
  LD (16540),A
  JP 8316
  CALL 16842
  CP 35
  JR NZ,40668
  CALL 644
  LD (16540),A
  DEC HL
  RST 16
  CALL Z,8446
  JP Z,8553
  OR 32
  CP 96
  JR NZ,40709
  CALL 11009
  CP 4
  JP NC,7754
  PUSH HL
  LD HL,15360
  ADD HL,DE
  LD (16416),HL
  LD A,E
  AND 63
  LD (16550),A
  DEFS 256

; Entity definitions
;
; Used by the routine at INITROOM.
;
; The following (empty) entity definition (0) is copied into one of the entity
; buffers at ENTITYBUF for any entity specification whose first byte is zero.
ENTITYDEFS:
  DEFB 0,0,0,0,0,0,0,0
; The following entity definition (1) is used in We must perform a Quirkafleeg,
; On the Roof, Cold Store, Swimming Pool and The Beach.
ENTITY1:
  DEFB %00000011          ; Rope (bits 0-2), initially swinging right to left
                          ; (bit 7)
  DEFB 34                 ; Initial animation frame index
  DEFB 0                  ; Replaced by the x-coordinate of the top of the rope
                          ; (copied from the second byte of the entity
                          ; specification in the room definition)
  DEFB 0                  ; Unused
  DEFB 32                 ; Length
  DEFB 0                  ; Unused
  DEFB 131                ; Unused
  DEFB 54                 ; Animation frame at which the rope changes direction
; The following entity definition (2) is used in The Security Guard, Rescue
; Esmerelda, I'm sure I've seen this before.. and Up on the Battlements.
ENTITY2:
  DEFB %00000010          ; Vertical guardian (bits 0-2), animation frame
                          ; updated on every second pass (bit 4), initial
                          ; animation frame 0 (bits 5 and 6)
  DEFB %00100100          ; INK 4 (bits 0-2), BRIGHT 0 (bit 3), animation frame
                          ; mask 001 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and x-coordinate
                          ; (copied from the second byte of the entity
                          ; specification in the room definition)
  DEFB 128                ; Initial pixel y-coordinate: 64
  DEFB 2                  ; Initial pixel y-coordinate increment: 1 (moving
                          ; down)
  DEFB 191                ; Page containing the sprite graphic data: 191
  DEFB 80                 ; Minimum pixel y-coordinate: 40
  DEFB 208                ; Maximum pixel y-coordinate: 104
; The following entity definition (3) is used in The Security Guard, I'm sure
; I've seen this before.. and Up on the Battlements.
ENTITY3:
  DEFB %00000010          ; Vertical guardian (bits 0-2), animation frame
                          ; updated on every second pass (bit 4), initial
                          ; animation frame 0 (bits 5 and 6)
  DEFB %00101010          ; INK 2 (bits 0-2), BRIGHT 1 (bit 3), animation frame
                          ; mask 001 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and x-coordinate
                          ; (copied from the second byte of the entity
                          ; specification in the room definition)
  DEFB 160                ; Initial pixel y-coordinate: 80
  DEFB 4                  ; Initial pixel y-coordinate increment: 2 (moving
                          ; down)
  DEFB 191                ; Page containing the sprite graphic data: 191
  DEFB 88                 ; Minimum pixel y-coordinate: 44
  DEFB 208                ; Maximum pixel y-coordinate: 104
; The following entity definition (4) is used in The Security Guard, Rescue
; Esmerelda, I'm sure I've seen this before.. and Up on the Battlements.
ENTITY4:
  DEFB %00010010          ; Vertical guardian (bits 0-2), animation frame
                          ; updated on every pass (bit 4), initial animation
                          ; frame 0 (bits 5 and 6)
  DEFB %00100110          ; INK 6 (bits 0-2), BRIGHT 0 (bit 3), animation frame
                          ; mask 001 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and x-coordinate
                          ; (copied from the second byte of the entity
                          ; specification in the room definition)
  DEFB 96                 ; Initial pixel y-coordinate: 48
  DEFB 8                  ; Initial pixel y-coordinate increment: 4 (moving
                          ; down)
  DEFB 191                ; Page containing the sprite graphic data: 191
  DEFB 96                 ; Minimum pixel y-coordinate: 48
  DEFB 192                ; Maximum pixel y-coordinate: 96
; The following entity definition (5) is used in I'm sure I've seen this
; before.. and Up on the Battlements.
ENTITY5:
  DEFB %00000010          ; Vertical guardian (bits 0-2), animation frame
                          ; updated on every second pass (bit 4), initial
                          ; animation frame 0 (bits 5 and 6)
  DEFB %00100101          ; INK 5 (bits 0-2), BRIGHT 0 (bit 3), animation frame
                          ; mask 001 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and x-coordinate
                          ; (copied from the second byte of the entity
                          ; specification in the room definition)
  DEFB 64                 ; Initial pixel y-coordinate: 32
  DEFB 12                 ; Initial pixel y-coordinate increment: 6 (moving
                          ; down)
  DEFB 191                ; Page containing the sprite graphic data: 191
  DEFB 64                 ; Minimum pixel y-coordinate: 32
  DEFB 192                ; Maximum pixel y-coordinate: 96
; The following entity definition (6) is used in At the Foot of the MegaTree.
ENTITY6:
  DEFB %00000010          ; Vertical guardian (bits 0-2), animation frame
                          ; updated on every second pass (bit 4), initial
                          ; animation frame 0 (bits 5 and 6)
  DEFB %00100101          ; INK 5 (bits 0-2), BRIGHT 0 (bit 3), animation frame
                          ; mask 001 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and x-coordinate
                          ; (copied from the second byte of the entity
                          ; specification in the room definition)
  DEFB 64                 ; Initial pixel y-coordinate: 32
  DEFB 10                 ; Initial pixel y-coordinate increment: 5 (moving
                          ; down)
  DEFB 191                ; Page containing the sprite graphic data: 191
  DEFB 64                 ; Minimum pixel y-coordinate: 32
  DEFB 176                ; Maximum pixel y-coordinate: 88
; The following entity definition (7) is used in At the Foot of the MegaTree
; and Above the West Bedroom.
ENTITY7:
  DEFB %00000010          ; Vertical guardian (bits 0-2), animation frame
                          ; updated on every second pass (bit 4), initial
                          ; animation frame 0 (bits 5 and 6)
  DEFB %01100101          ; INK 5 (bits 0-2), BRIGHT 0 (bit 3), animation frame
                          ; mask 011 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and x-coordinate
                          ; (copied from the second byte of the entity
                          ; specification in the room definition)
  DEFB 100                ; Initial pixel y-coordinate: 50
  DEFB 244                ; Initial pixel y-coordinate increment: -6 (moving
                          ; up)
  DEFB 191                ; Page containing the sprite graphic data: 191
  DEFB 32                 ; Minimum pixel y-coordinate: 16
  DEFB 160                ; Maximum pixel y-coordinate: 80
; The following entity definition (8) is used in At the Foot of the MegaTree.
ENTITY8:
  DEFB %00000010          ; Vertical guardian (bits 0-2), animation frame
                          ; updated on every second pass (bit 4), initial
                          ; animation frame 0 (bits 5 and 6)
  DEFB %01101010          ; INK 2 (bits 0-2), BRIGHT 1 (bit 3), animation frame
                          ; mask 011 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and x-coordinate
                          ; (copied from the second byte of the entity
                          ; specification in the room definition)
  DEFB 128                ; Initial pixel y-coordinate: 64
  DEFB 6                  ; Initial pixel y-coordinate increment: 3 (moving
                          ; down)
  DEFB 191                ; Page containing the sprite graphic data: 191
  DEFB 38                 ; Minimum pixel y-coordinate: 19
  DEFB 176                ; Maximum pixel y-coordinate: 88
; The following entity definition (9) is used in Inside the MegaTrunk and On a
; Branch Over the Drive.
ENTITY9:
  DEFB %00000010          ; Vertical guardian (bits 0-2), animation frame
                          ; updated on every second pass (bit 4), initial
                          ; animation frame 0 (bits 5 and 6)
  DEFB %00101011          ; INK 3 (bits 0-2), BRIGHT 1 (bit 3), animation frame
                          ; mask 001 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and x-coordinate
                          ; (copied from the second byte of the entity
                          ; specification in the room definition)
  DEFB 160                ; Initial pixel y-coordinate: 80
  DEFB 4                  ; Initial pixel y-coordinate increment: 2 (moving
                          ; down)
  DEFB 191                ; Page containing the sprite graphic data: 191
  DEFB 128                ; Minimum pixel y-coordinate: 64
  DEFB 224                ; Maximum pixel y-coordinate: 112
; The following entity definition (10) is used in The Off Licence.
ENTITY10:
  DEFB %00010010          ; Vertical guardian (bits 0-2), animation frame
                          ; updated on every pass (bit 4), initial animation
                          ; frame 0 (bits 5 and 6)
  DEFB %01101101          ; INK 5 (bits 0-2), BRIGHT 1 (bit 3), animation frame
                          ; mask 011 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and x-coordinate
                          ; (copied from the second byte of the entity
                          ; specification in the room definition)
  DEFB 96                 ; Initial pixel y-coordinate: 48
  DEFB 8                  ; Initial pixel y-coordinate increment: 4 (moving
                          ; down)
  DEFB 190                ; Page containing the sprite graphic data: 190
  DEFB 16                 ; Minimum pixel y-coordinate: 8
  DEFB 208                ; Maximum pixel y-coordinate: 104
; The following entity definition (11) is used in Dr Jones will never believe
; this and Nomen Luni.
ENTITY11:
  DEFB %00010010          ; Vertical guardian (bits 0-2), animation frame
                          ; updated on every pass (bit 4), initial animation
                          ; frame 0 (bits 5 and 6)
  DEFB %01100110          ; INK 6 (bits 0-2), BRIGHT 0 (bit 3), animation frame
                          ; mask 011 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and x-coordinate
                          ; (copied from the second byte of the entity
                          ; specification in the room definition)
  DEFB 192                ; Initial pixel y-coordinate: 96
  DEFB 246                ; Initial pixel y-coordinate increment: -5 (moving
                          ; up)
  DEFB 191                ; Page containing the sprite graphic data: 191
  DEFB 112                ; Minimum pixel y-coordinate: 56
  DEFB 192                ; Maximum pixel y-coordinate: 96
; The following entity definition (12) is used in The Off Licence.
ENTITY12:
  DEFB %00000001          ; Horizontal guardian (bits 0-2), initial animation
                          ; frame 0 (bits 5 and 6), initially moving right to
                          ; left (bit 7)
  DEFB %01100100          ; INK 4 (bits 0-2), BRIGHT 0 (bit 3), animation frame
                          ; mask 011 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and initial
                          ; x-coordinate (copied from the second byte of the
                          ; entity specification in the room definition)
  DEFB 112                ; Pixel y-coordinate: 56
  DEFB 1                  ; Unused
  DEFB 190                ; Page containing the sprite graphic data: 190
  DEFB 19                 ; Minimum x-coordinate: 19
  DEFB 29                 ; Maximum x-coordinate: 29
; The following entity definition (13) is used in Rescue Esmerelda, I'm sure
; I've seen this before.. and We must perform a Quirkafleeg.
ENTITY13:
  DEFB %00000001          ; Horizontal guardian (bits 0-2), initial animation
                          ; frame 0 (bits 5 and 6), initially moving right to
                          ; left (bit 7)
  DEFB %11100100          ; INK 4 (bits 0-2), BRIGHT 0 (bit 3), animation frame
                          ; mask 111 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and initial
                          ; x-coordinate (copied from the second byte of the
                          ; entity specification in the room definition)
  DEFB 64                 ; Pixel y-coordinate: 32
  DEFB 0                  ; Unused
  DEFB 188                ; Page containing the sprite graphic data: 188
  DEFB 0                  ; Minimum x-coordinate: 0
  DEFB 9                  ; Maximum x-coordinate: 9
; The following entity definition (14) is used in The Bridge.
ENTITY14:
  DEFB %00010010          ; Vertical guardian (bits 0-2), animation frame
                          ; updated on every pass (bit 4), initial animation
                          ; frame 0 (bits 5 and 6)
  DEFB %01101100          ; INK 4 (bits 0-2), BRIGHT 1 (bit 3), animation frame
                          ; mask 011 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and x-coordinate
                          ; (copied from the second byte of the entity
                          ; specification in the room definition)
  DEFB 48                 ; Initial pixel y-coordinate: 24
  DEFB 12                 ; Initial pixel y-coordinate increment: 6 (moving
                          ; down)
  DEFB 185                ; Page containing the sprite graphic data: 185
  DEFB 0                  ; Minimum pixel y-coordinate: 0
  DEFB 192                ; Maximum pixel y-coordinate: 96
; The following entity definition (15) is used in Rescue Esmerelda.
ENTITY15:
  DEFB %00010010          ; Vertical guardian (bits 0-2), animation frame
                          ; updated on every pass (bit 4), initial animation
                          ; frame 0 (bits 5 and 6)
  DEFB %00100111          ; INK 7 (bits 0-2), BRIGHT 0 (bit 3), animation frame
                          ; mask 001 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and x-coordinate
                          ; (copied from the second byte of the entity
                          ; specification in the room definition)
  DEFB 16                 ; Initial pixel y-coordinate: 8
  DEFB 4                  ; Initial pixel y-coordinate increment: 2 (moving
                          ; down)
  DEFB 176                ; Page containing the sprite graphic data: 176
  DEFB 0                  ; Minimum pixel y-coordinate: 0
  DEFB 32                 ; Maximum pixel y-coordinate: 16
; The following entity definition (16) is used in Entrance to Hades, The Chapel
; and Priests' Hole.
ENTITY16:
  DEFB %00000010          ; Vertical guardian (bits 0-2), initial animation
                          ; frame 0 (bits 5 and 6)
  DEFB %00000100          ; INK 4 (bits 0-2), BRIGHT 0 (bit 3), animation frame
                          ; mask 000 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and x-coordinate
                          ; (copied from the second byte of the entity
                          ; specification in the room definition)
  DEFB 96                 ; Initial pixel y-coordinate: 48
  DEFB 2                  ; Initial pixel y-coordinate increment: 1 (moving
                          ; down)
  DEFB 186                ; Page containing the sprite graphic data: 186
  DEFB 0                  ; Minimum pixel y-coordinate: 0
  DEFB 176                ; Maximum pixel y-coordinate: 88
; The following entity definition (17) is used in Entrance to Hades, The Chapel
; and Priests' Hole.
ENTITY17:
  DEFB %00000010          ; Vertical guardian (bits 0-2), initial animation
                          ; frame 0 (bits 5 and 6)
  DEFB %00000100          ; INK 4 (bits 0-2), BRIGHT 0 (bit 3), animation frame
                          ; mask 000 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and x-coordinate
                          ; (copied from the second byte of the entity
                          ; specification in the room definition)
  DEFB 96                 ; Initial pixel y-coordinate: 48
  DEFB 2                  ; Initial pixel y-coordinate increment: 1 (moving
                          ; down)
  DEFB 186                ; Page containing the sprite graphic data: 186
  DEFB 0                  ; Minimum pixel y-coordinate: 0
  DEFB 176                ; Maximum pixel y-coordinate: 88
; The following entity definition (18) is used in Entrance to Hades, The Chapel
; and Priests' Hole.
ENTITY18:
  DEFB %00010010          ; Vertical guardian (bits 0-2), animation frame
                          ; updated on every pass (bit 4), initial animation
                          ; frame 0 (bits 5 and 6)
  DEFB %00100100          ; INK 4 (bits 0-2), BRIGHT 0 (bit 3), animation frame
                          ; mask 001 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and x-coordinate
                          ; (copied from the second byte of the entity
                          ; specification in the room definition)
  DEFB 128                ; Initial pixel y-coordinate: 64
  DEFB 2                  ; Initial pixel y-coordinate increment: 1 (moving
                          ; down)
  DEFB 186                ; Page containing the sprite graphic data: 186
  DEFB 32                 ; Minimum pixel y-coordinate: 16
  DEFB 208                ; Maximum pixel y-coordinate: 104
; The following entity definition (19) is used in Ballroom East and Top
; Landing.
ENTITY19:
  DEFB %00000010          ; Vertical guardian (bits 0-2), initial animation
                          ; frame 0 (bits 5 and 6)
  DEFB %00001010          ; INK 2 (bits 0-2), BRIGHT 1 (bit 3), animation frame
                          ; mask 000 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and x-coordinate
                          ; (copied from the second byte of the entity
                          ; specification in the room definition)
  DEFB 32                 ; Initial pixel y-coordinate: 16
  DEFB 2                  ; Initial pixel y-coordinate increment: 1 (moving
                          ; down)
  DEFB 156                ; Page containing the sprite graphic data: 156
  DEFB 32                 ; Minimum pixel y-coordinate: 16
  DEFB 96                 ; Maximum pixel y-coordinate: 48
; The following entity definition (20) is used in The Bridge and West  Wing.
ENTITY20:
  DEFB %00000001          ; Horizontal guardian (bits 0-2), initial animation
                          ; frame 0 (bits 5 and 6), initially moving right to
                          ; left (bit 7)
  DEFB %11101010          ; INK 2 (bits 0-2), BRIGHT 1 (bit 3), animation frame
                          ; mask 111 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and initial
                          ; x-coordinate (copied from the second byte of the
                          ; entity specification in the room definition)
  DEFB 160                ; Pixel y-coordinate: 80
  DEFB 0                  ; Unused
  DEFB 188                ; Page containing the sprite graphic data: 188
  DEFB 0                  ; Minimum x-coordinate: 0
  DEFB 10                 ; Maximum x-coordinate: 10
; The following entity definition (21) is used in The Bridge, The Drive and
; Ballroom East.
ENTITY21:
  DEFB %10000001          ; Horizontal guardian (bits 0-2), initial animation
                          ; frame 0 (bits 5 and 6), initially moving left to
                          ; right (bit 7)
  DEFB %11100011          ; INK 3 (bits 0-2), BRIGHT 0 (bit 3), animation frame
                          ; mask 111 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and initial
                          ; x-coordinate (copied from the second byte of the
                          ; entity specification in the room definition)
  DEFB 160                ; Pixel y-coordinate: 80
  DEFB 0                  ; Unused
  DEFB 188                ; Page containing the sprite graphic data: 188
  DEFB 14                 ; Minimum x-coordinate: 14
  DEFB 29                 ; Maximum x-coordinate: 29
; The following entity definition (22) is used in The Drive and Inside the
; MegaTrunk.
ENTITY22:
  DEFB %00000001          ; Horizontal guardian (bits 0-2), initial animation
                          ; frame 0 (bits 5 and 6), initially moving right to
                          ; left (bit 7)
  DEFB %11100100          ; INK 4 (bits 0-2), BRIGHT 0 (bit 3), animation frame
                          ; mask 111 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and initial
                          ; x-coordinate (copied from the second byte of the
                          ; entity specification in the room definition)
  DEFB 176                ; Pixel y-coordinate: 88
  DEFB 1                  ; Unused
  DEFB 188                ; Page containing the sprite graphic data: 188
  DEFB 16                 ; Minimum x-coordinate: 16
  DEFB 29                 ; Maximum x-coordinate: 29
; The following entity definition (23) is used in The Drive.
ENTITY23:
  DEFB %00000001          ; Horizontal guardian (bits 0-2), initial animation
                          ; frame 0 (bits 5 and 6), initially moving right to
                          ; left (bit 7)
  DEFB %11100110          ; INK 6 (bits 0-2), BRIGHT 0 (bit 3), animation frame
                          ; mask 111 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and initial
                          ; x-coordinate (copied from the second byte of the
                          ; entity specification in the room definition)
  DEFB 128                ; Pixel y-coordinate: 64
  DEFB 0                  ; Unused
  DEFB 188                ; Page containing the sprite graphic data: 188
  DEFB 5                  ; Minimum x-coordinate: 5
  DEFB 29                 ; Maximum x-coordinate: 29
; The following entity definition (24) is used in The Drive, Inside the
; MegaTrunk and Tree Top.
ENTITY24:
  DEFB %00000001          ; Horizontal guardian (bits 0-2), initial animation
                          ; frame 0 (bits 5 and 6), initially moving right to
                          ; left (bit 7)
  DEFB %11101101          ; INK 5 (bits 0-2), BRIGHT 1 (bit 3), animation frame
                          ; mask 111 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and initial
                          ; x-coordinate (copied from the second byte of the
                          ; entity specification in the room definition)
  DEFB 80                 ; Pixel y-coordinate: 40
  DEFB 0                  ; Unused
  DEFB 188                ; Page containing the sprite graphic data: 188
  DEFB 0                  ; Minimum x-coordinate: 0
  DEFB 10                 ; Maximum x-coordinate: 10
; The following entity definition (25) is used in Out on a limb.
ENTITY25:
  DEFB %00000001          ; Horizontal guardian (bits 0-2), initial animation
                          ; frame 0 (bits 5 and 6), initially moving right to
                          ; left (bit 7)
  DEFB %11101011          ; INK 3 (bits 0-2), BRIGHT 1 (bit 3), animation frame
                          ; mask 111 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and initial
                          ; x-coordinate (copied from the second byte of the
                          ; entity specification in the room definition)
  DEFB 112                ; Pixel y-coordinate: 56
  DEFB 0                  ; Unused
  DEFB 188                ; Page containing the sprite graphic data: 188
  DEFB 14                 ; Minimum x-coordinate: 14
  DEFB 23                 ; Maximum x-coordinate: 23
; The following entity definition (26) is used in Under the MegaTree, The Hall,
; Tree Top and Emergency Generator.
ENTITY26:
  DEFB %00000001          ; Horizontal guardian (bits 0-2), initial animation
                          ; frame 0 (bits 5 and 6), initially moving right to
                          ; left (bit 7)
  DEFB %11101010          ; INK 2 (bits 0-2), BRIGHT 1 (bit 3), animation frame
                          ; mask 111 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and initial
                          ; x-coordinate (copied from the second byte of the
                          ; entity specification in the room definition)
  DEFB 48                 ; Pixel y-coordinate: 24
  DEFB 0                  ; Unused
  DEFB 182                ; Page containing the sprite graphic data: 182
  DEFB 5                  ; Minimum x-coordinate: 5
  DEFB 30                 ; Maximum x-coordinate: 30
; The following entity definition (27) is used in On a Branch Over the Drive,
; Orangery, Dr Jones will never believe this and The Yacht.
ENTITY27:
  DEFB %00010010          ; Vertical guardian (bits 0-2), animation frame
                          ; updated on every pass (bit 4), initial animation
                          ; frame 0 (bits 5 and 6)
  DEFB %01101010          ; INK 2 (bits 0-2), BRIGHT 1 (bit 3), animation frame
                          ; mask 011 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and x-coordinate
                          ; (copied from the second byte of the entity
                          ; specification in the room definition)
  DEFB 144                ; Initial pixel y-coordinate: 72
  DEFB 252                ; Initial pixel y-coordinate increment: -2 (moving
                          ; up)
  DEFB 178                ; Page containing the sprite graphic data: 178
  DEFB 128                ; Minimum pixel y-coordinate: 64
  DEFB 208                ; Maximum pixel y-coordinate: 104
; The following entity definition (28) is used in Under the Drive and West Wing
; Roof.
ENTITY28:
  DEFB %00000001          ; Horizontal guardian (bits 0-2), initial animation
                          ; frame 0 (bits 5 and 6), initially moving right to
                          ; left (bit 7)
  DEFB %11100110          ; INK 6 (bits 0-2), BRIGHT 0 (bit 3), animation frame
                          ; mask 111 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and initial
                          ; x-coordinate (copied from the second byte of the
                          ; entity specification in the room definition)
  DEFB 176                ; Pixel y-coordinate: 88
  DEFB 0                  ; Unused
  DEFB 188                ; Page containing the sprite graphic data: 188
  DEFB 12                 ; Minimum x-coordinate: 12
  DEFB 29                 ; Maximum x-coordinate: 29
; The following entity definition (29) is used in On top of the house, Under
; the Drive, Nomen Luni and Back Stairway.
ENTITY29:
  DEFB %00010010          ; Vertical guardian (bits 0-2), animation frame
                          ; updated on every pass (bit 4), initial animation
                          ; frame 0 (bits 5 and 6)
  DEFB %01100101          ; INK 5 (bits 0-2), BRIGHT 0 (bit 3), animation frame
                          ; mask 011 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and x-coordinate
                          ; (copied from the second byte of the entity
                          ; specification in the room definition)
  DEFB 48                 ; Initial pixel y-coordinate: 24
  DEFB 4                  ; Initial pixel y-coordinate increment: 2 (moving
                          ; down)
  DEFB 178                ; Page containing the sprite graphic data: 178
  DEFB 0                  ; Minimum pixel y-coordinate: 0
  DEFB 128                ; Maximum pixel y-coordinate: 64
; The following entity definition (30) is used in Tree Root and West Bedroom.
ENTITY30:
  DEFB %00010010          ; Vertical guardian (bits 0-2), animation frame
                          ; updated on every pass (bit 4), initial animation
                          ; frame 0 (bits 5 and 6)
  DEFB %11100101          ; INK 5 (bits 0-2), BRIGHT 0 (bit 3), animation frame
                          ; mask 111 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and x-coordinate
                          ; (copied from the second byte of the entity
                          ; specification in the room definition)
  DEFB 64                 ; Initial pixel y-coordinate: 32
  DEFB 252                ; Initial pixel y-coordinate increment: -2 (moving
                          ; up)
  DEFB 173                ; Page containing the sprite graphic data: 173
  DEFB 0                  ; Minimum pixel y-coordinate: 0
  DEFB 128                ; Maximum pixel y-coordinate: 64
; The following entity definition (31) is used in Tree Root.
ENTITY31:
  DEFB %00010010          ; Vertical guardian (bits 0-2), animation frame
                          ; updated on every pass (bit 4), initial animation
                          ; frame 0 (bits 5 and 6)
  DEFB %00100011          ; INK 3 (bits 0-2), BRIGHT 0 (bit 3), animation frame
                          ; mask 001 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and x-coordinate
                          ; (copied from the second byte of the entity
                          ; specification in the room definition)
  DEFB 144                ; Initial pixel y-coordinate: 72
  DEFB 4                  ; Initial pixel y-coordinate increment: 2 (moving
                          ; down)
  DEFB 183                ; Page containing the sprite graphic data: 183
  DEFB 64                 ; Minimum pixel y-coordinate: 32
  DEFB 160                ; Maximum pixel y-coordinate: 80
; The following entity definition (32) is used in Under the MegaTree.
ENTITY32:
  DEFB %00000001          ; Horizontal guardian (bits 0-2), initial animation
                          ; frame 0 (bits 5 and 6), initially moving right to
                          ; left (bit 7)
  DEFB %11100101          ; INK 5 (bits 0-2), BRIGHT 0 (bit 3), animation frame
                          ; mask 111 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and initial
                          ; x-coordinate (copied from the second byte of the
                          ; entity specification in the room definition)
  DEFB 208                ; Pixel y-coordinate: 104
  DEFB 1                  ; Unused
  DEFB 184                ; Page containing the sprite graphic data: 184
  DEFB 0                  ; Minimum x-coordinate: 0
  DEFB 30                 ; Maximum x-coordinate: 30
; The following entity definition (33) is used in Ballroom West.
ENTITY33:
  DEFB %00000001          ; Horizontal guardian (bits 0-2), initial animation
                          ; frame 0 (bits 5 and 6), initially moving right to
                          ; left (bit 7)
  DEFB %11100011          ; INK 3 (bits 0-2), BRIGHT 0 (bit 3), animation frame
                          ; mask 111 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and initial
                          ; x-coordinate (copied from the second byte of the
                          ; entity specification in the room definition)
  DEFB 176                ; Pixel y-coordinate: 88
  DEFB 0                  ; Unused
  DEFB 184                ; Page containing the sprite graphic data: 184
  DEFB 16                 ; Minimum x-coordinate: 16
  DEFB 26                 ; Maximum x-coordinate: 26
; The following entity definition (34) is used in On the Roof.
ENTITY34:
  DEFB %10000001          ; Horizontal guardian (bits 0-2), initial animation
                          ; frame 0 (bits 5 and 6), initially moving left to
                          ; right (bit 7)
  DEFB %11100101          ; INK 5 (bits 0-2), BRIGHT 0 (bit 3), animation frame
                          ; mask 111 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and initial
                          ; x-coordinate (copied from the second byte of the
                          ; entity specification in the room definition)
  DEFB 208                ; Pixel y-coordinate: 104
  DEFB 1                  ; Unused
  DEFB 184                ; Page containing the sprite graphic data: 184
  DEFB 14                 ; Minimum x-coordinate: 14
  DEFB 24                 ; Maximum x-coordinate: 24
; The following entity definition (35) is used in Tree Root.
ENTITY35:
  DEFB %10000001          ; Horizontal guardian (bits 0-2), initial animation
                          ; frame 0 (bits 5 and 6), initially moving left to
                          ; right (bit 7)
  DEFB %11101010          ; INK 2 (bits 0-2), BRIGHT 1 (bit 3), animation frame
                          ; mask 111 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and initial
                          ; x-coordinate (copied from the second byte of the
                          ; entity specification in the room definition)
  DEFB 144                ; Pixel y-coordinate: 72
  DEFB 0                  ; Unused
  DEFB 184                ; Page containing the sprite graphic data: 184
  DEFB 15                 ; Minimum x-coordinate: 15
  DEFB 23                 ; Maximum x-coordinate: 23
; The following entity definition (36) is used in The Drive, Top Landing and
; Back Stairway.
ENTITY36:
  DEFB %10000001          ; Horizontal guardian (bits 0-2), initial animation
                          ; frame 0 (bits 5 and 6), initially moving left to
                          ; right (bit 7)
  DEFB %01100101          ; INK 5 (bits 0-2), BRIGHT 0 (bit 3), animation frame
                          ; mask 011 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and initial
                          ; x-coordinate (copied from the second byte of the
                          ; entity specification in the room definition)
  DEFB 208                ; Pixel y-coordinate: 104
  DEFB 0                  ; Unused
  DEFB 175                ; Page containing the sprite graphic data: 175
  DEFB 10                 ; Minimum x-coordinate: 10
  DEFB 30                 ; Maximum x-coordinate: 30
; The following entity definition (37) is used in Priests' Hole.
ENTITY37:
  DEFB %00000001          ; Horizontal guardian (bits 0-2), initial animation
                          ; frame 0 (bits 5 and 6), initially moving right to
                          ; left (bit 7)
  DEFB %11101010          ; INK 2 (bits 0-2), BRIGHT 1 (bit 3), animation frame
                          ; mask 111 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and initial
                          ; x-coordinate (copied from the second byte of the
                          ; entity specification in the room definition)
  DEFB 160                ; Pixel y-coordinate: 80
  DEFB 0                  ; Unused
  DEFB 188                ; Page containing the sprite graphic data: 188
  DEFB 8                  ; Minimum x-coordinate: 8
  DEFB 24                 ; Maximum x-coordinate: 24
; The following entity definition (38) is used in Halfway up the East Wall.
ENTITY38:
  DEFB %00000001          ; Horizontal guardian (bits 0-2), initial animation
                          ; frame 0 (bits 5 and 6), initially moving right to
                          ; left (bit 7)
  DEFB %01101110          ; INK 6 (bits 0-2), BRIGHT 1 (bit 3), animation frame
                          ; mask 011 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and initial
                          ; x-coordinate (copied from the second byte of the
                          ; entity specification in the room definition)
  DEFB 96                 ; Pixel y-coordinate: 48
  DEFB 0                  ; Unused
  DEFB 174                ; Page containing the sprite graphic data: 174
  DEFB 2                  ; Minimum x-coordinate: 2
  DEFB 7                  ; Maximum x-coordinate: 7
; The following entity definition (39) is used in Cuckoo's Nest and Under the
; Roof.
ENTITY39:
  DEFB %00000001          ; Horizontal guardian (bits 0-2), initial animation
                          ; frame 0 (bits 5 and 6), initially moving right to
                          ; left (bit 7)
  DEFB %01100101          ; INK 5 (bits 0-2), BRIGHT 0 (bit 3), animation frame
                          ; mask 011 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and initial
                          ; x-coordinate (copied from the second byte of the
                          ; entity specification in the room definition)
  DEFB 128                ; Pixel y-coordinate: 64
  DEFB 0                  ; Unused
  DEFB 175                ; Page containing the sprite graphic data: 175
  DEFB 12                 ; Minimum x-coordinate: 12
  DEFB 18                 ; Maximum x-coordinate: 18
; The following entity definition (40) is used in Ballroom East, Ballroom West
; and Tree Root.
ENTITY40:
  DEFB %00010010          ; Vertical guardian (bits 0-2), animation frame
                          ; updated on every pass (bit 4), initial animation
                          ; frame 0 (bits 5 and 6)
  DEFB %01000101          ; INK 5 (bits 0-2), BRIGHT 0 (bit 3), animation frame
                          ; mask 010 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and x-coordinate
                          ; (copied from the second byte of the entity
                          ; specification in the room definition)
  DEFB 112                ; Initial pixel y-coordinate: 56
  DEFB 4                  ; Initial pixel y-coordinate increment: 2 (moving
                          ; down)
  DEFB 186                ; Page containing the sprite graphic data: 186
  DEFB 96                 ; Minimum pixel y-coordinate: 48
  DEFB 192                ; Maximum pixel y-coordinate: 96
; The following entity definition (41) is used in Ballroom East.
ENTITY41:
  DEFB %00010010          ; Vertical guardian (bits 0-2), animation frame
                          ; updated on every pass (bit 4), initial animation
                          ; frame 0 (bits 5 and 6)
  DEFB %01100110          ; INK 6 (bits 0-2), BRIGHT 0 (bit 3), animation frame
                          ; mask 011 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and x-coordinate
                          ; (copied from the second byte of the entity
                          ; specification in the room definition)
  DEFB 150                ; Initial pixel y-coordinate: 75
  DEFB 2                  ; Initial pixel y-coordinate increment: 1 (moving
                          ; down)
  DEFB 186                ; Page containing the sprite graphic data: 186
  DEFB 144                ; Minimum pixel y-coordinate: 72
  DEFB 192                ; Maximum pixel y-coordinate: 96
; The following entity definition (42) is used in Under the MegaTree, Ballroom
; East and Ballroom West.
ENTITY42:
  DEFB %00010010          ; Vertical guardian (bits 0-2), animation frame
                          ; updated on every pass (bit 4), initial animation
                          ; frame 0 (bits 5 and 6)
  DEFB %01001011          ; INK 3 (bits 0-2), BRIGHT 1 (bit 3), animation frame
                          ; mask 010 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and x-coordinate
                          ; (copied from the second byte of the entity
                          ; specification in the room definition)
  DEFB 144                ; Initial pixel y-coordinate: 72
  DEFB 250                ; Initial pixel y-coordinate increment: -3 (moving
                          ; up)
  DEFB 186                ; Page containing the sprite graphic data: 186
  DEFB 96                 ; Minimum pixel y-coordinate: 48
  DEFB 174                ; Maximum pixel y-coordinate: 87
; The following entity definition (43) is not used.
ENTITY43:
  DEFB %00000010          ; Vertical guardian (bits 0-2), animation frame
                          ; updated on every second pass (bit 4), initial
                          ; animation frame 0 (bits 5 and 6)
  DEFB %01001010          ; INK 2 (bits 0-2), BRIGHT 1 (bit 3), animation frame
                          ; mask 010 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and x-coordinate
                          ; (copied from the second byte of the entity
                          ; specification in the room definition)
  DEFB 176                ; Initial pixel y-coordinate: 88
  DEFB 4                  ; Initial pixel y-coordinate increment: 2 (moving
                          ; down)
  DEFB 186                ; Page containing the sprite graphic data: 186
  DEFB 144                ; Minimum pixel y-coordinate: 72
  DEFB 184                ; Maximum pixel y-coordinate: 92
; The following entity definition (44) is used in The Off Licence and Inside
; the MegaTrunk.
ENTITY44:
  DEFB %00010010          ; Vertical guardian (bits 0-2), animation frame
                          ; updated on every pass (bit 4), initial animation
                          ; frame 0 (bits 5 and 6)
  DEFB %01001010          ; INK 2 (bits 0-2), BRIGHT 1 (bit 3), animation frame
                          ; mask 010 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and x-coordinate
                          ; (copied from the second byte of the entity
                          ; specification in the room definition)
  DEFB 64                 ; Initial pixel y-coordinate: 32
  DEFB 4                  ; Initial pixel y-coordinate increment: 2 (moving
                          ; down)
  DEFB 186                ; Page containing the sprite graphic data: 186
  DEFB 0                  ; Minimum pixel y-coordinate: 0
  DEFB 208                ; Maximum pixel y-coordinate: 104
; The following entity definition (45) is used in Out on a limb and East Wall
; Base.
ENTITY45:
  DEFB %00010010          ; Vertical guardian (bits 0-2), animation frame
                          ; updated on every pass (bit 4), initial animation
                          ; frame 0 (bits 5 and 6)
  DEFB %01000101          ; INK 5 (bits 0-2), BRIGHT 0 (bit 3), animation frame
                          ; mask 010 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and x-coordinate
                          ; (copied from the second byte of the entity
                          ; specification in the room definition)
  DEFB 176                ; Initial pixel y-coordinate: 88
  DEFB 8                  ; Initial pixel y-coordinate increment: 4 (moving
                          ; down)
  DEFB 186                ; Page containing the sprite graphic data: 186
  DEFB 0                  ; Minimum pixel y-coordinate: 0
  DEFB 208                ; Maximum pixel y-coordinate: 104
; The following entity definition (46) is used in Tree Top.
ENTITY46:
  DEFB %00000001          ; Horizontal guardian (bits 0-2), initial animation
                          ; frame 0 (bits 5 and 6), initially moving right to
                          ; left (bit 7)
  DEFB %01100110          ; INK 6 (bits 0-2), BRIGHT 0 (bit 3), animation frame
                          ; mask 011 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and initial
                          ; x-coordinate (copied from the second byte of the
                          ; entity specification in the room definition)
  DEFB 160                ; Pixel y-coordinate: 80
  DEFB 0                  ; Unused
  DEFB 187                ; Page containing the sprite graphic data: 187
  DEFB 0                  ; Minimum x-coordinate: 0
  DEFB 19                 ; Maximum x-coordinate: 19
; The following entity definition (47) is used in Inside the MegaTrunk.
ENTITY47:
  DEFB %00000001          ; Horizontal guardian (bits 0-2), initial animation
                          ; frame 0 (bits 5 and 6), initially moving right to
                          ; left (bit 7)
  DEFB %01101110          ; INK 6 (bits 0-2), BRIGHT 1 (bit 3), animation frame
                          ; mask 011 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and initial
                          ; x-coordinate (copied from the second byte of the
                          ; entity specification in the room definition)
  DEFB 48                 ; Pixel y-coordinate: 24
  DEFB 0                  ; Unused
  DEFB 187                ; Page containing the sprite graphic data: 187
  DEFB 17                 ; Minimum x-coordinate: 17
  DEFB 30                 ; Maximum x-coordinate: 30
; The following entity definition (48) is used in The Kitchen and West of
; Kitchen.
ENTITY48:
  DEFB %00000010          ; Vertical guardian (bits 0-2), animation frame
                          ; updated on every second pass (bit 4), initial
                          ; animation frame 0 (bits 5 and 6)
  DEFB %00101011          ; INK 3 (bits 0-2), BRIGHT 1 (bit 3), animation frame
                          ; mask 001 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and x-coordinate
                          ; (copied from the second byte of the entity
                          ; specification in the room definition)
  DEFB 64                 ; Initial pixel y-coordinate: 32
  DEFB 6                  ; Initial pixel y-coordinate increment: 3 (moving
                          ; down)
  DEFB 183                ; Page containing the sprite graphic data: 183
  DEFB 16                 ; Minimum pixel y-coordinate: 8
  DEFB 208                ; Maximum pixel y-coordinate: 104
; The following entity definition (49) is used in The Kitchen and West of
; Kitchen.
ENTITY49:
  DEFB %00010010          ; Vertical guardian (bits 0-2), animation frame
                          ; updated on every pass (bit 4), initial animation
                          ; frame 0 (bits 5 and 6)
  DEFB %00100110          ; INK 6 (bits 0-2), BRIGHT 0 (bit 3), animation frame
                          ; mask 001 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and x-coordinate
                          ; (copied from the second byte of the entity
                          ; specification in the room definition)
  DEFB 192                ; Initial pixel y-coordinate: 96
  DEFB 4                  ; Initial pixel y-coordinate increment: 2 (moving
                          ; down)
  DEFB 183                ; Page containing the sprite graphic data: 183
  DEFB 0                  ; Minimum pixel y-coordinate: 0
  DEFB 208                ; Maximum pixel y-coordinate: 104
; The following entity definition (50) is used in The Kitchen and West of
; Kitchen.
ENTITY50:
  DEFB %00010010          ; Vertical guardian (bits 0-2), animation frame
                          ; updated on every pass (bit 4), initial animation
                          ; frame 0 (bits 5 and 6)
  DEFB %00100100          ; INK 4 (bits 0-2), BRIGHT 0 (bit 3), animation frame
                          ; mask 001 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and x-coordinate
                          ; (copied from the second byte of the entity
                          ; specification in the room definition)
  DEFB 128                ; Initial pixel y-coordinate: 64
  DEFB 248                ; Initial pixel y-coordinate increment: -4 (moving
                          ; up)
  DEFB 183                ; Page containing the sprite graphic data: 183
  DEFB 0                  ; Minimum pixel y-coordinate: 0
  DEFB 208                ; Maximum pixel y-coordinate: 104
; The following entity definition (51) is used in West Bedroom and Above the
; West Bedroom.
ENTITY51:
  DEFB %00010010          ; Vertical guardian (bits 0-2), animation frame
                          ; updated on every pass (bit 4), initial animation
                          ; frame 0 (bits 5 and 6)
  DEFB %00101010          ; INK 2 (bits 0-2), BRIGHT 1 (bit 3), animation frame
                          ; mask 001 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and x-coordinate
                          ; (copied from the second byte of the entity
                          ; specification in the room definition)
  DEFB 48                 ; Initial pixel y-coordinate: 24
  DEFB 2                  ; Initial pixel y-coordinate increment: 1 (moving
                          ; down)
  DEFB 183                ; Page containing the sprite graphic data: 183
  DEFB 0                  ; Minimum pixel y-coordinate: 0
  DEFB 160                ; Maximum pixel y-coordinate: 80
; The following entity definition (52) is used in The Wine Cellar, Tool  Shed
; and West Wing Roof.
ENTITY52:
  DEFB %00000001          ; Horizontal guardian (bits 0-2), initial animation
                          ; frame 0 (bits 5 and 6), initially moving right to
                          ; left (bit 7)
  DEFB %11100011          ; INK 3 (bits 0-2), BRIGHT 0 (bit 3), animation frame
                          ; mask 111 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and initial
                          ; x-coordinate (copied from the second byte of the
                          ; entity specification in the room definition)
  DEFB 208                ; Pixel y-coordinate: 104
  DEFB 0                  ; Unused
  DEFB 181                ; Page containing the sprite graphic data: 181
  DEFB 7                  ; Minimum x-coordinate: 7
  DEFB 22                 ; Maximum x-coordinate: 22
; The following entity definition (53) is used in At the Foot of the MegaTree
; and The Yacht.
ENTITY53:
  DEFB %00000001          ; Horizontal guardian (bits 0-2), initial animation
                          ; frame 0 (bits 5 and 6), initially moving right to
                          ; left (bit 7)
  DEFB %11101011          ; INK 3 (bits 0-2), BRIGHT 1 (bit 3), animation frame
                          ; mask 111 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and initial
                          ; x-coordinate (copied from the second byte of the
                          ; entity specification in the room definition)
  DEFB 208                ; Pixel y-coordinate: 104
  DEFB 0                  ; Unused
  DEFB 181                ; Page containing the sprite graphic data: 181
  DEFB 4                  ; Minimum x-coordinate: 4
  DEFB 14                 ; Maximum x-coordinate: 14
; The following entity definition (54) is used in Cold Store.
ENTITY54:
  DEFB %00000001          ; Horizontal guardian (bits 0-2), initial animation
                          ; frame 0 (bits 5 and 6), initially moving right to
                          ; left (bit 7)
  DEFB %11100110          ; INK 6 (bits 0-2), BRIGHT 0 (bit 3), animation frame
                          ; mask 111 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and initial
                          ; x-coordinate (copied from the second byte of the
                          ; entity specification in the room definition)
  DEFB 208                ; Pixel y-coordinate: 104
  DEFB 0                  ; Unused
  DEFB 189                ; Page containing the sprite graphic data: 189
  DEFB 0                  ; Minimum x-coordinate: 0
  DEFB 24                 ; Maximum x-coordinate: 24
; The following entity definition (55) is used in Cold Store and Under the
; Roof.
ENTITY55:
  DEFB %10000001          ; Horizontal guardian (bits 0-2), initial animation
                          ; frame 0 (bits 5 and 6), initially moving left to
                          ; right (bit 7)
  DEFB %01100111          ; INK 7 (bits 0-2), BRIGHT 0 (bit 3), animation frame
                          ; mask 011 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and initial
                          ; x-coordinate (copied from the second byte of the
                          ; entity specification in the room definition)
  DEFB 144                ; Pixel y-coordinate: 72
  DEFB 0                  ; Unused
  DEFB 187                ; Page containing the sprite graphic data: 187
  DEFB 0                  ; Minimum x-coordinate: 0
  DEFB 5                  ; Maximum x-coordinate: 5
; The following entity definition (56) is used in Cold Store.
ENTITY56:
  DEFB %10000001          ; Horizontal guardian (bits 0-2), initial animation
                          ; frame 0 (bits 5 and 6), initially moving left to
                          ; right (bit 7)
  DEFB %11100100          ; INK 4 (bits 0-2), BRIGHT 0 (bit 3), animation frame
                          ; mask 111 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and initial
                          ; x-coordinate (copied from the second byte of the
                          ; entity specification in the room definition)
  DEFB 96                 ; Pixel y-coordinate: 48
  DEFB 0                  ; Unused
  DEFB 189                ; Page containing the sprite graphic data: 189
  DEFB 0                  ; Minimum x-coordinate: 0
  DEFB 6                  ; Maximum x-coordinate: 6
; The following entity definition (57) is used in Cold Store.
ENTITY57:
  DEFB %10000001          ; Horizontal guardian (bits 0-2), initial animation
                          ; frame 0 (bits 5 and 6), initially moving left to
                          ; right (bit 7)
  DEFB %01100011          ; INK 3 (bits 0-2), BRIGHT 0 (bit 3), animation frame
                          ; mask 011 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and initial
                          ; x-coordinate (copied from the second byte of the
                          ; entity specification in the room definition)
  DEFB 48                 ; Pixel y-coordinate: 24
  DEFB 0                  ; Unused
  DEFB 187                ; Page containing the sprite graphic data: 187
  DEFB 0                  ; Minimum x-coordinate: 0
  DEFB 9                  ; Maximum x-coordinate: 9
; The following entity definition (58) is used in Top Landing.
ENTITY58:
  DEFB %00010010          ; Vertical guardian (bits 0-2), animation frame
                          ; updated on every pass (bit 4), initial animation
                          ; frame 0 (bits 5 and 6)
  DEFB %01100111          ; INK 7 (bits 0-2), BRIGHT 0 (bit 3), animation frame
                          ; mask 011 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and x-coordinate
                          ; (copied from the second byte of the entity
                          ; specification in the room definition)
  DEFB 64                 ; Initial pixel y-coordinate: 32
  DEFB 4                  ; Initial pixel y-coordinate increment: 2 (moving
                          ; down)
  DEFB 173                ; Page containing the sprite graphic data: 173
  DEFB 32                 ; Minimum pixel y-coordinate: 16
  DEFB 208                ; Maximum pixel y-coordinate: 104
; The following entity definition (59) is used in The Bathroom.
ENTITY59:
  DEFB %00000001          ; Horizontal guardian (bits 0-2), initial animation
                          ; frame 0 (bits 5 and 6), initially moving right to
                          ; left (bit 7)
  DEFB %01100100          ; INK 4 (bits 0-2), BRIGHT 0 (bit 3), animation frame
                          ; mask 011 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and initial
                          ; x-coordinate (copied from the second byte of the
                          ; entity specification in the room definition)
  DEFB 48                 ; Pixel y-coordinate: 24
  DEFB 0                  ; Unused
  DEFB 185                ; Page containing the sprite graphic data: 185
  DEFB 0                  ; Minimum x-coordinate: 0
  DEFB 27                 ; Maximum x-coordinate: 27
; The following entity definition (60) is used in Cuckoo's Nest, On a Branch
; Over the Drive, The Hall, I'm sure I've seen this before.., We must perform a
; Quirkafleeg, Orangery, The Attic, Under the Roof, West Wing Roof and The
; Beach.
ENTITY60:
  DEFB %10000100          ; Arrow (bits 0-2), flying left to right (bit 7)
  DEFB 6                  ; Unused
  DEFB 0                  ; Replaced by the pixel y-coordinate (copied from the
                          ; second byte of the entity specification in the room
                          ; definition)
  DEFB 0                  ; Unused
  DEFB 208                ; Initial x-coordinate: 208
  DEFB 0                  ; Unused
  DEFB %10000010          ; Top/bottom pixel row (drawn either side of the
                          ; shaft)
  DEFB 0                  ; Unused
; The following entity definition (61) is used in On a Branch Over the Drive
; and Conservatory Roof.
ENTITY61:
  DEFB %10000001          ; Horizontal guardian (bits 0-2), initial animation
                          ; frame 0 (bits 5 and 6), initially moving left to
                          ; right (bit 7)
  DEFB %01100110          ; INK 6 (bits 0-2), BRIGHT 0 (bit 3), animation frame
                          ; mask 011 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and initial
                          ; x-coordinate (copied from the second byte of the
                          ; entity specification in the room definition)
  DEFB 80                 ; Pixel y-coordinate: 40
  DEFB 0                  ; Unused
  DEFB 173                ; Page containing the sprite graphic data: 173
  DEFB 16                 ; Minimum x-coordinate: 16
  DEFB 30                 ; Maximum x-coordinate: 30
; The following entity definition (62) is used in On a Branch Over the Drive.
ENTITY62:
  DEFB %00000010          ; Vertical guardian (bits 0-2), animation frame
                          ; updated on every second pass (bit 4), initial
                          ; animation frame 0 (bits 5 and 6)
  DEFB %01100101          ; INK 5 (bits 0-2), BRIGHT 0 (bit 3), animation frame
                          ; mask 011 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and x-coordinate
                          ; (copied from the second byte of the entity
                          ; specification in the room definition)
  DEFB 96                 ; Initial pixel y-coordinate: 48
  DEFB 2                  ; Initial pixel y-coordinate increment: 1 (moving
                          ; down)
  DEFB 185                ; Page containing the sprite graphic data: 185
  DEFB 0                  ; Minimum pixel y-coordinate: 0
  DEFB 112                ; Maximum pixel y-coordinate: 56
; The following entity definition (63) is not used.
  DEFB 255,0,0,0,0,0,0,0
; The following entity definition (64) is used in The Wine Cellar.
ENTITY64:
  DEFB %00000001          ; Horizontal guardian (bits 0-2), initial animation
                          ; frame 0 (bits 5 and 6), initially moving right to
                          ; left (bit 7)
  DEFB %11100011          ; INK 3 (bits 0-2), BRIGHT 0 (bit 3), animation frame
                          ; mask 111 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and initial
                          ; x-coordinate (copied from the second byte of the
                          ; entity specification in the room definition)
  DEFB 48                 ; Pixel y-coordinate: 24
  DEFB 0                  ; Unused
  DEFB 180                ; Page containing the sprite graphic data: 180
  DEFB 0                  ; Minimum x-coordinate: 0
  DEFB 27                 ; Maximum x-coordinate: 27
; The following entity definition (65) is used in The Wine Cellar.
ENTITY65:
  DEFB %10000001          ; Horizontal guardian (bits 0-2), initial animation
                          ; frame 0 (bits 5 and 6), initially moving left to
                          ; right (bit 7)
  DEFB %11100110          ; INK 6 (bits 0-2), BRIGHT 0 (bit 3), animation frame
                          ; mask 111 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and initial
                          ; x-coordinate (copied from the second byte of the
                          ; entity specification in the room definition)
  DEFB 96                 ; Pixel y-coordinate: 48
  DEFB 0                  ; Unused
  DEFB 180                ; Page containing the sprite graphic data: 180
  DEFB 4                  ; Minimum x-coordinate: 4
  DEFB 27                 ; Maximum x-coordinate: 27
; The following entity definition (66) is used in The Wine Cellar.
ENTITY66:
  DEFB %00000001          ; Horizontal guardian (bits 0-2), initial animation
                          ; frame 0 (bits 5 and 6), initially moving right to
                          ; left (bit 7)
  DEFB %11100100          ; INK 4 (bits 0-2), BRIGHT 0 (bit 3), animation frame
                          ; mask 111 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and initial
                          ; x-coordinate (copied from the second byte of the
                          ; entity specification in the room definition)
  DEFB 144                ; Pixel y-coordinate: 72
  DEFB 0                  ; Unused
  DEFB 180                ; Page containing the sprite graphic data: 180
  DEFB 2                  ; Minimum x-coordinate: 2
  DEFB 27                 ; Maximum x-coordinate: 27
; The following entity definition (67) is used in First Landing.
ENTITY67:
  DEFB %10000001          ; Horizontal guardian (bits 0-2), initial animation
                          ; frame 0 (bits 5 and 6), initially moving left to
                          ; right (bit 7)
  DEFB %11100110          ; INK 6 (bits 0-2), BRIGHT 0 (bit 3), animation frame
                          ; mask 111 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and initial
                          ; x-coordinate (copied from the second byte of the
                          ; entity specification in the room definition)
  DEFB 192                ; Pixel y-coordinate: 96
  DEFB 0                  ; Unused
  DEFB 180                ; Page containing the sprite graphic data: 180
  DEFB 24                 ; Minimum x-coordinate: 24
  DEFB 30                 ; Maximum x-coordinate: 30
; The following entity definition (68) is used in Under the Drive.
ENTITY68:
  DEFB %00000001          ; Horizontal guardian (bits 0-2), initial animation
                          ; frame 0 (bits 5 and 6), initially moving right to
                          ; left (bit 7)
  DEFB %11100011          ; INK 3 (bits 0-2), BRIGHT 0 (bit 3), animation frame
                          ; mask 111 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and initial
                          ; x-coordinate (copied from the second byte of the
                          ; entity specification in the room definition)
  DEFB 128                ; Pixel y-coordinate: 64
  DEFB 0                  ; Unused
  DEFB 174                ; Page containing the sprite graphic data: 174
  DEFB 21                 ; Minimum x-coordinate: 21
  DEFB 30                 ; Maximum x-coordinate: 30
; The following entity definition (69) is used in The Hall, Tree Top, I'm sure
; I've seen this before.., Up on the Battlements, A bit of tree and The Attic.
ENTITY69:
  DEFB %00000100          ; Arrow (bits 0-2), flying right to left (bit 7)
  DEFB 6                  ; Unused
  DEFB 0                  ; Replaced by the pixel y-coordinate (copied from the
                          ; second byte of the entity specification in the room
                          ; definition)
  DEFB 0                  ; Unused
  DEFB 28                 ; Initial x-coordinate: 28
  DEFB 0                  ; Unused
  DEFB %01000001          ; Top/bottom pixel row (drawn either side of the
                          ; shaft)
  DEFB 0                  ; Unused
; The following entity definition (70) is used in The Nightmare Room.
ENTITY70:
  DEFB %00010010          ; Vertical guardian (bits 0-2), animation frame
                          ; updated on every pass (bit 4), initial animation
                          ; frame 0 (bits 5 and 6)
  DEFB %01100101          ; INK 5 (bits 0-2), BRIGHT 0 (bit 3), animation frame
                          ; mask 011 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and x-coordinate
                          ; (copied from the second byte of the entity
                          ; specification in the room definition)
  DEFB 64                 ; Initial pixel y-coordinate: 32
  DEFB 252                ; Initial pixel y-coordinate increment: -2 (moving
                          ; up)
  DEFB 156                ; Page containing the sprite graphic data: 156
  DEFB 0                  ; Minimum pixel y-coordinate: 0
  DEFB 160                ; Maximum pixel y-coordinate: 80
; The following entity definition (71) is used in The Nightmare Room.
ENTITY71:
  DEFB %00010010          ; Vertical guardian (bits 0-2), animation frame
                          ; updated on every pass (bit 4), initial animation
                          ; frame 0 (bits 5 and 6)
  DEFB %00100011          ; INK 3 (bits 0-2), BRIGHT 0 (bit 3), animation frame
                          ; mask 001 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and x-coordinate
                          ; (copied from the second byte of the entity
                          ; specification in the room definition)
  DEFB 32                 ; Initial pixel y-coordinate: 16
  DEFB 2                  ; Initial pixel y-coordinate increment: 1 (moving
                          ; down)
  DEFB 156                ; Page containing the sprite graphic data: 156
  DEFB 0                  ; Minimum pixel y-coordinate: 0
  DEFB 208                ; Maximum pixel y-coordinate: 104
; The following entity definition (72) is used in The Nightmare Room.
ENTITY72:
  DEFB %00010010          ; Vertical guardian (bits 0-2), initial animation
                          ; frame 0 (bits 5 and 6)
  DEFB %00000110          ; INK 6 (bits 0-2), BRIGHT 0 (bit 3), animation frame
                          ; mask 000 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and x-coordinate
                          ; (copied from the second byte of the entity
                          ; specification in the room definition)
  DEFB 144                ; Initial pixel y-coordinate: 72
  DEFB 6                  ; Initial pixel y-coordinate increment: 3 (moving
                          ; down)
  DEFB 156                ; Page containing the sprite graphic data: 156
  DEFB 0                  ; Minimum pixel y-coordinate: 0
  DEFB 192                ; Maximum pixel y-coordinate: 96
; The following entity definition (73) is used in The Nightmare Room.
ENTITY73:
  DEFB %00010010          ; Vertical guardian (bits 0-2), animation frame
                          ; updated on every pass (bit 4), initial animation
                          ; frame 0 (bits 5 and 6)
  DEFB %01101010          ; INK 2 (bits 0-2), BRIGHT 1 (bit 3), animation frame
                          ; mask 011 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and x-coordinate
                          ; (copied from the second byte of the entity
                          ; specification in the room definition)
  DEFB 128                ; Initial pixel y-coordinate: 64
  DEFB 248                ; Initial pixel y-coordinate increment: -4 (moving
                          ; up)
  DEFB 156                ; Page containing the sprite graphic data: 156
  DEFB 16                 ; Minimum pixel y-coordinate: 8
  DEFB 208                ; Maximum pixel y-coordinate: 104
; The following entity definition (74) is used in The Forgotten Abbey.
ENTITY74:
  DEFB %10000001          ; Horizontal guardian (bits 0-2), initial animation
                          ; frame 0 (bits 5 and 6), initially moving left to
                          ; right (bit 7)
  DEFB %11100100          ; INK 4 (bits 0-2), BRIGHT 0 (bit 3), animation frame
                          ; mask 111 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and initial
                          ; x-coordinate (copied from the second byte of the
                          ; entity specification in the room definition)
  DEFB 80                 ; Pixel y-coordinate: 40
  DEFB 0                  ; Unused
  DEFB 180                ; Page containing the sprite graphic data: 180
  DEFB 4                  ; Minimum x-coordinate: 4
  DEFB 20                 ; Maximum x-coordinate: 20
; The following entity definition (75) is used in The Forgotten Abbey.
ENTITY75:
  DEFB %10000001          ; Horizontal guardian (bits 0-2), initial animation
                          ; frame 0 (bits 5 and 6), initially moving left to
                          ; right (bit 7)
  DEFB %11100110          ; INK 6 (bits 0-2), BRIGHT 0 (bit 3), animation frame
                          ; mask 111 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and initial
                          ; x-coordinate (copied from the second byte of the
                          ; entity specification in the room definition)
  DEFB 80                 ; Pixel y-coordinate: 40
  DEFB 0                  ; Unused
  DEFB 180                ; Page containing the sprite graphic data: 180
  DEFB 12                 ; Minimum x-coordinate: 12
  DEFB 28                 ; Maximum x-coordinate: 28
; The following entity definition (76) is used in The Forgotten Abbey.
ENTITY76:
  DEFB %00100001          ; Horizontal guardian (bits 0-2), initial animation
                          ; frame 1 (bits 5 and 6), initially moving right to
                          ; left (bit 7)
  DEFB %11101010          ; INK 2 (bits 0-2), BRIGHT 1 (bit 3), animation frame
                          ; mask 111 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and initial
                          ; x-coordinate (copied from the second byte of the
                          ; entity specification in the room definition)
  DEFB 144                ; Pixel y-coordinate: 72
  DEFB 0                  ; Unused
  DEFB 180                ; Page containing the sprite graphic data: 180
  DEFB 9                  ; Minimum x-coordinate: 9
  DEFB 20                 ; Maximum x-coordinate: 20
; The following entity definition (77) is used in The Forgotten Abbey.
ENTITY77:
  DEFB %10000001          ; Horizontal guardian (bits 0-2), initial animation
                          ; frame 0 (bits 5 and 6), initially moving left to
                          ; right (bit 7)
  DEFB %11100111          ; INK 7 (bits 0-2), BRIGHT 0 (bit 3), animation frame
                          ; mask 111 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and initial
                          ; x-coordinate (copied from the second byte of the
                          ; entity specification in the room definition)
  DEFB 144                ; Pixel y-coordinate: 72
  DEFB 0                  ; Unused
  DEFB 180                ; Page containing the sprite graphic data: 180
  DEFB 22                 ; Minimum x-coordinate: 22
  DEFB 27                 ; Maximum x-coordinate: 27
; The following entity definition (78) is used in The Forgotten Abbey and
; Swimming Pool.
ENTITY78:
  DEFB %00100001          ; Horizontal guardian (bits 0-2), initial animation
                          ; frame 1 (bits 5 and 6), initially moving right to
                          ; left (bit 7)
  DEFB %11100110          ; INK 6 (bits 0-2), BRIGHT 0 (bit 3), animation frame
                          ; mask 111 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and initial
                          ; x-coordinate (copied from the second byte of the
                          ; entity specification in the room definition)
  DEFB 208                ; Pixel y-coordinate: 104
  DEFB 0                  ; Unused
  DEFB 180                ; Page containing the sprite graphic data: 180
  DEFB 5                  ; Minimum x-coordinate: 5
  DEFB 12                 ; Maximum x-coordinate: 12
; The following entity definition (79) is used in The Forgotten Abbey.
ENTITY79:
  DEFB %01000001          ; Horizontal guardian (bits 0-2), initial animation
                          ; frame 2 (bits 5 and 6), initially moving right to
                          ; left (bit 7)
  DEFB %11101001          ; INK 1 (bits 0-2), BRIGHT 1 (bit 3), animation frame
                          ; mask 111 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and initial
                          ; x-coordinate (copied from the second byte of the
                          ; entity specification in the room definition)
  DEFB 208                ; Pixel y-coordinate: 104
  DEFB 0                  ; Unused
  DEFB 180                ; Page containing the sprite graphic data: 180
  DEFB 11                 ; Minimum x-coordinate: 11
  DEFB 18                 ; Maximum x-coordinate: 18
; The following entity definition (80) is used in The Forgotten Abbey.
ENTITY80:
  DEFB %01100001          ; Horizontal guardian (bits 0-2), initial animation
                          ; frame 3 (bits 5 and 6), initially moving right to
                          ; left (bit 7)
  DEFB %11100100          ; INK 4 (bits 0-2), BRIGHT 0 (bit 3), animation frame
                          ; mask 111 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and initial
                          ; x-coordinate (copied from the second byte of the
                          ; entity specification in the room definition)
  DEFB 208                ; Pixel y-coordinate: 104
  DEFB 0                  ; Unused
  DEFB 180                ; Page containing the sprite graphic data: 180
  DEFB 16                 ; Minimum x-coordinate: 16
  DEFB 23                 ; Maximum x-coordinate: 23
; The following entity definition (81) is used in The Forgotten Abbey.
ENTITY81:
  DEFB %00000001          ; Horizontal guardian (bits 0-2), initial animation
                          ; frame 0 (bits 5 and 6), initially moving right to
                          ; left (bit 7)
  DEFB %11100010          ; INK 2 (bits 0-2), BRIGHT 0 (bit 3), animation frame
                          ; mask 111 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and initial
                          ; x-coordinate (copied from the second byte of the
                          ; entity specification in the room definition)
  DEFB 208                ; Pixel y-coordinate: 104
  DEFB 0                  ; Unused
  DEFB 180                ; Page containing the sprite graphic data: 180
  DEFB 23                 ; Minimum x-coordinate: 23
  DEFB 30                 ; Maximum x-coordinate: 30
; The following entity definition (82) is used in The Attic.
ENTITY82:
  DEFB %00000010          ; Vertical guardian (bits 0-2), animation frame
                          ; updated on every second pass (bit 4), initial
                          ; animation frame 0 (bits 5 and 6)
  DEFB %00100011          ; INK 3 (bits 0-2), BRIGHT 0 (bit 3), animation frame
                          ; mask 001 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and x-coordinate
                          ; (copied from the second byte of the entity
                          ; specification in the room definition)
  DEFB 132                ; Initial pixel y-coordinate: 66
  DEFB 4                  ; Initial pixel y-coordinate increment: 2 (moving
                          ; down)
  DEFB 176                ; Page containing the sprite graphic data: 176
  DEFB 112                ; Minimum pixel y-coordinate: 56
  DEFB 176                ; Maximum pixel y-coordinate: 88
; The following entity definition (83) is used in The Attic.
ENTITY83:
  DEFB %00000010          ; Vertical guardian (bits 0-2), animation frame
                          ; updated on every second pass (bit 4), initial
                          ; animation frame 0 (bits 5 and 6)
  DEFB %00100110          ; INK 6 (bits 0-2), BRIGHT 0 (bit 3), animation frame
                          ; mask 001 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and x-coordinate
                          ; (copied from the second byte of the entity
                          ; specification in the room definition)
  DEFB 140                ; Initial pixel y-coordinate: 70
  DEFB 4                  ; Initial pixel y-coordinate increment: 2 (moving
                          ; down)
  DEFB 176                ; Page containing the sprite graphic data: 176
  DEFB 112                ; Minimum pixel y-coordinate: 56
  DEFB 176                ; Maximum pixel y-coordinate: 88
; The following entity definition (84) is used in The Attic.
ENTITY84:
  DEFB %00000010          ; Vertical guardian (bits 0-2), animation frame
                          ; updated on every second pass (bit 4), initial
                          ; animation frame 0 (bits 5 and 6)
  DEFB %00101001          ; INK 1 (bits 0-2), BRIGHT 1 (bit 3), animation frame
                          ; mask 001 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and x-coordinate
                          ; (copied from the second byte of the entity
                          ; specification in the room definition)
  DEFB 148                ; Initial pixel y-coordinate: 74
  DEFB 4                  ; Initial pixel y-coordinate increment: 2 (moving
                          ; down)
  DEFB 176                ; Page containing the sprite graphic data: 176
  DEFB 112                ; Minimum pixel y-coordinate: 56
  DEFB 176                ; Maximum pixel y-coordinate: 88
; The following entity definition (85) is used in The Attic.
ENTITY85:
  DEFB %00000010          ; Vertical guardian (bits 0-2), animation frame
                          ; updated on every second pass (bit 4), initial
                          ; animation frame 0 (bits 5 and 6)
  DEFB %00100100          ; INK 4 (bits 0-2), BRIGHT 0 (bit 3), animation frame
                          ; mask 001 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and x-coordinate
                          ; (copied from the second byte of the entity
                          ; specification in the room definition)
  DEFB 156                ; Initial pixel y-coordinate: 78
  DEFB 4                  ; Initial pixel y-coordinate increment: 2 (moving
                          ; down)
  DEFB 176                ; Page containing the sprite graphic data: 176
  DEFB 112                ; Minimum pixel y-coordinate: 56
  DEFB 176                ; Maximum pixel y-coordinate: 88
; The following entity definition (86) is used in The Attic.
ENTITY86:
  DEFB %00000010          ; Vertical guardian (bits 0-2), animation frame
                          ; updated on every second pass (bit 4), initial
                          ; animation frame 0 (bits 5 and 6)
  DEFB %00100010          ; INK 2 (bits 0-2), BRIGHT 0 (bit 3), animation frame
                          ; mask 001 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and x-coordinate
                          ; (copied from the second byte of the entity
                          ; specification in the room definition)
  DEFB 164                ; Initial pixel y-coordinate: 82
  DEFB 4                  ; Initial pixel y-coordinate increment: 2 (moving
                          ; down)
  DEFB 176                ; Page containing the sprite graphic data: 176
  DEFB 112                ; Minimum pixel y-coordinate: 56
  DEFB 176                ; Maximum pixel y-coordinate: 88
; The following entity definition (87) is used in The Attic.
ENTITY87:
  DEFB %00000010          ; Vertical guardian (bits 0-2), animation frame
                          ; updated on every second pass (bit 4), initial
                          ; animation frame 0 (bits 5 and 6)
  DEFB %00100101          ; INK 5 (bits 0-2), BRIGHT 0 (bit 3), animation frame
                          ; mask 001 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and x-coordinate
                          ; (copied from the second byte of the entity
                          ; specification in the room definition)
  DEFB 172                ; Initial pixel y-coordinate: 86
  DEFB 4                  ; Initial pixel y-coordinate increment: 2 (moving
                          ; down)
  DEFB 176                ; Page containing the sprite graphic data: 176
  DEFB 112                ; Minimum pixel y-coordinate: 56
  DEFB 176                ; Maximum pixel y-coordinate: 88
; The following entity definition (88) is used in Out on a limb and The Banyan
; Tree.
ENTITY88:
  DEFB %00000001          ; Horizontal guardian (bits 0-2), initial animation
                          ; frame 0 (bits 5 and 6), initially moving right to
                          ; left (bit 7)
  DEFB %11100110          ; INK 6 (bits 0-2), BRIGHT 0 (bit 3), animation frame
                          ; mask 111 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and initial
                          ; x-coordinate (copied from the second byte of the
                          ; entity specification in the room definition)
  DEFB 160                ; Pixel y-coordinate: 80
  DEFB 0                  ; Unused
  DEFB 181                ; Page containing the sprite graphic data: 181
  DEFB 19                 ; Minimum x-coordinate: 19
  DEFB 30                 ; Maximum x-coordinate: 30
; The following entity definition (89) is used in The Hall and West  Wing.
ENTITY89:
  DEFB %00000010          ; Vertical guardian (bits 0-2), animation frame
                          ; updated on every second pass (bit 4), initial
                          ; animation frame 0 (bits 5 and 6)
  DEFB %00100110          ; INK 6 (bits 0-2), BRIGHT 0 (bit 3), animation frame
                          ; mask 001 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and x-coordinate
                          ; (copied from the second byte of the entity
                          ; specification in the room definition)
  DEFB 96                 ; Initial pixel y-coordinate: 48
  DEFB 0                  ; Initial pixel y-coordinate increment: 0 (not
                          ; moving)
  DEFB 176                ; Page containing the sprite graphic data: 176
  DEFB 80                 ; Minimum pixel y-coordinate: 40
  DEFB 112                ; Maximum pixel y-coordinate: 56
; The following entity definition (90) is used in To the Kitchens    Main
; Stairway.
ENTITY90:
  DEFB %10000001          ; Horizontal guardian (bits 0-2), initial animation
                          ; frame 0 (bits 5 and 6), initially moving left to
                          ; right (bit 7)
  DEFB %01100011          ; INK 3 (bits 0-2), BRIGHT 0 (bit 3), animation frame
                          ; mask 011 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and initial
                          ; x-coordinate (copied from the second byte of the
                          ; entity specification in the room definition)
  DEFB 48                 ; Pixel y-coordinate: 24
  DEFB 0                  ; Unused
  DEFB 174                ; Page containing the sprite graphic data: 174
  DEFB 0                  ; Minimum x-coordinate: 0
  DEFB 11                 ; Maximum x-coordinate: 11
; The following entity definition (91) is used in To the Kitchens    Main
; Stairway.
ENTITY91:
  DEFB %00000001          ; Horizontal guardian (bits 0-2), initial animation
                          ; frame 0 (bits 5 and 6), initially moving right to
                          ; left (bit 7)
  DEFB %01100110          ; INK 6 (bits 0-2), BRIGHT 0 (bit 3), animation frame
                          ; mask 011 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and initial
                          ; x-coordinate (copied from the second byte of the
                          ; entity specification in the room definition)
  DEFB 48                 ; Pixel y-coordinate: 24
  DEFB 0                  ; Unused
  DEFB 190                ; Page containing the sprite graphic data: 190
  DEFB 13                 ; Minimum x-coordinate: 13
  DEFB 21                 ; Maximum x-coordinate: 21
; The following entity definition (92) is used in To the Kitchens    Main
; Stairway and Back Stairway.
ENTITY92:
  DEFB %00000001          ; Horizontal guardian (bits 0-2), initial animation
                          ; frame 0 (bits 5 and 6), initially moving right to
                          ; left (bit 7)
  DEFB %01100111          ; INK 7 (bits 0-2), BRIGHT 0 (bit 3), animation frame
                          ; mask 011 (bits 5-7)
  DEFB 1                  ; Replaced by the base sprite index and initial
                          ; x-coordinate (copied from the second byte of the
                          ; entity specification in the room definition)
  DEFB 96                 ; Pixel y-coordinate: 48
  DEFB 0                  ; Unused
  DEFB 183                ; Page containing the sprite graphic data: 183
  DEFB 12                 ; Minimum x-coordinate: 12
  DEFB 24                 ; Maximum x-coordinate: 24
; The following entity definition (93) is used in To the Kitchens    Main
; Stairway.
ENTITY93:
  DEFB %10000001          ; Horizontal guardian (bits 0-2), initial animation
                          ; frame 0 (bits 5 and 6), initially moving left to
                          ; right (bit 7)
  DEFB %01100010          ; INK 2 (bits 0-2), BRIGHT 0 (bit 3), animation frame
                          ; mask 011 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and initial
                          ; x-coordinate (copied from the second byte of the
                          ; entity specification in the room definition)
  DEFB 144                ; Pixel y-coordinate: 72
  DEFB 0                  ; Unused
  DEFB 185                ; Page containing the sprite graphic data: 185
  DEFB 2                  ; Minimum x-coordinate: 2
  DEFB 6                  ; Maximum x-coordinate: 6
; The following entity definition (94) is used in To the Kitchens    Main
; Stairway.
ENTITY94:
  DEFB %00000001          ; Horizontal guardian (bits 0-2), initial animation
                          ; frame 0 (bits 5 and 6), initially moving right to
                          ; left (bit 7)
  DEFB %11100100          ; INK 4 (bits 0-2), BRIGHT 0 (bit 3), animation frame
                          ; mask 111 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and initial
                          ; x-coordinate (copied from the second byte of the
                          ; entity specification in the room definition)
  DEFB 192                ; Pixel y-coordinate: 96
  DEFB 0                  ; Unused
  DEFB 175                ; Page containing the sprite graphic data: 175
  DEFB 0                  ; Minimum x-coordinate: 0
  DEFB 30                 ; Maximum x-coordinate: 30
; The following entity definition (95) is used in The Hall and To the Kitchens
; Main Stairway.
ENTITY95:
  DEFB %00000001          ; Horizontal guardian (bits 0-2), initial animation
                          ; frame 0 (bits 5 and 6), initially moving right to
                          ; left (bit 7)
  DEFB %11100110          ; INK 6 (bits 0-2), BRIGHT 0 (bit 3), animation frame
                          ; mask 111 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and initial
                          ; x-coordinate (copied from the second byte of the
                          ; entity specification in the room definition)
  DEFB 160                ; Pixel y-coordinate: 80
  DEFB 0                  ; Unused
  DEFB 188                ; Page containing the sprite graphic data: 188
  DEFB 9                  ; Minimum x-coordinate: 9
  DEFB 17                 ; Maximum x-coordinate: 17
; The following entity definition (96) is used in East Wall Base.
ENTITY96:
  DEFB %00000010          ; Vertical guardian (bits 0-2), animation frame
                          ; updated on every second pass (bit 4), initial
                          ; animation frame 0 (bits 5 and 6)
  DEFB %01101010          ; INK 2 (bits 0-2), BRIGHT 1 (bit 3), animation frame
                          ; mask 011 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and x-coordinate
                          ; (copied from the second byte of the entity
                          ; specification in the room definition)
  DEFB 144                ; Initial pixel y-coordinate: 72
  DEFB 252                ; Initial pixel y-coordinate increment: -2 (moving
                          ; up)
  DEFB 172                ; Page containing the sprite graphic data: 172
  DEFB 0                  ; Minimum pixel y-coordinate: 0
  DEFB 192                ; Maximum pixel y-coordinate: 96
; The following entity definition (97) is used in Orangery and West  Wing.
ENTITY97:
  DEFB %00010010          ; Vertical guardian (bits 0-2), animation frame
                          ; updated on every pass (bit 4), initial animation
                          ; frame 0 (bits 5 and 6)
  DEFB %01100101          ; INK 5 (bits 0-2), BRIGHT 0 (bit 3), animation frame
                          ; mask 011 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and x-coordinate
                          ; (copied from the second byte of the entity
                          ; specification in the room definition)
  DEFB 64                 ; Initial pixel y-coordinate: 32
  DEFB 8                  ; Initial pixel y-coordinate increment: 4 (moving
                          ; down)
  DEFB 172                ; Page containing the sprite graphic data: 172
  DEFB 0                  ; Minimum pixel y-coordinate: 0
  DEFB 192                ; Maximum pixel y-coordinate: 96
; The following entity definition (98) is used in Tool  Shed.
ENTITY98:
  DEFB %10000001          ; Horizontal guardian (bits 0-2), initial animation
                          ; frame 0 (bits 5 and 6), initially moving left to
                          ; right (bit 7)
  DEFB %01100110          ; INK 6 (bits 0-2), BRIGHT 0 (bit 3), animation frame
                          ; mask 011 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and initial
                          ; x-coordinate (copied from the second byte of the
                          ; entity specification in the room definition)
  DEFB 144                ; Pixel y-coordinate: 72
  DEFB 0                  ; Unused
  DEFB 175                ; Page containing the sprite graphic data: 175
  DEFB 7                  ; Minimum x-coordinate: 7
  DEFB 20                 ; Maximum x-coordinate: 20
; The following entity definition (99) is used in Tool  Shed.
ENTITY99:
  DEFB %00000001          ; Horizontal guardian (bits 0-2), initial animation
                          ; frame 0 (bits 5 and 6), initially moving right to
                          ; left (bit 7)
  DEFB %01100100          ; INK 4 (bits 0-2), BRIGHT 0 (bit 3), animation frame
                          ; mask 011 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and initial
                          ; x-coordinate (copied from the second byte of the
                          ; entity specification in the room definition)
  DEFB 96                 ; Pixel y-coordinate: 48
  DEFB 0                  ; Unused
  DEFB 173                ; Page containing the sprite graphic data: 173
  DEFB 7                  ; Minimum x-coordinate: 7
  DEFB 17                 ; Maximum x-coordinate: 17
; The following entity definition (100) is used in The Chapel and The Banyan
; Tree.
ENTITY100:
  DEFB %00010010          ; Vertical guardian (bits 0-2), animation frame
                          ; updated on every pass (bit 4), initial animation
                          ; frame 0 (bits 5 and 6)
  DEFB %01100100          ; INK 4 (bits 0-2), BRIGHT 0 (bit 3), animation frame
                          ; mask 011 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and x-coordinate
                          ; (copied from the second byte of the entity
                          ; specification in the room definition)
  DEFB 128                ; Initial pixel y-coordinate: 64
  DEFB 254                ; Initial pixel y-coordinate increment: -1 (moving
                          ; up)
  DEFB 172                ; Page containing the sprite graphic data: 172
  DEFB 112                ; Minimum pixel y-coordinate: 56
  DEFB 160                ; Maximum pixel y-coordinate: 80
; The following entity definition (101) is used in The Banyan Tree and A bit of
; tree.
ENTITY101:
  DEFB %00000010          ; Vertical guardian (bits 0-2), animation frame
                          ; updated on every second pass (bit 4), initial
                          ; animation frame 0 (bits 5 and 6)
  DEFB %01101011          ; INK 3 (bits 0-2), BRIGHT 1 (bit 3), animation frame
                          ; mask 011 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and x-coordinate
                          ; (copied from the second byte of the entity
                          ; specification in the room definition)
  DEFB 96                 ; Initial pixel y-coordinate: 48
  DEFB 4                  ; Initial pixel y-coordinate increment: 2 (moving
                          ; down)
  DEFB 171                ; Page containing the sprite graphic data: 171
  DEFB 80                 ; Minimum pixel y-coordinate: 40
  DEFB 160                ; Maximum pixel y-coordinate: 80
; The following entity definition (102) is used in The Chapel and The Banyan
; Tree.
ENTITY102:
  DEFB %00010010          ; Vertical guardian (bits 0-2), animation frame
                          ; updated on every pass (bit 4), initial animation
                          ; frame 0 (bits 5 and 6)
  DEFB %01100101          ; INK 5 (bits 0-2), BRIGHT 0 (bit 3), animation frame
                          ; mask 011 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and x-coordinate
                          ; (copied from the second byte of the entity
                          ; specification in the room definition)
  DEFB 152                ; Initial pixel y-coordinate: 76
  DEFB 2                  ; Initial pixel y-coordinate increment: 1 (moving
                          ; down)
  DEFB 171                ; Page containing the sprite graphic data: 171
  DEFB 80                 ; Minimum pixel y-coordinate: 40
  DEFB 160                ; Maximum pixel y-coordinate: 80
; The following entity definition (103) is used in The Chapel.
ENTITY103:
  DEFB %00000001          ; Horizontal guardian (bits 0-2), initial animation
                          ; frame 0 (bits 5 and 6), initially moving right to
                          ; left (bit 7)
  DEFB %01100011          ; INK 3 (bits 0-2), BRIGHT 0 (bit 3), animation frame
                          ; mask 011 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and initial
                          ; x-coordinate (copied from the second byte of the
                          ; entity specification in the room definition)
  DEFB 192                ; Pixel y-coordinate: 96
  DEFB 0                  ; Unused
  DEFB 180                ; Page containing the sprite graphic data: 180
  DEFB 0                  ; Minimum x-coordinate: 0
  DEFB 15                 ; Maximum x-coordinate: 15
; The following entity definition (104) is used in A bit of tree and Nomen
; Luni.
ENTITY104:
  DEFB %00000001          ; Horizontal guardian (bits 0-2), initial animation
                          ; frame 0 (bits 5 and 6), initially moving right to
                          ; left (bit 7)
  DEFB %11100110          ; INK 6 (bits 0-2), BRIGHT 0 (bit 3), animation frame
                          ; mask 111 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and initial
                          ; x-coordinate (copied from the second byte of the
                          ; entity specification in the room definition)
  DEFB 32                 ; Pixel y-coordinate: 16
  DEFB 0                  ; Unused
  DEFB 188                ; Page containing the sprite graphic data: 188
  DEFB 0                  ; Minimum x-coordinate: 0
  DEFB 10                 ; Maximum x-coordinate: 10
; The following entity definition (105) is used in The Bow.
ENTITY105:
  DEFB %00000001          ; Horizontal guardian (bits 0-2), initial animation
                          ; frame 0 (bits 5 and 6), initially moving right to
                          ; left (bit 7)
  DEFB %01100110          ; INK 6 (bits 0-2), BRIGHT 0 (bit 3), animation frame
                          ; mask 011 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and initial
                          ; x-coordinate (copied from the second byte of the
                          ; entity specification in the room definition)
  DEFB 48                 ; Pixel y-coordinate: 24
  DEFB 0                  ; Unused
  DEFB 190                ; Page containing the sprite graphic data: 190
  DEFB 22                 ; Minimum x-coordinate: 22
  DEFB 30                 ; Maximum x-coordinate: 30
; The following entity definition (106) is used in Conservatory Roof.
ENTITY106:
  DEFB %00000001          ; Horizontal guardian (bits 0-2), initial animation
                          ; frame 0 (bits 5 and 6), initially moving right to
                          ; left (bit 7)
  DEFB %01101010          ; INK 2 (bits 0-2), BRIGHT 1 (bit 3), animation frame
                          ; mask 011 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and initial
                          ; x-coordinate (copied from the second byte of the
                          ; entity specification in the room definition)
  DEFB 176                ; Pixel y-coordinate: 88
  DEFB 0                  ; Unused
  DEFB 174                ; Page containing the sprite graphic data: 174
  DEFB 17                 ; Minimum x-coordinate: 17
  DEFB 30                 ; Maximum x-coordinate: 30
; The following entity definition (107) is used in Nomen Luni.
ENTITY107:
  DEFB %10000001          ; Horizontal guardian (bits 0-2), initial animation
                          ; frame 0 (bits 5 and 6), initially moving left to
                          ; right (bit 7)
  DEFB %01100100          ; INK 4 (bits 0-2), BRIGHT 0 (bit 3), animation frame
                          ; mask 011 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and initial
                          ; x-coordinate (copied from the second byte of the
                          ; entity specification in the room definition)
  DEFB 48                 ; Pixel y-coordinate: 24
  DEFB 0                  ; Unused
  DEFB 190                ; Page containing the sprite graphic data: 190
  DEFB 18                 ; Minimum x-coordinate: 18
  DEFB 22                 ; Maximum x-coordinate: 22
; The following entity definition (108) is used in Watch Tower.
ENTITY108:
  DEFB %10000001          ; Horizontal guardian (bits 0-2), initial animation
                          ; frame 0 (bits 5 and 6), initially moving left to
                          ; right (bit 7)
  DEFB %01100011          ; INK 3 (bits 0-2), BRIGHT 0 (bit 3), animation frame
                          ; mask 011 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and initial
                          ; x-coordinate (copied from the second byte of the
                          ; entity specification in the room definition)
  DEFB 144                ; Pixel y-coordinate: 72
  DEFB 0                  ; Unused
  DEFB 187                ; Page containing the sprite graphic data: 187
  DEFB 11                 ; Minimum x-coordinate: 11
  DEFB 18                 ; Maximum x-coordinate: 18
; The following entity definition (109) is used in Watch Tower.
ENTITY109:
  DEFB %00000001          ; Horizontal guardian (bits 0-2), initial animation
                          ; frame 0 (bits 5 and 6), initially moving right to
                          ; left (bit 7)
  DEFB %11100110          ; INK 6 (bits 0-2), BRIGHT 0 (bit 3), animation frame
                          ; mask 111 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and initial
                          ; x-coordinate (copied from the second byte of the
                          ; entity specification in the room definition)
  DEFB 112                ; Pixel y-coordinate: 56
  DEFB 0                  ; Unused
  DEFB 188                ; Page containing the sprite graphic data: 188
  DEFB 9                  ; Minimum x-coordinate: 9
  DEFB 30                 ; Maximum x-coordinate: 30
; The following entity definition (110) is used in The Bow.
ENTITY110:
  DEFB %00000001          ; Horizontal guardian (bits 0-2), initial animation
                          ; frame 0 (bits 5 and 6), initially moving right to
                          ; left (bit 7)
  DEFB %01100011          ; INK 3 (bits 0-2), BRIGHT 0 (bit 3), animation frame
                          ; mask 011 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and initial
                          ; x-coordinate (copied from the second byte of the
                          ; entity specification in the room definition)
  DEFB 208                ; Pixel y-coordinate: 104
  DEFB 0                  ; Unused
  DEFB 174                ; Page containing the sprite graphic data: 174
  DEFB 17                 ; Minimum x-coordinate: 17
  DEFB 30                 ; Maximum x-coordinate: 30
; The following entity definition (111) is used in Cuckoo's Nest.
ENTITY111:
  DEFB %00000001          ; Horizontal guardian (bits 0-2), initial animation
                          ; frame 0 (bits 5 and 6), initially moving right to
                          ; left (bit 7)
  DEFB %01100011          ; INK 3 (bits 0-2), BRIGHT 0 (bit 3), animation frame
                          ; mask 011 (bits 5-7)
  DEFB 0                  ; Replaced by the base sprite index and initial
                          ; x-coordinate (copied from the second byte of the
                          ; entity specification in the room definition)
  DEFB 176                ; Pixel y-coordinate: 88
  DEFB 0                  ; Unused
  DEFB 181                ; Page containing the sprite graphic data: 181
  DEFB 0                  ; Minimum x-coordinate: 0
  DEFB 11                 ; Maximum x-coordinate: 11
; The next 15 entity definitions (112-126) are unused.
  DEFB 0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0
; The following entity definition (127) - whose eighth byte is at FIRSTITEM -
; is copied into one of the entity buffers at ENTITYBUF for any entity
; specification whose first byte is 127 or 255; the first byte of the
; definition (255) serves to terminate the entity buffers.
ENTITY127:
  DEFB 255,0,0,0,0,0,0

; Index of the first item
;
; Used by the routines at TITLESCREEN and DRAWITEMS.
FIRSTITEM:
  DEFB 173

; Item table
;
; Used by the routines at TITLESCREEN and DRAWITEMS.
;
; The location of item N (173<=N<=255) is defined by the pair of bytes at
; addresses ITEMTABLE1+N and ITEMTABLE2+N. The meaning of the bits in each
; byte-pair is as follows:
;
; +--------+----------------------------------------------------+
; | Bit(s) | Meaning                                            |
; +--------+----------------------------------------------------+
; | 15     | Most significant bit of the y-coordinate           |
; | 14     | Collection flag (reset=collected, set=uncollected) |
; | 8-13   | Room number                                        |
; | 5-7    | Least significant bits of the y-coordinate         |
; | 0-4    | x-coordinate                                       |
; +--------+----------------------------------------------------+
ITEMTABLE1:
  DEFS 173                ; Unused
  DEFB 178                ; Item 173 at (8,25) in Watch Tower
  DEFB 178                ; Item 174 at (9,10) in Watch Tower
  DEFB 178                ; Item 175 at (9,20) in Watch Tower
  DEFB 178                ; Item 176 at (9,24) in Watch Tower
  DEFB 184                ; Item 177 at (14,9) in West Wing Roof
  DEFB 184                ; Item 178 at (13,19) in West Wing Roof
  DEFB 184                ; Item 179 at (11,27) in West Wing Roof
  DEFB 184                ; Item 180 at (10,6) in West Wing Roof
  DEFB 171                ; Item 181 at (8,31) in Conservatory Roof
  DEFB 171                ; Item 182 at (8,25) in Conservatory Roof
  DEFB 171                ; Item 183 at (8,28) in Conservatory Roof
  DEFB 171                ; Item 184 at (8,22) in Conservatory Roof
  DEFB 159                ; Item 185 at (11,4) in Swimming Pool
  DEFB 146                ; Item 186 at (12,25) in On the Roof
  DEFB 15                 ; Item 187 at (4,31) in I'm sure I've seen this
                          ; before..
  DEFB 16                 ; Item 188 at (4,31) in We must perform a Quirkafleeg
  DEFB 17                 ; Item 189 at (4,31) in Up on the Battlements
  DEFB 188                ; Item 190 at (8,26) in The Bow
  DEFB 187                ; Item 191 at (14,13) in The Yacht
  DEFB 186                ; Item 192 at (13,22) in The Beach
  DEFB 37                 ; Item 193 at (1,28) in Orangery
  DEFB 37                 ; Item 194 at (6,16) in Orangery
  DEFB 165                ; Item 195 at (13,6) in Orangery
  DEFB 27                 ; Item 196 at (6,29) in The Chapel
  DEFB 185                ; Item 197 at (9,23) in Above the West Bedroom
  DEFB 186                ; Item 198 at (13,22) in The Beach
  DEFB 28                 ; Item 199 at (3,26) in First Landing
  DEFB 162                ; Item 200 at (9,2) in Top Landing
  DEFB 25                 ; Item 201 at (2,3) in Cold Store
  DEFB 25                 ; Item 202 at (5,8) in Cold Store
  DEFB 153                ; Item 203 at (8,6) in Cold Store
  DEFB 153                ; Item 204 at (11,6) in Cold Store
  DEFB 179                ; Item 205 at (14,7) in Tool  Shed
  DEFB 22                 ; Item 206 at (6,9) in To the Kitchens    Main
                          ; Stairway
  DEFB 150                ; Item 207 at (9,2) in To the Kitchens    Main
                          ; Stairway
  DEFB 40                 ; Item 208 at (7,19) in Dr Jones will never believe
                          ; this
  DEFB 140                ; Item 209 at (13,19) in Tree Top
  DEFB 12                 ; Item 210 at (3,16) in Tree Top
  DEFB 12                 ; Item 211 at (4,22) in Tree Top
  DEFB 137                ; Item 212 at (11,5) in On a Branch Over the Drive
  DEFB 7                  ; Item 213 at (7,15) in Cuckoo's Nest
  DEFB 46                 ; Item 214 at (3,10) in Tree Root
  DEFB 46                 ; Item 215 at (4,29) in Tree Root
  DEFB 38                 ; Item 216 at (2,9) in Priests' Hole
  DEFB 149                ; Item 217 at (12,17) in Ballroom West
  DEFB 149                ; Item 218 at (12,18) in Ballroom West
  DEFB 149                ; Item 219 at (12,20) in Ballroom West
  DEFB 149                ; Item 220 at (12,22) in Ballroom West
  DEFB 149                ; Item 221 at (12,23) in Ballroom West
  DEFB 14                 ; Item 222 at (2,26) in Rescue Esmerelda
  DEFB 10                 ; Item 223 at (4,11) in The Front Door
  DEFB 149                ; Item 224 at (12,25) in Ballroom West
  DEFB 149                ; Item 225 at (12,27) in Ballroom West
  DEFB 14                 ; Item 226 at (3,26) in Rescue Esmerelda
  DEFB 13                 ; Item 227 at (6,2) in Out on a limb
  DEFB 13                 ; Item 228 at (1,5) in Out on a limb
  DEFB 44                 ; Item 229 at (4,18) in On top of the house
  DEFB 19                 ; Item 230 at (3,2) in The Forgotten Abbey
  DEFB 131                ; Item 231 at (11,1) in At the Foot of the MegaTree
  DEFB 131                ; Item 232 at (10,4) in At the Foot of the MegaTree
  DEFB 131                ; Item 233 at (11,7) in At the Foot of the MegaTree
  DEFB 49                 ; Item 234 at (4,28) in The Wine Cellar
  DEFB 49                 ; Item 235 at (7,4) in The Wine Cellar
  DEFB 49                 ; Item 236 at (7,28) in The Wine Cellar
  DEFB 177                ; Item 237 at (10,2) in The Wine Cellar
  DEFB 177                ; Item 238 at (10,28) in The Wine Cellar
  DEFB 177                ; Item 239 at (13,4) in The Wine Cellar
  DEFB 0                  ; Item 240 at (4,19) in The Off Licence
  DEFB 0                  ; Item 241 at (4,20) in The Off Licence
  DEFB 0                  ; Item 242 at (4,21) in The Off Licence
  DEFB 0                  ; Item 243 at (4,22) in The Off Licence
  DEFB 0                  ; Item 244 at (4,23) in The Off Licence
  DEFB 0                  ; Item 245 at (4,24) in The Off Licence
  DEFB 0                  ; Item 246 at (4,25) in The Off Licence
  DEFB 0                  ; Item 247 at (4,26) in The Off Licence
  DEFB 0                  ; Item 248 at (4,27) in The Off Licence
  DEFB 0                  ; Item 249 at (4,28) in The Off Licence
  DEFB 0                  ; Item 250 at (4,29) in The Off Licence
  DEFB 0                  ; Item 251 at (4,30) in The Off Licence
  DEFB 2                  ; Item 252 at (0,22) in Under the MegaTree
  DEFB 157                ; Item 253 at (9,26) in The Nightmare Room
  DEFB 158                ; Item 254 at (14,8) in The Banyan Tree
  DEFB 161                ; Item 255 at (13,23) in The Bathroom
ITEMTABLE2:
  DEFS 173                ; Unused
  DEFB 25                 ; Item 173 at (8,25) in Watch Tower
  DEFB 42                 ; Item 174 at (9,10) in Watch Tower
  DEFB 52                 ; Item 175 at (9,20) in Watch Tower
  DEFB 56                 ; Item 176 at (9,24) in Watch Tower
  DEFB 201                ; Item 177 at (14,9) in West Wing Roof
  DEFB 179                ; Item 178 at (13,19) in West Wing Roof
  DEFB 123                ; Item 179 at (11,27) in West Wing Roof
  DEFB 70                 ; Item 180 at (10,6) in West Wing Roof
  DEFB 31                 ; Item 181 at (8,31) in Conservatory Roof
  DEFB 25                 ; Item 182 at (8,25) in Conservatory Roof
  DEFB 28                 ; Item 183 at (8,28) in Conservatory Roof
  DEFB 22                 ; Item 184 at (8,22) in Conservatory Roof
  DEFB 100                ; Item 185 at (11,4) in Swimming Pool
  DEFB 153                ; Item 186 at (12,25) in On the Roof
  DEFB 159                ; Item 187 at (4,31) in I'm sure I've seen this
                          ; before..
  DEFB 159                ; Item 188 at (4,31) in We must perform a Quirkafleeg
  DEFB 159                ; Item 189 at (4,31) in Up on the Battlements
  DEFB 26                 ; Item 190 at (8,26) in The Bow
  DEFB 205                ; Item 191 at (14,13) in The Yacht
  DEFB 182                ; Item 192 at (13,22) in The Beach
  DEFB 60                 ; Item 193 at (1,28) in Orangery
  DEFB 208                ; Item 194 at (6,16) in Orangery
  DEFB 166                ; Item 195 at (13,6) in Orangery
  DEFB 221                ; Item 196 at (6,29) in The Chapel
  DEFB 55                 ; Item 197 at (9,23) in Above the West Bedroom
  DEFB 182                ; Item 198 at (13,22) in The Beach
  DEFB 122                ; Item 199 at (3,26) in First Landing
  DEFB 34                 ; Item 200 at (9,2) in Top Landing
  DEFB 67                 ; Item 201 at (2,3) in Cold Store
  DEFB 168                ; Item 202 at (5,8) in Cold Store
  DEFB 6                  ; Item 203 at (8,6) in Cold Store
  DEFB 102                ; Item 204 at (11,6) in Cold Store
  DEFB 199                ; Item 205 at (14,7) in Tool  Shed
  DEFB 201                ; Item 206 at (6,9) in To the Kitchens    Main
                          ; Stairway
  DEFB 34                 ; Item 207 at (9,2) in To the Kitchens    Main
                          ; Stairway
  DEFB 243                ; Item 208 at (7,19) in Dr Jones will never believe
                          ; this
  DEFB 179                ; Item 209 at (13,19) in Tree Top
  DEFB 112                ; Item 210 at (3,16) in Tree Top
  DEFB 150                ; Item 211 at (4,22) in Tree Top
  DEFB 101                ; Item 212 at (11,5) in On a Branch Over the Drive
  DEFB 239                ; Item 213 at (7,15) in Cuckoo's Nest
  DEFB 106                ; Item 214 at (3,10) in Tree Root
  DEFB 157                ; Item 215 at (4,29) in Tree Root
  DEFB 73                 ; Item 216 at (2,9) in Priests' Hole
  DEFB 145                ; Item 217 at (12,17) in Ballroom West
  DEFB 146                ; Item 218 at (12,18) in Ballroom West
  DEFB 148                ; Item 219 at (12,20) in Ballroom West
  DEFB 150                ; Item 220 at (12,22) in Ballroom West
  DEFB 151                ; Item 221 at (12,23) in Ballroom West
  DEFB 90                 ; Item 222 at (2,26) in Rescue Esmerelda
  DEFB 139                ; Item 223 at (4,11) in The Front Door
  DEFB 153                ; Item 224 at (12,25) in Ballroom West
  DEFB 155                ; Item 225 at (12,27) in Ballroom West
  DEFB 122                ; Item 226 at (3,26) in Rescue Esmerelda
  DEFB 194                ; Item 227 at (6,2) in Out on a limb
  DEFB 37                 ; Item 228 at (1,5) in Out on a limb
  DEFB 146                ; Item 229 at (4,18) in On top of the house
  DEFB 98                 ; Item 230 at (3,2) in The Forgotten Abbey
  DEFB 97                 ; Item 231 at (11,1) in At the Foot of the MegaTree
  DEFB 68                 ; Item 232 at (10,4) in At the Foot of the MegaTree
  DEFB 103                ; Item 233 at (11,7) in At the Foot of the MegaTree
  DEFB 156                ; Item 234 at (4,28) in The Wine Cellar
  DEFB 228                ; Item 235 at (7,4) in The Wine Cellar
  DEFB 252                ; Item 236 at (7,28) in The Wine Cellar
  DEFB 66                 ; Item 237 at (10,2) in The Wine Cellar
  DEFB 92                 ; Item 238 at (10,28) in The Wine Cellar
  DEFB 164                ; Item 239 at (13,4) in The Wine Cellar
  DEFB 147                ; Item 240 at (4,19) in The Off Licence
  DEFB 148                ; Item 241 at (4,20) in The Off Licence
  DEFB 149                ; Item 242 at (4,21) in The Off Licence
  DEFB 150                ; Item 243 at (4,22) in The Off Licence
  DEFB 151                ; Item 244 at (4,23) in The Off Licence
  DEFB 152                ; Item 245 at (4,24) in The Off Licence
  DEFB 153                ; Item 246 at (4,25) in The Off Licence
  DEFB 154                ; Item 247 at (4,26) in The Off Licence
  DEFB 155                ; Item 248 at (4,27) in The Off Licence
  DEFB 156                ; Item 249 at (4,28) in The Off Licence
  DEFB 157                ; Item 250 at (4,29) in The Off Licence
  DEFB 158                ; Item 251 at (4,30) in The Off Licence
  DEFB 22                 ; Item 252 at (0,22) in Under the MegaTree
  DEFB 58                 ; Item 253 at (9,26) in The Nightmare Room
  DEFB 200                ; Item 254 at (14,8) in The Banyan Tree
  DEFB 183                ; Item 255 at (13,23) in The Bathroom

; Toilet graphics
;
; Used by the routine at DRAWTOILET.
TOILET0:
  DEFB 0,15,0,63,0,15,48,15,12,15,3,15,0,207,0,47
  DEFB 0,8,63,248,63,240,63,238,31,223,31,219,15,251,15,251
TOILET1:
  DEFB 0,15,0,63,0,15,0,15,0,15,0,15,0,15,63,239
  DEFB 0,8,63,248,63,240,63,238,31,223,31,219,15,251,15,251
TOILET2:
  DEFB 3,175,1,191,1,143,1,143,127,143,127,143,71,207,15,207
  DEFB 0,8,63,248,63,240,63,238,31,223,31,219,15,251,15,251
TOILET3:
  DEFB 0,15,4,63,30,47,59,47,93,143,14,143,7,207,15,207
  DEFB 0,8,63,248,63,240,63,238,31,223,31,219,15,251,15,251

; Unused
  DEFB 128,128,128,128,140,143,143,143,143,143,128,128,191,143,143,143
  DEFB 133,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128
  DEFB 128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128
  DEFB 128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128
  DEFB 128,128,191,191,191,191,191,191,191,191,191,143,179,176,176,128
  DEFB 133,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128
  DEFB 128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128
  DEFB 128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128
  DEFS 1024

; Guardian graphics
;
; Used by the routine at DRAWTHINGS.
;
; This guardian (page 171, sprites 0-3) appears in The Banyan Tree and A bit of
; tree.
GUARDIANS:
  DEFB 0,0,0,0,42,170,127,255,127,255,124,63,124,63,127,255
  DEFB 127,255,127,255,0,0,42,170,0,0,42,170,0,0,0,0
  DEFB 0,72,1,44,4,188,18,254,75,254,46,127,188,63,254,124
  DEFB 255,242,127,201,127,37,60,148,50,80,9,64,5,0,4,0
  DEFB 0,0,7,224,15,240,23,232,15,240,22,104,14,112,22,104
  DEFB 15,240,23,232,15,240,23,232,8,16,23,232,16,8,0,0
  DEFB 18,0,52,128,61,32,127,72,127,210,254,116,252,61,62,127
  DEFB 79,255,147,254,164,254,41,60,10,76,2,144,0,160,0,32
; This guardian (page 171, sprites 4-7) appears in The Chapel and The Banyan
; Tree.
  DEFB 0,0,0,0,0,0,2,2,34,2,169,84,168,216,113,116
  DEFB 34,218,59,174,46,171,36,249,32,219,32,216,1,84,2,138
  DEFB 0,0,2,2,2,2,1,116,0,216,81,84,82,250,35,222
  DEFB 231,171,30,249,24,217,8,217,9,84,6,138,4,0,0,0
  DEFB 2,2,3,142,0,248,1,84,2,218,3,254,17,139,75,169
  DEFB 43,251,150,248,108,216,5,84,2,138,1,0,0,128,0,64
  DEFB 0,0,2,2,2,2,1,116,0,216,81,84,82,250,35,222
  DEFB 231,171,30,249,24,217,8,217,9,84,6,138,4,0,0,0
; This guardian (page 172, sprites 0-3) appears in East Wall Base, The Chapel
; and West  Wing.
  DEFB 32,4,174,236,187,125,224,7,189,181,160,4,59,124,0,0
  DEFB 29,176,32,4,59,124,162,68,253,183,160,4,59,220,32,4
  DEFB 32,4,55,180,160,4,251,127,160,4,61,180,32,4,27,120
  DEFB 0,0,61,180,162,68,187,125,224,7,189,181,183,236,32,4
  DEFB 32,4,58,220,61,189,224,7,59,117,32,4,61,188,0,0
  DEFB 27,112,32,4,61,189,162,69,251,119,160,5,61,237,32,4
  DEFB 32,4,59,117,160,5,253,191,160,5,59,117,32,4,29,184
  DEFB 0,0,59,116,36,132,61,189,224,7,59,117,46,236,32,4
; This guardian (page 172, sprites 4-7) appears in The Banyan Tree and
; Orangery.
  DEFB 15,240,23,232,24,24,55,204,40,36,83,146,100,74,169,43
  DEFB 170,43,106,74,105,146,36,36,51,204,24,24,23,232,15,240
  DEFB 0,224,7,240,56,24,115,204,100,38,73,147,82,75,212,43
  DEFB 213,42,212,202,210,18,73,228,36,12,19,244,8,120,7,128
  DEFB 1,128,7,224,24,24,115,206,164,37,201,149,210,85,212,85
  DEFB 212,149,210,37,201,201,228,19,115,230,24,24,7,224,1,128
  DEFB 7,0,8,224,31,220,48,38,103,146,200,74,211,42,212,171
  DEFB 84,43,82,75,73,147,36,38,51,204,40,24,31,240,1,224
; This guardian (page 173, sprites 0-3) appears in On a Branch Over the Drive,
; Conservatory Roof and Tool  Shed.
  DEFB 12,0,12,0,12,0,12,0,12,0,30,0,18,0,51,0
  DEFB 63,0,115,128,97,128,97,128,192,192,192,192,128,64,128,64
  DEFB 8,64,8,64,12,192,4,128,7,128,7,128,12,192,28,224
  DEFB 59,112,48,48,96,24,96,24,64,8,192,12,128,4,128,4
  DEFB 2,16,2,16,3,48,1,32,1,224,1,224,3,48,7,56
  DEFB 14,220,12,12,24,6,24,6,16,2,48,3,32,1,32,1
  DEFB 0,48,0,48,0,48,0,48,0,48,0,120,0,72,0,204
  DEFB 0,252,1,206,1,134,1,134,3,3,3,3,2,1,2,1
; This guardian (page 173, sprites 4-7) appears in Top Landing, Tree Root and
; West Bedroom.
  DEFB 0,0,0,0,0,0,127,254,64,2,255,255,222,123,192,3
  DEFB 192,3,222,123,255,255,64,2,127,254,0,0,0,0,0,0
  DEFB 10,0,29,0,54,128,103,64,195,160,113,208,184,104,92,52
  DEFB 44,58,22,29,11,142,5,195,2,230,1,108,0,184,0,80
  DEFB 7,224,31,248,20,40,22,104,22,104,22,104,22,104,20,40
  DEFB 20,40,22,104,22,104,22,104,22,104,20,40,31,248,7,224
  DEFB 0,80,0,184,1,108,2,230,5,195,11,142,22,29,44,58
  DEFB 92,52,184,104,113,208,195,160,103,64,54,128,29,0,10,0
; This guardian (page 174, sprites 0-3) appears in To the Kitchens    Main
; Stairway, Halfway up the East Wall, Conservatory Roof and The Bow.
  DEFB 126,0,153,0,255,0,129,0,126,0,24,0,36,0,36,0
  DEFB 66,0,66,0,129,0,231,0,165,0,195,0,165,0,231,0
  DEFB 0,0,31,128,38,64,57,192,48,192,31,128,9,0,16,128
  DEFB 32,64,64,32,128,16,224,112,160,80,192,48,160,80,224,112
  DEFB 0,0,0,0,0,0,7,224,9,144,14,112,14,112,6,96
  DEFB 27,216,96,6,128,1,224,7,160,5,192,3,160,5,224,7
  DEFB 0,0,1,248,2,100,3,156,3,12,1,248,0,144,1,8
  DEFB 2,4,4,2,8,1,14,7,10,5,12,3,10,5,14,7
; This guardian (page 174, sprites 4-7) appears in Under the Drive.
  DEFB 12,0,18,0,33,16,18,32,140,64,82,128,47,0,47,0
  DEFB 95,128,95,128,95,128,0,0,255,192,93,128,93,128,255,192
  DEFB 0,1,0,2,131,4,76,200,35,16,20,160,11,192,11,192
  DEFB 23,224,23,224,23,224,0,0,63,240,27,176,27,176,63,240
  DEFB 128,0,64,0,32,1,17,226,8,196,5,40,2,240,2,240
  DEFB 5,248,5,248,5,248,0,0,15,252,11,116,11,116,15,252
  DEFB 0,0,0,0,8,48,4,204,2,49,1,74,0,188,0,188
  DEFB 1,126,1,126,1,126,0,0,3,255,2,237,2,237,3,255
; This guardian (page 175, sprites 0-3) appears in Top Landing, Under the Roof
; and Tool  Shed.
  DEFB 56,0,124,0,126,0,109,128,68,64,108,64,124,32,124,0
  DEFB 124,12,124,50,252,204,255,48,252,192,127,0,108,0,56,0
  DEFB 14,96,31,148,31,8,27,32,17,80,27,80,31,160,31,160
  DEFB 127,64,223,64,223,128,159,128,127,0,31,0,27,0,14,0
  DEFB 3,128,7,192,7,224,6,216,4,68,14,196,127,194,239,192
  DEFB 239,192,143,192,119,192,7,192,7,192,7,192,6,192,3,128
  DEFB 0,224,1,240,1,240,1,178,1,21,1,181,1,250,1,250
  DEFB 7,244,13,244,13,248,9,248,7,240,1,240,1,176,0,224
; This guardian (page 175, sprites 4-7) appears in The Drive, Cuckoo's Nest, To
; the Kitchens    Main Stairway and Back Stairway.
  DEFB 12,0,22,0,47,0,47,0,79,128,95,128,95,128,159,192
  DEFB 191,192,189,192,186,192,189,64,90,128,93,128,63,0,12,0
  DEFB 0,0,0,0,0,0,0,112,3,152,12,120,51,248,71,240
  DEFB 95,240,191,240,190,160,189,96,90,192,93,64,63,128,14,0
  DEFB 0,0,0,0,0,0,0,0,3,192,28,120,33,252,79,254
  DEFB 95,254,159,255,191,239,95,86,95,174,35,84,31,248,3,192
  DEFB 0,0,0,0,0,0,14,0,23,192,23,240,19,252,11,254
  DEFB 11,254,11,215,5,235,5,215,2,234,2,126,1,140,0,112
; This guardian (page 176, sprites 0-3) appears in The Attic.
  DEFB 7,192,24,48,35,136,68,68,136,34,144,19,144,19,136,34
  DEFB 68,68,35,136,24,48,7,192,3,0,3,0,3,0,3,128
  DEFB 7,192,24,48,32,8,67,132,132,66,136,35,136,35,132,66
  DEFB 67,132,32,8,24,48,7,192,15,128,29,208,8,224,4,64
  DEFB 7,192,31,240,62,120,124,60,126,124,255,254,255,254,240,0
  DEFB 255,128,127,240,127,252,63,248,31,240,7,192,0,0,0,0
  DEFB 7,192,31,240,60,248,120,124,124,252,255,128,252,0,240,0
  DEFB 248,0,126,0,127,128,63,224,31,240,7,192,0,0,0,0
; This guardian (page 176, sprites 4-5) appears in Rescue Esmerelda.
  DEFB 7,192,8,32,10,160,8,32,11,160,16,16,37,72,10,160
  DEFB 61,120,70,196,7,192,2,128,5,64,15,224,20,128,8,192
  DEFB 7,192,8,32,10,160,8,32,57,56,0,0,5,64,10,160
  DEFB 29,112,22,208,23,208,10,160,5,64,15,224,2,80,6,32
; This guardian (page 176, sprites 6-7) appears in The Hall and West  Wing.
  DEFB 1,128,0,0,1,128,69,145,1,128,137,162,69,145,1,128
  DEFB 205,179,205,179,205,179,205,179,35,196,18,72,13,176,0,0
  DEFB 1,128,0,0,1,128,137,162,1,128,69,145,137,162,1,128
  DEFB 205,179,205,179,205,179,205,179,35,196,18,72,13,176,0,0
; This guardian (page 177, sprites 0-7) is not used.
  DEFB 22,0,22,0,22,0,22,0,22,0,22,0,22,0,22,0
  DEFB 255,192,0,0,82,128,192,192,51,0,179,64,8,0,45,0
  DEFB 5,128,5,128,5,128,5,128,5,128,5,128,5,128,5,128
  DEFB 63,240,0,0,13,32,36,0,12,208,12,240,40,0,13,32
  DEFB 1,96,1,96,1,96,1,96,1,96,1,96,1,96,1,96
  DEFB 15,252,0,0,2,208,0,64,11,52,3,48,12,12,5,40
  DEFB 0,88,0,88,0,88,0,88,0,88,0,88,0,88,0,88
  DEFB 3,255,0,0,1,44,0,5,3,204,2,204,0,9,1,44
  DEFB 0,64,1,128,2,64,5,192,11,192,11,192,23,192,23,192
  DEFB 22,64,22,0,22,0,22,0,22,0,22,0,22,0,22,0
  DEFB 3,0,15,192,8,64,19,32,19,32,8,64,15,192,7,128
  DEFB 5,128,5,128,5,128,5,128,5,128,5,128,5,128,5,128
  DEFB 8,0,14,0,11,0,11,128,11,192,11,192,11,224,15,224
  DEFB 9,96,1,96,1,96,1,96,1,96,1,96,1,96,1,96
  DEFB 0,48,0,220,0,188,1,126,1,126,0,188,0,188,0,88
  DEFB 0,88,0,88,0,88,0,88,0,88,0,88,0,88,0,88
; This guardian (page 178, sprites 0-3) appears in On top of the house and The
; Yacht.
  DEFB 0,3,0,15,0,31,128,63,192,127,209,255,95,255,159,255
  DEFB 207,255,211,255,223,252,255,240,255,224,127,192,63,128,14,0
  DEFB 0,0,0,0,80,1,216,3,156,7,79,31,215,255,219,255
  DEFB 255,255,255,255,255,255,255,255,143,254,7,252,3,248,0,224
  DEFB 0,0,3,128,7,192,143,192,223,224,219,241,87,255,143,255
  DEFB 223,255,223,255,255,255,252,127,248,63,112,63,0,31,0,14
  DEFB 0,56,0,254,1,255,131,255,199,255,223,255,95,255,155,255
  DEFB 199,255,223,255,223,199,255,1,254,0,124,0,56,0,0,0
; This guardian (page 178, sprites 4-7) appears in On a Branch Over the Drive,
; Orangery, Dr Jones will never believe this, Under the Drive, Nomen Luni and
; Back Stairway.
  DEFB 3,192,14,240,29,248,62,252,83,206,101,166,164,39,113,143
  DEFB 191,247,85,239,110,118,83,202,56,28,22,120,11,240,2,192
  DEFB 3,192,13,240,26,248,63,252,81,140,100,38,197,167,179,207
  DEFB 93,187,170,119,95,250,114,78,56,28,28,56,11,240,2,192
  DEFB 3,192,11,240,21,120,63,252,80,12,101,166,199,231,241,143
  DEFB 190,127,221,247,44,58,90,94,44,60,22,120,11,240,3,192
  DEFB 3,192,13,240,26,248,63,252,81,140,100,38,197,167,179,207
  DEFB 93,187,170,119,95,250,114,78,56,28,28,56,11,240,2,192
; The next 256 bytes are unused.
  DEFS 256
; This guardian (page 180, sprites 0-7) appears in The Forgotten Abbey, The
; Chapel, First Landing, Swimming Pool and The Wine Cellar.
  DEFB 14,0,21,0,42,128,23,0,255,0,22,0,12,0,31,128
  DEFB 63,128,127,0,42,0,127,0,111,0,127,0,24,0,56,0
  DEFB 3,128,5,64,10,160,5,192,127,192,5,128,3,0,7,224
  DEFB 15,224,31,192,10,128,31,192,27,192,31,192,24,128,56,0
  DEFB 0,224,1,80,2,168,1,112,15,240,1,96,0,192,1,248
  DEFB 3,248,7,240,2,160,7,240,6,240,7,240,6,48,0,96
  DEFB 0,56,0,84,0,170,0,92,1,252,0,88,0,48,0,126
  DEFB 0,254,1,252,0,168,1,252,1,188,1,252,1,24,0,56
  DEFB 28,0,42,0,85,0,58,0,63,128,26,0,12,0,126,0
  DEFB 127,0,63,128,21,0,63,128,61,128,63,128,24,128,28,0
  DEFB 7,0,10,128,21,64,14,128,15,240,6,128,3,0,31,128
  DEFB 31,192,15,224,5,64,15,224,15,96,15,224,12,96,6,0
  DEFB 1,192,2,160,5,80,3,160,3,254,1,160,0,192,7,224
  DEFB 7,240,3,248,1,80,3,248,3,216,3,248,1,24,0,28
  DEFB 0,112,0,168,1,84,0,232,0,255,0,104,0,48,1,248
  DEFB 1,252,0,254,0,84,0,254,0,246,0,254,0,24,0,28
; This guardian (page 181, sprites 0-7) appears in At the Foot of the MegaTree,
; Cuckoo's Nest, Out on a limb, The Banyan Tree, The Wine Cellar, Tool  Shed,
; West Wing Roof and The Yacht.
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0,6,0,63,0,31,0,13,0
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,128
  DEFB 15,192,7,64,3,64,79,64,7,64,3,32,15,32,135,32
  DEFB 0,96,3,240,1,208,0,208,3,208,1,208,0,200,3,200
  DEFB 1,200,0,200,11,200,33,200,0,200,11,196,1,196,32,196
  DEFB 0,0,0,0,0,0,0,0,0,0,0,24,0,252,0,116
  DEFB 0,52,0,244,0,116,1,54,0,250,2,122,0,58,4,250
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0,48,0,126,0,124,0,88,0
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,0,12,0
  DEFB 31,128,23,0,22,0,23,128,23,0,38,16,39,128,39,8
  DEFB 3,0,7,224,5,192,5,128,5,224,5,192,9,128,9,224
  DEFB 9,208,9,128,9,232,9,194,9,128,17,232,17,192,17,130
  DEFB 0,0,0,0,0,0,0,0,0,0,0,192,1,248,1,112
  DEFB 1,96,1,120,1,112,2,96,2,120,2,112,2,97,2,120
; This guardian (page 182, sprites 0-7) appears in Under the MegaTree, The
; Hall, Tree Top and Emergency Generator.
FLYINGPIG0:
  DEFB 0,0,0,0,0,0,0,0,0,0,19,64,23,128,63,192
  DEFB 85,64,250,160,253,64,31,160,8,128,5,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0,0,0,8,208,5,224,15,240
  DEFB 22,176,61,80,62,168,7,80,2,168,2,84,0,40,0,20
  DEFB 0,0,0,0,0,0,0,0,0,0,2,52,1,120,3,252
  DEFB 5,84,15,170,15,212,1,250,0,136,1,4,0,0,0,0
  DEFB 0,3,0,10,0,21,0,10,0,21,0,171,0,86,0,235
  DEFB 1,85,3,235,3,255,0,126,0,34,0,34,0,0,0,0
  DEFB 128,0,80,0,168,0,80,0,168,0,213,0,106,0,215,0
  DEFB 170,128,215,192,255,192,126,0,68,0,68,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0,0,0,44,64,30,128,63,192
  DEFB 42,160,85,240,43,240,95,128,17,0,32,128,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0,0,0,11,16,7,160,15,240
  DEFB 13,104,10,188,21,124,10,224,21,64,42,64,20,0,40,0
  DEFB 0,0,0,0,0,0,0,0,0,0,2,200,1,232,3,252
  DEFB 2,170,5,95,2,191,5,248,1,16,0,160,0,0,0,0
; This guardian (page 183, sprites 0-1) appears in The Kitchen and West of
; Kitchen.
  DEFB 10,160,21,80,10,160,5,66,5,69,5,69,7,194,13,98
  DEFB 15,227,60,127,123,191,239,227,174,226,71,194,206,192,192,224
  DEFB 192,0,202,160,213,80,202,160,69,64,69,66,167,197,205,101
  DEFB 111,226,124,122,58,191,15,239,14,227,7,194,6,226,14,2
; This guardian (page 183, sprites 2-3) appears in Tree Root, West Bedroom and
; Above the West Bedroom.
  DEFB 3,0,4,128,5,192,11,160,21,64,23,96,23,96,31,224
  DEFB 19,144,45,104,47,120,47,120,47,120,47,120,175,121,127,254
  DEFB 0,192,1,32,1,112,2,232,5,80,5,216,5,216,7,248
  DEFB 9,200,22,180,23,188,23,188,23,188,23,188,151,189,127,254
; This guardian (page 183, sprites 4-7) appears in To the Kitchens    Main
; Stairway and Back Stairway.
  DEFB 118,0,0,0,110,0,118,0,110,0,0,0,118,0,110,0
  DEFB 0,0,118,0,0,0,0,0,110,0,118,0,0,0,110,0
  DEFB 29,128,42,0,39,96,81,176,75,208,144,128,170,232,148,184
  DEFB 145,128,170,216,145,0,85,0,75,176,43,96,36,0,29,128
  DEFB 3,192,12,48,16,152,50,20,104,130,64,42,181,17,200,69
  DEFB 164,145,154,5,72,66,85,10,40,84,18,136,13,112,3,192
  DEFB 1,184,0,84,6,228,13,138,11,210,1,9,23,85,29,41
  DEFB 1,137,27,85,0,137,0,170,13,210,6,212,0,36,1,184
; This guardian (page 184, sprites 0-7) appears in Under the MegaTree, On the
; Roof, Ballroom West and Tree Root.
  DEFB 0,0,6,0,8,0,56,0,80,0,240,0,248,0,60,0
  DEFB 62,0,126,0,159,0,31,0,31,192,14,192,24,0,96,0
  DEFB 0,0,0,0,15,0,20,128,60,0,14,0,63,0,15,128
  DEFB 31,128,47,192,15,192,7,240,3,176,1,0,2,0,4,0
  DEFB 0,0,0,96,0,128,3,128,13,0,7,0,3,128,7,192
  DEFB 11,224,7,224,9,240,1,240,1,252,0,236,1,128,6,0
  DEFB 0,0,0,0,0,4,0,8,0,112,0,160,1,224,0,112
  DEFB 1,248,0,120,1,252,0,124,0,124,0,127,0,59,0,240
  DEFB 4,0,4,0,12,0,12,0,22,0,30,0,62,0,108,0
  DEFB 158,0,63,0,63,0,63,0,127,0,86,0,28,0,6,0
  DEFB 4,128,8,64,19,32,19,32,23,160,19,32,31,224,27,96
  DEFB 39,144,15,192,15,192,15,192,15,192,29,128,3,128,1,0
  DEFB 0,128,0,128,0,192,0,192,1,160,1,224,1,240,0,216
  DEFB 1,228,3,240,3,240,3,240,3,248,1,168,0,224,1,128
  DEFB 0,72,0,132,1,50,1,2,1,122,1,122,1,254,1,182
  DEFB 2,121,0,252,0,252,0,252,0,252,0,110,0,112,0,32
; This guardian (page 185, sprites 0-3) appears in On a Branch Over the Drive,
; To the Kitchens    Main Stairway and The Bathroom.
  DEFB 60,0,110,0,167,0,165,0,119,0,163,0,98,0,44,0
  DEFB 52,0,70,0,197,0,238,0,165,0,229,0,118,0,60,0
  DEFB 0,0,0,0,0,176,1,72,3,252,0,68,2,12,2,124
  DEFB 53,216,70,240,197,0,238,0,165,0,229,0,118,0,60,0
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  DEFB 52,52,70,70,197,197,238,238,165,165,229,229,118,118,60,60
  DEFB 0,0,0,0,15,0,27,128,62,64,48,64,34,0,63,192
  DEFB 18,180,13,70,0,197,0,238,0,165,0,229,0,118,0,60
; This guardian (page 185, sprites 4-7) appears in The Bridge.
  DEFB 3,192,6,96,13,240,26,216,53,92,32,4,127,254,215,117
  DEFB 178,39,215,117,127,254,21,120,37,164,2,64,17,136,0,0
  DEFB 3,192,6,96,13,240,26,184,53,92,32,4,127,254,174,235
  DEFB 228,77,174,235,127,254,21,120,10,80,1,128,4,32,1,0
  DEFB 3,192,6,96,13,240,26,216,53,92,32,4,127,254,237,219
  DEFB 168,147,237,219,127,254,21,120,37,164,2,64,17,136,4,32
  DEFB 3,192,6,96,13,240,26,232,53,92,32,4,127,254,219,183
  DEFB 201,21,219,183,127,254,21,120,10,66,65,144,4,0,17,68
; This guardian (page 186, sprites 0-3) appears in The Off Licence, Under the
; MegaTree, Inside the MegaTrunk, Out on a limb, Ballroom East, Ballroom West,
; East Wall Base and Tree Root.
  DEFB 7,224,15,240,223,251,63,252,31,248,31,248,49,140,106,86
  DEFB 177,141,158,249,13,240,29,184,54,108,89,154,140,49,7,224
  DEFB 7,224,15,240,31,248,255,255,31,248,31,248,49,140,42,84
  DEFB 113,142,158,249,141,241,29,184,50,76,88,26,76,50,7,224
  DEFB 7,224,15,240,31,248,223,251,63,252,31,248,49,140,42,84
  DEFB 48,140,93,250,141,177,154,89,18,72,24,24,44,52,199,227
  DEFB 7,224,143,241,95,250,63,252,31,248,17,136,46,116,42,84
  DEFB 113,142,94,250,141,241,29,184,18,72,120,30,140,49,7,224
; This guardian (page 186, sprites 4-7) appears in Entrance to Hades, The
; Chapel and Priests' Hole.
  DEFB 21,3,42,190,21,94,74,168,53,92,138,174,109,92,153,249
  DEFB 110,191,221,99,46,73,89,129,4,130,43,198,18,252,12,253
  DEFB 64,168,253,84,122,168,21,82,58,172,117,81,58,182,159,153
  DEFB 253,118,198,187,146,116,129,154,65,32,99,212,63,72,191,48
  DEFB 253,191,191,239,234,215,170,69,104,212,72,132,64,4,64,1
  DEFB 80,0,18,10,64,12,42,34,20,168,32,132,20,40,5,32
  DEFB 253,191,255,251,172,215,170,209,42,84,130,68,66,1,8,4
  DEFB 74,16,34,130,8,168,20,4,10,144,1,64,0,0,0,0
; This guardian (page 187, sprites 0-3) appears in Cold Store and Under the
; Roof.
  DEFB 12,0,54,128,127,64,126,128,253,0,254,128,255,128,255,0
  DEFB 85,0,42,0,20,0,42,0,20,0,8,0,20,0,8,0
  DEFB 3,0,13,160,31,208,31,160,63,64,63,160,63,224,63,192
  DEFB 53,64,42,128,5,0,10,128,5,0,2,0,5,0,2,0
  DEFB 0,192,3,104,7,244,7,232,15,208,15,232,15,248,15,240
  DEFB 5,80,10,160,1,64,2,160,9,64,0,128,1,64,0,128
  DEFB 0,48,0,218,1,253,1,250,3,244,3,250,3,254,3,252
  DEFB 3,84,2,168,0,80,0,168,0,80,0,32,0,80,0,32
; This guardian (page 187, sprites 4-7) appears in Inside the MegaTrunk, Tree
; Top and Watch Tower.
  DEFB 4,0,42,128,85,64,43,128,68,64,175,160,95,64,32,128
  DEFB 85,64,42,128,4,0,4,0,8,0,62,0,65,0,129,0
  DEFB 2,0,21,64,42,160,21,192,34,32,95,80,42,160,16,64
  DEFB 42,160,21,64,2,0,2,0,14,0,17,128,32,64,0,0
  DEFB 0,64,2,168,5,84,3,168,4,68,11,234,5,244,2,8
  DEFB 5,84,2,168,0,64,0,64,0,112,1,136,2,4,0,0
  DEFB 0,32,1,84,2,170,1,212,2,34,5,125,2,250,1,84
  DEFB 2,170,1,84,0,32,0,32,0,16,0,124,0,130,0,129
; This guardian (page 188, sprites 0-7) appears in The Bridge, The Drive,
; Inside the MegaTrunk, The Hall, Tree Top, Out on a limb, Rescue Esmerelda,
; I'm sure I've seen this before.., We must perform a Quirkafleeg, Ballroom
; East, To the Kitchens    Main Stairway, A bit of tree, Priests' Hole, Under
; the Drive, Nomen Luni, Watch Tower, West  Wing and West Wing Roof.
  DEFB 0,0,0,0,0,0,0,0,0,0,111,64,187,128,117,64
  DEFB 26,128,5,0,0,128,0,0,0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,160,1,64,2,128,1,80,54,160,93,112
  DEFB 55,224,3,192,0,0,0,0,0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0,0,0,0,244,25,184,47,84
  DEFB 25,168,0,80,0,8,0,0,0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0,0,0,0,61,3,126,5,215
  DEFB 3,106,0,20,0,40,0,20,0,10,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0,0,0,188,0,126,192,235,160
  DEFB 86,192,40,0,20,0,40,0,80,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0,0,0,47,0,29,152,42,244
  DEFB 21,152,10,0,16,0,0,0,0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,5,0,2,128,1,64,10,128,5,108,14,186
  DEFB 7,236,3,192,0,0,0,0,0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0,0,0,2,246,1,221,2,174
  DEFB 1,88,0,160,1,0,0,0,0,0,0,0,0,0,0,0
; This guardian (page 189, sprites 0-7) appears in Cold Store.
  DEFB 24,0,60,0,212,0,46,0,58,0,58,0,90,0,93,0
  DEFB 79,0,67,0,33,128,34,0,30,0,20,0,19,0,60,0
  DEFB 6,0,15,0,63,0,11,128,14,128,14,128,31,0,23,128
  DEFB 17,192,16,96,8,64,8,128,7,128,2,0,2,0,7,0
  DEFB 1,128,11,192,5,64,10,224,3,160,3,160,5,160,5,208
  DEFB 4,240,4,48,2,24,2,32,1,224,1,64,6,48,1,64
  DEFB 0,96,0,176,3,80,0,184,0,232,0,232,1,104,1,116
  DEFB 1,60,1,12,0,134,0,136,0,120,0,37,0,34,0,116
  DEFB 6,0,13,0,10,192,29,0,23,0,23,0,22,128,46,128
  DEFB 60,128,48,128,97,0,17,0,30,0,164,0,68,0,46,0
  DEFB 1,128,3,208,2,160,7,80,5,192,5,192,5,160,11,160
  DEFB 15,32,12,32,24,64,4,64,7,128,2,128,12,96,2,128
  DEFB 0,96,0,240,0,252,1,208,1,112,1,112,0,248,1,232
  DEFB 3,136,6,8,2,16,1,16,1,224,0,64,0,64,0,224
  DEFB 0,24,0,60,0,43,0,116,0,92,0,92,0,90,0,186
  DEFB 0,242,0,194,1,132,0,68,0,120,0,80,1,144,0,120
; This guardian (page 190, sprites 0-3) appears in The Off Licence, To the
; Kitchens    Main Stairway, Nomen Luni and The Bow.
  DEFB 12,0,18,0,33,0,45,0,33,0,18,0,12,0,12,0
  DEFB 30,0,0,0,55,0,64,0,13,64,172,64,64,128,46,0
  DEFB 14,192,29,160,31,0,31,224,31,0,29,160,14,192,3,0
  DEFB 7,128,0,0,14,192,0,32,43,0,35,80,16,96,7,0
  DEFB 4,8,6,24,7,56,15,252,7,56,6,216,4,200,0,192
  DEFB 1,224,0,0,1,208,4,8,8,212,10,192,0,8,3,176
  DEFB 0,220,1,110,0,62,1,254,0,62,1,110,0,220,0,48
  DEFB 0,120,0,0,0,184,1,2,2,177,0,53,1,0,0,220
; This guardian (page 190, sprites 4-7) appears in The Off Licence.
  DEFB 0,0,0,0,7,224,31,184,61,236,127,254,125,246,251,247
  DEFB 251,247,127,238,127,222,63,252,31,248,7,224,0,0,0,0
  DEFB 0,0,1,128,7,224,13,112,31,248,63,252,63,244,125,254
  DEFB 123,246,59,236,63,220,31,56,15,240,7,224,1,128,0,0
  DEFB 1,128,7,224,15,240,30,248,31,120,63,124,59,252,55,244
  DEFB 55,252,63,236,63,236,31,216,31,56,15,240,7,224,1,128
  DEFB 0,0,1,128,7,224,15,240,30,184,63,252,63,244,123,254
  DEFB 119,246,55,236,63,220,31,56,15,240,7,224,1,128,0,0
; This guardian (page 191, sprites 0-3) appears in At the Foot of the MegaTree,
; Inside the MegaTrunk, On a Branch Over the Drive, Dr Jones will never believe
; this and Nomen Luni.
  DEFB 128,0,64,0,163,96,83,96,41,192,22,160,11,224,7,112
  DEFB 2,168,5,212,2,42,3,229,3,226,3,97,3,98,7,112
  DEFB 0,0,0,0,6,48,86,48,169,192,86,160,11,224,6,48
  DEFB 3,232,5,212,2,42,3,228,3,234,23,100,14,96,4,112
  DEFB 0,0,0,0,3,96,3,96,1,192,30,160,43,224,86,180
  DEFB 163,106,69,213,130,34,3,225,3,226,3,96,3,96,7,112
  DEFB 0,0,0,0,6,48,86,48,169,192,86,160,11,224,7,244
  DEFB 2,42,5,213,2,34,3,225,3,224,3,116,3,56,7,16
; This guardian (page 191, sprites 4-5) appears in The Security Guard, I'm sure
; I've seen this before.. and Up on the Battlements.
  DEFB 2,4,5,14,10,132,21,196,23,68,43,228,47,224,18,68
  DEFB 8,130,7,44,21,80,42,164,85,68,170,132,69,0,29,192
  DEFB 2,0,5,0,10,132,21,206,23,68,43,228,47,228,18,68
  DEFB 8,128,7,4,21,82,42,172,85,64,170,132,69,4,29,196
; This guardian (page 191, sprites 6-7) appears in Rescue Esmerelda and Above
; the West Bedroom.
  DEFB 0,4,7,6,47,164,95,212,48,100,66,20,50,96,31,196
  DEFB 8,130,7,44,21,80,42,164,85,68,170,132,69,0,12,192
  DEFB 0,0,7,0,47,164,80,86,34,36,82,84,63,228,23,68
  DEFB 8,128,7,4,21,82,42,172,85,64,170,132,69,4,25,132

; Room 0: The Off Licence (teleport: 9)
;
; Used by the routine at INITROOM.
;
; The first 128 bytes are copied to ROOMLAYOUT and define the room layout. Each
; bit-pair (bits 7 and 6, 5 and 4, 3 and 2, or 1 and 0 of each byte) determines
; the type of tile (background, floor, wall or nasty) that will be drawn at the
; corresponding location.
  DEFB 0,0,0,0,0,0,0,0           ; Room layout
  DEFB 0,0,0,0,0,0,0,0           ;
  DEFB 0,0,0,0,0,0,0,0           ;
  DEFB 0,0,0,170,170,170,170,170 ;
  DEFB 0,0,0,128,0,0,0,2         ;
  DEFB 0,0,0,128,0,0,0,2         ;
  DEFB 0,0,0,140,0,0,0,2         ;
  DEFB 0,0,0,128,0,0,0,2         ;
  DEFB 0,0,0,128,0,0,0,2         ;
  DEFB 0,0,0,128,0,0,0,2         ;
  DEFB 0,0,0,128,0,0,0,2         ;
  DEFB 0,0,0,170,0,0,2,170       ;
  DEFB 0,0,0,0,0,0,0,2           ;
  DEFB 0,0,0,0,0,0,0,2           ;
  DEFB 0,0,0,0,0,0,0,2           ;
  DEFB 85,85,85,85,85,85,85,85   ;
; The next 32 bytes are copied to ROOMNAME and specify the room name.
  DEFM "         The Off Licence        " ; Room name
; The next 54 bytes are copied to BACKGROUND and contain the attributes and
; graphic data for the tiles used to build the room.
  DEFB 0,0,0,0,0,0,0,0,0  ; Background
  DEFB 68,255,222,108,136,18,64,4,0 ; Floor
  DEFB 22,34,255,136,255,34,255,136,255 ; Wall
  DEFB 5,248,136,158,189,189,158,136,248 ; Nasty
  DEFB 7,3,0,12,0,48,0,192,0 ; Ramp
  DEFB 67,51,255,51,0,255,0,170,0 ; Conveyor
; The next four bytes are copied to CONVDIR and specify the direction, location
; and length of the conveyor.
  DEFB 0                  ; Direction (left)
  DEFW 24371              ; Location in the attribute buffer at 24064: (9,19)
  DEFB 12                 ; Length
; The next four bytes are copied to RAMPDIR and specify the direction, location
; and length of the ramp.
  DEFB 1                  ; Direction (up to the right)
  DEFW 24535              ; Location in the attribute buffer at 24064: (14,23)
  DEFB 4                  ; Length
; The next byte is copied to BORDER and specifies the border colour.
  DEFB 5                  ; Border colour
; The next two bytes are copied to XROOM223, but are not used.
  DEFB 0,160              ; Unused
; The next eight bytes are copied to ITEM and define the item graphic.
  DEFB 24,24,60,126,98,98,98,126 ; Item graphic
; The next four bytes are copied to LEFT and specify the rooms to the left, to
; the right, above and below.
  DEFB 1                  ; Room to the left (The Bridge)
  DEFB 0                  ; Room to the right (The Off Licence)
  DEFB 0                  ; Room above (The Off Licence)
  DEFB 0                  ; Room below (The Off Licence)
; The next three bytes are copied to XROOM237, but are not used.
  DEFB 0,0,0              ; Unused
; The next eight pairs of bytes are copied to ENTITIES and specify the entities
; (ropes, arrows, guardians) in this room.
  DEFB 10,138             ; Guardian no. 10 (vertical), base sprite 4, x=10
                          ; (ENTITY10)
  DEFB 12,29              ; Guardian no. 12 (horizontal), base sprite 0,
                          ; initial x=29 (ENTITY12)
  DEFB 44,39              ; Guardian no. 44 (vertical), base sprite 1, x=7
                          ; (ENTITY44)
  DEFB 255,0              ; Terminator (ENTITY127)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)

; Room 1: The Bridge (teleport: 19)
;
; Used by the routine at INITROOM.
;
; The first 128 bytes are copied to ROOMLAYOUT and define the room layout. Each
; bit-pair (bits 7 and 6, 5 and 4, 3 and 2, or 1 and 0 of each byte) determines
; the type of tile (background, floor, wall or nasty) that will be drawn at the
; corresponding location.
  DEFB 0,0,0,0,0,0,0,0             ; Room layout
  DEFB 0,0,0,0,0,0,0,0             ;
  DEFB 0,0,0,0,0,0,0,0             ;
  DEFB 0,0,0,0,0,0,0,0             ;
  DEFB 0,0,0,0,0,0,0,0             ;
  DEFB 0,0,0,0,0,0,0,0             ;
  DEFB 0,0,0,0,0,0,0,0             ;
  DEFB 0,0,0,0,0,0,0,0             ;
  DEFB 0,0,0,0,0,0,0,0             ;
  DEFB 0,0,0,0,0,0,0,0             ;
  DEFB 0,0,0,0,0,0,0,0             ;
  DEFB 0,0,0,0,0,0,0,0             ;
  DEFB 0,0,10,0,42,0,0,0           ;
  DEFB 0,0,42,0,42,128,0,0         ;
  DEFB 0,0,170,255,234,160,0,0     ;
  DEFB 85,85,170,255,234,165,85,85 ;
; The next 32 bytes are copied to ROOMNAME and specify the room name.
  DEFM "         The Bridge             " ; Room name
; The next 54 bytes are copied to BACKGROUND and contain the attributes and
; graphic data for the tiles used to build the room.
  DEFB 0,0,0,0,0,0,0,0,0  ; Background
  DEFB 68,255,255,253,187,214,114,96,64 ; Floor
  DEFB 70,34,119,34,136,34,119,34,136 ; Wall
  DEFB 47,3,132,72,48,3,132,72,48 ; Nasty
  DEFB 7,3,3,12,13,50,55,194,200 ; Ramp
  DEFB 44,1,64,4,70,79,121,240,249 ; Conveyor
; The next four bytes are copied to CONVDIR and specify the direction, location
; and length of the conveyor.
  DEFB 1                  ; Direction (right)
  DEFW 24556              ; Location in the attribute buffer at 24064: (15,12)
  DEFB 5                  ; Length
; The next four bytes are copied to RAMPDIR and specify the direction, location
; and length of the ramp.
  DEFB 1                  ; Direction (up to the right)
  DEFW 24519              ; Location in the attribute buffer at 24064: (14,7)
  DEFB 3                  ; Length
; The next byte is copied to BORDER and specifies the border colour.
  DEFB 4                  ; Border colour
; The next two bytes are copied to XROOM223, but are not used.
  DEFB 64,160             ; Unused
; The next eight bytes are copied to ITEM and define the item graphic.
  DEFB 0,0,0,0,0,0,0,0    ; Item graphic (unused)
; The next four bytes are copied to LEFT and specify the rooms to the left, to
; the right, above and below.
  DEFB 2                  ; Room to the left (Under the MegaTree)
  DEFB 0                  ; Room to the right (The Off Licence)
  DEFB 0                  ; Room above (The Off Licence)
  DEFB 0                  ; Room below (The Off Licence)
; The next three bytes are copied to XROOM237, but are not used.
  DEFB 0,0,0              ; Unused
; The next eight pairs of bytes are copied to ENTITIES and specify the entities
; (ropes, arrows, guardians) in this room.
  DEFB 20,8               ; Guardian no. 20 (horizontal), base sprite 0,
                          ; initial x=8 (ENTITY20)
  DEFB 21,20              ; Guardian no. 21 (horizontal), base sprite 0,
                          ; initial x=20 (ENTITY21)
  DEFB 14,140             ; Guardian no. 14 (vertical), base sprite 4, x=12
                          ; (ENTITY14)
  DEFB 255,0              ; Terminator (ENTITY127)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)

; Room 2: Under the MegaTree (teleport: 29)
;
; Used by the routine at INITROOM.
;
; The first 128 bytes are copied to ROOMLAYOUT and define the room layout. Each
; bit-pair (bits 7 and 6, 5 and 4, 3 and 2, or 1 and 0 of each byte) determines
; the type of tile (background, floor, wall or nasty) that will be drawn at the
; corresponding location.
  DEFB 0,3,255,207,252,0,0,0    ; Room layout
  DEFB 170,170,175,255,63,0,0,0 ;
  DEFB 0,0,63,208,92,0,0,0      ;
  DEFB 0,0,0,0,0,0,0,0          ;
  DEFB 170,128,0,0,0,0,0,0      ;
  DEFB 0,1,64,4,0,4,0,0         ;
  DEFB 0,0,1,0,4,0,0,0          ;
  DEFB 170,168,0,0,0,1,0,0      ;
  DEFB 0,0,20,16,16,0,0,0       ;
  DEFB 0,0,0,0,0,0,0,0          ;
  DEFB 0,0,0,0,0,0,0,0          ;
  DEFB 0,0,0,0,0,0,0,0          ;
  DEFB 0,0,0,0,0,0,0,0          ;
  DEFB 0,0,0,0,0,0,0,0          ;
  DEFB 0,0,0,0,0,0,0,0          ;
  DEFB 85,85,85,85,85,85,85,85  ;
; The next 32 bytes are copied to ROOMNAME and specify the room name.
  DEFM "       Under the MegaTree       " ; Room name
; The next 54 bytes are copied to BACKGROUND and contain the attributes and
; graphic data for the tiles used to build the room.
  DEFB 0,0,0,0,0,0,0,0,0  ; Background
  DEFB 68,215,179,117,109,92,24,8,4 ; Floor
  DEFB 66,189,170,93,162,243,10,6,1 ; Wall
  DEFB 70,73,106,59,58,252,30,107,232 ; Nasty
  DEFB 255,0,0,0,255,0,0,0,0 ; Ramp (unused)
  DEFB 255,0,0,0,0,0,0,0,0 ; Conveyor (unused)
; The next four bytes are copied to CONVDIR and specify the direction, location
; and length of the conveyor.
  DEFB 0                  ; Direction (left)
  DEFW 0                  ; Location in the attribute buffer at 24064 (unused)
  DEFB 0                  ; Length: 0 (there is no conveyor in this room)
; The next four bytes are copied to RAMPDIR and specify the direction, location
; and length of the ramp.
  DEFB 0                  ; Direction (up to the left)
  DEFW 0                  ; Location in the attribute buffer at 24064 (unused)
  DEFB 0                  ; Length: 0 (there is no ramp in this room)
; The next byte is copied to BORDER and specifies the border colour.
  DEFB 6                  ; Border colour
; The next two bytes are copied to XROOM223, but are not used.
  DEFB 128,160            ; Unused
; The next eight bytes are copied to ITEM and define the item graphic.
  DEFB 129,66,36,24,60,24,60,195 ; Item graphic
; The next four bytes are copied to LEFT and specify the rooms to the left, to
; the right, above and below.
  DEFB 3                  ; Room to the left (At the Foot of the MegaTree)
  DEFB 1                  ; Room to the right (The Bridge)
  DEFB 0                  ; Room above (The Off Licence)
  DEFB 0                  ; Room below (The Off Licence)
; The next three bytes are copied to XROOM237, but are not used.
  DEFB 0,0,0              ; Unused
; The next eight pairs of bytes are copied to ENTITIES and specify the entities
; (ropes, arrows, guardians) in this room.
  DEFB 32,16              ; Guardian no. 32 (horizontal), base sprite 0,
                          ; initial x=16 (ENTITY32)
  DEFB 42,14              ; Guardian no. 42 (vertical), base sprite 0, x=14
                          ; (ENTITY42)
  DEFB 26,27              ; Guardian no. 26 (horizontal), base sprite 0,
                          ; initial x=27 (ENTITY26)
  DEFB 255,0              ; Terminator (ENTITY127)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)

; Room 3: At the Foot of the MegaTree (teleport: 129)
;
; Used by the routine at INITROOM.
;
; The first 128 bytes are copied to ROOMLAYOUT and define the room layout. Each
; bit-pair (bits 7 and 6, 5 and 4, 3 and 2, or 1 and 0 of each byte) determines
; the type of tile (background, floor, wall or nasty) that will be drawn at the
; corresponding location.
  DEFB 48,195,8,0,8,0,0,0        ; Room layout
  DEFB 48,195,8,0,10,170,170,170 ;
  DEFB 48,195,8,0,8,0,0,0        ;
  DEFB 48,195,8,1,88,0,0,0       ;
  DEFB 48,195,9,0,10,170,170,170 ;
  DEFB 48,195,8,20,0,0,0,0       ;
  DEFB 48,195,8,0,0,0,0,0        ;
  DEFB 48,195,9,0,26,170,170,170 ;
  DEFB 48,195,8,0,8,0,0,0        ;
  DEFB 48,195,8,0,88,0,0,0       ;
  DEFB 48,3,9,64,8,0,0,0         ;
  DEFB 0,0,8,0,8,0,0,0           ;
  DEFB 0,0,8,0,8,0,0,0           ;
  DEFB 0,0,0,0,80,0,0,0          ;
  DEFB 0,0,0,0,0,0,0,0           ;
  DEFB 85,85,85,85,85,85,85,85   ;
; The next 32 bytes are copied to ROOMNAME and specify the room name.
  DEFM "  At the Foot of the MegaTree   " ; Room name
; The next 54 bytes are copied to BACKGROUND and contain the attributes and
; graphic data for the tiles used to build the room.
  DEFB 0,0,0,0,0,0,0,0,0  ; Background
  DEFB 4,240,249,191,31,12,16,40,68 ; Floor
  DEFB 22,171,171,173,170,170,174,85,85 ; Wall
  DEFB 6,110,116,56,30,110,116,56,30 ; Nasty
  DEFB 7,192,0,48,0,12,0,3,0 ; Ramp
  DEFB 255,0,0,0,0,0,0,0,0 ; Conveyor (unused)
; The next four bytes are copied to CONVDIR and specify the direction, location
; and length of the conveyor.
  DEFB 0                  ; Direction (left)
  DEFW 0                  ; Location in the attribute buffer at 24064 (unused)
  DEFB 0                  ; Length: 0 (there is no conveyor in this room)
; The next four bytes are copied to RAMPDIR and specify the direction, location
; and length of the ramp.
  DEFB 0                  ; Direction (up to the left)
  DEFW 24463              ; Location in the attribute buffer at 24064: (12,15)
  DEFB 3                  ; Length
; The next byte is copied to BORDER and specifies the border colour.
  DEFB 2                  ; Border colour
; The next two bytes are copied to XROOM223, but are not used.
  DEFB 192,160            ; Unused
; The next eight bytes are copied to ITEM and define the item graphic.
  DEFB 64,160,92,34,33,34,20,8 ; Item graphic
; The next four bytes are copied to LEFT and specify the rooms to the left, to
; the right, above and below.
  DEFB 4                  ; Room to the left (The Drive)
  DEFB 2                  ; Room to the right (Under the MegaTree)
  DEFB 8                  ; Room above (Inside the MegaTrunk)
  DEFB 0                  ; Room below (The Off Licence)
; The next three bytes are copied to XROOM237, but are not used.
  DEFB 0,0,0              ; Unused
; The next eight pairs of bytes are copied to ENTITIES and specify the entities
; (ropes, arrows, guardians) in this room.
  DEFB 6,2                ; Guardian no. 6 (vertical), base sprite 0, x=2
                          ; (ENTITY6)
  DEFB 7,5                ; Guardian no. 7 (vertical), base sprite 0, x=5
                          ; (ENTITY7)
  DEFB 8,8                ; Guardian no. 8 (vertical), base sprite 0, x=8
                          ; (ENTITY8)
  DEFB 53,9               ; Guardian no. 53 (horizontal), base sprite 0,
                          ; initial x=9 (ENTITY53)
  DEFB 255,0              ; Terminator (ENTITY127)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)

; Room 4: The Drive (teleport: 39)
;
; Used by the routine at INITROOM.
;
; The first 128 bytes are copied to ROOMLAYOUT and define the room layout. Each
; bit-pair (bits 7 and 6, 5 and 4, 3 and 2, or 1 and 0 of each byte) determines
; the type of tile (background, floor, wall or nasty) that will be drawn at the
; corresponding location.
  DEFB 0,0,0,0,0,0,0,0            ; Room layout
  DEFB 0,0,0,0,0,0,0,0            ;
  DEFB 0,0,0,0,0,0,0,0            ;
  DEFB 0,0,0,0,0,0,0,0            ;
  DEFB 0,0,0,0,0,0,0,0            ;
  DEFB 0,0,0,0,0,0,0,0            ;
  DEFB 0,0,0,0,0,0,0,0            ;
  DEFB 0,0,0,0,0,0,0,0            ;
  DEFB 84,0,0,0,0,0,0,0           ;
  DEFB 170,0,0,0,0,0,0,0          ;
  DEFB 170,128,0,0,0,0,0,0        ;
  DEFB 0,160,0,0,0,0,0,0          ;
  DEFB 0,168,0,0,0,0,0,0          ;
  DEFB 0,170,0,0,0,0,0,0          ;
  DEFB 160,170,128,0,0,0,0,0      ;
  DEFB 160,170,165,85,85,90,85,85 ;
; The next 32 bytes are copied to ROOMNAME and specify the room name.
  DEFM "            The Drive           " ; Room name
; The next 54 bytes are copied to BACKGROUND and contain the attributes and
; graphic data for the tiles used to build the room.
  DEFB 0,0,0,0,0,0,0,0,0  ; Background
  DEFB 6,170,85,170,255,255,85,170,85 ; Floor
  DEFB 13,112,7,119,119,0,59,11,125 ; Wall
  DEFB 68,20,42,20,9,106,156,8,8 ; Nasty (unused)
  DEFB 5,192,64,112,112,12,116,119,5 ; Ramp
  DEFB 66,123,0,237,0,170,85,170,85 ; Conveyor
; The next four bytes are copied to CONVDIR and specify the direction, location
; and length of the conveyor.
  DEFB 1                  ; Direction (right)
  DEFW 24480              ; Location in the attribute buffer at 24064: (13,0)
  DEFB 2                  ; Length
; The next four bytes are copied to RAMPDIR and specify the direction, location
; and length of the ramp.
  DEFB 0                  ; Direction (up to the left)
  DEFW 24521              ; Location in the attribute buffer at 24064: (14,9)
  DEFB 7                  ; Length
; The next byte is copied to BORDER and specifies the border colour.
  DEFB 3                  ; Border colour
; The next two bytes are copied to XROOM223, but are not used.
  DEFB 192,160            ; Unused
; The next eight bytes are copied to ITEM and define the item graphic.
  DEFB 0,0,0,0,0,0,0,0    ; Item graphic (unused)
; The next four bytes are copied to LEFT and specify the rooms to the left, to
; the right, above and below.
  DEFB 5                  ; Room to the left (The Security Guard)
  DEFB 3                  ; Room to the right (At the Foot of the MegaTree)
  DEFB 6                  ; Room above (Entrance to Hades)
  DEFB 45                 ; Room below (Under the Drive)
; The next three bytes are copied to XROOM237, but are not used.
  DEFB 0,0,0              ; Unused
; The next eight pairs of bytes are copied to ENTITIES and specify the entities
; (ropes, arrows, guardians) in this room.
  DEFB 21,18              ; Guardian no. 21 (horizontal), base sprite 0,
                          ; initial x=18 (ENTITY21)
  DEFB 22,21              ; Guardian no. 22 (horizontal), base sprite 0,
                          ; initial x=21 (ENTITY22)
  DEFB 23,8               ; Guardian no. 23 (horizontal), base sprite 0,
                          ; initial x=8 (ENTITY23)
  DEFB 24,20              ; Guardian no. 24 (horizontal), base sprite 0,
                          ; initial x=20 (ENTITY24)
  DEFB 36,143             ; Guardian no. 36 (horizontal), base sprite 4,
                          ; initial x=15 (ENTITY36)
  DEFB 255,0              ; Terminator (ENTITY127)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)

; Room 5: The Security Guard (teleport: 139)
;
; Used by the routine at INITROOM.
;
; The first 128 bytes are copied to ROOMLAYOUT and define the room layout. Each
; bit-pair (bits 7 and 6, 5 and 4, 3 and 2, or 1 and 0 of each byte) determines
; the type of tile (background, floor, wall or nasty) that will be drawn at the
; corresponding location.
  DEFB 128,0,0,0,0,0,0,0              ; Room layout
  DEFB 160,0,0,0,0,0,0,0              ;
  DEFB 168,0,0,0,0,0,0,0              ;
  DEFB 170,0,0,0,0,0,0,0              ;
  DEFB 170,128,0,0,0,0,0,0            ;
  DEFB 170,160,0,0,0,0,0,0            ;
  DEFB 168,0,0,0,0,0,0,0              ;
  DEFB 168,0,0,0,0,0,0,0              ;
  DEFB 168,21,84,16,65,84,21,85       ;
  DEFB 168,42,168,32,130,168,42,170   ;
  DEFB 0,0,168,32,130,168,42,170      ;
  DEFB 0,0,168,32,130,168,40,0        ;
  DEFB 0,0,168,32,130,168,40,0        ;
  DEFB 0,0,0,0,0,0,0,0                ;
  DEFB 0,0,0,0,0,0,0,42               ;
  DEFB 160,10,170,170,170,170,170,170 ;
; The next 32 bytes are copied to ROOMNAME and specify the room name.
  DEFM "       The Security Guard       " ; Room name
; The next 54 bytes are copied to BACKGROUND and contain the attributes and
; graphic data for the tiles used to build the room.
  DEFB 0,0,0,0,0,0,0,0,0  ; Background
  DEFB 4,255,255,170,85,170,85,170,85 ; Floor
  DEFB 13,14,198,149,176,54,134,192,237 ; Wall
  DEFB 255,0,0,0,0,0,0,0,0 ; Nasty (unused)
  DEFB 7,224,64,32,16,14,4,2,1 ; Ramp
  DEFB 66,111,0,183,0,170,85,170,85 ; Conveyor
; The next four bytes are copied to CONVDIR and specify the direction, location
; and length of the conveyor.
  DEFB 1                  ; Direction (right)
  DEFW 24509              ; Location in the attribute buffer at 24064: (13,29)
  DEFB 3                  ; Length
; The next four bytes are copied to RAMPDIR and specify the direction, location
; and length of the ramp.
  DEFB 0                  ; Direction (up to the left)
  DEFW 24296              ; Location in the attribute buffer at 24064: (7,8)
  DEFB 8                  ; Length
; The next byte is copied to BORDER and specifies the border colour.
  DEFB 1                  ; Border colour
; The next two bytes are copied to XROOM223, but are not used.
  DEFB 0,0                ; Unused
; The next eight bytes are copied to ITEM and define the item graphic.
  DEFB 0,0,0,0,0,0,0,0    ; Item graphic (unused)
; The next four bytes are copied to LEFT and specify the rooms to the left, to
; the right, above and below.
  DEFB 19                 ; Room to the left (The Forgotten Abbey)
  DEFB 4                  ; Room to the right (The Drive)
  DEFB 10                 ; Room above (The Front Door)
  DEFB 6                  ; Room below (Entrance to Hades)
; The next three bytes are copied to XROOM237, but are not used.
  DEFB 0,0,0              ; Unused
; The next eight pairs of bytes are copied to ENTITIES and specify the entities
; (ropes, arrows, guardians) in this room.
  DEFB 2,142              ; Guardian no. 2 (vertical), base sprite 4, x=14
                          ; (ENTITY2)
  DEFB 3,139              ; Guardian no. 3 (vertical), base sprite 4, x=11
                          ; (ENTITY3)
  DEFB 3,151              ; Guardian no. 3 (vertical), base sprite 4, x=23
                          ; (ENTITY3)
  DEFB 4,145              ; Guardian no. 4 (vertical), base sprite 4, x=17
                          ; (ENTITY4)
  DEFB 255,0              ; Terminator (ENTITY127)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)

; Room 6: Entrance to Hades (teleport: 239)
;
; Used by the routine at INITROOM.
;
; The first 128 bytes are copied to ROOMLAYOUT and define the room layout. Each
; bit-pair (bits 7 and 6, 5 and 4, 3 and 2, or 1 and 0 of each byte) determines
; the type of tile (background, floor, wall or nasty) that will be drawn at the
; corresponding location.
  DEFB 160,0,0,0,0,0,0,0      ; Room layout
  DEFB 160,0,0,0,0,0,0,0      ;
  DEFB 160,0,0,21,4,85,0,0    ;
  DEFB 160,0,0,16,68,67,0,0   ;
  DEFB 160,0,0,16,68,84,0,0   ;
  DEFB 160,0,0,16,68,64,0,0   ;
  DEFB 160,0,0,21,4,85,0,0    ;
  DEFB 160,0,0,12,12,48,0,0   ;
  DEFB 160,0,0,0,0,0,0,0      ;
  DEFB 160,1,1,4,20,21,4,16   ;
  DEFB 160,1,69,17,17,4,17,16 ;
  DEFB 160,1,17,17,20,4,17,16 ;
  DEFB 160,1,49,17,17,4,21,16 ;
  DEFB 160,1,1,4,17,4,17,21   ;
  DEFB 160,3,3,12,51,12,51,12 ;
  DEFB 160,0,0,0,0,0,0,0      ;
; The next 32 bytes are copied to ROOMNAME and specify the room name.
  DEFM "        Entrance to Hades       " ; Room name
; The next 54 bytes are copied to BACKGROUND and contain the attributes and
; graphic data for the tiles used to build the room.
  DEFB 52,0,0,0,0,0,0,0,0 ; Background
  DEFB 163,160,20,130,80,10,65,40,5 ; Floor
  DEFB 99,187,187,56,187,187,187,131,187 ; Wall
  DEFB 50,191,92,88,48,40,88,72,48 ; Nasty
  DEFB 7,64,64,16,16,4,4,1,1 ; Ramp (unused)
  DEFB 255,0,0,0,0,0,0,0,0 ; Conveyor (unused)
; The next four bytes are copied to CONVDIR and specify the direction, location
; and length of the conveyor.
  DEFB 0                  ; Direction (left)
  DEFW 0                  ; Location in the attribute buffer at 24064 (unused)
  DEFB 0                  ; Length: 0 (there is no conveyor in this room)
; The next four bytes are copied to RAMPDIR and specify the direction, location
; and length of the ramp.
  DEFB 0                  ; Direction (up to the left)
  DEFW 24519              ; Location in the attribute buffer at 24064: (14,7)
  DEFB 0                  ; Length: 0 (there is no ramp in this room)
; The next byte is copied to BORDER and specifies the border colour.
  DEFB 2                  ; Border colour
; The next two bytes are copied to XROOM223, but are not used.
  DEFB 0,0                ; Unused
; The next eight bytes are copied to ITEM and define the item graphic.
  DEFB 0,0,0,0,0,0,0,0    ; Item graphic (unused)
; The next four bytes are copied to LEFT and specify the rooms to the left, to
; the right, above and below.
  DEFB 14                 ; Room to the left (Rescue Esmerelda)
  DEFB 5                  ; Room to the right (The Security Guard)
  DEFB 0                  ; Room above (The Off Licence)
  DEFB 0                  ; Room below (The Off Licence)
; The next three bytes are copied to XROOM237, but are not used.
  DEFB 0,0,0              ; Unused
; The next eight pairs of bytes are copied to ENTITIES and specify the entities
; (ropes, arrows, guardians) in this room.
  DEFB 16,130             ; Guardian no. 16 (vertical), base sprite 4, x=2
                          ; (ENTITY16)
  DEFB 17,164             ; Guardian no. 17 (vertical), base sprite 5, x=4
                          ; (ENTITY17)
  DEFB 18,195             ; Guardian no. 18 (vertical), base sprite 6, x=3
                          ; (ENTITY18)
  DEFB 255,0              ; Terminator (ENTITY127)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)

; Room 7: Cuckoo's Nest (teleport: 1239)
;
; Used by the routine at INITROOM.
;
; The first 128 bytes are copied to ROOMLAYOUT and define the room layout. Each
; bit-pair (bits 7 and 6, 5 and 4, 3 and 2, or 1 and 0 of each byte) determines
; the type of tile (background, floor, wall or nasty) that will be drawn at the
; corresponding location.
  DEFB 0,0,0,0,0,0,0,0          ; Room layout
  DEFB 0,0,0,0,0,0,0,0          ;
  DEFB 0,0,0,0,0,0,0,0          ;
  DEFB 0,0,0,0,0,0,0,0          ;
  DEFB 0,0,0,0,0,0,0,0          ;
  DEFB 0,0,0,0,0,0,0,0          ;
  DEFB 0,0,0,0,0,0,0,0          ;
  DEFB 0,0,0,0,0,0,0,0          ;
  DEFB 0,0,0,0,0,0,0,0          ;
  DEFB 0,0,0,0,0,0,0,0          ;
  DEFB 0,0,0,0,240,0,0,0        ;
  DEFB 0,0,0,1,84,0,0,0         ;
  DEFB 0,0,0,5,84,0,0,0         ;
  DEFB 170,170,170,169,84,0,0,0 ;
  DEFB 0,0,0,5,80,0,0,0         ;
  DEFB 0,0,0,0,0,0,0,0          ;
; The next 32 bytes are copied to ROOMNAME and specify the room name.
  DEFM "         Cuckoo's Nest          " ; Room name
; The next 54 bytes are copied to BACKGROUND and contain the attributes and
; graphic data for the tiles used to build the room.
  DEFB 0,0,0,0,0,0,0,0,0  ; Background
  DEFB 4,4,3,201,216,48,54,35,1 ; Floor
  DEFB 22,255,96,14,240,3,60,199,56 ; Wall
  DEFB 6,194,52,11,68,136,11,48,8 ; Nasty
  DEFB 7,3,0,12,0,48,0,192,0 ; Ramp
  DEFB 255,0,0,0,0,0,0,0,0 ; Conveyor (unused)
; The next four bytes are copied to CONVDIR and specify the direction, location
; and length of the conveyor.
  DEFB 0                  ; Direction (left)
  DEFW 0                  ; Location in the attribute buffer at 24064 (unused)
  DEFB 0                  ; Length: 0 (there is no conveyor in this room)
; The next four bytes are copied to RAMPDIR and specify the direction, location
; and length of the ramp.
  DEFB 1                  ; Direction (up to the right)
  DEFW 24461              ; Location in the attribute buffer at 24064: (12,13)
  DEFB 2                  ; Length
; The next byte is copied to BORDER and specifies the border colour.
  DEFB 1                  ; Border colour
; The next two bytes are copied to XROOM223, but are not used.
  DEFB 0,0                ; Unused
; The next eight bytes are copied to ITEM and define the item graphic.
  DEFB 32,24,169,219,126,126,60,90 ; Item graphic
; The next four bytes are copied to LEFT and specify the rooms to the left, to
; the right, above and below.
  DEFB 8                  ; Room to the left (Inside the MegaTrunk)
  DEFB 0                  ; Room to the right (The Off Licence)
  DEFB 0                  ; Room above (The Off Licence)
  DEFB 2                  ; Room below (Under the MegaTree)
; The next three bytes are copied to XROOM237, but are not used.
  DEFB 0,0,0              ; Unused
; The next eight pairs of bytes are copied to ENTITIES and specify the entities
; (ropes, arrows, guardians) in this room.
  DEFB 39,144             ; Guardian no. 39 (horizontal), base sprite 4,
                          ; initial x=16 (ENTITY39)
  DEFB 111,5              ; Guardian no. 111 (horizontal), base sprite 0,
                          ; initial x=5 (ENTITY111)
  DEFB 60,164             ; Arrow flying left to right at pixel y-coordinate 82
                          ; (ENTITY60)
  DEFB 255,0              ; Terminator (ENTITY127)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)

; Room 8: Inside the MegaTrunk (teleport: 49)
;
; Used by the routine at INITROOM.
;
; The first 128 bytes are copied to ROOMLAYOUT and define the room layout. Each
; bit-pair (bits 7 and 6, 5 and 4, 3 and 2, or 1 and 0 of each byte) determines
; the type of tile (background, floor, wall or nasty) that will be drawn at the
; corresponding location.
  DEFB 0,0,8,0,8,0,0,0           ; Room layout
  DEFB 0,0,8,0,8,0,0,0           ;
  DEFB 0,0,8,0,8,0,0,0           ;
  DEFB 0,0,8,1,64,0,0,0          ;
  DEFB 0,0,9,0,0,0,0,0           ;
  DEFB 0,0,0,0,26,170,128,0      ;
  DEFB 0,0,0,5,8,0,0,0           ;
  DEFB 170,170,168,0,8,0,0,0     ;
  DEFB 48,195,9,0,8,0,0,0        ;
  DEFB 48,195,8,0,88,0,0,0       ;
  DEFB 48,195,8,0,8,0,0,0        ;
  DEFB 48,195,8,5,0,0,0,0        ;
  DEFB 48,195,8,0,0,0,0,0        ;
  DEFB 48,195,9,0,10,170,170,170 ;
  DEFB 48,195,8,0,8,0,0,0        ;
  DEFB 48,195,8,0,88,0,0,0       ;
; The next 32 bytes are copied to ROOMNAME and specify the room name.
  DEFM "      Inside the MegaTrunk      " ; Room name
; The next 54 bytes are copied to BACKGROUND and contain the attributes and
; graphic data for the tiles used to build the room.
  DEFB 0,0,0,0,0,0,0,0,0  ; Background
  DEFB 4,245,250,191,31,38,20,34,16 ; Floor
  DEFB 22,171,171,173,170,170,174,85,85 ; Wall
  DEFB 6,110,116,56,30,110,116,56,30 ; Nasty
  DEFB 7,64,0,16,0,4,0,1,0 ; Ramp
  DEFB 255,0,0,0,0,0,0,0,0 ; Conveyor (unused)
; The next four bytes are copied to CONVDIR and specify the direction, location
; and length of the conveyor.
  DEFB 0                  ; Direction (left)
  DEFW 0                  ; Location in the attribute buffer at 24064 (unused)
  DEFB 0                  ; Length: 0 (there is no conveyor in this room)
; The next four bytes are copied to RAMPDIR and specify the direction, location
; and length of the ramp.
  DEFB 0                  ; Direction (up to the left)
  DEFW 24315              ; Location in the attribute buffer at 24064: (7,27)
  DEFB 3                  ; Length
; The next byte is copied to BORDER and specifies the border colour.
  DEFB 4                  ; Border colour
; The next two bytes are copied to XROOM223, but are not used.
  DEFB 0,0                ; Unused
; The next eight bytes are copied to ITEM and define the item graphic.
  DEFB 0,0,0,0,0,0,0,0    ; Item graphic (unused)
; The next four bytes are copied to LEFT and specify the rooms to the left, to
; the right, above and below.
  DEFB 9                  ; Room to the left (On a Branch Over the Drive)
  DEFB 7                  ; Room to the right (Cuckoo's Nest)
  DEFB 12                 ; Room above (Tree Top)
  DEFB 3                  ; Room below (At the Foot of the MegaTree)
; The next three bytes are copied to XROOM237, but are not used.
  DEFB 0,0,0              ; Unused
; The next eight pairs of bytes are copied to ENTITIES and specify the entities
; (ropes, arrows, guardians) in this room.
  DEFB 9,2                ; Guardian no. 9 (vertical), base sprite 0, x=2
                          ; (ENTITY9)
  DEFB 9,5                ; Guardian no. 9 (vertical), base sprite 0, x=5
                          ; (ENTITY9)
  DEFB 9,8                ; Guardian no. 9 (vertical), base sprite 0, x=8
                          ; (ENTITY9)
  DEFB 24,6               ; Guardian no. 24 (horizontal), base sprite 0,
                          ; initial x=6 (ENTITY24)
  DEFB 44,44              ; Guardian no. 44 (vertical), base sprite 1, x=12
                          ; (ENTITY44)
  DEFB 22,27              ; Guardian no. 22 (horizontal), base sprite 0,
                          ; initial x=27 (ENTITY22)
  DEFB 47,147             ; Guardian no. 47 (horizontal), base sprite 4,
                          ; initial x=19 (ENTITY47)
  DEFB 255,0              ; Terminator (ENTITY127)

; Room 9: On a Branch Over the Drive (teleport: 149)
;
; Used by the routine at INITROOM.
;
; The first 128 bytes are copied to ROOMLAYOUT and define the room layout. Each
; bit-pair (bits 7 and 6, 5 and 4, 3 and 2, or 1 and 0 of each byte) determines
; the type of tile (background, floor, wall or nasty) that will be drawn at the
; corresponding location.
  DEFB 0,0,0,0,0,0,0,0       ; Room layout
  DEFB 0,0,0,0,0,0,0,0       ;
  DEFB 0,0,0,0,0,0,0,0       ;
  DEFB 0,0,0,0,0,0,0,0       ;
  DEFB 0,0,0,0,0,0,0,0       ;
  DEFB 0,0,60,0,0,0,0,0      ;
  DEFB 0,0,253,80,0,0,0,0    ;
  DEFB 0,0,63,192,5,85,85,85 ;
  DEFB 0,0,0,0,15,191,59,192 ;
  DEFB 0,0,0,0,3,240,59,0    ;
  DEFB 0,0,0,0,0,0,15,0      ;
  DEFB 0,0,0,80,0,0,0,0      ;
  DEFB 0,80,0,0,0,0,0,0      ;
  DEFB 0,0,5,0,0,0,0,0       ;
  DEFB 0,0,0,0,0,0,0,0       ;
  DEFB 0,0,0,0,0,0,0,0       ;
; The next 32 bytes are copied to ROOMNAME and specify the room name.
  DEFM "   On a Branch Over the Drive   " ; Room name
; The next 54 bytes are copied to BACKGROUND and contain the attributes and
; graphic data for the tiles used to build the room.
  DEFB 8,0,0,0,0,0,0,0,0  ; Background
  DEFB 10,31,243,225,96,48,44,64,128 ; Floor
  DEFB 74,9,18,10,31,228,8,8,8 ; Wall
  DEFB 12,66,41,84,38,135,35,42,4 ; Nasty
  DEFB 15,5,2,4,8,80,32,64,128 ; Ramp
  DEFB 255,0,0,0,0,0,0,0,0 ; Conveyor (unused)
; The next four bytes are copied to CONVDIR and specify the direction, location
; and length of the conveyor.
  DEFB 0                  ; Direction (left)
  DEFW 0                  ; Location in the attribute buffer at 24064 (unused)
  DEFB 0                  ; Length: 0 (there is no conveyor in this room)
; The next four bytes are copied to RAMPDIR and specify the direction, location
; and length of the ramp.
  DEFB 1                  ; Direction (up to the right)
  DEFW 24398              ; Location in the attribute buffer at 24064: (10,14)
  DEFB 4                  ; Length
; The next byte is copied to BORDER and specifies the border colour.
  DEFB 2                  ; Border colour
; The next two bytes are copied to XROOM223, but are not used.
  DEFB 0,0                ; Unused
; The next eight bytes are copied to ITEM and define the item graphic.
  DEFB 48,60,79,67,130,134,228,252 ; Item graphic
; The next four bytes are copied to LEFT and specify the rooms to the left, to
; the right, above and below.
  DEFB 10                 ; Room to the left (The Front Door)
  DEFB 8                  ; Room to the right (Inside the MegaTrunk)
  DEFB 13                 ; Room above (Out on a limb)
  DEFB 4                  ; Room below (The Drive)
; The next three bytes are copied to XROOM237, but are not used.
  DEFB 0,0,0              ; Unused
; The next eight pairs of bytes are copied to ENTITIES and specify the entities
; (ropes, arrows, guardians) in this room.
  DEFB 9,30               ; Guardian no. 9 (vertical), base sprite 0, x=30
                          ; (ENTITY9)
  DEFB 27,135             ; Guardian no. 27 (vertical), base sprite 4, x=7
                          ; (ENTITY27)
  DEFB 61,19              ; Guardian no. 61 (horizontal), base sprite 0,
                          ; initial x=19 (ENTITY61)
  DEFB 62,14              ; Guardian no. 62 (vertical), base sprite 0, x=14
                          ; (ENTITY62)
  DEFB 60,68              ; Arrow flying left to right at pixel y-coordinate 34
                          ; (ENTITY60)
  DEFB 255,0              ; Terminator (ENTITY127)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)

; Room 10: The Front Door (teleport: 249)
;
; Used by the routine at INITROOM.
;
; The first 128 bytes are copied to ROOMLAYOUT and define the room layout. Each
; bit-pair (bits 7 and 6, 5 and 4, 3 and 2, or 1 and 0 of each byte) determines
; the type of tile (background, floor, wall or nasty) that will be drawn at the
; corresponding location.
  DEFB 0,0,0,0,0,0,0,0       ; Room layout
  DEFB 0,0,0,0,0,0,0,0       ;
  DEFB 0,0,0,0,0,0,0,0       ;
  DEFB 0,0,0,0,0,0,0,0       ;
  DEFB 0,0,0,0,0,0,0,0       ;
  DEFB 170,170,170,0,0,0,0,0 ;
  DEFB 0,0,0,0,0,0,0,0       ;
  DEFB 0,0,0,0,0,0,0,0       ;
  DEFB 0,0,0,0,0,0,0,0       ;
  DEFB 0,0,0,0,0,0,0,0       ;
  DEFB 0,0,0,0,0,0,0,0       ;
  DEFB 0,0,0,0,0,0,0,0       ;
  DEFB 0,0,0,0,0,0,0,0       ;
  DEFB 0,0,0,0,0,0,0,0       ;
  DEFB 64,0,0,0,0,0,0,0      ;
  DEFB 160,0,0,0,0,0,0,0     ;
; The next 32 bytes are copied to ROOMNAME and specify the room name.
  DEFM "         The Front Door         " ; Room name
; The next 54 bytes are copied to BACKGROUND and contain the attributes and
; graphic data for the tiles used to build the room.
  DEFB 40,0,0,0,0,0,0,0,0 ; Background
  DEFB 71,255,170,119,170,119,170,85,170 ; Floor
  DEFB 58,32,16,14,193,48,9,198,32 ; Wall
  DEFB 255,0,0,0,0,0,0,0,0 ; Nasty (unused)
  DEFB 47,192,64,176,80,172,84,171,85 ; Ramp
  DEFB 255,0,0,0,0,0,0,0,0 ; Conveyor (unused)
; The next four bytes are copied to CONVDIR and specify the direction, location
; and length of the conveyor.
  DEFB 0                  ; Direction (left)
  DEFW 0                  ; Location in the attribute buffer at 24064 (unused)
  DEFB 0                  ; Length: 0 (there is no conveyor in this room)
; The next four bytes are copied to RAMPDIR and specify the direction, location
; and length of the ramp.
  DEFB 0                  ; Direction (up to the left)
  DEFW 24546              ; Location in the attribute buffer at 24064: (15,2)
  DEFB 2                  ; Length
; The next byte is copied to BORDER and specifies the border colour.
  DEFB 2                  ; Border colour
; The next two bytes are copied to XROOM223, but are not used.
  DEFB 0,0                ; Unused
; The next eight bytes are copied to ITEM and define the item graphic.
  DEFB 12,28,28,30,27,63,120,255 ; Item graphic
; The next four bytes are copied to LEFT and specify the rooms to the left, to
; the right, above and below.
  DEFB 11                 ; Room to the left (The Hall)
  DEFB 9                  ; Room to the right (On a Branch Over the Drive)
  DEFB 0                  ; Room above (The Off Licence)
  DEFB 5                  ; Room below (The Security Guard)
; The next three bytes are copied to XROOM237, but are not used.
  DEFB 0,0,0              ; Unused
; The next eight pairs of bytes are copied to ENTITIES and specify the entities
; (ropes, arrows, guardians) in this room.
  DEFB 255,0              ; Terminator (ENTITY127)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)

; Room 11: The Hall (teleport: 1249)
;
; Used by the routine at INITROOM.
;
; The first 128 bytes are copied to ROOMLAYOUT and define the room layout. Each
; bit-pair (bits 7 and 6, 5 and 4, 3 and 2, or 1 and 0 of each byte) determines
; the type of tile (background, floor, wall or nasty) that will be drawn at the
; corresponding location.
  DEFB 0,0,0,0,0,0,0,0                 ; Room layout
  DEFB 0,0,0,0,0,0,0,0                 ;
  DEFB 0,0,0,0,0,0,0,0                 ;
  DEFB 0,0,0,0,0,0,0,0                 ;
  DEFB 0,0,0,0,0,0,0,0                 ;
  DEFB 170,170,170,170,170,170,170,170 ;
  DEFB 0,0,0,0,0,40,0,0                ;
  DEFB 0,0,0,0,0,40,0,0                ;
  DEFB 0,0,0,0,0,40,0,0                ;
  DEFB 0,0,0,0,0,40,0,0                ;
  DEFB 0,0,0,0,0,40,0,0                ;
  DEFB 0,0,0,0,0,40,0,0                ;
  DEFB 0,0,0,0,0,0,0,0                 ;
  DEFB 0,0,192,192,48,0,0,0            ;
  DEFB 170,170,170,170,170,170,170,170 ;
  DEFB 170,170,170,170,170,170,170,170 ;
; The next 32 bytes are copied to ROOMNAME and specify the room name.
  DEFM "            The Hall            " ; Room name
; The next 54 bytes are copied to BACKGROUND and contain the attributes and
; graphic data for the tiles used to build the room.
  DEFB 0,0,0,0,0,0,0,0,0  ; Background
  DEFB 71,255,170,119,170,119,170,85,170 ; Floor (unused)
  DEFB 58,32,16,14,193,48,9,198,32 ; Wall
  DEFB 68,136,169,169,187,191,191,191,94 ; Nasty
  DEFB 7,192,192,48,48,12,12,3,3 ; Ramp
  DEFB 255,0,0,0,0,0,0,0,0 ; Conveyor (unused)
; The next four bytes are copied to CONVDIR and specify the direction, location
; and length of the conveyor.
  DEFB 0                  ; Direction (left)
  DEFW 0                  ; Location in the attribute buffer at 24064 (unused)
  DEFB 0                  ; Length: 0 (there is no conveyor in this room)
; The next four bytes are copied to RAMPDIR and specify the direction, location
; and length of the ramp.
  DEFB 0                  ; Direction (up to the left)
  DEFW 24485              ; Location in the attribute buffer at 24064: (13,5)
  DEFB 6                  ; Length
; The next byte is copied to BORDER and specifies the border colour.
  DEFB 4                  ; Border colour
; The next two bytes are copied to XROOM223, but are not used.
  DEFB 0,0                ; Unused
; The next eight bytes are copied to ITEM and define the item graphic.
  DEFB 0,0,0,0,0,0,0,0    ; Item graphic (unused)
; The next four bytes are copied to LEFT and specify the rooms to the left, to
; the right, above and below.
  DEFB 20                 ; Room to the left (Ballroom East)
  DEFB 10                 ; Room to the right (The Front Door)
  DEFB 0                  ; Room above (The Off Licence)
  DEFB 0                  ; Room below (The Off Licence)
; The next three bytes are copied to XROOM237, but are not used.
  DEFB 0,0,0              ; Unused
; The next eight pairs of bytes are copied to ENTITIES and specify the entities
; (ropes, arrows, guardians) in this room.
  DEFB 89,202             ; Guardian no. 89 (vertical), base sprite 6, x=10
                          ; (ENTITY89)
  DEFB 26,19              ; Guardian no. 26 (horizontal), base sprite 0,
                          ; initial x=19 (ENTITY26)
  DEFB 95,19              ; Guardian no. 95 (horizontal), base sprite 0,
                          ; initial x=19 (ENTITY95)
  DEFB 69,68              ; Arrow flying right to left at pixel y-coordinate 34
                          ; (ENTITY69)
  DEFB 60,22              ; Arrow flying left to right at pixel y-coordinate 11
                          ; (ENTITY60)
  DEFB 255,0              ; Terminator (ENTITY127)
  DEFB 89,0               ; Guardian no. 89 (vertical), base sprite 0, x=0
                          ; (ENTITY89) (unused)
  DEFB 0,0                ; Nothing (ENTITYDEFS)

; Room 12: Tree Top (teleport: 349)
;
; Used by the routine at INITROOM.
;
; The first 128 bytes are copied to ROOMLAYOUT and define the room layout. Each
; bit-pair (bits 7 and 6, 5 and 4, 3 and 2, or 1 and 0 of each byte) determines
; the type of tile (background, floor, wall or nasty) that will be drawn at the
; corresponding location.
  DEFB 0,0,0,0,0,0,0,0        ; Room layout
  DEFB 0,0,0,0,0,0,0,0        ;
  DEFB 0,0,0,0,0,0,0,0        ;
  DEFB 0,0,0,0,0,0,0,0        ;
  DEFB 0,0,0,0,0,0,0,0        ;
  DEFB 0,0,0,0,0,0,0,0        ;
  DEFB 0,0,0,0,64,0,0,0       ;
  DEFB 0,0,84,0,0,0,0,0       ;
  DEFB 0,0,0,20,0,4,0,0       ;
  DEFB 1,80,0,0,20,0,0,0      ;
  DEFB 0,0,0,0,0,0,0,0        ;
  DEFB 0,0,0,0,0,20,0,0       ;
  DEFB 85,85,84,0,8,0,0,0     ;
  DEFB 12,243,200,16,8,0,80,0 ;
  DEFB 207,63,8,0,8,80,0,0    ;
  DEFB 48,48,8,0,88,0,0,0     ;
; The next 32 bytes are copied to ROOMNAME and specify the room name.
  DEFM "            Tree Top            " ; Room name
; The next 54 bytes are copied to BACKGROUND and contain the attributes and
; graphic data for the tiles used to build the room.
  DEFB 0,0,0,0,0,0,0,0,0  ; Background
  DEFB 4,170,85,170,88,48,16,16,8 ; Floor
  DEFB 22,165,40,82,85,85,73,162,181 ; Wall
  DEFB 66,137,74,18,52,24,52,82,145 ; Nasty
  DEFB 7,64,0,16,0,4,0,1,0 ; Ramp
  DEFB 255,0,0,0,0,0,0,0,0 ; Conveyor (unused)
; The next four bytes are copied to CONVDIR and specify the direction, location
; and length of the conveyor.
  DEFB 0                  ; Direction (left)
  DEFW 0                  ; Location in the attribute buffer at 24064 (unused)
  DEFB 0                  ; Length: 0 (there is no conveyor in this room)
; The next four bytes are copied to RAMPDIR and specify the direction, location
; and length of the ramp.
  DEFB 0                  ; Direction (up to the left)
  DEFW 24527              ; Location in the attribute buffer at 24064: (14,15)
  DEFB 2                  ; Length
; The next byte is copied to BORDER and specifies the border colour.
  DEFB 6                  ; Border colour
; The next two bytes are copied to XROOM223, but are not used.
  DEFB 0,0                ; Unused
; The next eight bytes are copied to ITEM and define the item graphic.
  DEFB 30,45,55,123,109,94,184,192 ; Item graphic
; The next four bytes are copied to LEFT and specify the rooms to the left, to
; the right, above and below.
  DEFB 13                 ; Room to the left (Out on a limb)
  DEFB 0                  ; Room to the right (The Off Licence)
  DEFB 0                  ; Room above (The Off Licence)
  DEFB 8                  ; Room below (Inside the MegaTrunk)
; The next three bytes are copied to XROOM237, but are not used.
  DEFB 0,0,0              ; Unused
; The next eight pairs of bytes are copied to ENTITIES and specify the entities
; (ropes, arrows, guardians) in this room.
  DEFB 24,7               ; Guardian no. 24 (horizontal), base sprite 0,
                          ; initial x=7 (ENTITY24)
  DEFB 46,136             ; Guardian no. 46 (horizontal), base sprite 4,
                          ; initial x=8 (ENTITY46)
  DEFB 26,24              ; Guardian no. 26 (horizontal), base sprite 0,
                          ; initial x=24 (ENTITY26)
  DEFB 69,132             ; Arrow flying right to left at pixel y-coordinate 66
                          ; (ENTITY69)
  DEFB 255,0              ; Terminator (ENTITY127)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)

; Room 13: Out on a limb (teleport: 1349)
;
; Used by the routine at INITROOM.
;
; The first 128 bytes are copied to ROOMLAYOUT and define the room layout. Each
; bit-pair (bits 7 and 6, 5 and 4, 3 and 2, or 1 and 0 of each byte) determines
; the type of tile (background, floor, wall or nasty) that will be drawn at the
; corresponding location.
  DEFB 0,0,0,0,0,0,0,0        ; Room layout
  DEFB 0,0,0,0,0,0,0,0        ;
  DEFB 0,0,0,0,0,0,0,0        ;
  DEFB 0,0,0,0,0,0,0,0        ;
  DEFB 0,0,0,0,0,0,0,0        ;
  DEFB 0,0,0,4,0,0,0,0        ;
  DEFB 0,16,0,0,4,1,0,0       ;
  DEFB 0,0,4,0,0,0,0,0        ;
  DEFB 0,0,0,0,0,0,16,0       ;
  DEFB 0,0,0,1,0,64,0,0       ;
  DEFB 4,0,0,4,64,0,0,0       ;
  DEFB 0,4,0,1,16,0,0,0       ;
  DEFB 0,0,1,0,74,170,170,170 ;
  DEFB 1,0,0,0,0,127,255,0    ;
  DEFB 0,0,64,0,64,31,252,0   ;
  DEFB 0,0,0,64,0,1,80,0      ;
; The next 32 bytes are copied to ROOMNAME and specify the room name.
  DEFM "        Out on a limb           " ; Room name
; The next 54 bytes are copied to BACKGROUND and contain the attributes and
; graphic data for the tiles used to build the room.
  DEFB 0,0,0,0,0,0,0,0,0  ; Background
  DEFB 4,1,30,6,122,26,232,104,160 ; Floor
  DEFB 66,7,255,252,56,24,16,32,0 ; Wall
  DEFB 2,1,129,6,88,32,64,136,130 ; Nasty
  DEFB 4,0,0,0,0,0,0,0,0  ; Ramp (unused)
  DEFB 255,0,0,0,0,0,0,0,0 ; Conveyor (unused)
; The next four bytes are copied to CONVDIR and specify the direction, location
; and length of the conveyor.
  DEFB 0                  ; Direction (left)
  DEFW 0                  ; Location in the attribute buffer at 24064 (unused)
  DEFB 0                  ; Length: 0 (there is no conveyor in this room)
; The next four bytes are copied to RAMPDIR and specify the direction, location
; and length of the ramp.
  DEFB 1                  ; Direction (up to the right)
  DEFW 0                  ; Location in the attribute buffer at 24064 (unused)
  DEFB 0                  ; Length: 0 (there is no ramp in this room)
; The next byte is copied to BORDER and specifies the border colour.
  DEFB 5                  ; Border colour
; The next two bytes are copied to XROOM223, but are not used.
  DEFB 0,0                ; Unused
; The next eight bytes are copied to ITEM and define the item graphic.
  DEFB 96,184,190,93,90,53,42,21 ; Item graphic
; The next four bytes are copied to LEFT and specify the rooms to the left, to
; the right, above and below.
  DEFB 10                 ; Room to the left (The Front Door)
  DEFB 12                 ; Room to the right (Tree Top)
  DEFB 0                  ; Room above (The Off Licence)
  DEFB 9                  ; Room below (On a Branch Over the Drive)
; The next three bytes are copied to XROOM237, but are not used.
  DEFB 0,0,0              ; Unused
; The next eight pairs of bytes are copied to ENTITIES and specify the entities
; (ropes, arrows, guardians) in this room.
  DEFB 45,76              ; Guardian no. 45 (vertical), base sprite 2, x=12
                          ; (ENTITY45)
  DEFB 25,19              ; Guardian no. 25 (horizontal), base sprite 0,
                          ; initial x=19 (ENTITY25)
  DEFB 88,20              ; Guardian no. 88 (horizontal), base sprite 0,
                          ; initial x=20 (ENTITY88)
  DEFB 255,0              ; Terminator (ENTITY127)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)

; Room 14: Rescue Esmerelda (teleport: 2349)
;
; Used by the routine at INITROOM.
;
; The first 128 bytes are copied to ROOMLAYOUT and define the room layout. Each
; bit-pair (bits 7 and 6, 5 and 4, 3 and 2, or 1 and 0 of each byte) determines
; the type of tile (background, floor, wall or nasty) that will be drawn at the
; corresponding location.
  DEFB 0,0,0,0,0,0,8,2              ; Room layout
  DEFB 0,0,0,0,0,0,8,2              ;
  DEFB 0,0,0,0,0,0,0,2              ;
  DEFB 0,0,0,0,0,0,0,0              ;
  DEFB 0,0,0,0,0,0,170,0            ;
  DEFB 0,0,0,0,0,0,8,6              ;
  DEFB 0,0,0,0,0,0,0,18             ;
  DEFB 0,0,0,0,0,0,0,66             ;
  DEFB 0,0,0,0,0,0,1,2              ;
  DEFB 170,6,170,6,170,10,170,6     ;
  DEFB 170,14,170,14,170,0,0,6      ;
  DEFB 170,14,170,14,170,0,0,6      ;
  DEFB 170,14,170,14,170,128,0,18   ;
  DEFB 170,14,170,14,170,160,0,64   ;
  DEFB 170,14,170,14,170,168,1,0    ;
  DEFB 170,170,170,170,170,170,4,10 ;
; The next 32 bytes are copied to ROOMNAME and specify the room name.
  DEFM "        Rescue Esmerelda        " ; Room name
; The next 54 bytes are copied to BACKGROUND and contain the attributes and
; graphic data for the tiles used to build the room.
  DEFB 16,0,0,0,0,0,0,0,0 ; Background
  DEFB 23,3,3,12,12,49,50,196,200 ; Floor
  DEFB 4,238,0,187,0,238,0,187,0 ; Wall
  DEFB 85,209,226,196,200,209,226,196,200 ; Nasty
  DEFB 23,0,0,0,0,0,0,0,0 ; Ramp (unused)
  DEFB 22,241,170,248,170,0,238,0,187 ; Conveyor
; The next four bytes are copied to CONVDIR and specify the direction, location
; and length of the conveyor.
  DEFB 1                  ; Direction (right)
  DEFW 24216              ; Location in the attribute buffer at 24064: (4,24)
  DEFB 4                  ; Length
; The next four bytes are copied to RAMPDIR and specify the direction, location
; and length of the ramp.
  DEFB 1                  ; Direction (up to the right)
  DEFW 0                  ; Location in the attribute buffer at 24064 (unused)
  DEFB 0                  ; Length: 0 (there is no ramp in this room)
; The next byte is copied to BORDER and specifies the border colour.
  DEFB 1                  ; Border colour
; The next two bytes are copied to XROOM223, but are not used.
  DEFB 0,0                ; Unused
; The next eight bytes are copied to ITEM and define the item graphic.
  DEFB 255,73,73,73,73,73,73,255 ; Item graphic
; The next four bytes are copied to LEFT and specify the rooms to the left, to
; the right, above and below.
  DEFB 15                 ; Room to the left (I'm sure I've seen this before..)
  DEFB 44                 ; Room to the right (On top of the house)
  DEFB 20                 ; Room above (Ballroom East)
  DEFB 39                 ; Room below (Emergency Generator)
; The next three bytes are copied to XROOM237, but are not used.
  DEFB 0,0,0              ; Unused
; The next eight pairs of bytes are copied to ENTITIES and specify the entities
; (ropes, arrows, guardians) in this room.
  DEFB 4,196              ; Guardian no. 4 (vertical), base sprite 6, x=4
                          ; (ENTITY4)
  DEFB 2,204              ; Guardian no. 2 (vertical), base sprite 6, x=12
                          ; (ENTITY2)
  DEFB 13,5               ; Guardian no. 13 (horizontal), base sprite 0,
                          ; initial x=5 (ENTITY13)
  DEFB 15,152             ; Guardian no. 15 (vertical), base sprite 4, x=24
                          ; (ENTITY15)
  DEFB 255,0              ; Terminator (ENTITY127)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)

; Room 15: I'm sure I've seen this before.. (teleport: 12349)
;
; Used by the routine at INITROOM.
;
; The first 128 bytes are copied to ROOMLAYOUT and define the room layout. Each
; bit-pair (bits 7 and 6, 5 and 4, 3 and 2, or 1 and 0 of each byte) determines
; the type of tile (background, floor, wall or nasty) that will be drawn at the
; corresponding location.
  DEFB 0,0,0,0,0,0,0,0                 ; Room layout
  DEFB 0,0,0,0,0,0,0,0                 ;
  DEFB 0,0,0,0,0,0,0,0                 ;
  DEFB 0,0,0,0,0,0,0,0                 ;
  DEFB 0,0,0,0,0,0,0,0                 ;
  DEFB 0,0,0,0,0,0,0,0                 ;
  DEFB 0,0,0,0,0,0,0,0                 ;
  DEFB 0,0,0,0,0,0,0,0                 ;
  DEFB 0,0,0,0,0,0,0,0                 ;
  DEFB 170,66,164,42,164,42,66,170     ;
  DEFB 170,194,172,42,172,42,194,170   ;
  DEFB 170,194,172,42,172,42,194,170   ;
  DEFB 170,194,172,42,172,42,194,170   ;
  DEFB 170,194,172,42,172,42,194,170   ;
  DEFB 170,194,172,42,172,42,194,170   ;
  DEFB 170,170,170,170,170,170,170,170 ;
; The next 32 bytes are copied to ROOMNAME and specify the room name.
  DEFM "I'm sure I've seen this before.." ; Room name
; The next 54 bytes are copied to BACKGROUND and contain the attributes and
; graphic data for the tiles used to build the room.
  DEFB 8,0,0,0,0,0,0,0,0  ; Background
  DEFB 12,128,64,32,80,72,84,82,85 ; Floor
  DEFB 83,102,102,102,0,102,102,102,102 ; Wall
  DEFB 76,81,85,84,85,21,85,69,85 ; Nasty
  DEFB 12,0,0,0,0,0,0,0,0 ; Ramp (unused)
  DEFB 255,0,0,0,0,0,0,0,0 ; Conveyor (unused)
; The next four bytes are copied to CONVDIR and specify the direction, location
; and length of the conveyor.
  DEFB 0                  ; Direction (left)
  DEFW 0                  ; Location in the attribute buffer at 24064 (unused)
  DEFB 0                  ; Length: 0 (there is no conveyor in this room)
; The next four bytes are copied to RAMPDIR and specify the direction, location
; and length of the ramp.
  DEFB 0                  ; Direction (up to the left)
  DEFW 0                  ; Location in the attribute buffer at 24064 (unused)
  DEFB 0                  ; Length: 0 (there is no ramp in this room)
; The next byte is copied to BORDER and specifies the border colour.
  DEFB 3                  ; Border colour
; The next two bytes are copied to XROOM223, but are not used.
  DEFB 0,0                ; Unused
; The next eight bytes are copied to ITEM and define the item graphic.
  DEFB 24,36,24,44,94,94,231,102 ; Item graphic
; The next four bytes are copied to LEFT and specify the rooms to the left, to
; the right, above and below.
  DEFB 16                 ; Room to the left (We must perform a Quirkafleeg)
  DEFB 14                 ; Room to the right (Rescue Esmerelda)
  DEFB 0                  ; Room above (The Off Licence)
  DEFB 0                  ; Room below (The Off Licence)
; The next three bytes are copied to XROOM237, but are not used.
  DEFB 0,0,0              ; Unused
; The next eight pairs of bytes are copied to ENTITIES and specify the entities
; (ropes, arrows, guardians) in this room.
  DEFB 4,133              ; Guardian no. 4 (vertical), base sprite 4, x=5
                          ; (ENTITY4)
  DEFB 3,139              ; Guardian no. 3 (vertical), base sprite 4, x=11
                          ; (ENTITY3)
  DEFB 5,147              ; Guardian no. 5 (vertical), base sprite 4, x=19
                          ; (ENTITY5)
  DEFB 2,153              ; Guardian no. 2 (vertical), base sprite 4, x=25
                          ; (ENTITY2)
  DEFB 13,6               ; Guardian no. 13 (horizontal), base sprite 0,
                          ; initial x=6 (ENTITY13)
  DEFB 69,100             ; Arrow flying right to left at pixel y-coordinate 50
                          ; (ENTITY69)
  DEFB 60,84              ; Arrow flying left to right at pixel y-coordinate 42
                          ; (ENTITY60)
  DEFB 255,0              ; Terminator (ENTITY127)

; Room 16: We must perform a Quirkafleeg (teleport: 59)
;
; Used by the routine at INITROOM.
;
; The first 128 bytes are copied to ROOMLAYOUT and define the room layout. Each
; bit-pair (bits 7 and 6, 5 and 4, 3 and 2, or 1 and 0 of each byte) determines
; the type of tile (background, floor, wall or nasty) that will be drawn at the
; corresponding location.
  DEFB 0,0,0,0,0,0,0,0                 ; Room layout
  DEFB 0,0,0,0,0,0,0,0                 ;
  DEFB 0,0,0,0,0,0,0,0                 ;
  DEFB 0,0,0,0,0,0,0,0                 ;
  DEFB 0,0,0,0,0,0,0,0                 ;
  DEFB 0,0,0,0,0,0,0,0                 ;
  DEFB 0,0,0,0,0,0,0,0                 ;
  DEFB 0,0,0,0,0,0,0,0                 ;
  DEFB 0,0,0,0,0,0,0,0                 ;
  DEFB 170,0,0,0,0,0,2,170             ;
  DEFB 170,64,0,0,0,0,2,170            ;
  DEFB 170,64,0,0,0,0,2,170            ;
  DEFB 170,64,0,0,0,0,2,170            ;
  DEFB 170,64,0,0,0,0,2,170            ;
  DEFB 170,64,0,0,0,0,2,170            ;
  DEFB 170,127,255,255,255,255,254,170 ;
; The next 32 bytes are copied to ROOMNAME and specify the room name.
  DEFM "  We must perform a Quirkafleeg " ; Room name
; The next 54 bytes are copied to BACKGROUND and contain the attributes and
; graphic data for the tiles used to build the room.
  DEFB 6,0,0,0,0,0,0,0,0  ; Background
  DEFB 56,51,187,221,204,85,153,221,238 ; Floor
  DEFB 13,39,246,0,220,209,4,119,128 ; Wall
  DEFB 69,64,228,78,68,68,68,238,255 ; Nasty
  DEFB 7,128,64,32,48,168,100,34,17 ; Ramp
  DEFB 255,0,0,0,0,0,0,0,0 ; Conveyor (unused)
; The next four bytes are copied to CONVDIR and specify the direction, location
; and length of the conveyor.
  DEFB 0                  ; Direction (left)
  DEFW 0                  ; Location in the attribute buffer at 24064 (unused)
  DEFB 0                  ; Length: 0 (there is no conveyor in this room)
; The next four bytes are copied to RAMPDIR and specify the direction, location
; and length of the ramp.
  DEFB 0                  ; Direction (up to the left)
  DEFW 24356              ; Location in the attribute buffer at 24064: (9,4)
  DEFB 1                  ; Length
; The next byte is copied to BORDER and specifies the border colour.
  DEFB 2                  ; Border colour
; The next two bytes are copied to XROOM223, but are not used.
  DEFB 0,0                ; Unused
; The next eight bytes are copied to ITEM and define the item graphic.
  DEFB 24,36,24,44,94,94,231,102 ; Item graphic
; The next four bytes are copied to LEFT and specify the rooms to the left, to
; the right, above and below.
  DEFB 17                 ; Room to the left (Up on the Battlements)
  DEFB 15                 ; Room to the right (I'm sure I've seen this
                          ; before..)
  DEFB 50                 ; Room above (Watch Tower)
  DEFB 0                  ; Room below (The Off Licence)
; The next three bytes are copied to XROOM237, but are not used.
  DEFB 0,0,0              ; Unused
; The next eight pairs of bytes are copied to ENTITIES and specify the entities
; (ropes, arrows, guardians) in this room.
  DEFB 13,8               ; Guardian no. 13 (horizontal), base sprite 0,
                          ; initial x=8 (ENTITY13)
  DEFB 1,16               ; Rope at x=16 (ENTITY1)
  DEFB 60,132             ; Arrow flying left to right at pixel y-coordinate 66
                          ; (ENTITY60)
  DEFB 255,0              ; Terminator (ENTITY127)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)

; Room 17: Up on the Battlements (teleport: 159)
;
; Used by the routine at INITROOM.
;
; The first 128 bytes are copied to ROOMLAYOUT and define the room layout. Each
; bit-pair (bits 7 and 6, 5 and 4, 3 and 2, or 1 and 0 of each byte) determines
; the type of tile (background, floor, wall or nasty) that will be drawn at the
; corresponding location.
  DEFB 0,0,0,0,0,0,0,0                 ; Room layout
  DEFB 0,0,0,0,0,0,0,0                 ;
  DEFB 0,0,0,0,0,0,0,0                 ;
  DEFB 0,0,0,0,0,0,0,0                 ;
  DEFB 0,0,0,0,0,0,0,0                 ;
  DEFB 0,0,0,0,0,0,0,0                 ;
  DEFB 0,0,0,0,0,0,0,0                 ;
  DEFB 0,0,0,0,0,0,0,0                 ;
  DEFB 0,0,0,0,0,0,0,0                 ;
  DEFB 170,66,164,42,164,42,66,170     ;
  DEFB 170,194,172,42,172,42,194,170   ;
  DEFB 170,194,172,42,172,42,194,170   ;
  DEFB 170,194,172,42,172,42,194,170   ;
  DEFB 170,194,172,42,172,42,194,170   ;
  DEFB 170,194,172,42,172,42,194,170   ;
  DEFB 170,170,170,170,170,170,170,170 ;
; The next 32 bytes are copied to ROOMNAME and specify the room name.
  DEFM "      Up on the Battlements     " ; Room name
; The next 54 bytes are copied to BACKGROUND and contain the attributes and
; graphic data for the tiles used to build the room.
  DEFB 0,0,0,0,0,0,0,0,0  ; Background
  DEFB 4,128,192,224,176,144,148,214,119 ; Floor
  DEFB 14,0,127,127,99,107,99,127,127 ; Wall
  DEFB 32,74,42,26,72,108,106,41,136 ; Nasty
  DEFB 4,0,0,0,0,0,0,0,0  ; Ramp (unused)
  DEFB 66,40,170,10,0,85,85,85,255 ; Conveyor
; The next four bytes are copied to CONVDIR and specify the direction, location
; and length of the conveyor.
  DEFB 0                  ; Direction (left)
  DEFW 24365              ; Location in the attribute buffer at 24064: (9,13)
  DEFB 5                  ; Length
; The next four bytes are copied to RAMPDIR and specify the direction, location
; and length of the ramp.
  DEFB 0                  ; Direction (up to the left)
  DEFW 0                  ; Location in the attribute buffer at 24064 (unused)
  DEFB 0                  ; Length: 0 (there is no ramp in this room)
; The next byte is copied to BORDER and specifies the border colour.
  DEFB 1                  ; Border colour
; The next two bytes are copied to XROOM223, but are not used.
  DEFB 0,0                ; Unused
; The next eight bytes are copied to ITEM and define the item graphic.
  DEFB 24,36,24,44,94,94,231,102 ; Item graphic
; The next four bytes are copied to LEFT and specify the rooms to the left, to
; the right, above and below.
  DEFB 18                 ; Room to the left (On the Roof)
  DEFB 16                 ; Room to the right (We must perform a Quirkafleeg)
  DEFB 0                  ; Room above (The Off Licence)
  DEFB 0                  ; Room below (The Off Licence)
; The next three bytes are copied to XROOM237, but are not used.
  DEFB 0,0,0              ; Unused
; The next eight pairs of bytes are copied to ENTITIES and specify the entities
; (ropes, arrows, guardians) in this room.
  DEFB 2,133              ; Guardian no. 2 (vertical), base sprite 4, x=5
                          ; (ENTITY2)
  DEFB 3,139              ; Guardian no. 3 (vertical), base sprite 4, x=11
                          ; (ENTITY3)
  DEFB 4,147              ; Guardian no. 4 (vertical), base sprite 4, x=19
                          ; (ENTITY4)
  DEFB 5,153              ; Guardian no. 5 (vertical), base sprite 4, x=25
                          ; (ENTITY5)
  DEFB 69,132             ; Arrow flying right to left at pixel y-coordinate 66
                          ; (ENTITY69)
  DEFB 255,0              ; Terminator (ENTITY127)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)

; Room 18: On the Roof (teleport: 259)
;
; Used by the routine at INITROOM.
;
; The first 128 bytes are copied to ROOMLAYOUT and define the room layout. Each
; bit-pair (bits 7 and 6, 5 and 4, 3 and 2, or 1 and 0 of each byte) determines
; the type of tile (background, floor, wall or nasty) that will be drawn at the
; corresponding location.
  DEFB 0,0,0,0,0,0,0,0                 ; Room layout
  DEFB 0,0,0,0,0,0,0,0                 ;
  DEFB 0,0,0,0,0,0,0,0                 ;
  DEFB 0,0,0,0,0,0,0,0                 ;
  DEFB 0,0,0,0,0,0,0,0                 ;
  DEFB 0,0,0,0,0,0,0,0                 ;
  DEFB 0,0,0,0,0,0,0,0                 ;
  DEFB 0,0,0,0,0,0,10,64               ;
  DEFB 0,0,0,0,0,0,10,208              ;
  DEFB 170,64,0,0,0,0,10,170           ;
  DEFB 170,192,0,0,0,0,10,170          ;
  DEFB 170,192,0,0,0,0,10,170          ;
  DEFB 170,144,0,0,0,0,10,170          ;
  DEFB 170,176,0,0,0,0,10,170          ;
  DEFB 170,176,10,64,0,0,10,170        ;
  DEFB 170,170,170,170,170,170,170,170 ;
; The next 32 bytes are copied to ROOMNAME and specify the room name.
  DEFM "          On the Roof           " ; Room name
; The next 54 bytes are copied to BACKGROUND and contain the attributes and
; graphic data for the tiles used to build the room.
  DEFB 68,0,0,0,0,0,0,0,0 ; Background
  DEFB 7,128,192,224,208,88,156,222,205 ; Floor
  DEFB 38,17,248,7,8,240,15,208,63 ; Wall
  DEFB 70,85,153,221,204,85,153,221,204 ; Nasty
  DEFB 7,0,0,0,0,0,0,0,0  ; Ramp (unused)
  DEFB 10,240,240,240,255,255,0,0,255 ; Conveyor
; The next four bytes are copied to CONVDIR and specify the direction, location
; and length of the conveyor.
  DEFB 1                  ; Direction (right)
  DEFW 24558              ; Location in the attribute buffer at 24064: (15,14)
  DEFB 8                  ; Length
; The next four bytes are copied to RAMPDIR and specify the direction, location
; and length of the ramp.
  DEFB 0                  ; Direction (up to the left)
  DEFW 0                  ; Location in the attribute buffer at 24064 (unused)
  DEFB 0                  ; Length: 0 (there is no ramp in this room)
; The next byte is copied to BORDER and specifies the border colour.
  DEFB 2                  ; Border colour
; The next two bytes are copied to XROOM223, but are not used.
  DEFB 0,0                ; Unused
; The next eight bytes are copied to ITEM and define the item graphic.
  DEFB 1,2,5,10,84,40,48,72 ; Item graphic
; The next four bytes are copied to LEFT and specify the rooms to the left, to
; the right, above and below.
  DEFB 48                 ; Room to the left (Nomen Luni)
  DEFB 17                 ; Room to the right (Up on the Battlements)
  DEFB 18                 ; Room above (On the Roof)
  DEFB 0                  ; Room below (The Off Licence)
; The next three bytes are copied to XROOM237, but are not used.
  DEFB 0,0,0              ; Unused
; The next eight pairs of bytes are copied to ENTITIES and specify the entities
; (ropes, arrows, guardians) in this room.
  DEFB 34,22              ; Guardian no. 34 (horizontal), base sprite 0,
                          ; initial x=22 (ENTITY34)
  DEFB 1,16               ; Rope at x=16 (ENTITY1)
  DEFB 255,0              ; Terminator (ENTITY127)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)

; Room 19: The Forgotten Abbey (teleport: 1259)
;
; Used by the routine at INITROOM.
;
; The first 128 bytes are copied to ROOMLAYOUT and define the room layout. Each
; bit-pair (bits 7 and 6, 5 and 4, 3 and 2, or 1 and 0 of each byte) determines
; the type of tile (background, floor, wall or nasty) that will be drawn at the
; corresponding location.
  DEFB 170,170,170,170,170,170,170,170 ; Room layout
  DEFB 170,170,170,170,170,170,170,170 ;
  DEFB 160,0,48,0,192,0,192,10         ;
  DEFB 160,0,0,0,0,0,0,10              ;
  DEFB 160,0,0,0,0,0,0,10              ;
  DEFB 168,0,0,0,0,0,0,10              ;
  DEFB 160,0,0,0,0,0,0,10              ;
  DEFB 160,0,0,0,0,0,0,10              ;
  DEFB 160,0,0,0,48,0,0,10             ;
  DEFB 160,0,0,0,0,0,0,26              ;
  DEFB 165,0,0,0,0,0,0,10              ;
  DEFB 80,5,85,85,85,85,85,90          ;
  DEFB 80,0,0,0,0,0,0,10               ;
  DEFB 168,0,0,0,0,0,0,0               ;
  DEFB 170,0,0,0,0,0,0,0               ;
  DEFB 170,170,170,170,170,170,170,170 ;
; The next 32 bytes are copied to ROOMNAME and specify the room name.
  DEFM "      The Forgotten Abbey       " ; Room name
; The next 54 bytes are copied to BACKGROUND and contain the attributes and
; graphic data for the tiles used to build the room.
  DEFB 0,0,0,0,0,0,0,0,0  ; Background
  DEFB 4,255,255,242,36,64,2,0,136 ; Floor
  DEFB 29,68,170,17,68,17,170,68,17 ; Wall
  DEFB 66,24,60,126,153,153,126,66,60 ; Nasty
  DEFB 7,128,64,224,48,168,212,34,137 ; Ramp
  DEFB 15,112,85,0,187,170,170,170,170 ; Conveyor
; The next four bytes are copied to CONVDIR and specify the direction, location
; and length of the conveyor.
  DEFB 0                  ; Direction (left)
  DEFW 24292              ; Location in the attribute buffer at 24064: (7,4)
  DEFB 26                 ; Length
; The next four bytes are copied to RAMPDIR and specify the direction, location
; and length of the ramp.
  DEFB 0                  ; Direction (up to the left)
  DEFW 24516              ; Location in the attribute buffer at 24064: (14,4)
  DEFB 2                  ; Length
; The next byte is copied to BORDER and specifies the border colour.
  DEFB 1                  ; Border colour
; The next two bytes are copied to XROOM223, but are not used.
  DEFB 0,0                ; Unused
; The next eight bytes are copied to ITEM and define the item graphic.
  DEFB 24,36,219,219,36,24,24,24 ; Item graphic
; The next four bytes are copied to LEFT and specify the rooms to the left, to
; the right, above and below.
  DEFB 49                 ; Room to the left (The Wine Cellar)
  DEFB 5                  ; Room to the right (The Security Guard)
  DEFB 0                  ; Room above (The Off Licence)
  DEFB 0                  ; Room below (The Off Licence)
; The next three bytes are copied to XROOM237, but are not used.
  DEFB 0,0,0              ; Unused
; The next eight pairs of bytes are copied to ENTITIES and specify the entities
; (ropes, arrows, guardians) in this room.
  DEFB 74,8               ; Guardian no. 74 (horizontal), base sprite 0,
                          ; initial x=8 (ENTITY74)
  DEFB 75,16              ; Guardian no. 75 (horizontal), base sprite 0,
                          ; initial x=16 (ENTITY75)
  DEFB 76,11              ; Guardian no. 76 (horizontal), base sprite 0,
                          ; initial x=11 (ENTITY76)
  DEFB 77,24              ; Guardian no. 77 (horizontal), base sprite 0,
                          ; initial x=24 (ENTITY77)
  DEFB 78,7               ; Guardian no. 78 (horizontal), base sprite 0,
                          ; initial x=7 (ENTITY78)
  DEFB 79,12              ; Guardian no. 79 (horizontal), base sprite 0,
                          ; initial x=12 (ENTITY79)
  DEFB 80,18              ; Guardian no. 80 (horizontal), base sprite 0,
                          ; initial x=18 (ENTITY80)
  DEFB 81,24              ; Guardian no. 81 (horizontal), base sprite 0,
                          ; initial x=24 (ENTITY81)

; Room 20: Ballroom East (teleport: 359)
;
; Used by the routine at INITROOM.
;
; The first 128 bytes are copied to ROOMLAYOUT and define the room layout. Each
; bit-pair (bits 7 and 6, 5 and 4, 3 and 2, or 1 and 0 of each byte) determines
; the type of tile (background, floor, wall or nasty) that will be drawn at the
; corresponding location.
  DEFB 160,64,10,0,0,0,0,0             ; Room layout
  DEFB 160,64,10,0,0,0,0,0             ;
  DEFB 160,64,10,0,0,0,0,0             ;
  DEFB 0,64,0,0,0,0,0,0                ;
  DEFB 0,64,0,0,0,0,0,0                ;
  DEFB 85,85,90,10,170,170,170,170     ;
  DEFB 0,0,16,0,0,0,0,0                ;
  DEFB 0,0,16,0,0,0,0,0                ;
  DEFB 0,0,21,85,85,85,85,85           ;
  DEFB 0,0,16,0,0,0,0,0                ;
  DEFB 0,0,16,0,0,0,0,0                ;
  DEFB 0,0,16,0,0,0,0,0                ;
  DEFB 0,0,16,0,0,0,0,0                ;
  DEFB 0,0,16,0,0,0,0,0                ;
  DEFB 170,170,170,170,170,170,170,170 ;
  DEFB 170,170,170,170,170,170,170,170 ;
; The next 32 bytes are copied to ROOMNAME and specify the room name.
  DEFM "         Ballroom East          " ; Room name
; The next 54 bytes are copied to BACKGROUND and contain the attributes and
; graphic data for the tiles used to build the room.
  DEFB 0,0,0,0,0,0,0,0,0  ; Background
  DEFB 6,170,85,34,68,34,68,34,68 ; Floor
  DEFB 15,68,68,255,17,17,255,85,170 ; Wall
  DEFB 255,0,0,0,0,0,0,0,0 ; Nasty (unused)
  DEFB 255,0,0,0,0,0,0,0,0 ; Ramp (unused)
  DEFB 255,0,0,0,0,0,0,0,0 ; Conveyor (unused)
; The next four bytes are copied to CONVDIR and specify the direction, location
; and length of the conveyor.
  DEFB 0                  ; Direction (left)
  DEFW 0                  ; Location in the attribute buffer at 24064 (unused)
  DEFB 0                  ; Length: 0 (there is no conveyor in this room)
; The next four bytes are copied to RAMPDIR and specify the direction, location
; and length of the ramp.
  DEFB 0                  ; Direction (up to the left)
  DEFW 0                  ; Location in the attribute buffer at 24064 (unused)
  DEFB 0                  ; Length: 0 (there is no ramp in this room)
; The next byte is copied to BORDER and specifies the border colour.
  DEFB 2                  ; Border colour
; The next two bytes are copied to XROOM223, but are not used.
  DEFB 0,0                ; Unused
; The next eight bytes are copied to ITEM and define the item graphic.
  DEFB 129,129,66,36,24,24,24,231 ; Item graphic (unused)
; The next four bytes are copied to LEFT and specify the rooms to the left, to
; the right, above and below.
  DEFB 21                 ; Room to the left (Ballroom West)
  DEFB 11                 ; Room to the right (The Hall)
  DEFB 26                 ; Room above (East Wall Base)
  DEFB 0                  ; Room below (The Off Licence)
; The next three bytes are copied to XROOM237, but are not used.
  DEFB 0,0,0              ; Unused
; The next eight pairs of bytes are copied to ENTITIES and specify the entities
; (ropes, arrows, guardians) in this room.
  DEFB 19,108             ; Guardian no. 19 (vertical), base sprite 3, x=12
                          ; (ENTITY19)
  DEFB 42,5               ; Guardian no. 42 (vertical), base sprite 0, x=5
                          ; (ENTITY42)
  DEFB 40,7               ; Guardian no. 40 (vertical), base sprite 0, x=7
                          ; (ENTITY40)
  DEFB 41,43              ; Guardian no. 41 (vertical), base sprite 1, x=11
                          ; (ENTITY41)
  DEFB 21,24              ; Guardian no. 21 (horizontal), base sprite 0,
                          ; initial x=24 (ENTITY21)
  DEFB 255,0              ; Terminator (ENTITY127)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)

; Room 21: Ballroom West (teleport: 1359)
;
; Used by the routine at INITROOM.
;
; The first 128 bytes are copied to ROOMLAYOUT and define the room layout. Each
; bit-pair (bits 7 and 6, 5 and 4, 3 and 2, or 1 and 0 of each byte) determines
; the type of tile (background, floor, wall or nasty) that will be drawn at the
; corresponding location.
  DEFB 0,0,0,0,0,0,0,10                ; Room layout
  DEFB 0,0,0,0,0,0,0,10                ;
  DEFB 0,0,0,0,0,0,0,10                ;
  DEFB 0,0,0,0,0,0,0,0                 ;
  DEFB 0,0,0,0,0,0,0,0                 ;
  DEFB 85,85,85,85,85,85,85,85         ;
  DEFB 0,0,160,0,0,0,0,0               ;
  DEFB 0,0,160,0,0,0,0,0               ;
  DEFB 0,0,160,0,0,0,85,85             ;
  DEFB 0,0,160,0,0,0,0,0               ;
  DEFB 0,0,160,0,0,0,0,0               ;
  DEFB 0,0,160,0,0,0,0,0               ;
  DEFB 0,0,0,0,0,0,0,0                 ;
  DEFB 0,0,0,0,0,0,0,0                 ;
  DEFB 85,85,85,85,85,85,85,85         ;
  DEFB 170,170,170,170,170,170,170,170 ;
; The next 32 bytes are copied to ROOMNAME and specify the room name.
  DEFM "         Ballroom West          " ; Room name
; The next 54 bytes are copied to BACKGROUND and contain the attributes and
; graphic data for the tiles used to build the room.
  DEFB 0,0,0,0,0,0,0,0,0  ; Background
  DEFB 50,170,168,173,175,32,170,85,85 ; Floor
  DEFB 35,17,34,136,17,68,136,34,68 ; Wall
  DEFB 66,40,84,170,84,168,84,20,46 ; Nasty (unused)
  DEFB 7,192,192,48,176,12,12,3,11 ; Ramp
  DEFB 70,255,255,255,170,85,24,24,24 ; Conveyor
; The next four bytes are copied to CONVDIR and specify the direction, location
; and length of the conveyor.
  DEFB 1                  ; Direction (right)
  DEFW 24496              ; Location in the attribute buffer at 24064: (13,16)
  DEFB 12                 ; Length
; The next four bytes are copied to RAMPDIR and specify the direction, location
; and length of the ramp.
  DEFB 0                  ; Direction (up to the left)
  DEFW 24483              ; Location in the attribute buffer at 24064: (13,3)
  DEFB 4                  ; Length
; The next byte is copied to BORDER and specifies the border colour.
  DEFB 1                  ; Border colour
; The next two bytes are copied to XROOM223, but are not used.
  DEFB 0,0                ; Unused
; The next eight bytes are copied to ITEM and define the item graphic.
  DEFB 4,4,174,174,162,66,66,238 ; Item graphic
; The next four bytes are copied to LEFT and specify the rooms to the left, to
; the right, above and below.
  DEFB 22                 ; Room to the left (To the Kitchens    Main Stairway)
  DEFB 20                 ; Room to the right (Ballroom East)
  DEFB 27                 ; Room above (The Chapel)
  DEFB 0                  ; Room below (The Off Licence)
; The next three bytes are copied to XROOM237, but are not used.
  DEFB 0,0,0              ; Unused
; The next eight pairs of bytes are copied to ENTITIES and specify the entities
; (ropes, arrows, guardians) in this room.
  DEFB 33,24              ; Guardian no. 33 (horizontal), base sprite 0,
                          ; initial x=24 (ENTITY33)
  DEFB 40,14              ; Guardian no. 40 (vertical), base sprite 0, x=14
                          ; (ENTITY40)
  DEFB 42,6               ; Guardian no. 42 (vertical), base sprite 0, x=6
                          ; (ENTITY42)
  DEFB 255,0              ; Terminator (ENTITY127)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)

; Room 22: To the Kitchens    Main Stairway (teleport: 2359)
;
; Used by the routine at INITROOM.
;
; The first 128 bytes are copied to ROOMLAYOUT and define the room layout. Each
; bit-pair (bits 7 and 6, 5 and 4, 3 and 2, or 1 and 0 of each byte) determines
; the type of tile (background, floor, wall or nasty) that will be drawn at the
; corresponding location.
  DEFB 0,0,128,0,0,0,0,0               ; Room layout
  DEFB 0,0,128,0,0,0,0,0               ;
  DEFB 0,0,128,0,0,0,0,0               ;
  DEFB 0,0,0,0,0,0,3,0                 ;
  DEFB 0,0,0,0,0,0,0,0                 ;
  DEFB 0,1,149,85,85,80,0,85           ;
  DEFB 0,0,128,0,0,0,0,0               ;
  DEFB 0,0,128,0,0,0,0,0               ;
  DEFB 165,85,133,0,0,0,0,0            ;
  DEFB 160,0,128,0,0,0,85,64           ;
  DEFB 160,0,128,0,1,64,0,0            ;
  DEFB 0,0,0,0,0,0,0,0                 ;
  DEFB 0,0,0,0,0,0,0,0                 ;
  DEFB 0,0,0,0,0,0,0,0                 ;
  DEFB 170,170,170,170,170,170,170,170 ;
  DEFB 170,170,170,170,170,170,170,170 ;
; The next 32 bytes are copied to ROOMNAME and specify the room name.
  DEFM "To the Kitchens    Main Stairway" ; Room name
; The next 54 bytes are copied to BACKGROUND and contain the attributes and
; graphic data for the tiles used to build the room.
  DEFB 8,0,0,0,0,0,0,0,0  ; Background
  DEFB 13,255,255,0,255,0,0,0,0 ; Floor
  DEFB 58,0,21,191,21,0,81,251,81 ; Wall
  DEFB 77,24,90,189,25,126,153,36,36 ; Nasty
  DEFB 15,192,128,48,32,12,8,3,2 ; Ramp
  DEFB 107,240,170,120,255,170,85,170,85 ; Conveyor
; The next four bytes are copied to CONVDIR and specify the direction, location
; and length of the conveyor.
  DEFB 0                  ; Direction (left)
  DEFW 24334              ; Location in the attribute buffer at 24064: (8,14)
  DEFB 2                  ; Length
; The next four bytes are copied to RAMPDIR and specify the direction, location
; and length of the ramp.
  DEFB 0                  ; Direction (up to the left)
  DEFW 24447              ; Location in the attribute buffer at 24064: (11,31)
  DEFB 12                 ; Length
; The next byte is copied to BORDER and specifies the border colour.
  DEFB 4                  ; Border colour
; The next two bytes are copied to XROOM223, but are not used.
  DEFB 0,0                ; Unused
; The next eight bytes are copied to ITEM and define the item graphic.
  DEFB 64,160,240,248,252,122,37,2 ; Item graphic
; The next four bytes are copied to LEFT and specify the rooms to the left, to
; the right, above and below.
  DEFB 23                 ; Room to the left (The Kitchen)
  DEFB 21                 ; Room to the right (Ballroom West)
  DEFB 28                 ; Room above (First Landing)
  DEFB 0                  ; Room below (The Off Licence)
; The next three bytes are copied to XROOM237, but are not used.
  DEFB 0,0,0              ; Unused
; The next eight pairs of bytes are copied to ENTITIES and specify the entities
; (ropes, arrows, guardians) in this room.
  DEFB 90,4               ; Guardian no. 90 (horizontal), base sprite 0,
                          ; initial x=4 (ENTITY90)
  DEFB 91,16              ; Guardian no. 91 (horizontal), base sprite 0,
                          ; initial x=16 (ENTITY91)
  DEFB 92,150             ; Guardian no. 92 (horizontal), base sprite 4,
                          ; initial x=22 (ENTITY92)
  DEFB 93,4               ; Guardian no. 93 (horizontal), base sprite 0,
                          ; initial x=4 (ENTITY93)
  DEFB 94,150             ; Guardian no. 94 (horizontal), base sprite 4,
                          ; initial x=22 (ENTITY94)
  DEFB 95,11              ; Guardian no. 95 (horizontal), base sprite 0,
                          ; initial x=11 (ENTITY95)
  DEFB 255,0              ; Terminator (ENTITY127)
  DEFB 0,0                ; Nothing (ENTITYDEFS)

; Room 23: The Kitchen (teleport: 12359)
;
; Used by the routine at INITROOM.
;
; The first 128 bytes are copied to ROOMLAYOUT and define the room layout. Each
; bit-pair (bits 7 and 6, 5 and 4, 3 and 2, or 1 and 0 of each byte) determines
; the type of tile (background, floor, wall or nasty) that will be drawn at the
; corresponding location.
  DEFB 0,0,0,0,0,0,0,0                 ; Room layout
  DEFB 0,0,0,0,0,0,0,0                 ;
  DEFB 0,0,0,0,0,0,0,0                 ;
  DEFB 0,0,0,0,0,0,0,0                 ;
  DEFB 0,0,0,0,0,0,0,0                 ;
  DEFB 84,21,65,80,0,0,0,0             ;
  DEFB 0,0,0,0,0,0,0,0                 ;
  DEFB 0,0,0,0,4,0,0,0                 ;
  DEFB 84,21,64,0,0,0,0,42             ;
  DEFB 0,0,1,80,0,0,0,170              ;
  DEFB 0,0,0,0,0,0,2,170               ;
  DEFB 0,0,0,0,0,0,0,0                 ;
  DEFB 84,21,65,80,0,0,0,0             ;
  DEFB 0,0,0,0,21,0,0,0                ;
  DEFB 0,0,0,0,0,2,170,170             ;
  DEFB 170,170,170,170,170,170,170,170 ;
; The next 32 bytes are copied to ROOMNAME and specify the room name.
  DEFM "          The Kitchen           " ; Room name
; The next 54 bytes are copied to BACKGROUND and contain the attributes and
; graphic data for the tiles used to build the room.
  DEFB 0,0,0,0,0,0,0,0,0  ; Background
  DEFB 4,254,1,125,133,129,129,129,1 ; Floor
  DEFB 77,66,165,90,36,36,90,165,66 ; Wall
  DEFB 255,0,0,0,0,0,0,0,0 ; Nasty (unused)
  DEFB 71,3,0,12,0,48,0,192,0 ; Ramp
  DEFB 255,0,0,0,0,0,0,0,0 ; Conveyor (unused)
; The next four bytes are copied to CONVDIR and specify the direction, location
; and length of the conveyor.
  DEFB 0                  ; Direction (left)
  DEFW 0                  ; Location in the attribute buffer at 24064 (unused)
  DEFB 0                  ; Length: 0 (there is no conveyor in this room)
; The next four bytes are copied to RAMPDIR and specify the direction, location
; and length of the ramp.
  DEFB 1                  ; Direction (up to the right)
  DEFW 24534              ; Location in the attribute buffer at 24064: (14,22)
  DEFB 7                  ; Length
; The next byte is copied to BORDER and specifies the border colour.
  DEFB 4                  ; Border colour
; The next two bytes are copied to XROOM223, but are not used.
  DEFB 0,0                ; Unused
; The next eight bytes are copied to ITEM and define the item graphic.
  DEFB 32,64,160,24,22,9,8,4 ; Item graphic (unused)
; The next four bytes are copied to LEFT and specify the rooms to the left, to
; the right, above and below.
  DEFB 24                 ; Room to the left (West of Kitchen)
  DEFB 22                 ; Room to the right (To the Kitchens    Main
                          ; Stairway)
  DEFB 0                  ; Room above (The Off Licence)
  DEFB 0                  ; Room below (The Off Licence)
; The next three bytes are copied to XROOM237, but are not used.
  DEFB 0,0,0              ; Unused
; The next eight pairs of bytes are copied to ENTITIES and specify the entities
; (ropes, arrows, guardians) in this room.
  DEFB 48,3               ; Guardian no. 48 (vertical), base sprite 0, x=3
                          ; (ENTITY48)
  DEFB 49,9               ; Guardian no. 49 (vertical), base sprite 0, x=9
                          ; (ENTITY49)
  DEFB 50,15              ; Guardian no. 50 (vertical), base sprite 0, x=15
                          ; (ENTITY50)
  DEFB 48,20              ; Guardian no. 48 (vertical), base sprite 0, x=20
                          ; (ENTITY48)
  DEFB 255,0              ; Terminator (ENTITY127)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)

; Room 24: West of Kitchen (teleport: 459)
;
; Used by the routine at INITROOM.
;
; The first 128 bytes are copied to ROOMLAYOUT and define the room layout. Each
; bit-pair (bits 7 and 6, 5 and 4, 3 and 2, or 1 and 0 of each byte) determines
; the type of tile (background, floor, wall or nasty) that will be drawn at the
; corresponding location.
  DEFB 0,0,1,0,0,0,0,0                 ; Room layout
  DEFB 0,0,1,0,0,0,0,0                 ;
  DEFB 0,0,1,0,0,0,0,0                 ;
  DEFB 0,0,1,0,0,0,0,0                 ;
  DEFB 0,0,1,0,0,0,0,0                 ;
  DEFB 0,0,1,0,16,5,84,21              ;
  DEFB 0,0,0,0,0,0,0,0                 ;
  DEFB 0,0,0,0,0,0,0,0                 ;
  DEFB 160,0,0,0,0,4,0,21              ;
  DEFB 168,0,0,0,21,0,80,0             ;
  DEFB 170,0,0,80,0,0,0,0              ;
  DEFB 170,128,0,0,0,0,0,0             ;
  DEFB 170,160,0,0,21,5,84,21          ;
  DEFB 170,168,0,0,0,0,0,0             ;
  DEFB 170,170,0,0,0,0,0,0             ;
  DEFB 170,170,170,170,170,170,170,170 ;
; The next 32 bytes are copied to ROOMNAME and specify the room name.
  DEFM "    West of Kitchen             " ; Room name
; The next 54 bytes are copied to BACKGROUND and contain the attributes and
; graphic data for the tiles used to build the room. Note that because of a bug
; in the game engine, the conveyor tile is not drawn correctly (see the room
; image above).
  DEFB 0,0,0,0,0,0,0,0,0  ; Background
  DEFB 5,255,189,165,165,165,66,0,0 ; Floor
  DEFB 34,9,66,144,36,9,66,144,36 ; Wall
  DEFB 255,0,0,0,0,0,0,0,0 ; Nasty (unused)
  DEFB 71,192,64,176,80,172,84,171,85 ; Ramp
  DEFB 66,240,85,170,85,85,170,42,8 ; Conveyor
; The next four bytes are copied to CONVDIR and specify the direction, location
; and length of the conveyor.
  DEFB 0                  ; Direction (left)
  DEFW 24470              ; Location in the attribute buffer at 24064: (12,22)
  DEFB 5                  ; Length
; The next four bytes are copied to RAMPDIR and specify the direction, location
; and length of the ramp.
  DEFB 0                  ; Direction (up to the left)
  DEFW 24520              ; Location in the attribute buffer at 24064: (14,8)
  DEFB 7                  ; Length
; The next byte is copied to BORDER and specifies the border colour.
  DEFB 1                  ; Border colour
; The next two bytes are copied to XROOM223, but are not used.
  DEFB 0,0                ; Unused
; The next eight bytes are copied to ITEM and define the item graphic.
  DEFB 0,0,0,0,0,0,0,0    ; Item graphic (unused)
; The next four bytes are copied to LEFT and specify the rooms to the left, to
; the right, above and below.
  DEFB 25                 ; Room to the left (Cold Store)
  DEFB 23                 ; Room to the right (The Kitchen)
  DEFB 30                 ; Room above (The Banyan Tree)
  DEFB 0                  ; Room below (The Off Licence)
; The next three bytes are copied to XROOM237, but are not used.
  DEFB 0,0,0              ; Unused
; The next eight pairs of bytes are copied to ENTITIES and specify the entities
; (ropes, arrows, guardians) in this room.
  DEFB 49,9               ; Guardian no. 49 (vertical), base sprite 0, x=9
                          ; (ENTITY49)
  DEFB 50,14              ; Guardian no. 50 (vertical), base sprite 0, x=14
                          ; (ENTITY50)
  DEFB 49,20              ; Guardian no. 49 (vertical), base sprite 0, x=20
                          ; (ENTITY49)
  DEFB 48,27              ; Guardian no. 48 (vertical), base sprite 0, x=27
                          ; (ENTITY48)
  DEFB 255,0              ; Terminator (ENTITY127)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)

; Room 25: Cold Store (teleport: 1459)
;
; Used by the routine at INITROOM.
;
; The first 128 bytes are copied to ROOMLAYOUT and define the room layout. Each
; bit-pair (bits 7 and 6, 5 and 4, 3 and 2, or 1 and 0 of each byte) determines
; the type of tile (background, floor, wall or nasty) that will be drawn at the
; corresponding location.
  DEFB 0,3,0,0,0,0,0,0          ; Room layout
  DEFB 0,0,0,48,0,0,192,0       ;
  DEFB 84,0,0,0,0,0,0,0         ;
  DEFB 0,0,0,0,0,192,3,0        ;
  DEFB 0,0,0,0,0,0,0,0          ;
  DEFB 85,85,0,0,0,0,0,0        ;
  DEFB 0,0,0,0,0,0,0,0          ;
  DEFB 0,0,0,0,0,0,0,0          ;
  DEFB 85,80,0,0,0,0,10,170     ;
  DEFB 0,0,0,0,0,0,10,170       ;
  DEFB 0,0,0,0,0,0,10,170       ;
  DEFB 85,80,0,0,0,0,10,170     ;
  DEFB 0,0,48,0,0,20,10,170     ;
  DEFB 0,0,0,0,0,0,10,170       ;
  DEFB 0,0,0,0,0,0,10,170       ;
  DEFB 85,85,85,85,85,85,90,170 ;
; The next 32 bytes are copied to ROOMNAME and specify the room name.
  DEFM "           Cold Store           " ; Room name
; The next 54 bytes are copied to BACKGROUND and contain the attributes and
; graphic data for the tiles used to build the room.
  DEFB 14,0,0,0,0,0,0,0,0 ; Background
  DEFB 13,255,255,85,170,85,170,0,0 ; Floor
  DEFB 34,146,82,74,73,41,37,164,146 ; Wall
  DEFB 79,32,34,20,216,27,40,68,4 ; Nasty
  DEFB 255,0,0,0,0,0,0,0,0 ; Ramp (unused)
  DEFB 255,0,0,0,0,0,0,0,0 ; Conveyor (unused)
; The next four bytes are copied to CONVDIR and specify the direction, location
; and length of the conveyor.
  DEFB 0                  ; Direction (left)
  DEFW 0                  ; Location in the attribute buffer at 24064 (unused)
  DEFB 0                  ; Length: 0 (there is no conveyor in this room)
; The next four bytes are copied to RAMPDIR and specify the direction, location
; and length of the ramp.
  DEFB 0                  ; Direction (up to the left)
  DEFW 0                  ; Location in the attribute buffer at 24064 (unused)
  DEFB 0                  ; Length: 0 (there is no ramp in this room)
; The next byte is copied to BORDER and specifies the border colour.
  DEFB 1                  ; Border colour
; The next two bytes are copied to XROOM223, but are not used.
  DEFB 0,0                ; Unused
; The next eight bytes are copied to ITEM and define the item graphic.
  DEFB 112,140,114,250,239,173,37,33 ; Item graphic
; The next four bytes are copied to LEFT and specify the rooms to the left, to
; the right, above and below.
  DEFB 52                 ; Room to the left (Back Stairway)
  DEFB 24                 ; Room to the right (West of Kitchen)
  DEFB 31                 ; Room above (Swimming Pool)
  DEFB 0                  ; Room below (The Off Licence)
; The next three bytes are copied to XROOM237, but are not used.
  DEFB 0,0,0              ; Unused
; The next eight pairs of bytes are copied to ENTITIES and specify the entities
; (ropes, arrows, guardians) in this room.
  DEFB 54,16              ; Guardian no. 54 (horizontal), base sprite 0,
                          ; initial x=16 (ENTITY54)
  DEFB 55,4               ; Guardian no. 55 (horizontal), base sprite 0,
                          ; initial x=4 (ENTITY55)
  DEFB 56,3               ; Guardian no. 56 (horizontal), base sprite 0,
                          ; initial x=3 (ENTITY56)
  DEFB 57,8               ; Guardian no. 57 (horizontal), base sprite 0,
                          ; initial x=8 (ENTITY57)
  DEFB 1,16               ; Rope at x=16 (ENTITY1)
  DEFB 255,0              ; Terminator (ENTITY127)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)

; Room 26: East Wall Base (teleport: 2459)
;
; Used by the routine at INITROOM.
;
; The first 128 bytes are copied to ROOMLAYOUT and define the room layout. Each
; bit-pair (bits 7 and 6, 5 and 4, 3 and 2, or 1 and 0 of each byte) determines
; the type of tile (background, floor, wall or nasty) that will be drawn at the
; corresponding location.
  DEFB 160,0,10,0,0,0,0,0  ; Room layout
  DEFB 160,0,10,0,0,0,0,0  ;
  DEFB 160,0,10,0,0,0,0,0  ;
  DEFB 160,0,10,0,0,0,0,0  ;
  DEFB 160,0,26,0,0,0,0,0  ;
  DEFB 160,4,10,0,0,0,0,0  ;
  DEFB 160,0,10,0,0,0,0,0  ;
  DEFB 164,0,10,0,0,0,0,0  ;
  DEFB 160,4,0,0,0,0,0,0   ;
  DEFB 160,0,0,0,0,0,0,0   ;
  DEFB 160,0,26,0,0,0,0,0  ;
  DEFB 160,16,10,0,0,0,0,0 ;
  DEFB 160,0,10,0,0,0,0,0  ;
  DEFB 160,4,10,0,0,0,0,0  ;
  DEFB 160,0,26,0,0,0,0,0  ;
  DEFB 160,64,10,0,0,0,0,0 ;
; The next 32 bytes are copied to ROOMNAME and specify the room name.
  DEFM "         East Wall Base         " ; Room name
; The next 54 bytes are copied to BACKGROUND and contain the attributes and
; graphic data for the tiles used to build the room.
  DEFB 0,0,0,0,0,0,0,0,0  ; Background
  DEFB 5,91,164,164,85,10,0,0,0 ; Floor
  DEFB 38,89,37,38,217,41,214,84,150 ; Wall
  DEFB 255,0,0,0,0,0,0,0,0 ; Nasty (unused)
  DEFB 255,0,0,0,0,0,0,0,0 ; Ramp (unused)
  DEFB 255,0,0,0,0,0,0,0,0 ; Conveyor (unused)
; The next four bytes are copied to CONVDIR and specify the direction, location
; and length of the conveyor.
  DEFB 0                  ; Direction (left)
  DEFW 0                  ; Location in the attribute buffer at 24064 (unused)
  DEFB 0                  ; Length: 0 (there is no conveyor in this room)
; The next four bytes are copied to RAMPDIR and specify the direction, location
; and length of the ramp.
  DEFB 0                  ; Direction (up to the left)
  DEFW 0                  ; Location in the attribute buffer at 24064 (unused)
  DEFB 0                  ; Length: 0 (there is no ramp in this room)
; The next byte is copied to BORDER and specifies the border colour.
  DEFB 5                  ; Border colour
; The next two bytes are copied to XROOM223, but are not used.
  DEFB 0,0                ; Unused
; The next eight bytes are copied to ITEM and define the item graphic.
  DEFB 0,0,0,0,0,0,0,0    ; Item graphic (unused)
; The next four bytes are copied to LEFT and specify the rooms to the left, to
; the right, above and below.
  DEFB 27                 ; Room to the left (The Chapel)
  DEFB 0                  ; Room to the right (The Off Licence)
  DEFB 32                 ; Room above (Halfway up the East Wall)
  DEFB 20                 ; Room below (Ballroom East)
; The next three bytes are copied to XROOM237, but are not used.
  DEFB 0,0,0              ; Unused
; The next eight pairs of bytes are copied to ENTITIES and specify the entities
; (ropes, arrows, guardians) in this room.
  DEFB 96,3               ; Guardian no. 96 (vertical), base sprite 0, x=3
                          ; (ENTITY96)
  DEFB 45,39              ; Guardian no. 45 (vertical), base sprite 1, x=7
                          ; (ENTITY45)
  DEFB 255,0              ; Terminator (ENTITY127)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)

; Room 27: The Chapel (teleport: 12459)
;
; Used by the routine at INITROOM.
;
; The first 128 bytes are copied to ROOMLAYOUT and define the room layout. Each
; bit-pair (bits 7 and 6, 5 and 4, 3 and 2, or 1 and 0 of each byte) determines
; the type of tile (background, floor, wall or nasty) that will be drawn at the
; corresponding location.
  DEFB 0,0,0,0,0,0,0,10                ; Room layout
  DEFB 0,0,0,0,0,0,0,10                ;
  DEFB 0,0,0,0,0,0,0,10                ;
  DEFB 0,0,0,0,0,0,0,10                ;
  DEFB 0,0,0,0,0,0,0,10                ;
  DEFB 0,0,0,0,0,0,0,10                ;
  DEFB 0,0,0,0,0,0,0,10                ;
  DEFB 0,0,0,0,0,0,128,26              ;
  DEFB 0,0,0,0,0,2,128,26              ;
  DEFB 0,0,0,0,0,10,128,26             ;
  DEFB 0,0,0,0,0,42,128,26             ;
  DEFB 0,0,0,0,0,170,128,26            ;
  DEFB 0,0,0,0,0,0,0,26                ;
  DEFB 0,0,0,0,0,0,0,26                ;
  DEFB 170,170,170,170,170,170,160,170 ;
  DEFB 170,170,170,170,170,170,160,170 ;
; The next 32 bytes are copied to ROOMNAME and specify the room name.
  DEFM "           The Chapel           " ; Room name
; The next 54 bytes are copied to BACKGROUND and contain the attributes and
; graphic data for the tiles used to build the room.
  DEFB 0,0,0,0,0,0,0,0,0  ; Background
  DEFB 13,73,73,73,73,73,73,73,73 ; Floor
  DEFB 22,168,37,0,37,168,64,221,64 ; Wall
  DEFB 66,40,84,170,85,170,40,40,40 ; Nasty (unused)
  DEFB 7,3,0,12,0,48,0,192,0 ; Ramp
  DEFB 7,0,10,189,255,0,170,85,170 ; Conveyor (unused)
; The next four bytes are copied to CONVDIR and specify the direction, location
; and length of the conveyor.
  DEFB 0                  ; Direction (left)
  DEFW 0                  ; Location in the attribute buffer at 24064 (unused)
  DEFB 0                  ; Length: 0 (there is no conveyor in this room)
; The next four bytes are copied to RAMPDIR and specify the direction, location
; and length of the ramp.
  DEFB 1                  ; Direction (up to the right)
  DEFW 24497              ; Location in the attribute buffer at 24064: (13,17)
  DEFB 7                  ; Length
; The next byte is copied to BORDER and specifies the border colour.
  DEFB 2                  ; Border colour
; The next two bytes are copied to XROOM223, but are not used.
  DEFB 0,0                ; Unused
; The next eight bytes are copied to ITEM and define the item graphic.
  DEFB 16,48,56,108,198,198,108,16 ; Item graphic
; The next four bytes are copied to LEFT and specify the rooms to the left, to
; the right, above and below.
  DEFB 28                 ; Room to the left (First Landing)
  DEFB 26                 ; Room to the right (East Wall Base)
  DEFB 33                 ; Room above (The Bathroom)
  DEFB 21                 ; Room below (Ballroom West)
; The next three bytes are copied to XROOM237, but are not used.
  DEFB 0,0,0              ; Unused
; The next eight pairs of bytes are copied to ENTITIES and specify the entities
; (ropes, arrows, guardians) in this room.
  DEFB 16,153             ; Guardian no. 16 (vertical), base sprite 4, x=25
                          ; (ENTITY16)
  DEFB 17,187             ; Guardian no. 17 (vertical), base sprite 5, x=27
                          ; (ENTITY17)
  DEFB 18,218             ; Guardian no. 18 (vertical), base sprite 6, x=26
                          ; (ENTITY18)
  DEFB 103,12             ; Guardian no. 103 (horizontal), base sprite 0,
                          ; initial x=12 (ENTITY103)
  DEFB 100,6              ; Guardian no. 100 (vertical), base sprite 0, x=6
                          ; (ENTITY100)
  DEFB 102,145            ; Guardian no. 102 (vertical), base sprite 4, x=17
                          ; (ENTITY102)
  DEFB 255,0              ; Terminator (ENTITY127)
  DEFB 0,0                ; Nothing (ENTITYDEFS)

; Room 28: First Landing (teleport: 3459)
;
; Used by the routine at INITROOM.
;
; The first 128 bytes are copied to ROOMLAYOUT and define the room layout. Each
; bit-pair (bits 7 and 6, 5 and 4, 3 and 2, or 1 and 0 of each byte) determines
; the type of tile (background, floor, wall or nasty) that will be drawn at the
; corresponding location.
  DEFB 160,0,0,0,0,0,160,0       ; Room layout
  DEFB 160,0,0,0,0,0,160,0       ;
  DEFB 160,0,0,0,0,0,160,0       ;
  DEFB 160,0,0,0,0,0,160,48      ;
  DEFB 160,0,0,0,0,0,160,252     ;
  DEFB 160,0,0,0,0,0,160,48      ;
  DEFB 160,0,0,0,0,0,160,48      ;
  DEFB 160,0,0,0,0,0,160,48      ;
  DEFB 160,0,0,0,0,0,160,0       ;
  DEFB 160,0,0,0,0,0,160,0       ;
  DEFB 160,0,0,0,0,0,160,0       ;
  DEFB 160,0,0,0,0,0,160,0       ;
  DEFB 160,0,0,0,0,0,0,0         ;
  DEFB 0,0,0,0,0,0,0,0           ;
  DEFB 0,0,0,0,0,0,153,153       ;
  DEFB 85,85,85,85,85,80,170,170 ;
; The next 32 bytes are copied to ROOMNAME and specify the room name.
  DEFM "        First Landing           " ; Room name
; The next 54 bytes are copied to BACKGROUND and contain the attributes and
; graphic data for the tiles used to build the room.
  DEFB 0,0,0,0,0,0,0,0,0  ; Background
  DEFB 4,255,170,85,128,93,148,93,128 ; Floor
  DEFB 14,34,85,0,85,136,85,0,34 ; Wall
  DEFB 214,126,129,165,153,153,165,129,126 ; Nasty
  DEFB 7,3,3,12,12,48,48,192,192 ; Ramp
  DEFB 255,0,0,0,0,0,0,0,0 ; Conveyor (unused)
; The next four bytes are copied to CONVDIR and specify the direction, location
; and length of the conveyor.
  DEFB 0                  ; Direction (left)
  DEFW 0                  ; Location in the attribute buffer at 24064 (unused)
  DEFB 0                  ; Length: 0 (there is no conveyor in this room)
; The next four bytes are copied to RAMPDIR and specify the direction, location
; and length of the ramp.
  DEFB 1                  ; Direction (up to the right)
  DEFW 24521              ; Location in the attribute buffer at 24064: (14,9)
  DEFB 15                 ; Length
; The next byte is copied to BORDER and specifies the border colour.
  DEFB 3                  ; Border colour
; The next two bytes are copied to XROOM223, but are not used.
  DEFB 0,0                ; Unused
; The next eight bytes are copied to ITEM and define the item graphic.
  DEFB 0,0,0,0,0,0,0,0    ; Item graphic
; The next four bytes are copied to LEFT and specify the rooms to the left, to
; the right, above and below.
  DEFB 29                 ; Room to the left (The Nightmare Room)
  DEFB 27                 ; Room to the right (The Chapel)
  DEFB 34                 ; Room above (Top Landing)
  DEFB 22                 ; Room below (To the Kitchens    Main Stairway)
; The next three bytes are copied to XROOM237, but are not used.
  DEFB 0,0,0              ; Unused
; The next eight pairs of bytes are copied to ENTITIES and specify the entities
; (ropes, arrows, guardians) in this room.
  DEFB 67,25              ; Guardian no. 67 (horizontal), base sprite 0,
                          ; initial x=25 (ENTITY67)
  DEFB 255,0              ; Terminator (ENTITY127)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)

; Room 29: The Nightmare Room (teleport: 13459)
;
; Used by the routine at INITROOM.
;
; The first 128 bytes are copied to ROOMLAYOUT and define the room layout. Each
; bit-pair (bits 7 and 6, 5 and 4, 3 and 2, or 1 and 0 of each byte) determines
; the type of tile (background, floor, wall or nasty) that will be drawn at the
; corresponding location.
  DEFB 0,0,0,0,0,0,0,10                ; Room layout
  DEFB 0,0,0,0,0,0,0,10                ;
  DEFB 0,0,0,0,0,0,0,10                ;
  DEFB 0,0,0,0,0,0,0,10                ;
  DEFB 0,0,0,0,0,0,0,10                ;
  DEFB 0,0,4,0,64,4,0,10               ;
  DEFB 0,0,0,16,0,0,0,10               ;
  DEFB 0,1,0,0,1,0,0,10                ;
  DEFB 0,0,4,0,0,4,0,10                ;
  DEFB 0,0,0,16,0,0,0,10               ;
  DEFB 0,0,0,0,1,0,4,10                ;
  DEFB 0,0,4,0,0,4,0,10                ;
  DEFB 170,168,0,16,0,0,0,10           ;
  DEFB 170,170,0,0,0,0,0,0             ;
  DEFB 170,170,128,0,0,0,0,0           ;
  DEFB 170,170,170,170,170,170,170,170 ;
; The next 32 bytes are copied to ROOMNAME and specify the room name.
  DEFM "       The Nightmare Room       " ; Room name
; The next 54 bytes are copied to BACKGROUND and contain the attributes and
; graphic data for the tiles used to build the room. Note that because of a bug
; in the game engine, the conveyor tile is not drawn correctly (see the room
; image above).
  DEFB 0,0,0,0,0,0,0,0,0  ; Background
  DEFB 68,255,165,82,36,66,34,64,4 ; Floor
  DEFB 51,0,42,84,0,0,162,69,0 ; Wall
  DEFB 69,66,153,102,161,159,129,102,90 ; Nasty (unused)
  DEFB 7,128,192,160,240,248,92,186,2 ; Ramp
  DEFB 165,255,90,255,255,170,85,170,85 ; Conveyor
; The next four bytes are copied to CONVDIR and specify the direction, location
; and length of the conveyor.
  DEFB 0                  ; Direction (left)
  DEFW 24315              ; Location in the attribute buffer at 24064: (7,27)
  DEFB 1                  ; Length
; The next four bytes are copied to RAMPDIR and specify the direction, location
; and length of the ramp.
  DEFB 0                  ; Direction (up to the left)
  DEFW 24521              ; Location in the attribute buffer at 24064: (14,9)
  DEFB 3                  ; Length
; The next byte is copied to BORDER and specifies the border colour.
  DEFB 2                  ; Border colour
; The next two bytes are copied to XROOM223, but are not used.
  DEFB 0,0                ; Unused
; The next eight bytes are copied to ITEM and define the item graphic.
  DEFB 172,174,1,173,173,2,172,88 ; Item graphic
; The next four bytes are copied to LEFT and specify the rooms to the left, to
; the right, above and below.
  DEFB 30                 ; Room to the left (The Banyan Tree)
  DEFB 28                 ; Room to the right (First Landing)
  DEFB 0                  ; Room above (The Off Licence)
  DEFB 0                  ; Room below (The Off Licence)
; The next three bytes are copied to XROOM237, but are not used.
  DEFB 0,0,0              ; Unused
; The next eight pairs of bytes are copied to ENTITIES and specify the entities
; (ropes, arrows, guardians) in this room.
  DEFB 70,136             ; Guardian no. 70 (vertical), base sprite 4, x=8
                          ; (ENTITY70)
  DEFB 71,203             ; Guardian no. 71 (vertical), base sprite 6, x=11
                          ; (ENTITY71)
  DEFB 72,78              ; Guardian no. 72 (vertical), base sprite 2, x=14
                          ; (ENTITY72)
  DEFB 73,209             ; Guardian no. 73 (vertical), base sprite 6, x=17
                          ; (ENTITY73)
  DEFB 70,148             ; Guardian no. 70 (vertical), base sprite 4, x=20
                          ; (ENTITY70)
  DEFB 71,151             ; Guardian no. 71 (vertical), base sprite 4, x=23
                          ; (ENTITY71)
  DEFB 72,156             ; Guardian no. 72 (vertical), base sprite 4, x=28
                          ; (ENTITY72)
  DEFB 255,0              ; Terminator (ENTITY127)

; Room 30: The Banyan Tree (teleport: 23459)
;
; Used by the routine at INITROOM.
;
; The first 128 bytes are copied to ROOMLAYOUT and define the room layout. Each
; bit-pair (bits 7 and 6, 5 and 4, 3 and 2, or 1 and 0 of each byte) determines
; the type of tile (background, floor, wall or nasty) that will be drawn at the
; corresponding location.
  DEFB 0,0,0,160,0,0,0,0              ; Room layout
  DEFB 0,0,0,160,0,0,0,0              ;
  DEFB 0,0,0,144,0,0,0,0              ;
  DEFB 0,0,0,148,0,0,0,0              ;
  DEFB 0,0,0,149,80,0,0,0             ;
  DEFB 0,0,0,129,8,0,0,0              ;
  DEFB 0,0,0,1,4,0,0,0                ;
  DEFB 0,0,0,0,4,0,0,0                ;
  DEFB 0,0,0,128,0,0,0,0              ;
  DEFB 0,0,0,130,0,0,0,0              ;
  DEFB 0,8,32,130,8,0,0,0             ;
  DEFB 0,8,32,130,8,0,0,0             ;
  DEFB 170,170,170,170,170,170,90,170 ;
  DEFB 170,170,0,0,0,0,90,170         ;
  DEFB 170,170,0,0,0,0,90,170         ;
  DEFB 170,170,85,85,85,85,90,170     ;
; The next 32 bytes are copied to ROOMNAME and specify the room name.
  DEFM "       The Banyan Tree          " ; Room name
; The next 54 bytes are copied to BACKGROUND and contain the attributes and
; graphic data for the tiles used to build the room.
  DEFB 0,0,0,0,0,0,0,0,0  ; Background
  DEFB 72,0,162,0,0,0,70,0,147 ; Floor
  DEFB 22,74,41,37,73,146,164,37,148 ; Wall
  DEFB 14,165,90,165,90,165,90,165,90 ; Nasty (unused)
  DEFB 7,3,0,12,0,48,0,192,0 ; Ramp
  DEFB 255,0,0,0,0,0,0,0,0 ; Conveyor (unused)
; The next four bytes are copied to CONVDIR and specify the direction, location
; and length of the conveyor.
  DEFB 0                  ; Direction (left)
  DEFW 0                  ; Location in the attribute buffer at 24064 (unused)
  DEFB 0                  ; Length: 0 (there is no conveyor in this room)
; The next four bytes are copied to RAMPDIR and specify the direction, location
; and length of the ramp.
  DEFB 1                  ; Direction (up to the right)
  DEFW 24420              ; Location in the attribute buffer at 24064: (11,4)
  DEFB 8                  ; Length
; The next byte is copied to BORDER and specifies the border colour.
  DEFB 2                  ; Border colour
; The next two bytes are copied to XROOM223, but are not used.
  DEFB 0,0                ; Unused
; The next eight bytes are copied to ITEM and define the item graphic.
  DEFB 5,56,69,130,130,130,68,56 ; Item graphic
; The next four bytes are copied to LEFT and specify the rooms to the left, to
; the right, above and below.
  DEFB 31                 ; Room to the left (Swimming Pool)
  DEFB 29                 ; Room to the right (The Nightmare Room)
  DEFB 36                 ; Room above (A bit of tree)
  DEFB 24                 ; Room below (West of Kitchen)
; The next three bytes are copied to XROOM237, but are not used.
  DEFB 0,0,0              ; Unused
; The next eight pairs of bytes are copied to ENTITIES and specify the entities
; (ropes, arrows, guardians) in this room.
  DEFB 100,138            ; Guardian no. 100 (vertical), base sprite 4, x=10
                          ; (ENTITY100)
  DEFB 101,141            ; Guardian no. 101 (vertical), base sprite 4, x=13
                          ; (ENTITY101)
  DEFB 102,16             ; Guardian no. 102 (vertical), base sprite 0, x=16
                          ; (ENTITY102)
  DEFB 88,22              ; Guardian no. 88 (horizontal), base sprite 0,
                          ; initial x=22 (ENTITY88)
  DEFB 255,0              ; Terminator (ENTITY127)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)

; Room 31: Swimming Pool (teleport: 123459)
;
; Used by the routine at INITROOM.
;
; The first 128 bytes are copied to ROOMLAYOUT and define the room layout. Each
; bit-pair (bits 7 and 6, 5 and 4, 3 and 2, or 1 and 0 of each byte) determines
; the type of tile (background, floor, wall or nasty) that will be drawn at the
; corresponding location.
  DEFB 0,0,0,0,0,0,0,0                 ; Room layout
  DEFB 0,0,0,0,0,0,0,0                 ;
  DEFB 0,0,0,0,0,0,0,0                 ;
  DEFB 0,0,0,0,0,0,0,0                 ;
  DEFB 0,0,0,0,0,0,0,0                 ;
  DEFB 0,0,0,0,0,0,0,0                 ;
  DEFB 0,0,0,0,0,0,0,0                 ;
  DEFB 0,0,0,0,0,0,0,0                 ;
  DEFB 0,0,0,0,0,0,0,0                 ;
  DEFB 0,0,0,0,0,0,0,0                 ;
  DEFB 0,0,0,0,0,0,0,0                 ;
  DEFB 0,0,0,0,0,0,0,0                 ;
  DEFB 170,85,85,85,85,85,84,170       ;
  DEFB 170,85,85,85,85,85,82,170       ;
  DEFB 170,85,85,85,85,85,74,170       ;
  DEFB 170,170,170,170,170,170,170,170 ;
; The next 32 bytes are copied to ROOMNAME and specify the room name.
  DEFM "          Swimming Pool         " ; Room name
; The next 54 bytes are copied to BACKGROUND and contain the attributes and
; graphic data for the tiles used to build the room.
  DEFB 7,0,0,0,0,0,0,0,0  ; Background
  DEFB 41,0,0,0,0,0,0,0,0 ; Floor
  DEFB 58,68,68,187,68,68,68,187,68 ; Wall
  DEFB 255,0,0,0,0,0,0,0,0 ; Nasty (unused)
  DEFB 47,3,3,15,15,63,63,255,255 ; Ramp
  DEFB 255,0,0,0,0,0,0,0,0 ; Conveyor (unused)
; The next four bytes are copied to CONVDIR and specify the direction, location
; and length of the conveyor.
  DEFB 0                  ; Direction (left)
  DEFW 0                  ; Location in the attribute buffer at 24064 (unused)
  DEFB 0                  ; Length: 0 (there is no conveyor in this room)
; The next four bytes are copied to RAMPDIR and specify the direction, location
; and length of the ramp.
  DEFB 1                  ; Direction (up to the right)
  DEFW 24537              ; Location in the attribute buffer at 24064: (14,25)
  DEFB 3                  ; Length
; The next byte is copied to BORDER and specifies the border colour.
  DEFB 2                  ; Border colour
; The next two bytes are copied to XROOM223, but are not used.
  DEFB 0,0                ; Unused
; The next eight bytes are copied to ITEM and define the item graphic.
  DEFB 112,32,32,80,80,136,136,136 ; Item graphic
; The next four bytes are copied to LEFT and specify the rooms to the left, to
; the right, above and below.
  DEFB 54                 ; Room to the left (West  Wing)
  DEFB 30                 ; Room to the right (The Banyan Tree)
  DEFB 37                 ; Room above (Orangery)
  DEFB 0                  ; Room below (The Off Licence)
; The next three bytes are copied to XROOM237, but are not used.
  DEFB 0,0,0              ; Unused
; The next eight pairs of bytes are copied to ENTITIES and specify the entities
; (ropes, arrows, guardians) in this room.
  DEFB 78,12              ; Guardian no. 78 (horizontal), base sprite 0,
                          ; initial x=12 (ENTITY78)
  DEFB 1,16               ; Rope at x=16 (ENTITY1)
  DEFB 255,0              ; Terminator (ENTITY127)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)

; Room 32: Halfway up the East Wall (teleport: 69)
;
; Used by the routine at INITROOM.
;
; The first 128 bytes are copied to ROOMLAYOUT and define the room layout. Each
; bit-pair (bits 7 and 6, 5 and 4, 3 and 2, or 1 and 0 of each byte) determines
; the type of tile (background, floor, wall or nasty) that will be drawn at the
; corresponding location.
  DEFB 160,0,10,0,0,0,0,0   ; Room layout
  DEFB 160,0,10,0,0,0,0,0   ;
  DEFB 160,0,10,0,0,0,0,0   ;
  DEFB 160,8,0,0,0,0,0,0    ;
  DEFB 128,10,0,0,0,0,0,0   ;
  DEFB 128,26,128,0,0,0,0,0 ;
  DEFB 160,0,32,0,0,0,0,0   ;
  DEFB 160,0,40,0,0,0,0,0   ;
  DEFB 160,74,170,0,0,0,0,0 ;
  DEFB 160,10,170,0,0,0,0,0 ;
  DEFB 160,16,10,0,0,0,0,0  ;
  DEFB 164,0,10,0,0,0,0,0   ;
  DEFB 160,0,10,0,0,0,0,0   ;
  DEFB 160,4,10,0,0,0,0,0   ;
  DEFB 160,0,10,0,0,0,0,0   ;
  DEFB 160,0,26,0,0,0,0,0   ;
; The next 32 bytes are copied to ROOMNAME and specify the room name.
  DEFM "Halfway up the East Wall        " ; Room name
; The next 54 bytes are copied to BACKGROUND and contain the attributes and
; graphic data for the tiles used to build the room.
  DEFB 0,0,0,0,0,0,0,0,0  ; Background
  DEFB 5,255,255,90,153,189,90,60,90 ; Floor
  DEFB 15,51,17,68,204,51,17,68,204 ; Wall
  DEFB 255,0,0,0,0,0,0,0,0 ; Nasty (unused)
  DEFB 7,128,192,224,112,184,28,78,199 ; Ramp
  DEFB 7,0,0,0,0,0,0,0,0  ; Conveyor (unused)
; The next four bytes are copied to CONVDIR and specify the direction, location
; and length of the conveyor.
  DEFB 1                  ; Direction (right)
  DEFW 0                  ; Location in the attribute buffer at 24064 (unused)
  DEFB 0                  ; Length: 0 (there is no conveyor in this room)
; The next four bytes are copied to RAMPDIR and specify the direction, location
; and length of the ramp.
  DEFB 0                  ; Direction (up to the left)
  DEFW 24299              ; Location in the attribute buffer at 24064: (7,11)
  DEFB 6                  ; Length
; The next byte is copied to BORDER and specifies the border colour.
  DEFB 2                  ; Border colour
; The next two bytes are copied to XROOM223, but are not used.
  DEFB 0,0                ; Unused
; The next eight bytes are copied to ITEM and define the item graphic.
  DEFB 0,0,0,0,0,0,0,0    ; Item graphic (unused)
; The next four bytes are copied to LEFT and specify the rooms to the left, to
; the right, above and below.
  DEFB 33                 ; Room to the left (The Bathroom)
  DEFB 0                  ; Room to the right (The Off Licence)
  DEFB 38                 ; Room above (Priests' Hole)
  DEFB 26                 ; Room below (East Wall Base)
; The next three bytes are copied to XROOM237, but are not used.
  DEFB 0,0,0              ; Unused
; The next eight pairs of bytes are copied to ENTITIES and specify the entities
; (ropes, arrows, guardians) in this room.
  DEFB 38,4               ; Guardian no. 38 (horizontal), base sprite 0,
                          ; initial x=4 (ENTITY38)
  DEFB 255,0              ; Terminator (ENTITY127)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)

; Room 33: The Bathroom (teleport: 169)
;
; Used by the routine at INITROOM.
;
; The first 128 bytes are copied to ROOMLAYOUT and define the room layout. Each
; bit-pair (bits 7 and 6, 5 and 4, 3 and 2, or 1 and 0 of each byte) determines
; the type of tile (background, floor, wall or nasty) that will be drawn at the
; corresponding location.
  DEFB 160,0,0,0,0,0,0,10      ; Room layout
  DEFB 160,0,0,0,0,0,0,10      ;
  DEFB 160,0,0,0,0,0,0,10      ;
  DEFB 0,0,0,0,0,0,0,10        ;
  DEFB 0,0,0,0,0,0,0,10        ;
  DEFB 85,85,85,80,37,85,85,90 ;
  DEFB 128,0,0,0,32,0,0,10     ;
  DEFB 128,0,0,0,32,0,0,10     ;
  DEFB 128,0,0,0,32,0,0,10     ;
  DEFB 128,0,0,0,32,0,0,10     ;
  DEFB 128,0,0,0,32,0,0,10     ;
  DEFB 128,0,0,0,32,0,0,10     ;
  DEFB 128,0,0,0,32,0,0,10     ;
  DEFB 0,5,64,0,0,0,0,10       ;
  DEFB 0,0,0,0,0,0,0,10        ;
  DEFB 85,85,85,85,85,85,85,90 ;
; The next 32 bytes are copied to ROOMNAME and specify the room name.
  DEFM "          The Bathroom          " ; Room name
; The next 54 bytes are copied to BACKGROUND and contain the attributes and
; graphic data for the tiles used to build the room.
  DEFB 0,0,0,0,0,0,0,0,0  ; Background
  DEFB 22,0,64,9,100,146,45,150,255 ; Floor
  DEFB 14,31,170,0,85,248,85,0,170 ; Wall
  DEFB 255,0,0,0,0,0,0,0,0 ; Nasty (unused)
  DEFB 7,3,0,12,0,48,0,192,0 ; Ramp
  DEFB 61,165,0,0,0,0,0,0,255 ; Conveyor
; The next four bytes are copied to CONVDIR and specify the direction, location
; and length of the conveyor.
  DEFB 0                  ; Direction (left)
  DEFW 24532              ; Location in the attribute buffer at 24064: (14,20)
  DEFB 4                  ; Length
; The next four bytes are copied to RAMPDIR and specify the direction, location
; and length of the ramp.
  DEFB 1                  ; Direction (up to the right)
  DEFW 24457              ; Location in the attribute buffer at 24064: (12,9)
  DEFB 8                  ; Length
; The next byte is copied to BORDER and specifies the border colour.
  DEFB 2                  ; Border colour
; The next two bytes are copied to XROOM223, but are not used.
  DEFB 0,0                ; Unused
; The next eight bytes are copied to ITEM and define the item graphic.
  DEFB 31,4,10,119,135,177,170,10 ; Item graphic
; The next four bytes are copied to LEFT and specify the rooms to the left, to
; the right, above and below.
  DEFB 34                 ; Room to the left (Top Landing)
  DEFB 32                 ; Room to the right (Halfway up the East Wall)
  DEFB 39                 ; Room above (Emergency Generator)
  DEFB 27                 ; Room below (The Chapel)
; The next three bytes are copied to XROOM237, but are not used.
  DEFB 0,0,0              ; Unused
; The next eight pairs of bytes are copied to ENTITIES and specify the entities
; (ropes, arrows, guardians) in this room.
  DEFB 59,16              ; Guardian no. 59 (horizontal), base sprite 0,
                          ; initial x=16 (ENTITY59)
  DEFB 255,0              ; Terminator (ENTITY127)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)

; Room 34: Top Landing (teleport: 269)
;
; Used by the routine at INITROOM.
;
; The first 128 bytes are copied to ROOMLAYOUT and define the room layout. Each
; bit-pair (bits 7 and 6, 5 and 4, 3 and 2, or 1 and 0 of each byte) determines
; the type of tile (background, floor, wall or nasty) that will be drawn at the
; corresponding location.
  DEFB 170,170,170,170,170,170,170,170 ; Room layout
  DEFB 170,170,170,170,170,170,170,170 ;
  DEFB 160,0,0,0,0,0,0,10              ;
  DEFB 160,0,0,0,0,0,0,0               ;
  DEFB 165,80,85,5,85,85,0,0           ;
  DEFB 160,0,0,0,0,0,170,170           ;
  DEFB 160,0,0,0,0,0,0,10              ;
  DEFB 160,0,0,0,0,0,0,10              ;
  DEFB 165,80,85,5,85,0,0,10           ;
  DEFB 160,0,0,0,0,0,0,10              ;
  DEFB 160,0,0,0,0,0,0,10              ;
  DEFB 160,0,0,0,0,0,0,10              ;
  DEFB 165,80,85,5,64,0,0,10           ;
  DEFB 0,0,0,0,0,0,0,0                 ;
  DEFB 0,0,0,0,0,0,0,0                 ;
  DEFB 85,85,85,85,85,5,85,85          ;
; The next 32 bytes are copied to ROOMNAME and specify the room name.
  DEFM "          Top Landing           " ; Room name
; The next 54 bytes are copied to BACKGROUND and contain the attributes and
; graphic data for the tiles used to build the room.
  DEFB 0,0,0,0,0,0,0,0,0  ; Background
  DEFB 6,255,170,85,255,0,0,0,0 ; Floor
  DEFB 26,105,144,144,105,150,105,105,150 ; Wall
  DEFB 255,0,0,0,0,0,0,0,0 ; Nasty (unused)
  DEFB 7,7,2,60,8,112,32,192,128 ; Ramp
  DEFB 255,0,0,0,0,0,0,0,0 ; Conveyor (unused)
; The next four bytes are copied to CONVDIR and specify the direction, location
; and length of the conveyor.
  DEFB 0                  ; Direction (left)
  DEFW 0                  ; Location in the attribute buffer at 24064 (unused)
  DEFB 0                  ; Length: 0 (there is no conveyor in this room)
; The next four bytes are copied to RAMPDIR and specify the direction, location
; and length of the ramp.
  DEFB 1                  ; Direction (up to the right)
  DEFW 24433              ; Location in the attribute buffer at 24064: (11,17)
  DEFB 7                  ; Length
; The next byte is copied to BORDER and specifies the border colour.
  DEFB 1                  ; Border colour
; The next two bytes are copied to XROOM223, but are not used.
  DEFB 0,0                ; Unused
; The next eight bytes are copied to ITEM and define the item graphic.
  DEFB 24,36,164,202,149,10,21,10 ; Item graphic
; The next four bytes are copied to LEFT and specify the rooms to the left, to
; the right, above and below.
  DEFB 35                 ; Room to the left (Master Bedroom)
  DEFB 33                 ; Room to the right (The Bathroom)
  DEFB 40                 ; Room above (Dr Jones will never believe this)
  DEFB 28                 ; Room below (First Landing)
; The next three bytes are copied to XROOM237, but are not used.
  DEFB 0,0,0              ; Unused
; The next eight pairs of bytes are copied to ENTITIES and specify the entities
; (ropes, arrows, guardians) in this room.
  DEFB 58,134             ; Guardian no. 58 (vertical), base sprite 4, x=6
                          ; (ENTITY58)
  DEFB 19,108             ; Guardian no. 19 (vertical), base sprite 3, x=12
                          ; (ENTITY19)
  DEFB 36,16              ; Guardian no. 36 (horizontal), base sprite 0,
                          ; initial x=16 (ENTITY36)
  DEFB 255,0              ; Terminator (ENTITY127)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)

; Room 35: Master Bedroom (teleport: 1269)
;
; Used by the routine at INITROOM.
;
; The first 128 bytes are copied to ROOMLAYOUT and define the room layout. Each
; bit-pair (bits 7 and 6, 5 and 4, 3 and 2, or 1 and 0 of each byte) determines
; the type of tile (background, floor, wall or nasty) that will be drawn at the
; corresponding location.
  DEFB 170,170,170,170,170,170,170,170 ; Room layout
  DEFB 160,0,0,10,0,0,0,0              ;
  DEFB 160,0,0,10,0,0,0,0              ;
  DEFB 160,0,0,10,0,0,0,0              ;
  DEFB 160,0,0,10,0,0,0,0              ;
  DEFB 160,0,0,10,0,0,0,0              ;
  DEFB 160,0,0,10,0,0,0,0              ;
  DEFB 160,0,0,10,0,0,0,0              ;
  DEFB 160,0,0,10,0,0,0,0              ;
  DEFB 160,0,0,10,0,0,0,0              ;
  DEFB 160,0,0,10,0,0,0,0              ;
  DEFB 172,0,0,0,0,0,0,0               ;
  DEFB 160,0,0,0,0,0,0,0               ;
  DEFB 149,85,85,85,64,0,0,0           ;
  DEFB 170,170,170,170,160,0,0,0       ;
  DEFB 170,170,170,170,169,85,85,85    ;
; The next 32 bytes are copied to ROOMNAME and specify the room name.
  DEFM "         Master Bedroom         " ; Room name
; The next 54 bytes are copied to BACKGROUND and contain the attributes and
; graphic data for the tiles used to build the room.
  DEFB 0,0,0,0,0,0,0,0,0  ; Background
  DEFB 6,255,107,181,0,173,0,74,0 ; Floor
  DEFB 51,0,81,251,81,0,170,191,170 ; Wall
  DEFB 71,0,0,0,0,124,254,127,62 ; Nasty
  DEFB 7,192,64,176,0,172,0,83,0 ; Ramp
  DEFB 41,85,85,85,85,81,95,64,170 ; Conveyor
; The next four bytes are copied to CONVDIR and specify the direction, location
; and length of the conveyor.
  DEFB 1                  ; Direction (right)
  DEFW 24450              ; Location in the attribute buffer at 24064: (12,2)
  DEFB 4                  ; Length
; The next four bytes are copied to RAMPDIR and specify the direction, location
; and length of the ramp.
  DEFB 0                  ; Direction (up to the left)
  DEFW 24530              ; Location in the attribute buffer at 24064: (14,18)
  DEFB 2                  ; Length
; The next byte is copied to BORDER and specifies the border colour.
  DEFB 1                  ; Border colour
; The next two bytes are copied to XROOM223, but are not used.
  DEFB 0,0                ; Unused
; The next eight bytes are copied to ITEM and define the item graphic.
  DEFB 0,0,0,0,0,0,0,0    ; Item graphic (unused)
; The next four bytes are copied to LEFT and specify the rooms to the left, to
; the right, above and below.
  DEFB 36                 ; Room to the left (A bit of tree)
  DEFB 34                 ; Room to the right (Top Landing)
  DEFB 41                 ; Room above (The Attic)
  DEFB 29                 ; Room below (The Nightmare Room)
; The next three bytes are copied to XROOM237, but are not used.
  DEFB 0,0,0              ; Unused
; The next eight pairs of bytes are copied to ENTITIES and specify the entities
; (ropes, arrows, guardians) in this room.
  DEFB 255,0              ; Terminator (ENTITY127)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)

; Room 36: A bit of tree (teleport: 369)
;
; Used by the routine at INITROOM.
;
; The first 128 bytes are copied to ROOMLAYOUT and define the room layout. Each
; bit-pair (bits 7 and 6, 5 and 4, 3 and 2, or 1 and 0 of each byte) determines
; the type of tile (background, floor, wall or nasty) that will be drawn at the
; corresponding location.
  DEFB 0,0,0,160,0,0,0,3    ; Room layout
  DEFB 0,0,0,160,0,0,0,3    ;
  DEFB 0,0,0,160,0,0,0,3    ;
  DEFB 0,0,0,160,0,0,0,3    ;
  DEFB 0,0,16,160,0,64,0,3  ;
  DEFB 0,0,0,160,0,0,0,3    ;
  DEFB 0,20,0,160,0,1,64,3  ;
  DEFB 0,0,16,160,0,0,0,3   ;
  DEFB 0,0,0,160,0,0,0,3    ;
  DEFB 0,0,20,160,0,0,64,3  ;
  DEFB 85,80,0,165,64,0,0,3 ;
  DEFB 0,0,0,160,0,0,1,3    ;
  DEFB 0,0,0,160,0,0,64,3   ;
  DEFB 0,0,0,160,5,0,0,3    ;
  DEFB 0,0,0,160,0,0,0,3    ;
  DEFB 0,0,1,164,0,0,0,3    ;
; The next 32 bytes are copied to ROOMNAME and specify the room name.
  DEFM "    A bit of tree               " ; Room name
; The next 54 bytes are copied to BACKGROUND and contain the attributes and
; graphic data for the tiles used to build the room. Note that because of a bug
; in the game engine, the nasty tile is not drawn correctly (see the room image
; above).
  DEFB 0,0,0,0,0,0,0,0,0  ; Background
  DEFB 4,173,82,41,0,1,2,0,0 ; Floor
  DEFB 30,214,107,102,219,105,107,217,109 ; Wall
  DEFB 2,21,0,170,85,170,0,21,10 ; Nasty
  DEFB 68,3,8,36,16,64,32,128,128 ; Ramp
  DEFB 255,0,0,0,0,0,0,0,0 ; Conveyor (unused)
; The next four bytes are copied to CONVDIR and specify the direction, location
; and length of the conveyor.
  DEFB 0                  ; Direction (left)
  DEFW 0                  ; Location in the attribute buffer at 24064 (unused)
  DEFB 0                  ; Length: 0 (there is no conveyor in this room)
; The next four bytes are copied to RAMPDIR and specify the direction, location
; and length of the ramp.
  DEFB 1                  ; Direction (up to the right)
  DEFW 24372              ; Location in the attribute buffer at 24064: (9,20)
  DEFB 2                  ; Length
; The next byte is copied to BORDER and specifies the border colour.
  DEFB 5                  ; Border colour
; The next two bytes are copied to XROOM223, but are not used.
  DEFB 0,0                ; Unused
; The next eight bytes are copied to ITEM and define the item graphic.
  DEFB 0,0,0,0,0,0,0,0    ; Item graphic (unused)
; The next four bytes are copied to LEFT and specify the rooms to the left, to
; the right, above and below.
  DEFB 37                 ; Room to the left (Orangery)
  DEFB 35                 ; Room to the right (Master Bedroom)
  DEFB 42                 ; Room above (Under the Roof)
  DEFB 30                 ; Room below (The Banyan Tree)
; The next three bytes are copied to XROOM237, but are not used.
  DEFB 0,0,0              ; Unused
; The next eight pairs of bytes are copied to ENTITIES and specify the entities
; (ropes, arrows, guardians) in this room.
  DEFB 104,2              ; Guardian no. 104 (horizontal), base sprite 0,
                          ; initial x=2 (ENTITY104)
  DEFB 101,7              ; Guardian no. 101 (vertical), base sprite 0, x=7
                          ; (ENTITY101)
  DEFB 69,146             ; Arrow flying right to left at pixel y-coordinate 73
                          ; (ENTITY69)
  DEFB 255,0              ; Terminator (ENTITY127)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)

; Room 37: Orangery (teleport: 1369)
;
; Used by the routine at INITROOM.
;
; The first 128 bytes are copied to ROOMLAYOUT and define the room layout. Each
; bit-pair (bits 7 and 6, 5 and 4, 3 and 2, or 1 and 0 of each byte) determines
; the type of tile (background, floor, wall or nasty) that will be drawn at the
; corresponding location.
  DEFB 0,0,0,0,0,0,0,0      ; Room layout
  DEFB 0,0,0,0,0,0,0,0      ;
  DEFB 0,0,0,0,0,1,0,0      ;
  DEFB 0,0,0,0,0,0,0,0      ;
  DEFB 0,0,0,0,0,0,0,0      ;
  DEFB 0,0,0,0,64,0,16,0    ;
  DEFB 0,0,0,0,0,4,0,64     ;
  DEFB 0,0,0,0,0,0,0,0      ;
  DEFB 160,0,0,4,0,0,0,0    ;
  DEFB 0,0,0,0,1,0,0,0      ;
  DEFB 0,32,0,64,0,0,5,0    ;
  DEFB 0,37,0,0,64,0,0,85   ;
  DEFB 0,32,0,0,0,0,16,0    ;
  DEFB 0,32,1,0,0,0,0,0     ;
  DEFB 0,32,0,0,0,64,0,0    ;
  DEFB 170,170,0,1,64,0,0,0 ;
; The next 32 bytes are copied to ROOMNAME and specify the room name.
  DEFM "            Orangery            " ; Room name
; The next 54 bytes are copied to BACKGROUND and contain the attributes and
; graphic data for the tiles used to build the room.
  DEFB 0,0,0,0,0,0,0,0,0  ; Background
  DEFB 4,165,74,36,2,4,0,2,0 ; Floor
  DEFB 22,21,85,85,36,37,85,73,34 ; Wall
  DEFB 6,130,65,62,106,183,25,37,68 ; Nasty (unused)
  DEFB 5,1,2,4,0,16,32,64,128 ; Ramp
  DEFB 38,198,170,168,42,89,36,0,0 ; Conveyor
; The next four bytes are copied to CONVDIR and specify the direction, location
; and length of the conveyor.
  DEFB 0                  ; Direction (left)
  DEFW 24412              ; Location in the attribute buffer at 24064: (10,28)
  DEFB 4                  ; Length
; The next four bytes are copied to RAMPDIR and specify the direction, location
; and length of the ramp.
  DEFB 1                  ; Direction (up to the right)
  DEFW 24480              ; Location in the attribute buffer at 24064: (13,0)
  DEFB 14                 ; Length
; The next byte is copied to BORDER and specifies the border colour.
  DEFB 6                  ; Border colour
; The next two bytes are copied to XROOM223, but are not used.
  DEFB 0,0                ; Unused
; The next eight bytes are copied to ITEM and define the item graphic.
  DEFB 2,20,46,85,42,69,42,20 ; Item graphic
; The next four bytes are copied to LEFT and specify the rooms to the left, to
; the right, above and below.
  DEFB 56                 ; Room to the left (West Wing Roof)
  DEFB 36                 ; Room to the right (A bit of tree)
  DEFB 43                 ; Room above (Conservatory Roof)
  DEFB 31                 ; Room below (Swimming Pool)
; The next three bytes are copied to XROOM237, but are not used.
  DEFB 0,0,0              ; Unused
; The next eight pairs of bytes are copied to ENTITIES and specify the entities
; (ropes, arrows, guardians) in this room.
  DEFB 97,148             ; Guardian no. 97 (vertical), base sprite 4, x=20
                          ; (ENTITY97)
  DEFB 27,137             ; Guardian no. 27 (vertical), base sprite 4, x=9
                          ; (ENTITY27)
  DEFB 60,132             ; Arrow flying left to right at pixel y-coordinate 66
                          ; (ENTITY60)
  DEFB 255,0              ; Terminator (ENTITY127)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)

; Room 38: Priests' Hole (teleport: 2369)
;
; Used by the routine at INITROOM.
;
; The first 128 bytes are copied to ROOMLAYOUT and define the room layout. Each
; bit-pair (bits 7 and 6, 5 and 4, 3 and 2, or 1 and 0 of each byte) determines
; the type of tile (background, floor, wall or nasty) that will be drawn at the
; corresponding location.
  DEFB 160,0,10,0,0,0,0,0  ; Room layout
  DEFB 160,0,10,0,0,0,0,0  ;
  DEFB 160,0,10,0,0,0,0,0  ;
  DEFB 0,0,10,0,0,0,0,0    ;
  DEFB 0,0,10,0,0,0,0,0    ;
  DEFB 165,0,90,0,0,0,0,0  ;
  DEFB 160,0,10,0,0,0,0,0  ;
  DEFB 161,0,10,0,0,0,0,0  ;
  DEFB 160,0,74,0,0,0,0,0  ;
  DEFB 160,0,10,0,0,0,0,0  ;
  DEFB 161,0,0,0,0,0,0,0   ;
  DEFB 160,0,0,0,0,0,0,0   ;
  DEFB 160,0,90,0,0,0,0,0  ;
  DEFB 160,0,10,0,0,0,0,0  ;
  DEFB 160,64,10,0,0,0,0,0 ;
  DEFB 160,1,10,0,0,0,0,0  ;
; The next 32 bytes are copied to ROOMNAME and specify the room name.
  DEFM "         Priests' Hole          " ; Room name
; The next 54 bytes are copied to BACKGROUND and contain the attributes and
; graphic data for the tiles used to build the room.
  DEFB 0,0,0,0,0,0,0,0,0  ; Background
  DEFB 6,85,85,93,215,112,0,0,0 ; Floor
  DEFB 42,34,17,68,136,34,17,68,136 ; Wall
  DEFB 255,0,0,0,0,0,0,0,0 ; Nasty (unused)
  DEFB 255,0,0,0,0,0,0,0,0 ; Ramp (unused)
  DEFB 255,0,0,0,0,0,0,0,0 ; Conveyor (unused)
; The next four bytes are copied to CONVDIR and specify the direction, location
; and length of the conveyor.
  DEFB 0                  ; Direction (left)
  DEFW 0                  ; Location in the attribute buffer at 24064 (unused)
  DEFB 0                  ; Length: 0 (there is no conveyor in this room)
; The next four bytes are copied to RAMPDIR and specify the direction, location
; and length of the ramp.
  DEFB 0                  ; Direction (up to the left)
  DEFW 0                  ; Location in the attribute buffer at 24064 (unused)
  DEFB 0                  ; Length: 0 (there is no ramp in this room)
; The next byte is copied to BORDER and specifies the border colour.
  DEFB 2                  ; Border colour
; The next two bytes are copied to XROOM223, but are not used.
  DEFB 0,0                ; Unused
; The next eight bytes are copied to ITEM and define the item graphic.
  DEFB 191,191,187,177,187,187,191,191 ; Item graphic
; The next four bytes are copied to LEFT and specify the rooms to the left, to
; the right, above and below.
  DEFB 39                 ; Room to the left (Emergency Generator)
  DEFB 0                  ; Room to the right (The Off Licence)
  DEFB 60                 ; Room above (The Bow)
  DEFB 32                 ; Room below (Halfway up the East Wall)
; The next three bytes are copied to XROOM237, but are not used.
  DEFB 0,0,0              ; Unused
; The next eight pairs of bytes are copied to ENTITIES and specify the entities
; (ropes, arrows, guardians) in this room.
  DEFB 16,132             ; Guardian no. 16 (vertical), base sprite 4, x=4
                          ; (ENTITY16)
  DEFB 17,166             ; Guardian no. 17 (vertical), base sprite 5, x=6
                          ; (ENTITY17)
  DEFB 18,197             ; Guardian no. 18 (vertical), base sprite 6, x=5
                          ; (ENTITY18)
  DEFB 37,22              ; Guardian no. 37 (horizontal), base sprite 0,
                          ; initial x=22 (ENTITY37)
  DEFB 255,0              ; Terminator (ENTITY127)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)

; Room 39: Emergency Generator (teleport: 12369)
;
; Used by the routine at INITROOM.
;
; The first 128 bytes are copied to ROOMLAYOUT and define the room layout. Each
; bit-pair (bits 7 and 6, 5 and 4, 3 and 2, or 1 and 0 of each byte) determines
; the type of tile (background, floor, wall or nasty) that will be drawn at the
; corresponding location.
  DEFB 0,0,0,0,0,0,0,10                ; Room layout
  DEFB 0,0,0,0,0,0,0,10                ;
  DEFB 0,0,0,0,0,0,0,10                ;
  DEFB 0,0,0,0,0,0,0,0                 ;
  DEFB 0,0,0,0,0,0,0,0                 ;
  DEFB 0,5,0,0,0,0,80,10               ;
  DEFB 0,5,0,0,0,0,80,3                ;
  DEFB 0,5,0,0,0,0,80,3                ;
  DEFB 0,5,0,0,0,0,80,3                ;
  DEFB 0,5,0,0,0,0,80,3                ;
  DEFB 0,42,128,0,0,2,168,3            ;
  DEFB 0,40,128,0,0,2,40,3             ;
  DEFB 0,170,170,170,170,170,170,3     ;
  DEFB 0,170,42,138,162,168,170,3      ;
  DEFB 0,170,170,170,170,170,170,3     ;
  DEFB 170,170,170,170,170,170,170,170 ;
; The next 32 bytes are copied to ROOMNAME and specify the room name.
  DEFM "       Emergency Generator      " ; Room name
; The next 54 bytes are copied to BACKGROUND and contain the attributes and
; graphic data for the tiles used to build the room.
  DEFB 8,0,0,0,0,0,0,0,0  ; Background
  DEFB 47,102,102,102,102,102,102,102,102 ; Floor
  DEFB 51,191,21,0,81,251,81,0,21 ; Wall
  DEFB 13,255,63,3,31,127,31,3,63 ; Nasty
  DEFB 15,3,0,12,0,48,0,192,0 ; Ramp
  DEFB 22,170,84,170,84,40,16,40,16 ; Conveyor
; The next four bytes are copied to CONVDIR and specify the direction, location
; and length of the conveyor.
  DEFB 0                  ; Direction (left)
  DEFW 24425              ; Location in the attribute buffer at 24064: (11,9)
  DEFB 14                 ; Length
; The next four bytes are copied to RAMPDIR and specify the direction, location
; and length of the ramp.
  DEFB 1                  ; Direction (up to the right)
  DEFW 24402              ; Location in the attribute buffer at 24064: (10,18)
  DEFB 6                  ; Length
; The next byte is copied to BORDER and specifies the border colour.
  DEFB 2                  ; Border colour
; The next two bytes are copied to XROOM223, but are not used.
  DEFB 0,0                ; Unused
; The next eight bytes are copied to ITEM and define the item graphic.
  DEFB 0,0,0,0,0,0,0,0    ; Item graphic (unused)
; The next four bytes are copied to LEFT and specify the rooms to the left, to
; the right, above and below.
  DEFB 40                 ; Room to the left (Dr Jones will never believe this)
  DEFB 38                 ; Room to the right (Priests' Hole)
  DEFB 14                 ; Room above (Rescue Esmerelda)
  DEFB 0                  ; Room below (The Off Licence)
; The next three bytes are copied to XROOM237, but are not used.
  DEFB 0,0,0              ; Unused
; The next eight pairs of bytes are copied to ENTITIES and specify the entities
; (ropes, arrows, guardians) in this room.
  DEFB 26,8               ; Guardian no. 26 (horizontal), base sprite 0,
                          ; initial x=8 (ENTITY26)
  DEFB 255,0              ; Terminator (ENTITY127)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)

; Room 40: Dr Jones will never believe this (teleport: 469)
;
; Used by the routine at INITROOM.
;
; The first 128 bytes are copied to ROOMLAYOUT and define the room layout. Each
; bit-pair (bits 7 and 6, 5 and 4, 3 and 2, or 1 and 0 of each byte) determines
; the type of tile (background, floor, wall or nasty) that will be drawn at the
; corresponding location.
  DEFB 0,0,0,0,0,0,0,0        ; Room layout
  DEFB 0,0,0,0,0,0,0,0        ;
  DEFB 0,0,0,234,0,0,0,0      ;
  DEFB 0,0,0,170,160,0,0,0    ;
  DEFB 0,0,0,149,84,0,0,0     ;
  DEFB 0,14,168,213,21,0,0,0  ;
  DEFB 0,10,170,129,170,0,0,0 ;
  DEFB 0,5,80,144,168,32,0,0  ;
  DEFB 80,5,80,129,160,0,0,0  ;
  DEFB 0,10,80,144,160,0,0,0  ;
  DEFB 4,10,80,129,128,64,0,0 ;
  DEFB 0,18,80,144,128,0,0,0  ;
  DEFB 0,66,80,1,128,16,0,16  ;
  DEFB 0,2,0,1,128,0,64,0     ;
  DEFB 0,2,148,1,144,0,0,0    ;
  DEFB 0,0,0,0,0,0,0,0        ;
; The next 32 bytes are copied to ROOMNAME and specify the room name.
  DEFM "Dr Jones will never believe this" ; Room name
; The next 54 bytes are copied to BACKGROUND and contain the attributes and
; graphic data for the tiles used to build the room.
  DEFB 0,0,0,0,0,0,0,0,0  ; Background
  DEFB 2,254,85,42,170,85,170,170,255 ; Floor
  DEFB 31,17,68,34,136,17,68,34,136 ; Wall
  DEFB 67,1,4,34,8,17,68,34,136 ; Nasty
  DEFB 5,64,144,36,72,18,36,9,2 ; Ramp
  DEFB 38,165,0,165,0,170,255,255,85 ; Conveyor
; The next four bytes are copied to CONVDIR and specify the direction, location
; and length of the conveyor.
  DEFB 0                  ; Direction (left)
  DEFW 24544              ; Location in the attribute buffer at 24064: (15,0)
  DEFB 32                 ; Length
; The next four bytes are copied to RAMPDIR and specify the direction, location
; and length of the ramp.
  DEFB 0                  ; Direction (up to the left)
  DEFW 24408              ; Location in the attribute buffer at 24064: (10,24)
  DEFB 5                  ; Length
; The next byte is copied to BORDER and specifies the border colour.
  DEFB 1                  ; Border colour
; The next two bytes are copied to XROOM223, but are not used.
  DEFB 0,0                ; Unused
; The next eight bytes are copied to ITEM and define the item graphic.
  DEFB 240,240,120,184,76,50,13,3 ; Item graphic
; The next four bytes are copied to LEFT and specify the rooms to the left, to
; the right, above and below.
  DEFB 41                 ; Room to the left (The Attic)
  DEFB 39                 ; Room to the right (Emergency Generator)
  DEFB 16                 ; Room above (We must perform a Quirkafleeg)
  DEFB 0                  ; Room below (The Off Licence)
; The next three bytes are copied to XROOM237, but are not used.
  DEFB 0,0,0              ; Unused
; The next eight pairs of bytes are copied to ENTITIES and specify the entities
; (ropes, arrows, guardians) in this room.
  DEFB 27,146             ; Guardian no. 27 (vertical), base sprite 4, x=18
                          ; (ENTITY27)
  DEFB 11,10              ; Guardian no. 11 (vertical), base sprite 0, x=10
                          ; (ENTITY11)
  DEFB 255,0              ; Terminator (ENTITY127)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)

; Room 41: The Attic (teleport: 1469)
;
; Used by the routine at INITROOM.
;
; The first 128 bytes are copied to ROOMLAYOUT and define the room layout. Each
; bit-pair (bits 7 and 6, 5 and 4, 3 and 2, or 1 and 0 of each byte) determines
; the type of tile (background, floor, wall or nasty) that will be drawn at the
; corresponding location.
  DEFB 0,0,0,0,0,0,0,0             ; Room layout
  DEFB 0,0,0,0,0,0,0,0             ;
  DEFB 0,0,0,0,0,0,0,0             ;
  DEFB 0,0,0,0,0,0,0,0             ;
  DEFB 0,0,0,0,0,0,0,0             ;
  DEFB 0,192,3,0,0,0,0,0           ;
  DEFB 85,149,86,80,0,0,0,0        ;
  DEFB 160,0,0,0,0,0,0,0           ;
  DEFB 164,0,0,0,5,0,0,5           ;
  DEFB 160,0,0,0,0,0,0,0           ;
  DEFB 164,0,0,0,0,80,0,0          ;
  DEFB 160,0,0,0,0,0,0,0           ;
  DEFB 164,0,0,0,0,4,0,0           ;
  DEFB 160,0,0,0,0,0,0,0           ;
  DEFB 164,3,0,192,192,0,0,0       ;
  DEFB 85,101,149,150,85,149,86,89 ;
; The next 32 bytes are copied to ROOMNAME and specify the room name.
  DEFM "          The Attic             " ; Room name
; The next 54 bytes are copied to BACKGROUND and contain the attributes and
; graphic data for the tiles used to build the room.
  DEFB 0,0,0,0,0,0,0,0,0  ; Background
  DEFB 22,206,255,0,231,0,127,0,251 ; Floor
  DEFB 30,238,17,108,146,17,201,34,156 ; Wall
  DEFB 68,36,18,68,170,85,170,91,255 ; Nasty
  DEFB 255,0,0,0,0,0,0,0,0 ; Ramp (unused)
  DEFB 66,240,170,60,170,85,170,85,170 ; Conveyor
; The next four bytes are copied to CONVDIR and specify the direction, location
; and length of the conveyor.
  DEFB 1                  ; Direction (right)
  DEFW 24344              ; Location in the attribute buffer at 24064: (8,24)
  DEFB 4                  ; Length
; The next four bytes are copied to RAMPDIR and specify the direction, location
; and length of the ramp.
  DEFB 0                  ; Direction (up to the left)
  DEFW 0                  ; Location in the attribute buffer at 24064 (unused)
  DEFB 0                  ; Length: 0 (there is no ramp in this room)
; The next byte is copied to BORDER and specifies the border colour.
  DEFB 1                  ; Border colour
; The next two bytes are copied to XROOM223, but are not used.
  DEFB 0,0                ; Unused
; The next eight bytes are copied to ITEM and define the item graphic.
  DEFB 17,34,51,68,85,102,119,136 ; Item graphic (unused)
; The next four bytes are copied to LEFT and specify the rooms to the left, to
; the right, above and below.
  DEFB 42                 ; Room to the left (Under the Roof)
  DEFB 40                 ; Room to the right (Dr Jones will never believe
                          ; this)
  DEFB 0                  ; Room above (The Off Licence)
  DEFB 34                 ; Room below (Top Landing)
; The next three bytes are copied to XROOM237, but are not used.
  DEFB 0,0,0              ; Unused
; The next eight pairs of bytes are copied to ENTITIES and specify the entities
; (ropes, arrows, guardians) in this room.
  DEFB 82,4               ; Guardian no. 82 (vertical), base sprite 0, x=4
                          ; (ENTITY82)
  DEFB 83,6               ; Guardian no. 83 (vertical), base sprite 0, x=6
                          ; (ENTITY83)
  DEFB 84,8               ; Guardian no. 84 (vertical), base sprite 0, x=8
                          ; (ENTITY84)
  DEFB 85,10              ; Guardian no. 85 (vertical), base sprite 0, x=10
                          ; (ENTITY85)
  DEFB 86,12              ; Guardian no. 86 (vertical), base sprite 0, x=12
                          ; (ENTITY86)
  DEFB 87,78              ; Guardian no. 87 (vertical), base sprite 2, x=14
                          ; (ENTITY87)
  DEFB 69,213             ; Arrow flying right to left at pixel y-coordinate
                          ; 536 (ENTITY69)
  DEFB 60,146             ; Arrow flying left to right at pixel y-coordinate 73
                          ; (ENTITY60)

; Room 42: Under the Roof (teleport: 2469)
;
; Used by the routine at INITROOM.
;
; The first 128 bytes are copied to ROOMLAYOUT and define the room layout. Each
; bit-pair (bits 7 and 6, 5 and 4, 3 and 2, or 1 and 0 of each byte) determines
; the type of tile (background, floor, wall or nasty) that will be drawn at the
; corresponding location.
  DEFB 0,0,0,255,255,255,255,240 ; Room layout
  DEFB 0,0,0,0,195,252,48,0      ;
  DEFB 0,0,0,3,243,252,252,0     ;
  DEFB 0,0,0,0,0,240,0,0         ;
  DEFB 0,0,0,0,0,0,0,0           ;
  DEFB 0,0,0,0,0,0,0,0           ;
  DEFB 0,0,0,0,0,0,0,0           ;
  DEFB 170,85,0,0,0,0,0,0        ;
  DEFB 0,0,0,0,0,0,0,0           ;
  DEFB 0,0,0,0,0,0,0,0           ;
  DEFB 0,0,0,0,0,0,0,0           ;
  DEFB 0,0,0,160,0,1,64,0        ;
  DEFB 0,0,80,160,1,0,0,0        ;
  DEFB 85,80,0,160,64,0,0,0      ;
  DEFB 84,0,1,160,0,0,0,0        ;
  DEFB 0,0,0,160,0,64,0,0        ;
; The next 32 bytes are copied to ROOMNAME and specify the room name.
  DEFM "         Under the Roof         " ; Room name
; The next 54 bytes are copied to BACKGROUND and contain the attributes and
; graphic data for the tiles used to build the room.
  DEFB 8,0,0,0,0,0,0,0,0  ; Background
  DEFB 12,181,170,85,74,37,66,1,2 ; Floor
  DEFB 30,102,182,109,187,102,185,109,182 ; Wall
  DEFB 245,102,170,95,188,99,172,107,164 ; Nasty
  DEFB 15,1,2,4,40,16,40,64,128 ; Ramp
  DEFB 14,165,170,189,102,102,102,102,102 ; Conveyor
; The next four bytes are copied to CONVDIR and specify the direction, location
; and length of the conveyor.
  DEFB 1                  ; Direction (right)
  DEFW 24268              ; Location in the attribute buffer at 24064: (6,12)
  DEFB 20                 ; Length
; The next four bytes are copied to RAMPDIR and specify the direction, location
; and length of the ramp.
  DEFB 1                  ; Direction (up to the right)
  DEFW 24260              ; Location in the attribute buffer at 24064: (6,4)
  DEFB 7                  ; Length
; The next byte is copied to BORDER and specifies the border colour.
  DEFB 2                  ; Border colour
; The next two bytes are copied to XROOM223, but are not used.
  DEFB 0,0                ; Unused
; The next eight bytes are copied to ITEM and define the item graphic.
  DEFB 255,255,255,255,255,255,255,255 ; Item graphic (unused)
; The next four bytes are copied to LEFT and specify the rooms to the left, to
; the right, above and below.
  DEFB 43                 ; Room to the left (Conservatory Roof)
  DEFB 41                 ; Room to the right (The Attic)
  DEFB 48                 ; Room above (Nomen Luni)
  DEFB 36                 ; Room below (A bit of tree)
; The next three bytes are copied to XROOM237, but are not used.
  DEFB 0,0,0              ; Unused
; The next eight pairs of bytes are copied to ENTITIES and specify the entities
; (ropes, arrows, guardians) in this room.
  DEFB 55,4               ; Guardian no. 55 (horizontal), base sprite 0,
                          ; initial x=4 (ENTITY55)
  DEFB 39,16              ; Guardian no. 39 (horizontal), base sprite 0,
                          ; initial x=16 (ENTITY39)
  DEFB 60,194             ; Arrow flying left to right at pixel y-coordinate 97
                          ; (ENTITY60)
  DEFB 255,0              ; Terminator (ENTITY127)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)

; Room 43: Conservatory Roof (teleport: 12469)
;
; Used by the routine at INITROOM.
;
; The first 128 bytes are copied to ROOMLAYOUT and define the room layout. Each
; bit-pair (bits 7 and 6, 5 and 4, 3 and 2, or 1 and 0 of each byte) determines
; the type of tile (background, floor, wall or nasty) that will be drawn at the
; corresponding location.
  DEFB 0,0,0,0,0,0,0,0      ; Room layout
  DEFB 0,0,0,0,0,0,0,0      ;
  DEFB 0,0,0,0,0,0,0,0      ;
  DEFB 0,0,0,0,0,0,0,0      ;
  DEFB 0,0,0,0,0,0,0,0      ;
  DEFB 0,0,0,0,0,0,0,0      ;
  DEFB 0,0,0,0,0,0,0,0      ;
  DEFB 0,0,0,0,0,42,170,170 ;
  DEFB 0,0,0,0,0,48,195,12  ;
  DEFB 0,0,0,0,0,0,0,0      ;
  DEFB 0,0,0,0,0,0,0,0      ;
  DEFB 0,0,0,0,0,0,0,0      ;
  DEFB 0,0,0,0,0,0,0,0      ;
  DEFB 0,0,0,0,0,0,80,0     ;
  DEFB 0,0,0,0,0,0,5,85     ;
  DEFB 0,0,0,0,0,4,0,0      ;
; The next 32 bytes are copied to ROOMNAME and specify the room name.
  DEFM "        Conservatory Roof       " ; Room name
; The next 54 bytes are copied to BACKGROUND and contain the attributes and
; graphic data for the tiles used to build the room.
  DEFB 0,0,0,0,0,0,0,0,0  ; Background
  DEFB 4,172,83,42,208,138,36,34,4 ; Floor
  DEFB 5,247,239,0,0,0,102,102,0 ; Wall
  DEFB 67,218,213,127,46,17,31,10,27 ; Nasty
  DEFB 7,1,2,4,16,8,32,64,128 ; Ramp
  DEFB 255,0,0,0,0,0,0,0,0 ; Conveyor
; The next four bytes are copied to CONVDIR and specify the direction, location
; and length of the conveyor.
  DEFB 1                  ; Direction (right)
  DEFW 24506              ; Location in the attribute buffer at 24064: (13,26)
  DEFB 6                  ; Length
; The next four bytes are copied to RAMPDIR and specify the direction, location
; and length of the ramp.
  DEFB 1                  ; Direction (up to the right)
  DEFW 24556              ; Location in the attribute buffer at 24064: (15,12)
  DEFB 9                  ; Length
; The next byte is copied to BORDER and specifies the border colour.
  DEFB 5                  ; Border colour
; The next two bytes are copied to XROOM223, but are not used.
  DEFB 0,0                ; Unused
; The next eight bytes are copied to ITEM and define the item graphic.
  DEFB 24,24,248,60,126,96,96,126 ; Item graphic
; The next four bytes are copied to LEFT and specify the rooms to the left, to
; the right, above and below.
  DEFB 0                  ; Room to the left (The Off Licence)
  DEFB 42                 ; Room to the right (Under the Roof)
  DEFB 0                  ; Room above (The Off Licence)
  DEFB 37                 ; Room below (Orangery)
; The next three bytes are copied to XROOM237, but are not used.
  DEFB 0,0,0              ; Unused
; The next eight pairs of bytes are copied to ENTITIES and specify the entities
; (ropes, arrows, guardians) in this room.
  DEFB 106,20             ; Guardian no. 106 (horizontal), base sprite 0,
                          ; initial x=20 (ENTITY106)
  DEFB 61,24              ; Guardian no. 61 (horizontal), base sprite 0,
                          ; initial x=24 (ENTITY61)
  DEFB 255,0              ; Terminator (ENTITY127)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)

; Room 44: On top of the house (teleport: 3469)
;
; Used by the routine at INITROOM.
;
; The first 128 bytes are copied to ROOMLAYOUT and define the room layout. Each
; bit-pair (bits 7 and 6, 5 and 4, 3 and 2, or 1 and 0 of each byte) determines
; the type of tile (background, floor, wall or nasty) that will be drawn at the
; corresponding location.
  DEFB 0,0,0,0,0,0,0,0        ; Room layout
  DEFB 0,0,0,0,0,0,0,0        ;
  DEFB 0,0,0,0,0,0,0,0        ;
  DEFB 0,0,0,0,0,0,0,0        ;
  DEFB 0,0,0,0,0,0,0,0        ;
  DEFB 146,0,0,0,4,0,0,0      ;
  DEFB 144,0,0,0,16,0,0,0     ;
  DEFB 144,0,0,0,64,0,0,0     ;
  DEFB 146,0,0,1,0,0,0,0      ;
  DEFB 146,128,0,4,0,0,0,0    ;
  DEFB 146,160,130,16,0,0,0,0 ;
  DEFB 146,170,170,64,0,0,0,0 ;
  DEFB 146,170,170,0,0,0,0,0  ;
  DEFB 16,0,0,0,0,0,0,0       ;
  DEFB 0,0,0,0,0,0,0,0        ;
  DEFB 170,170,170,0,0,0,0,0  ;
; The next 32 bytes are copied to ROOMNAME and specify the room name.
  DEFM "         On top of the house    " ; Room name
; The next 54 bytes are copied to BACKGROUND and contain the attributes and
; graphic data for the tiles used to build the room.
  DEFB 8,0,0,0,0,0,0,0,0  ; Background
  DEFB 14,160,64,128,0,0,0,0,0 ; Floor
  DEFB 75,81,170,0,170,21,170,0,170 ; Wall
  DEFB 255,0,0,0,0,0,0,0,0 ; Nasty (unused)
  DEFB 15,1,3,6,13,26,52,104,208 ; Ramp
  DEFB 255,0,0,0,0,0,0,0,0 ; Conveyor (unused)
; The next four bytes are copied to CONVDIR and specify the direction, location
; and length of the conveyor.
  DEFB 0                  ; Direction (left)
  DEFW 0                  ; Location in the attribute buffer at 24064 (unused)
  DEFB 0                  ; Length: 0 (there is no conveyor in this room)
; The next four bytes are copied to RAMPDIR and specify the direction, location
; and length of the ramp.
  DEFB 1                  ; Direction (up to the right)
  DEFW 24396              ; Location in the attribute buffer at 24064: (10,12)
  DEFB 6                  ; Length
; The next byte is copied to BORDER and specifies the border colour.
  DEFB 3                  ; Border colour
; The next two bytes are copied to XROOM223, but are not used.
  DEFB 0,0                ; Unused
; The next eight bytes are copied to ITEM and define the item graphic.
  DEFB 0,0,24,20,26,53,107,208 ; Item graphic
; The next four bytes are copied to LEFT and specify the rooms to the left, to
; the right, above and below.
  DEFB 14                 ; Room to the left (Rescue Esmerelda)
  DEFB 0                  ; Room to the right (The Off Licence)
  DEFB 0                  ; Room above (The Off Licence)
  DEFB 38                 ; Room below (Priests' Hole)
; The next three bytes are copied to XROOM237, but are not used.
  DEFB 0,0,0              ; Unused
; The next eight pairs of bytes are copied to ENTITIES and specify the entities
; (ropes, arrows, guardians) in this room.
  DEFB 29,20              ; Guardian no. 29 (vertical), base sprite 0, x=20
                          ; (ENTITY29)
  DEFB 255,0              ; Terminator (ENTITY127)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)

; Room 45: Under the Drive (teleport: 13469)
;
; Used by the routine at INITROOM.
;
; The first 128 bytes are copied to ROOMLAYOUT and define the room layout. Each
; bit-pair (bits 7 and 6, 5 and 4, 3 and 2, or 1 and 0 of each byte) determines
; the type of tile (background, floor, wall or nasty) that will be drawn at the
; corresponding location.
  DEFB 160,0,0,0,0,0,170,170           ; Room layout
  DEFB 160,0,0,0,0,0,170,170           ;
  DEFB 160,0,0,0,0,0,170,170           ;
  DEFB 160,0,0,0,0,5,170,170           ;
  DEFB 168,0,0,0,0,0,170,170           ;
  DEFB 170,0,0,0,16,0,2,170            ;
  DEFB 170,128,0,0,0,80,0,170          ;
  DEFB 170,160,0,0,0,0,0,10            ;
  DEFB 170,168,0,0,80,0,0,0            ;
  DEFB 170,170,0,0,0,192,0,0           ;
  DEFB 170,170,128,0,0,0,0,0           ;
  DEFB 170,170,160,0,0,0,0,0           ;
  DEFB 170,170,168,0,0,0,0,0           ;
  DEFB 170,170,170,0,0,0,0,0           ;
  DEFB 170,170,170,128,48,12,12,0      ;
  DEFB 170,170,170,170,170,170,170,170 ;
; The next 32 bytes are copied to ROOMNAME and specify the room name.
  DEFM "          Under the Drive       " ; Room name
; The next 54 bytes are copied to BACKGROUND and contain the attributes and
; graphic data for the tiles used to build the room.
  DEFB 0,0,0,0,0,0,0,0,0  ; Background
  DEFB 6,255,255,165,88,36,66,32,0 ; Floor
  DEFB 12,16,42,85,160,16,32,16,8 ; Wall
  DEFB 4,24,24,20,34,34,85,17,40 ; Nasty
  DEFB 7,128,64,160,80,168,84,170,85 ; Ramp
  DEFB 2,224,170,248,0,255,255,102,102 ; Conveyor
; The next four bytes are copied to CONVDIR and specify the direction, location
; and length of the conveyor.
  DEFB 0                  ; Direction (left)
  DEFW 24402              ; Location in the attribute buffer at 24064: (10,18)
  DEFB 14                 ; Length
; The next four bytes are copied to RAMPDIR and specify the direction, location
; and length of the ramp.
  DEFB 0                  ; Direction (up to the left)
  DEFW 24525              ; Location in the attribute buffer at 24064: (14,13)
  DEFB 12                 ; Length
; The next byte is copied to BORDER and specifies the border colour.
  DEFB 4                  ; Border colour
; The next two bytes are copied to XROOM223, but are not used.
  DEFB 0,0                ; Unused
; The next eight bytes are copied to ITEM and define the item graphic.
  DEFB 129,100,205,15,97,218,51,26 ; Item graphic (unused)
; The next four bytes are copied to LEFT and specify the rooms to the left, to
; the right, above and below.
  DEFB 6                  ; Room to the left (Entrance to Hades)
  DEFB 46                 ; Room to the right (Tree Root)
  DEFB 4                  ; Room above (The Drive)
  DEFB 0                  ; Room below (The Off Licence)
; The next three bytes are copied to XROOM237, but are not used.
  DEFB 0,0,0              ; Unused
; The next eight pairs of bytes are copied to ENTITIES and specify the entities
; (ropes, arrows, guardians) in this room.
  DEFB 28,16              ; Guardian no. 28 (horizontal), base sprite 0,
                          ; initial x=16 (ENTITY28)
  DEFB 29,146             ; Guardian no. 29 (vertical), base sprite 4, x=18
                          ; (ENTITY29)
  DEFB 68,150             ; Guardian no. 68 (horizontal), base sprite 4,
                          ; initial x=22 (ENTITY68)
  DEFB 255,0              ; Terminator (ENTITY127)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)

; Room 46: Tree Root (teleport: 23469)
;
; Used by the routine at INITROOM.
;
; The first 128 bytes are copied to ROOMLAYOUT and define the room layout. Each
; bit-pair (bits 7 and 6, 5 and 4, 3 and 2, or 1 and 0 of each byte) determines
; the type of tile (background, floor, wall or nasty) that will be drawn at the
; corresponding location.
  DEFB 170,10,170,170,170,170,170,170  ; Room layout
  DEFB 170,10,170,170,170,170,170,170  ;
  DEFB 170,10,170,170,170,170,170,170  ;
  DEFB 170,10,160,170,170,170,170,170  ;
  DEFB 170,10,160,170,0,160,0,10       ;
  DEFB 170,10,160,170,0,160,0,10       ;
  DEFB 170,10,160,0,0,160,0,10         ;
  DEFB 170,10,160,0,0,160,0,10         ;
  DEFB 0,0,5,0,0,160,0,10              ;
  DEFB 0,0,0,0,0,0,0,10                ;
  DEFB 0,0,0,0,0,0,0,42                ;
  DEFB 0,0,0,1,85,85,64,170            ;
  DEFB 0,0,0,0,0,0,2,170               ;
  DEFB 0,0,0,0,0,0,10,170              ;
  DEFB 0,12,3,3,0,48,42,170            ;
  DEFB 170,170,170,170,170,170,170,170 ;
; The next 32 bytes are copied to ROOMNAME and specify the room name.
  DEFM "           Tree Root            " ; Room name
; The next 54 bytes are copied to BACKGROUND and contain the attributes and
; graphic data for the tiles used to build the room.
  DEFB 0,0,0,0,0,0,0,0,0  ; Background
  DEFB 6,255,255,0,0,0,0,0,0 ; Floor
  DEFB 10,102,153,36,36,119,136,141,114 ; Wall
  DEFB 2,28,42,95,125,190,254,210,124 ; Nasty
  DEFB 67,3,2,13,2,49,8,213,34 ; Ramp
  DEFB 4,238,170,56,255,255,16,16,56 ; Conveyor
; The next four bytes are copied to CONVDIR and specify the direction, location
; and length of the conveyor.
  DEFB 0                  ; Direction (left)
  DEFW 24384              ; Location in the attribute buffer at 24064: (10,0)
  DEFB 13                 ; Length
; The next four bytes are copied to RAMPDIR and specify the direction, location
; and length of the ramp.
  DEFB 1                  ; Direction (up to the right)
  DEFW 24536              ; Location in the attribute buffer at 24064: (14,24)
  DEFB 6                  ; Length
; The next byte is copied to BORDER and specifies the border colour.
  DEFB 1                  ; Border colour
; The next two bytes are copied to XROOM223, but are not used.
  DEFB 0,0                ; Unused
; The next eight bytes are copied to ITEM and define the item graphic.
  DEFB 2,29,34,94,190,124,56,16 ; Item graphic
; The next four bytes are copied to LEFT and specify the rooms to the left, to
; the right, above and below.
  DEFB 45                 ; Room to the left (Under the Drive)
  DEFB 47                 ; Room to the right ([)
  DEFB 3                  ; Room above (At the Foot of the MegaTree)
  DEFB 0                  ; Room below (The Off Licence)
; The next three bytes are copied to XROOM237, but are not used.
  DEFB 0,0,0              ; Unused
; The next eight pairs of bytes are copied to ENTITIES and specify the entities
; (ropes, arrows, guardians) in this room.
  DEFB 30,132             ; Guardian no. 30 (vertical), base sprite 4, x=4
                          ; (ENTITY30)
  DEFB 40,13              ; Guardian no. 40 (vertical), base sprite 0, x=13
                          ; (ENTITY40)
  DEFB 35,19              ; Guardian no. 35 (horizontal), base sprite 0,
                          ; initial x=19 (ENTITY35)
  DEFB 31,89              ; Guardian no. 31 (vertical), base sprite 2, x=25
                          ; (ENTITY31)
  DEFB 255,0              ; Terminator (ENTITY127)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)

; Room 47: [ (teleport: 123469)
;
; This room is not used.
;
; The first 128 bytes define the room layout. Each bit-pair (bits 7 and 6, 5
; and 4, 3 and 2, or 1 and 0 of each byte) determines the type of tile
; (background, floor, wall or nasty) that will be drawn at the corresponding
; location.
  DEFB 0,0,0,0,0,0,0,0    ; Room layout (completely empty)
  DEFB 0,0,0,0,0,0,0,0    ;
  DEFB 0,0,0,0,0,0,0,0    ;
  DEFB 0,0,0,0,0,0,0,0    ;
  DEFB 0,0,0,0,0,0,0,0    ;
  DEFB 0,0,0,0,0,0,0,0    ;
  DEFB 0,0,0,0,0,0,0,0    ;
  DEFB 0,0,0,0,0,0,0,0    ;
  DEFB 0,0,0,0,0,0,0,0    ;
  DEFB 0,0,0,0,0,0,0,0    ;
  DEFB 0,0,0,0,0,0,0,0    ;
  DEFB 0,0,0,0,0,0,0,0    ;
  DEFB 0,0,0,0,0,0,0,0    ;
  DEFB 0,0,0,0,0,0,0,0    ;
  DEFB 0,0,0,0,0,0,0,0    ;
  DEFB 0,0,0,0,0,0,0,0    ;
; The next 32 bytes specify the room name.
  DEFM "         [                      " ; Room name
; In a working room definition, the next 80 bytes define the tiles, conveyor,
; ramp, border colour, item graphic, and exits. In this room, however, there
; are code remnants and unused data.
  DEFB 0,0,0,0,0,0,0,0,0  ; Background tile
  DEFB 0,0,0,0,0,0,0,0
  INC B
  LD (HL),0
  JP NZ,25315
  INC (IX+6)
  DEFB 16,255
  LD (IX+10),C
  LD (HL),B
  JP 24150
  LD BC,24583
  LD A,(16526)
  JR NZ,61411
  PUSH HL
  PUSH BC
  PUSH AF
  LD DE,24296
  PUSH DE
  PUSH BC
  RET
  POP AF
  POP BC
  DEC A
  DEFB 242,224
  DEFB 0                  ; Conveyor length (deliberately set to 0)
  POP HL
  RET
  POP BC
  DEFB 0                  ; Ramp length (deliberately set to 0)
  LD A,(HL)
  CP 44
  RET NZ
  RST 16
  PUSH BC
  LD A,(HL)
  CP 35
  CALL Z,7544
  CALL 11036
  EX (SP),HL
  PUSH HL
  DEFB 17
; The next eight pairs of bytes specify the entities (ropes, arrows, guardians)
; in this room.
  DEFB 255,0              ; Terminator (ENTITY127)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)

; Room 48: Nomen Luni (teleport: 569)
;
; Used by the routine at INITROOM.
;
; The first 128 bytes are copied to ROOMLAYOUT and define the room layout. Each
; bit-pair (bits 7 and 6, 5 and 4, 3 and 2, or 1 and 0 of each byte) determines
; the type of tile (background, floor, wall or nasty) that will be drawn at the
; corresponding location.
  DEFB 0,0,0,0,0,0,0,0           ; Room layout
  DEFB 0,0,0,0,0,0,0,0           ;
  DEFB 0,0,0,0,0,0,0,0           ;
  DEFB 0,0,0,0,0,0,0,0           ;
  DEFB 0,0,0,0,0,0,0,0           ;
  DEFB 0,0,0,0,10,170,0,0        ;
  DEFB 0,0,0,1,10,170,0,80       ;
  DEFB 0,0,0,0,0,160,0,0         ;
  DEFB 0,0,1,80,0,160,64,0       ;
  DEFB 0,0,0,0,4,160,0,10        ;
  DEFB 0,0,0,0,0,160,0,42        ;
  DEFB 0,0,0,0,64,160,10,162     ;
  DEFB 0,0,0,0,1,160,33,66       ;
  DEFB 0,0,0,0,0,160,0,10        ;
  DEFB 0,0,0,170,170,170,170,162 ;
  DEFB 0,0,0,170,170,170,170,162 ;
; The next 32 bytes are copied to ROOMNAME and specify the room name.
  DEFM "           Nomen Luni           " ; Room name
; The next 54 bytes are copied to BACKGROUND and contain the attributes and
; graphic data for the tiles used to build the room.
  DEFB 0,0,0,0,0,0,0,0,0  ; Background
  DEFB 13,255,34,68,136,85,34,0,0 ; Floor
  DEFB 243,170,112,170,108,177,95,170,85 ; Wall
  DEFB 6,85,162,85,152,28,42,69,138 ; Nasty (unused)
  DEFB 7,3,0,12,0,52,10,196,0 ; Ramp
  DEFB 255,0,0,0,0,0,0,0,0 ; Conveyor (unused)
; The next four bytes are copied to CONVDIR and specify the direction, location
; and length of the conveyor.
  DEFB 0                  ; Direction (left)
  DEFW 0                  ; Location in the attribute buffer at 24064 (unused)
  DEFB 0                  ; Length: 0 (there is no conveyor in this room)
; The next four bytes are copied to RAMPDIR and specify the direction, location
; and length of the ramp.
  DEFB 1                  ; Direction (up to the right)
  DEFW 24553              ; Location in the attribute buffer at 24064: (15,9)
  DEFB 5                  ; Length
; The next byte is copied to BORDER and specifies the border colour.
  DEFB 2                  ; Border colour
; The next two bytes are copied to XROOM223, but are not used.
  DEFB 0,0                ; Unused
; The next eight bytes are copied to ITEM and define the item graphic.
  DEFB 18,18,18,18,18,18,18,18 ; Item graphic (unused)
; The next four bytes are copied to LEFT and specify the rooms to the left, to
; the right, above and below.
  DEFB 0                  ; Room to the left (The Off Licence)
  DEFB 18                 ; Room to the right (On the Roof)
  DEFB 0                  ; Room above (The Off Licence)
  DEFB 42                 ; Room below (Under the Roof)
; The next three bytes are copied to XROOM237, but are not used.
  DEFB 0,0,0              ; Unused
; The next eight pairs of bytes are copied to ENTITIES and specify the entities
; (ropes, arrows, guardians) in this room.
  DEFB 104,5              ; Guardian no. 104 (horizontal), base sprite 0,
                          ; initial x=5 (ENTITY104)
  DEFB 107,20             ; Guardian no. 107 (horizontal), base sprite 0,
                          ; initial x=20 (ENTITY107)
  DEFB 29,144             ; Guardian no. 29 (vertical), base sprite 4, x=16
                          ; (ENTITY29)
  DEFB 11,22              ; Guardian no. 11 (vertical), base sprite 0, x=22
                          ; (ENTITY11)
  DEFB 255,0              ; Terminator (ENTITY127)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)

; Room 49: The Wine Cellar (teleport: 1569)
;
; Used by the routine at INITROOM.
;
; The first 128 bytes are copied to ROOMLAYOUT and define the room layout. Each
; bit-pair (bits 7 and 6, 5 and 4, 3 and 2, or 1 and 0 of each byte) determines
; the type of tile (background, floor, wall or nasty) that will be drawn at the
; corresponding location.
  DEFB 0,0,0,0,0,12,2,170              ; Room layout
  DEFB 0,0,0,0,0,0,2,170               ;
  DEFB 85,64,0,0,0,0,2,170             ;
  DEFB 0,0,0,0,0,0,0,42                ;
  DEFB 0,0,0,0,0,0,0,42                ;
  DEFB 170,168,0,65,0,21,86,170        ;
  DEFB 170,0,0,0,0,0,0,42              ;
  DEFB 170,0,0,0,0,0,0,42              ;
  DEFB 170,168,0,65,0,85,86,170        ;
  DEFB 160,0,0,0,0,0,0,42              ;
  DEFB 160,0,0,0,0,0,0,42              ;
  DEFB 170,168,0,65,1,85,85,85         ;
  DEFB 170,0,0,195,3,0,0,0             ;
  DEFB 170,0,0,0,0,0,10,170            ;
  DEFB 170,168,0,0,0,0,42,170          ;
  DEFB 170,170,170,170,170,170,170,170 ;
; The next 32 bytes are copied to ROOMNAME and specify the room name.
  DEFM "        The Wine Cellar         " ; Room name
; The next 54 bytes are copied to BACKGROUND and contain the attributes and
; graphic data for the tiles used to build the room. Note that because of a bug
; in the game engine, the conveyor tile is not drawn correctly (see the room
; image above).
  DEFB 0,0,0,0,0,0,0,0,0  ; Background
  DEFB 66,255,24,231,16,199,56,0,0 ; Floor
  DEFB 41,10,65,40,5,160,20,130,80 ; Wall
  DEFB 70,36,36,36,90,219,189,195,126 ; Nasty
  DEFB 7,3,3,13,14,53,58,213,234 ; Ramp
  DEFB 13,170,85,170,85,170,85,170,85 ; Conveyor
; The next four bytes are copied to CONVDIR and specify the direction, location
; and length of the conveyor.
  DEFB 1                  ; Direction (right)
  DEFW 24475              ; Location in the attribute buffer at 24064: (12,27)
  DEFB 5                  ; Length
; The next four bytes are copied to RAMPDIR and specify the direction, location
; and length of the ramp.
  DEFB 1                  ; Direction (up to the right)
  DEFW 24536              ; Location in the attribute buffer at 24064: (14,24)
  DEFB 3                  ; Length
; The next byte is copied to BORDER and specifies the border colour.
  DEFB 4                  ; Border colour
; The next two bytes are copied to XROOM223, but are not used.
  DEFB 0,0                ; Unused
; The next eight bytes are copied to ITEM and define the item graphic.
  DEFB 0,25,249,31,0,25,255,31 ; Item graphic
; The next four bytes are copied to LEFT and specify the rooms to the left, to
; the right, above and below.
  DEFB 51                 ; Room to the left (Tool  Shed)
  DEFB 19                 ; Room to the right (The Forgotten Abbey)
  DEFB 52                 ; Room above (Back Stairway)
  DEFB 0                  ; Room below (The Off Licence)
; The next three bytes are copied to XROOM237, but are not used.
  DEFB 0,0,0              ; Unused
; The next eight pairs of bytes are copied to ENTITIES and specify the entities
; (ropes, arrows, guardians) in this room.
  DEFB 52,16              ; Guardian no. 52 (horizontal), base sprite 0,
                          ; initial x=16 (ENTITY52)
  DEFB 64,25              ; Guardian no. 64 (horizontal), base sprite 0,
                          ; initial x=25 (ENTITY64)
  DEFB 65,9               ; Guardian no. 65 (horizontal), base sprite 0,
                          ; initial x=9 (ENTITY65)
  DEFB 66,19              ; Guardian no. 66 (horizontal), base sprite 0,
                          ; initial x=19 (ENTITY66)
  DEFB 255,0              ; Terminator (ENTITY127)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)

; Room 50: Watch Tower (teleport: 2569)
;
; Used by the routine at INITROOM.
;
; The first 128 bytes are copied to ROOMLAYOUT and define the room layout. Each
; bit-pair (bits 7 and 6, 5 and 4, 3 and 2, or 1 and 0 of each byte) determines
; the type of tile (background, floor, wall or nasty) that will be drawn at the
; corresponding location.
  DEFB 0,0,0,0,0,0,0,0           ; Room layout
  DEFB 0,0,0,0,0,0,0,0           ;
  DEFB 0,0,0,0,0,0,0,0           ;
  DEFB 0,0,0,0,0,0,0,0           ;
  DEFB 0,0,0,0,0,0,0,0           ;
  DEFB 0,0,0,0,0,0,0,0           ;
  DEFB 0,0,0,0,0,0,0,0           ;
  DEFB 0,0,0,0,0,0,0,0           ;
  DEFB 0,0,0,0,0,0,0,0           ;
  DEFB 0,8,0,0,0,0,32,0          ;
  DEFB 0,8,12,0,0,192,224,0      ;
  DEFB 0,10,170,160,26,170,160,0 ;
  DEFB 0,0,0,160,10,0,0,0        ;
  DEFB 0,0,0,165,10,0,0,0        ;
  DEFB 0,0,0,160,10,0,0,0        ;
  DEFB 0,0,0,160,90,0,0,0        ;
; The next 32 bytes are copied to ROOMNAME and specify the room name.
  DEFM "          Watch Tower           " ; Room name
; The next 54 bytes are copied to BACKGROUND and contain the attributes and
; graphic data for the tiles used to build the room.
  DEFB 8,0,0,0,0,0,0,0,0  ; Background
  DEFB 6,255,255,170,85,170,85,0,0 ; Floor
  DEFB 29,170,85,85,170,85,170,170,85 ; Wall
  DEFB 12,70,137,137,54,72,131,132,104 ; Nasty
  DEFB 15,1,2,4,8,16,32,64,128 ; Ramp
  DEFB 13,246,255,231,0,0,0,0,0 ; Conveyor
; The next four bytes are copied to CONVDIR and specify the direction, location
; and length of the conveyor.
  DEFB 1                  ; Direction (right)
  DEFW 24204              ; Location in the attribute buffer at 24064: (4,12)
  DEFB 8                  ; Length
; The next four bytes are copied to RAMPDIR and specify the direction, location
; and length of the ramp.
  DEFB 1                  ; Direction (up to the right)
  DEFW 24327              ; Location in the attribute buffer at 24064: (8,7)
  DEFB 5                  ; Length
; The next byte is copied to BORDER and specifies the border colour.
  DEFB 5                  ; Border colour
; The next two bytes are copied to XROOM223, but are not used.
  DEFB 0,0                ; Unused
; The next eight bytes are copied to ITEM and define the item graphic.
  DEFB 116,56,102,191,191,157,66,60 ; Item graphic
; The next four bytes are copied to LEFT and specify the rooms to the left, to
; the right, above and below.
  DEFB 0                  ; Room to the left (The Off Licence)
  DEFB 0                  ; Room to the right (The Off Licence)
  DEFB 0                  ; Room above (The Off Licence)
  DEFB 16                 ; Room below (We must perform a Quirkafleeg)
; The next three bytes are copied to XROOM237, but are not used.
  DEFB 0,0,0              ; Unused
; The next eight pairs of bytes are copied to ENTITIES and specify the entities
; (ropes, arrows, guardians) in this room.
  DEFB 108,145            ; Guardian no. 108 (horizontal), base sprite 4,
                          ; initial x=17 (ENTITY108)
  DEFB 109,11             ; Guardian no. 109 (horizontal), base sprite 0,
                          ; initial x=11 (ENTITY109)
  DEFB 255,0              ; Terminator (ENTITY127)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)

; Room 51: Tool  Shed (teleport: 12569)
;
; Used by the routine at INITROOM.
;
; The first 128 bytes are copied to ROOMLAYOUT and define the room layout. Each
; bit-pair (bits 7 and 6, 5 and 4, 3 and 2, or 1 and 0 of each byte) determines
; the type of tile (background, floor, wall or nasty) that will be drawn at the
; corresponding location.
  DEFB 0,0,2,170,160,0,0,0             ; Room layout
  DEFB 0,0,10,170,160,0,0,0            ;
  DEFB 0,0,42,170,170,170,170,170      ;
  DEFB 0,0,0,0,192,0,0,0               ;
  DEFB 0,0,0,0,0,0,0,0                 ;
  DEFB 0,0,0,0,0,0,0,170               ;
  DEFB 0,0,0,0,0,0,0,170               ;
  DEFB 0,0,0,0,0,0,0,170               ;
  DEFB 170,168,0,65,0,64,42,170        ;
  DEFB 170,168,0,0,0,0,42,170          ;
  DEFB 170,168,0,0,0,0,106,170         ;
  DEFB 170,168,17,0,16,0,42,170        ;
  DEFB 170,168,0,0,0,64,42,170         ;
  DEFB 170,168,0,0,0,0,106,170         ;
  DEFB 170,168,0,0,0,0,42,170          ;
  DEFB 170,170,170,170,170,170,170,170 ;
; The next 32 bytes are copied to ROOMNAME and specify the room name.
  DEFM "           Tool  Shed           " ; Room name
; The next 54 bytes are copied to BACKGROUND and contain the attributes and
; graphic data for the tiles used to build the room. Note that because of a bug
; in the game engine, the conveyor tile is not drawn correctly (see the room
; image above).
  DEFB 0,0,0,0,0,0,0,0,0  ; Background
  DEFB 5,90,255,34,68,34,64,0,0 ; Floor
  DEFB 30,85,170,68,34,85,170,68,34 ; Wall
  DEFB 6,4,4,4,136,80,32,16,8 ; Nasty
  DEFB 7,3,3,12,12,48,48,192,192 ; Ramp
  DEFB 68,224,170,0,102,102,102,102,102 ; Conveyor
; The next four bytes are copied to CONVDIR and specify the direction, location
; and length of the conveyor.
  DEFB 1                  ; Direction (right)
  DEFW 24551              ; Location in the attribute buffer at 24064: (15,7)
  DEFB 18                 ; Length
; The next four bytes are copied to RAMPDIR and specify the direction, location
; and length of the ramp.
  DEFB 1                  ; Direction (up to the right)
  DEFW 24291              ; Location in the attribute buffer at 24064: (7,3)
  DEFB 8                  ; Length
; The next byte is copied to BORDER and specifies the border colour.
  DEFB 5                  ; Border colour
; The next two bytes are copied to XROOM223, but are not used.
  DEFB 0,0                ; Unused
; The next eight bytes are copied to ITEM and define the item graphic.
  DEFB 24,24,24,24,24,155,127,59 ; Item graphic
; The next four bytes are copied to LEFT and specify the rooms to the left, to
; the right, above and below.
  DEFB 58                 ; Room to the left (The Beach)
  DEFB 49                 ; Room to the right (The Wine Cellar)
  DEFB 53                 ; Room above (Back Door)
  DEFB 0                  ; Room below (The Off Licence)
; The next three bytes are copied to XROOM237, but are not used.
  DEFB 0,0,0              ; Unused
; The next eight pairs of bytes are copied to ENTITIES and specify the entities
; (ropes, arrows, guardians) in this room.
  DEFB 52,16              ; Guardian no. 52 (horizontal), base sprite 0,
                          ; initial x=16 (ENTITY52)
  DEFB 98,9               ; Guardian no. 98 (horizontal), base sprite 0,
                          ; initial x=9 (ENTITY98)
  DEFB 99,20              ; Guardian no. 99 (horizontal), base sprite 0,
                          ; initial x=20 (ENTITY99)
  DEFB 255,0              ; Terminator (ENTITY127)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)

; Room 52: Back Stairway (teleport: 3569)
;
; Used by the routine at INITROOM.
;
; The first 128 bytes are copied to ROOMLAYOUT and define the room layout. Each
; bit-pair (bits 7 and 6, 5 and 4, 3 and 2, or 1 and 0 of each byte) determines
; the type of tile (background, floor, wall or nasty) that will be drawn at the
; corresponding location.
  DEFB 0,0,0,0,10,170,170,170  ; Room layout
  DEFB 0,0,0,0,42,170,170,170  ;
  DEFB 0,0,0,0,85,85,85,85     ;
  DEFB 0,0,0,0,0,0,0,0         ;
  DEFB 0,0,0,0,0,0,0,0         ;
  DEFB 0,0,0,21,85,85,85,85    ;
  DEFB 0,0,0,0,0,0,0,0         ;
  DEFB 0,0,0,0,0,0,0,0         ;
  DEFB 0,0,5,85,85,85,85,85    ;
  DEFB 0,0,0,0,0,0,0,0         ;
  DEFB 0,0,0,0,0,0,0,0         ;
  DEFB 85,84,85,85,85,85,85,85 ;
  DEFB 0,0,0,0,0,0,0,0         ;
  DEFB 0,0,0,0,0,0,0,0         ;
  DEFB 0,0,0,0,0,0,0,0         ;
  DEFB 85,85,85,85,85,85,85,85 ;
; The next 32 bytes are copied to ROOMNAME and specify the room name.
  DEFM "         Back Stairway          " ; Room name
; The next 54 bytes are copied to BACKGROUND and contain the attributes and
; graphic data for the tiles used to build the room.
  DEFB 0,0,0,0,0,0,0,0,0  ; Background
  DEFB 67,255,68,68,255,17,17,255,0 ; Floor
  DEFB 30,21,241,143,168,138,248,31,21 ; Wall
  DEFB 255,0,0,0,0,0,0,0,0 ; Nasty (unused)
  DEFB 71,3,1,12,4,48,16,192,64 ; Ramp
  DEFB 255,0,0,0,0,0,0,0,0 ; Conveyor (unused)
; The next four bytes are copied to CONVDIR and specify the direction, location
; and length of the conveyor.
  DEFB 0                  ; Direction (left)
  DEFW 0                  ; Location in the attribute buffer at 24064 (unused)
  DEFB 0                  ; Length: 0 (there is no conveyor in this room)
; The next four bytes are copied to RAMPDIR and specify the direction, location
; and length of the ramp.
  DEFB 1                  ; Direction (up to the right)
  DEFW 24546              ; Location in the attribute buffer at 24064: (15,2)
  DEFB 16                 ; Length
; The next byte is copied to BORDER and specifies the border colour.
  DEFB 2                  ; Border colour
; The next two bytes are copied to XROOM223, but are not used.
  DEFB 0,0                ; Unused
; The next eight bytes are copied to ITEM and define the item graphic.
  DEFB 0,0,0,0,0,0,0,0    ; Item graphic (unused)
; The next four bytes are copied to LEFT and specify the rooms to the left, to
; the right, above and below.
  DEFB 53                 ; Room to the left (Back Door)
  DEFB 25                 ; Room to the right (Cold Store)
  DEFB 54                 ; Room above (West  Wing)
  DEFB 49                 ; Room below (The Wine Cellar)
; The next three bytes are copied to XROOM237, but are not used.
  DEFB 0,0,0              ; Unused
; The next eight pairs of bytes are copied to ENTITIES and specify the entities
; (ropes, arrows, guardians) in this room.
  DEFB 36,150             ; Guardian no. 36 (horizontal), base sprite 4,
                          ; initial x=22 (ENTITY36)
  DEFB 92,144             ; Guardian no. 92 (horizontal), base sprite 4,
                          ; initial x=16 (ENTITY92)
  DEFB 29,131             ; Guardian no. 29 (vertical), base sprite 4, x=3
                          ; (ENTITY29)
  DEFB 255,0              ; Terminator (ENTITY127)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)

; Room 53: Back Door (teleport: 13569)
;
; Used by the routine at INITROOM.
;
; The first 128 bytes are copied to ROOMLAYOUT and define the room layout. Each
; bit-pair (bits 7 and 6, 5 and 4, 3 and 2, or 1 and 0 of each byte) determines
; the type of tile (background, floor, wall or nasty) that will be drawn at the
; corresponding location.
  DEFB 0,0,0,0,160,0,0,0    ; Room layout
  DEFB 0,0,0,0,160,0,0,0    ;
  DEFB 0,0,0,0,160,0,0,0    ;
  DEFB 0,0,0,0,160,0,0,0    ;
  DEFB 0,0,0,0,160,0,0,0    ;
  DEFB 0,0,0,0,160,0,0,0    ;
  DEFB 0,0,0,0,160,0,0,0    ;
  DEFB 0,0,0,0,160,0,0,0    ;
  DEFB 0,0,0,0,0,0,0,0      ;
  DEFB 0,0,0,0,0,0,0,0      ;
  DEFB 0,0,0,0,0,0,0,0      ;
  DEFB 0,0,0,5,85,85,85,85  ;
  DEFB 0,0,0,42,160,0,0,0   ;
  DEFB 0,0,0,170,160,0,0,0  ;
  DEFB 0,0,2,170,160,0,0,0  ;
  DEFB 0,0,10,170,160,0,0,0 ;
; The next 32 bytes are copied to ROOMNAME and specify the room name.
  DEFM "            Back Door           " ; Room name
; The next 54 bytes are copied to BACKGROUND and contain the attributes and
; graphic data for the tiles used to build the room.
  DEFB 0,0,0,0,0,0,0,0,0  ; Background
  DEFB 3,255,255,255,170,85,170,85,0 ; Floor
  DEFB 38,105,210,165,75,150,45,90,180 ; Wall
  DEFB 255,0,0,0,0,0,0,0,0 ; Nasty (unused)
  DEFB 7,3,3,13,15,54,61,218,244 ; Ramp
  DEFB 34,240,240,240,240,102,102,0,0 ; Conveyor
; The next four bytes are copied to CONVDIR and specify the direction, location
; and length of the conveyor.
  DEFB 1                  ; Direction (right)
  DEFW 24562              ; Location in the attribute buffer at 24064: (15,18)
  DEFB 14                 ; Length
; The next four bytes are copied to RAMPDIR and specify the direction, location
; and length of the ramp.
  DEFB 1                  ; Direction (up to the right)
  DEFW 24553              ; Location in the attribute buffer at 24064: (15,9)
  DEFB 5                  ; Length
; The next byte is copied to BORDER and specifies the border colour.
  DEFB 1                  ; Border colour
; The next two bytes are copied to XROOM223, but are not used.
  DEFB 0,0                ; Unused
; The next eight bytes are copied to ITEM and define the item graphic.
  DEFB 0,0,0,0,0,0,0,0    ; Item graphic (unused)
; The next four bytes are copied to LEFT and specify the rooms to the left, to
; the right, above and below.
  DEFB 0                  ; Room to the left (The Off Licence)
  DEFB 52                 ; Room to the right (Back Stairway)
  DEFB 55                 ; Room above (West Bedroom)
  DEFB 51                 ; Room below (Tool  Shed)
; The next three bytes are copied to XROOM237, but are not used.
  DEFB 0,0,0              ; Unused
; The next eight pairs of bytes are copied to ENTITIES and specify the entities
; (ropes, arrows, guardians) in this room.
  DEFB 255,0              ; Terminator (ENTITY127)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)

; Room 54: West  Wing (teleport: 23569)
;
; Used by the routine at INITROOM.
;
; The first 128 bytes are copied to ROOMLAYOUT and define the room layout. Each
; bit-pair (bits 7 and 6, 5 and 4, 3 and 2, or 1 and 0 of each byte) determines
; the type of tile (background, floor, wall or nasty) that will be drawn at the
; corresponding location.
  DEFB 0,160,0,0,0,0,0,0      ; Room layout
  DEFB 0,160,0,0,0,0,0,0      ;
  DEFB 0,160,0,0,0,0,0,0      ;
  DEFB 85,160,0,0,0,0,0,0     ;
  DEFB 0,160,0,0,0,0,0,0      ;
  DEFB 0,160,0,0,0,0,0,0      ;
  DEFB 0,0,0,0,0,0,0,0        ;
  DEFB 0,0,0,0,0,0,0,0        ;
  DEFB 0,0,0,0,0,0,0,0        ;
  DEFB 0,0,0,0,0,0,0,0        ;
  DEFB 0,0,0,0,0,0,0,0        ;
  DEFB 0,0,0,0,0,0,0,0        ;
  DEFB 85,85,85,80,0,85,85,85 ;
  DEFB 0,0,0,0,2,170,170,170  ;
  DEFB 0,0,0,0,10,170,170,170 ;
  DEFB 0,0,0,0,42,170,170,170 ;
; The next 32 bytes are copied to ROOMNAME and specify the room name.
  DEFM "           West  Wing           " ; Room name
; The next 54 bytes are copied to BACKGROUND and contain the attributes and
; graphic data for the tiles used to build the room.
  DEFB 0,0,0,0,0,0,0,0,0  ; Background
  DEFB 65,249,255,63,213,170,21,74,132 ; Floor
  DEFB 39,172,174,1,173,173,2,172,232 ; Wall
  DEFB 255,0,0,0,0,0,0,0,0 ; Nasty (unused)
  DEFB 7,3,3,12,12,48,48,192,192 ; Ramp
  DEFB 255,0,0,0,0,0,0,0,0 ; Conveyor (unused)
; The next four bytes are copied to CONVDIR and specify the direction, location
; and length of the conveyor.
  DEFB 0                  ; Direction (left)
  DEFW 0                  ; Location in the attribute buffer at 24064 (unused)
  DEFB 0                  ; Length: 0 (there is no conveyor in this room)
; The next four bytes are copied to RAMPDIR and specify the direction, location
; and length of the ramp.
  DEFB 1                  ; Direction (up to the right)
  DEFW 24560              ; Location in the attribute buffer at 24064: (15,16)
  DEFB 16                 ; Length
; The next byte is copied to BORDER and specifies the border colour.
  DEFB 2                  ; Border colour
; The next two bytes are copied to XROOM223, but are not used.
  DEFB 0,0                ; Unused
; The next eight bytes are copied to ITEM and define the item graphic.
  DEFB 0,0,0,0,0,0,0,0    ; Item graphic (unused)
; The next four bytes are copied to LEFT and specify the rooms to the left, to
; the right, above and below.
  DEFB 55                 ; Room to the left (West Bedroom)
  DEFB 31                 ; Room to the right (Swimming Pool)
  DEFB 56                 ; Room above (West Wing Roof)
  DEFB 52                 ; Room below (Back Stairway)
; The next three bytes are copied to XROOM237, but are not used.
  DEFB 0,0,0              ; Unused
; The next eight pairs of bytes are copied to ENTITIES and specify the entities
; (ropes, arrows, guardians) in this room.
  DEFB 97,14              ; Guardian no. 97 (vertical), base sprite 0, x=14
                          ; (ENTITY97)
  DEFB 89,196             ; Guardian no. 89 (vertical), base sprite 6, x=4
                          ; (ENTITY89)
  DEFB 20,9               ; Guardian no. 20 (horizontal), base sprite 0,
                          ; initial x=9 (ENTITY20)
  DEFB 255,0              ; Terminator (ENTITY127)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)

; Room 55: West Bedroom (teleport: 123569)
;
; Used by the routine at INITROOM.
;
; The first 128 bytes are copied to ROOMLAYOUT and define the room layout. Each
; bit-pair (bits 7 and 6, 5 and 4, 3 and 2, or 1 and 0 of each byte) determines
; the type of tile (background, floor, wall or nasty) that will be drawn at the
; corresponding location.
  DEFB 0,0,4,0,160,64,0,0   ; Room layout
  DEFB 0,0,0,0,160,64,0,0   ;
  DEFB 0,0,16,0,160,64,0,0  ;
  DEFB 0,0,0,0,160,64,0,21  ;
  DEFB 0,0,4,0,160,64,0,0   ;
  DEFB 0,0,0,0,160,64,0,0   ;
  DEFB 0,0,0,16,160,64,0,0  ;
  DEFB 0,0,0,0,160,64,0,0   ;
  DEFB 0,0,4,0,0,64,0,0     ;
  DEFB 0,0,0,0,0,64,0,0     ;
  DEFB 0,0,16,0,0,64,0,0    ;
  DEFB 0,0,8,0,0,64,0,0     ;
  DEFB 0,0,9,85,85,85,85,85 ;
  DEFB 0,0,10,170,160,0,0,0 ;
  DEFB 0,0,0,0,160,0,0,0    ;
  DEFB 0,0,0,0,160,0,0,0    ;
; The next 32 bytes are copied to ROOMNAME and specify the room name.
  DEFM "    West Bedroom                " ; Room name
; The next 54 bytes are copied to BACKGROUND and contain the attributes and
; graphic data for the tiles used to build the room.
  DEFB 0,0,0,0,0,0,0,0,0  ; Background
  DEFB 6,170,85,130,65,130,65,130,65 ; Floor
  DEFB 13,168,85,168,86,207,46,38,193 ; Wall
  DEFB 255,0,0,0,0,0,0,0,0 ; Nasty (unused)
  DEFB 255,0,0,0,0,0,0,0,0 ; Ramp (unused)
  DEFB 66,165,255,189,170,85,186,69,131 ; Conveyor
; The next four bytes are copied to CONVDIR and specify the direction, location
; and length of the conveyor.
  DEFB 0                  ; Direction (left)
  DEFW 24186              ; Location in the attribute buffer at 24064: (3,26)
  DEFB 3                  ; Length
; The next four bytes are copied to RAMPDIR and specify the direction, location
; and length of the ramp.
  DEFB 0                  ; Direction (up to the left)
  DEFW 0                  ; Location in the attribute buffer at 24064 (unused)
  DEFB 0                  ; Length: 0 (there is no ramp in this room)
; The next byte is copied to BORDER and specifies the border colour.
  DEFB 5                  ; Border colour
; The next two bytes are copied to XROOM223, but are not used.
  DEFB 0,0                ; Unused
; The next eight bytes are copied to ITEM and define the item graphic.
  DEFB 0,0,0,0,0,0,0,0    ; Item graphic (unused)
; The next four bytes are copied to LEFT and specify the rooms to the left, to
; the right, above and below.
  DEFB 0                  ; Room to the left (The Off Licence)
  DEFB 54                 ; Room to the right (West  Wing)
  DEFB 57                 ; Room above (Above the West Bedroom)
  DEFB 53                 ; Room below (Back Door)
; The next three bytes are copied to XROOM237, but are not used.
  DEFB 0,0,0              ; Unused
; The next eight pairs of bytes are copied to ENTITIES and specify the entities
; (ropes, arrows, guardians) in this room.
  DEFB 30,139             ; Guardian no. 30 (vertical), base sprite 4, x=11
                          ; (ENTITY30)
  DEFB 51,78              ; Guardian no. 51 (vertical), base sprite 2, x=14
                          ; (ENTITY51)
  DEFB 255,0              ; Terminator (ENTITY127)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)

; Room 56: West Wing Roof (teleport: 4569)
;
; Used by the routine at INITROOM.
;
; The first 128 bytes are copied to ROOMLAYOUT and define the room layout. Each
; bit-pair (bits 7 and 6, 5 and 4, 3 and 2, or 1 and 0 of each byte) determines
; the type of tile (background, floor, wall or nasty) that will be drawn at the
; corresponding location.
  DEFB 0,0,0,0,0,0,0,0         ; Room layout
  DEFB 0,0,0,0,0,0,0,0         ;
  DEFB 0,0,0,0,0,0,0,0         ;
  DEFB 0,0,0,0,0,0,0,0         ;
  DEFB 0,0,0,0,0,0,0,0         ;
  DEFB 0,0,0,0,0,0,0,0         ;
  DEFB 85,160,0,0,0,0,0,0      ;
  DEFB 0,160,48,48,12,12,3,0   ;
  DEFB 0,165,85,85,85,85,85,85 ;
  DEFB 0,160,0,0,0,0,0,0       ;
  DEFB 0,160,0,0,0,0,0,0       ;
  DEFB 80,160,0,0,0,0,0,0      ;
  DEFB 0,160,0,0,0,0,0,0       ;
  DEFB 1,160,0,0,0,0,0,0       ;
  DEFB 0,160,0,0,0,0,0,0       ;
  DEFB 4,165,85,85,85,85,85,5  ;
; The next 32 bytes are copied to ROOMNAME and specify the room name.
  DEFM "         West Wing Roof         " ; Room name
; The next 54 bytes are copied to BACKGROUND and contain the attributes and
; graphic data for the tiles used to build the room.
  DEFB 0,0,0,0,0,0,0,0,0  ; Background
  DEFB 3,255,170,255,164,164,167,228,60 ; Floor
  DEFB 37,225,132,30,72,225,132,30,72 ; Wall
  DEFB 66,24,60,126,255,175,85,58,16 ; Nasty
  DEFB 7,3,0,12,0,48,0,192,0 ; Ramp
  DEFB 255,0,0,0,0,0,0,0,0 ; Conveyor (unused)
; The next four bytes are copied to CONVDIR and specify the direction, location
; and length of the conveyor.
  DEFB 0                  ; Direction (left)
  DEFW 0                  ; Location in the attribute buffer at 24064 (unused)
  DEFB 0                  ; Length: 0 (there is no conveyor in this room)
; The next four bytes are copied to RAMPDIR and specify the direction, location
; and length of the ramp.
  DEFB 1                  ; Direction (up to the right)
  DEFW 24572              ; Location in the attribute buffer at 24064: (15,28)
  DEFB 4                  ; Length
; The next byte is copied to BORDER and specifies the border colour.
  DEFB 1                  ; Border colour
; The next two bytes are copied to XROOM223, but are not used.
  DEFB 0,0                ; Unused
; The next eight bytes are copied to ITEM and define the item graphic.
  DEFB 0,0,0,10,13,60,242,192 ; Item graphic
; The next four bytes are copied to LEFT and specify the rooms to the left, to
; the right, above and below.
  DEFB 57                 ; Room to the left (Above the West Bedroom)
  DEFB 37                 ; Room to the right (Orangery)
  DEFB 0                  ; Room above (The Off Licence)
  DEFB 54                 ; Room below (West  Wing)
; The next three bytes are copied to XROOM237, but are not used.
  DEFB 0,0,0              ; Unused
; The next eight pairs of bytes are copied to ENTITIES and specify the entities
; (ropes, arrows, guardians) in this room.
  DEFB 52,19              ; Guardian no. 52 (horizontal), base sprite 0,
                          ; initial x=19 (ENTITY52)
  DEFB 28,20              ; Guardian no. 28 (horizontal), base sprite 0,
                          ; initial x=20 (ENTITY28)
  DEFB 60,84              ; Arrow flying left to right at pixel y-coordinate 42
                          ; (ENTITY60)
  DEFB 255,0              ; Terminator (ENTITY127)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)

; Room 57: Above the West Bedroom (teleport: 14569)
;
; Used by the routine at INITROOM.
;
; The first 128 bytes are copied to ROOMLAYOUT and define the room layout. Each
; bit-pair (bits 7 and 6, 5 and 4, 3 and 2, or 1 and 0 of each byte) determines
; the type of tile (background, floor, wall or nasty) that will be drawn at the
; corresponding location.
  DEFB 0,0,0,0,0,0,0,0      ; Room layout
  DEFB 0,0,0,0,0,0,0,0      ;
  DEFB 0,0,0,0,0,0,0,0      ;
  DEFB 0,0,0,0,0,0,0,0      ;
  DEFB 0,0,0,0,0,0,0,0      ;
  DEFB 0,0,0,0,0,0,0,0      ;
  DEFB 0,0,0,0,0,0,21,85    ;
  DEFB 0,0,0,0,0,0,208,0    ;
  DEFB 0,0,0,0,0,3,16,0     ;
  DEFB 0,0,0,0,0,12,16,0    ;
  DEFB 0,0,0,0,0,48,16,0    ;
  DEFB 0,0,0,0,1,85,85,85   ;
  DEFB 0,0,0,0,3,0,0,0      ;
  DEFB 0,0,4,0,12,0,0,0     ;
  DEFB 0,0,0,0,160,0,0,0    ;
  DEFB 0,0,16,0,160,85,85,0 ;
; The next 32 bytes are copied to ROOMNAME and specify the room name.
  DEFM "   Above the West Bedroom       " ; Room name
; The next 54 bytes are copied to BACKGROUND and contain the attributes and
; graphic data for the tiles used to build the room.
  DEFB 0,0,0,0,0,0,0,0,0  ; Background
  DEFB 4,247,170,85,239,0,0,0,0 ; Floor
  DEFB 35,170,85,170,85,170,85,170,85 ; Wall
  DEFB 6,48,96,192,128,0,0,0,0 ; Nasty
  DEFB 7,1,0,6,9,19,6,108,152 ; Ramp
  DEFB 255,0,0,0,0,0,0,0,0 ; Conveyor (unused)
; The next four bytes are copied to CONVDIR and specify the direction, location
; and length of the conveyor.
  DEFB 0                  ; Direction (left)
  DEFW 0                  ; Location in the attribute buffer at 24064 (unused)
  DEFB 0                  ; Length: 0 (there is no conveyor in this room)
; The next four bytes are copied to RAMPDIR and specify the direction, location
; and length of the ramp.
  DEFB 1                  ; Direction (up to the right)
  DEFW 24497              ; Location in the attribute buffer at 24064: (13,17)
  DEFB 8                  ; Length
; The next byte is copied to BORDER and specifies the border colour.
  DEFB 2                  ; Border colour
; The next two bytes are copied to XROOM223, but are not used.
  DEFB 0,0                ; Unused
; The next eight bytes are copied to ITEM and define the item graphic.
  DEFB 8,16,32,33,18,172,64,32 ; Item graphic
; The next four bytes are copied to LEFT and specify the rooms to the left, to
; the right, above and below.
  DEFB 0                  ; Room to the left (The Off Licence)
  DEFB 56                 ; Room to the right (West Wing Roof)
  DEFB 0                  ; Room above (The Off Licence)
  DEFB 55                 ; Room below (West Bedroom)
; The next three bytes are copied to XROOM237, but are not used.
  DEFB 0,0,0              ; Unused
; The next eight pairs of bytes are copied to ENTITIES and specify the entities
; (ropes, arrows, guardians) in this room.
  DEFB 7,203              ; Guardian no. 7 (vertical), base sprite 6, x=11
                          ; (ENTITY7)
  DEFB 51,78              ; Guardian no. 51 (vertical), base sprite 2, x=14
                          ; (ENTITY51)
  DEFB 255,0              ; Terminator (ENTITY127)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)

; Room 58: The Beach (teleport: 24569)
;
; Used by the routine at INITROOM.
;
; The first 128 bytes are copied to ROOMLAYOUT and define the room layout. Each
; bit-pair (bits 7 and 6, 5 and 4, 3 and 2, or 1 and 0 of each byte) determines
; the type of tile (background, floor, wall or nasty) that will be drawn at the
; corresponding location.
  DEFB 0,0,0,0,0,0,0,0            ; Room layout
  DEFB 0,0,0,0,0,0,0,0            ;
  DEFB 0,0,0,0,0,0,0,0            ;
  DEFB 0,0,0,0,0,0,0,0            ;
  DEFB 0,0,0,0,0,0,0,0            ;
  DEFB 0,0,0,0,0,0,0,0            ;
  DEFB 0,0,0,0,0,0,0,0            ;
  DEFB 0,0,0,0,0,0,0,0            ;
  DEFB 0,0,0,0,0,0,170,170        ;
  DEFB 0,0,0,0,0,0,170,170        ;
  DEFB 170,160,0,0,0,0,170,170    ;
  DEFB 0,0,0,0,0,0,170,170        ;
  DEFB 0,0,0,0,0,0,170,170        ;
  DEFB 0,0,0,0,0,0,170,170        ;
  DEFB 0,5,213,117,93,117,170,170 ;
  DEFB 0,21,85,85,85,85,170,170   ;
; The next 32 bytes are copied to ROOMNAME and specify the room name.
  DEFM "            The Beach           " ; Room name
; The next 54 bytes are copied to BACKGROUND and contain the attributes and
; graphic data for the tiles used to build the room.
  DEFB 13,0,0,0,0,0,0,0,0 ; Background
  DEFB 50,0,0,64,4,0,16,0,1 ; Floor
  DEFB 42,68,0,146,36,128,40,130,80 ; Wall
  DEFB 48,66,129,231,129,90,126,60,66 ; Nasty
  DEFB 14,1,3,7,15,30,55,127,251 ; Ramp
  DEFB 44,78,170,0,64,64,68,170,255 ; Conveyor
; The next four bytes are copied to CONVDIR and specify the direction, location
; and length of the conveyor.
  DEFB 0                  ; Direction (left)
  DEFW 24544              ; Location in the attribute buffer at 24064: (15,0)
  DEFB 5                  ; Length
; The next four bytes are copied to RAMPDIR and specify the direction, location
; and length of the ramp.
  DEFB 1                  ; Direction (up to the right)
  DEFW 24517              ; Location in the attribute buffer at 24064: (14,5)
  DEFB 1                  ; Length
; The next byte is copied to BORDER and specifies the border colour.
  DEFB 2                  ; Border colour
; The next two bytes are copied to XROOM223, but are not used.
  DEFB 0,0                ; Unused
; The next eight bytes are copied to ITEM and define the item graphic.
  DEFB 4,5,11,11,23,55,111,239 ; Item graphic
; The next four bytes are copied to LEFT and specify the rooms to the left, to
; the right, above and below.
  DEFB 59                 ; Room to the left (The Yacht)
  DEFB 51                 ; Room to the right (Tool  Shed)
  DEFB 58                 ; Room above (The Beach)
  DEFB 0                  ; Room below (The Off Licence)
; The next three bytes are copied to XROOM237, but are not used.
  DEFB 0,0,0              ; Unused
; The next eight pairs of bytes are copied to ENTITIES and specify the entities
; (ropes, arrows, guardians) in this room.
  DEFB 1,14               ; Rope at x=14 (ENTITY1)
  DEFB 60,84              ; Arrow flying left to right at pixel y-coordinate 42
                          ; (ENTITY60)
  DEFB 255,0              ; Terminator (ENTITY127)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)

; Room 59: The Yacht (teleport: 124569)
;
; Used by the routine at INITROOM.
;
; The first 128 bytes are copied to ROOMLAYOUT and define the room layout. Each
; bit-pair (bits 7 and 6, 5 and 4, 3 and 2, or 1 and 0 of each byte) determines
; the type of tile (background, floor, wall or nasty) that will be drawn at the
; corresponding location.
  DEFB 0,0,0,0,0,0,0,0              ; Room layout
  DEFB 0,0,0,0,0,0,0,0              ;
  DEFB 0,0,0,0,0,0,0,0              ;
  DEFB 0,0,0,0,0,0,0,0              ;
  DEFB 0,0,0,0,0,0,0,0              ;
  DEFB 85,96,0,0,0,0,0,0            ;
  DEFB 0,32,0,0,0,0,0,0             ;
  DEFB 0,32,0,0,0,0,0,0             ;
  DEFB 0,32,0,0,0,0,0,0             ;
  DEFB 0,32,3,3,0,192,0,0           ;
  DEFB 85,106,170,170,170,129,85,85 ;
  DEFB 1,0,0,0,2,129,0,0            ;
  DEFB 1,0,0,0,2,129,0,0            ;
  DEFB 1,0,0,0,2,129,0,0            ;
  DEFB 1,0,0,0,2,128,0,0            ;
  DEFB 170,170,170,170,170,128,0,0  ;
; The next 32 bytes are copied to ROOMNAME and specify the room name.
  DEFM "           The Yacht            " ; Room name
; The next 54 bytes are copied to BACKGROUND and contain the attributes and
; graphic data for the tiles used to build the room.
  DEFB 0,0,0,0,0,0,0,0,0  ; Background
  DEFB 6,187,221,221,238,0,0,0,0 ; Floor
  DEFB 61,187,119,238,221,187,119,238,221 ; Wall
  DEFB 66,126,191,191,110,44,44,173,255 ; Nasty
  DEFB 7,128,64,32,16,8,4,2,1 ; Ramp
  DEFB 13,224,170,56,1,131,199,124,16 ; Conveyor
; The next four bytes are copied to CONVDIR and specify the direction, location
; and length of the conveyor.
  DEFB 0                  ; Direction (left)
  DEFW 24565              ; Location in the attribute buffer at 24064: (15,21)
  DEFB 11                 ; Length
; The next four bytes are copied to RAMPDIR and specify the direction, location
; and length of the ramp.
  DEFB 0                  ; Direction (up to the left)
  DEFW 24362              ; Location in the attribute buffer at 24064: (9,10)
  DEFB 5                  ; Length
; The next byte is copied to BORDER and specifies the border colour.
  DEFB 5                  ; Border colour
; The next two bytes are copied to XROOM223, but are not used.
  DEFB 0,0                ; Unused
; The next eight bytes are copied to ITEM and define the item graphic.
  DEFB 206,241,185,213,239,215,187,255 ; Item graphic
; The next four bytes are copied to LEFT and specify the rooms to the left, to
; the right, above and below.
  DEFB 60                 ; Room to the left (The Bow)
  DEFB 58                 ; Room to the right (The Beach)
  DEFB 0                  ; Room above (The Off Licence)
  DEFB 0                  ; Room below (The Off Licence)
; The next three bytes are copied to XROOM237, but are not used.
  DEFB 0,0,0              ; Unused
; The next eight pairs of bytes are copied to ENTITIES and specify the entities
; (ropes, arrows, guardians) in this room.
  DEFB 53,8               ; Guardian no. 53 (horizontal), base sprite 0,
                          ; initial x=8 (ENTITY53)
  DEFB 27,21              ; Guardian no. 27 (vertical), base sprite 0, x=21
                          ; (ENTITY27)
  DEFB 255,17             ; Terminator (ENTITY127)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)

; Room 60: The Bow (teleport: 34569)
;
; Used by the routine at INITROOM.
;
; The first 128 bytes are copied to ROOMLAYOUT and define the room layout. Each
; bit-pair (bits 7 and 6, 5 and 4, 3 and 2, or 1 and 0 of each byte) determines
; the type of tile (background, floor, wall or nasty) that will be drawn at the
; corresponding location.
  DEFB 0,0,0,0,0,0,0,0           ; Room layout
  DEFB 0,0,0,0,0,0,0,0           ;
  DEFB 0,0,0,0,0,0,0,0           ;
  DEFB 0,0,0,0,0,0,0,0           ;
  DEFB 0,0,0,0,0,0,0,0           ;
  DEFB 0,0,0,0,0,1,85,85         ;
  DEFB 0,0,0,0,0,0,128,0         ;
  DEFB 0,0,0,0,4,0,128,0         ;
  DEFB 0,0,0,0,0,0,144,0         ;
  DEFB 0,0,0,0,0,0,128,0         ;
  DEFB 0,0,0,21,85,85,85,85      ;
  DEFB 0,0,0,0,0,0,0,0           ;
  DEFB 0,0,0,0,0,0,0,0           ;
  DEFB 0,0,0,0,0,0,0,0           ;
  DEFB 0,0,0,0,0,0,0,0           ;
  DEFB 255,255,255,255,192,0,0,0 ;
; The next 32 bytes are copied to ROOMNAME and specify the room name.
  DEFM "             The Bow            " ; Room name
; The next 54 bytes are copied to BACKGROUND and contain the attributes and
; graphic data for the tiles used to build the room.
  DEFB 0,0,0,0,0,0,0,0,0  ; Background
  DEFB 68,255,170,85,255,0,0,0,0 ; Floor
  DEFB 61,201,201,201,201,201,201,201,201 ; Wall
  DEFB 79,64,130,82,0,0,0,0,0 ; Nasty
  DEFB 7,128,64,32,16,8,4,2,1 ; Ramp
  DEFB 14,255,170,170,0,170,255,0,0 ; Conveyor
; The next four bytes are copied to CONVDIR and specify the direction, location
; and length of the conveyor.
  DEFB 1                  ; Direction (right)
  DEFW 24561              ; Location in the attribute buffer at 24064: (15,17)
  DEFB 15                 ; Length
; The next four bytes are copied to RAMPDIR and specify the direction, location
; and length of the ramp.
  DEFB 0                  ; Direction (up to the left)
  DEFW 24528              ; Location in the attribute buffer at 24064: (14,16)
  DEFB 6                  ; Length
; The next byte is copied to BORDER and specifies the border colour.
  DEFB 5                  ; Border colour
; The next two bytes are copied to XROOM223, but are not used.
  DEFB 0,0                ; Unused
; The next eight bytes are copied to ITEM and define the item graphic.
  DEFB 72,57,38,218,91,100,156,18 ; Item graphic
; The next four bytes are copied to LEFT and specify the rooms to the left, to
; the right, above and below.
  DEFB 0                  ; Room to the left (The Off Licence)
  DEFB 59                 ; Room to the right (The Yacht)
  DEFB 0                  ; Room above (The Off Licence)
  DEFB 0                  ; Room below (The Off Licence)
; The next three bytes are copied to XROOM237, but are not used.
  DEFB 0,0,0              ; Unused
; The next eight pairs of bytes are copied to ENTITIES and specify the entities
; (ropes, arrows, guardians) in this room.
  DEFB 105,23             ; Guardian no. 105 (horizontal), base sprite 0,
                          ; initial x=23 (ENTITY105)
  DEFB 110,20             ; Guardian no. 110 (horizontal), base sprite 0,
                          ; initial x=20 (ENTITY110)
  DEFB 255,0              ; Terminator (ENTITY127)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)
  DEFB 0,0                ; Nothing (ENTITYDEFS)

; Unused TRS-DOS code
  ADD HL,BC
  LD A,(DE)
  CP 59
  JR Z,64720
  CP 13
  JR Z,64720
  PUSH HL
  LD HL,30720
  LD B,2
  CALL 26014
  POP BC
  JR NC,64796
  LD DE,26547
  JP 25689
  CALL 30026
  JR NZ,64790
  CALL 23476
  CALL NZ,23533
  LD E,(HL)
  INC HL
  LD A,(HL)
  LD (21550),A
  LD A,(21555)
  OR A
  JR Z,64835
  LD A,E
  CP 32
  JR C,64828
  CP 37
  JR C,64835
  LD A,(21254)
  OR A
  JP Z,27620
  LD HL,31280
  LD D,0
  ADD HL,DE
  ADD HL,DE
  LD A,(HL)
  INC HL
  LD H,(HL)
  LD L,A
  LD A,E
  LD DE,30039
  EX DE,HL
  EX (SP),HL
  EX DE,HL
  PUSH HL
  INC C
  DEC C
  RET Z
  CP 24
  LD A,1
  LD HL,(21556)
  RET NC
  PUSH HL
  PUSH AF
  LD HL,(21569)
  LD B,2
  CALL 26014
  JR NC,64932
  PUSH DE
  PUSH BC
  CALL 30599
  LD HL,(21569)
  LD A,C
  ADD A,B
  INC A
  LD C,A
  XOR A
  LD B,A
  SBC HL,BC
  RST 24
  JR NC,64913
  LD HL,26652
  PUSH HL
  LD A,(21266)
  OR A
  JR Z,64909
  CALL 30577
  POP HL
  JP 22418
  LD (21569),HL
  POP BC
  POP DE
  PUSH BC
  LD (HL),C
  EX DE,HL
  INC DE
  LD B,0
  LDIR
  EX DE,HL
  LD (HL),B
  INC HL
  LD (HL),B
  DEC HL
  POP BC
  POP DE
  PUSH HL
  XOR A
  LD B,A
  INC BC
  SBC HL,BC
  LD A,(21566)
  CP 2
  JR NZ,64953
  LD A,(22211)
  OR A
  CALL NZ,27936
  INC D
  DEC D
  JR NZ,64982
  LD A,(HL)
  AND 128
  LD DE,26685
  CALL Z,25694
  LD A,(HL)
  AND 64
  LD DE,26629
  CALL NZ,25694
  POP HL
  LD A,(HL)
  INC HL
  LD H,(HL)
  LD L,A
  POP DE
  RET
  LD A,(HL)
  OR A
  JP P,27922
  LD A,D
  EX (SP),HL
  POP BC
  POP DE
  PUSH DE
  PUSH HL
  PUSH AF
  LD A,(HL)
  INC HL
  LD H,(HL)
  LD L,A
  RST 24
  POP DE
  JR Z,65030
  LD A,(BC)
  AND 95
  DEFB 69,95,69,255,69,95,69,95,69,95,69,95,69,95,69,95
  DEFB 69,145,53,210,68,161,66,121,68,84,0,122,68,189,68,69
  DEFB 1,0,67,245,254,4,79,0,67,137,71,103,114,0,0,111
  DEFB 73,38,77,0,76,0,67,54,77,204,76,153,76,191,100,65
  DEFB 76,1,83,43,83,120,29,230,75,45,83,30,29,0,0,0
  DEFB 122,68,189,68,69,1,0,67,245,254,4,79,0,67,137,71
  DEFB 103,114,0,0,111,73,38,77,0,76,74,109,43,96,232,94
  DEFB 66,0,40,96,74,109,14,95,44,0,71,109,143,27,70,84
  DEFB 0,0,0,24,98,255,255,6
  DEFM "TRSHDx"
  LD HL,17480
  LD (15422),HL
  LD A,16
  OUT (193),A
  LD B,64
  CALL 96
  LD HL,ROOMNAME
  LD (15422),HL
  LD A,12
  OUT (193),A
  EX (SP),HL
  EX (SP),HL
  IN A,(207)
  CP 80
  JR NZ,65147
  XOR A
  OUT (206),A
  LD A,16
  OUT (207),A
  LD B,50
  CALL 96
  IN A,(207)
  BIT 7,A
  JR NZ,65192
  BIT 0,A
  JR NZ,65147
  RET
  DEFB 0,0
  DEFM "TRSHDx(C) Copyright 1982 by LSI"
  LD A,7
  CP B
  JR Z,65309
  BIT 3,B
  JR NZ,65247
  XOR A
  RET
  BIT 2,B
  PUSH HL
  JR NZ,65300
  BIT 1,B
  JR Z,65259
  LD HL,0
  PUSH DE
  CALL 65426
  LD A,32
  OUT (207),A
  EX (SP),HL
  EX (SP),HL
  IN A,(207)
  RLCA
  JR C,65269
  LD BC,200
  INIR
  IN A,(207)
  RRCA
  LD HL,65409
  JR C,65357
  POP DE
  PUSH DE
  LD A,D
  CP (IY+9)
  LD A,6
  JR Z,65364
  XOR A
  JR 65364
  CALL 65309
  JR Z,65332
  POP HL
  LD A,15
  RET
  LD A,(IY+3)
  AND 3
  LD C,A
  IN A,(192)
  CALL C,3335
  JR NZ,65318
  OR (IY+3)
  BIT 7,A
  LD A,64
  RET NZ
  XOR A
  RET
  PUSH DE
  LD A,B
  CP 15
  JR Z,65368
  CP 12
  LD A,8
  JR Z,65364
  CALL 65426
  LD A,48
  CALL 65498
  JR Z,65364
  LD HL,65417
  IN A,(201)
  RLCA
  INC HL
  JR NC,65359
  LD A,(HL)
  OR A
  POP DE
  POP HL
  RET
  LD A,(IY+7)
  LD B,A
  AND 31
  LD E,A
  XOR B
  RLCA
  RLCA
  RLCA
  INC A
  LD B,A
  PUSH BC
  PUSH DE
  CALL 65426
  PUSH HL
  LD A,(HL)
  OUT (202),A
  INC HL
  LD A,80
  CALL 65498
  POP HL
  POP DE
  POP BC
  JR NZ,65354
  LD A,32
  ADD A,E
  LD E,A
  DJNZ 65381
  XOR A
  JR 65364
  DEFB 7,4,1,5,127,8,2,3
  DEFB 14,12,9,13,127,8,10,11
  PUSH HL
  LD L,D
  LD H,0
  BIT 5,(IY+4)
  JR Z,65437
  ADD HL,HL
  LD A,(IY+7)
  PUSH DE
  LD D,A
  AND 31
  LD E,A
  INC E
  LD C,E
  XOR D
  RLCA
  RLCA
  RLCA
  INC A
  CALL 19307
  DEC A
  POP DE
  CP E
  JR NC,65464
  CPL
  ADD A,E
  LD E,A
  INC HL
  LD A,C
  CALL 19322
  LD D,A
  LD A,(IY+4)
  AND 15
  ADD A,D
  LD D,A
  LD A,(IY+3)
  AND 3
  RLCA
  RLCA
  RLCA
  OR D
  OUT (206),A
  LD A,E
  OUT (203),A
  LD A,L
  OUT (204),A
  LD A,H
  OUT (205),A
  POP HL
  RET
  OUT (207),A
  LD BC,200
  OTIR
  EX (SP),HL
  EX (SP),HL
  IN A,(207)
  RLCA
  JR C,65507
  IN A,(207)
  AND 1
  RET
  DEFS 19

